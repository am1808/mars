load "./Blue.colour"

#~Temperature profile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
Tp1(x) = 300.0 + (750.0-300.0)*exp(-((x-3*1.0e-9)/(1.0e-9))**2)
set ylabel "Temperature (K)"
set xlabel "Time (s)"
set xrange [0:6e-9]
set yrange [290:760]

#PNG
set term pngcairo
set output "Temp_profile_no_spatial.png"
set title "T_{min}=300K, T_{max}=750K, t_{cool}=1.0e-9s"
p Tp1(x) w l ls 6 lw 2 t ""
#-------------------------------------------------------------------------------------------#
#~Field profile~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
set ylabel "Field (Oe)"
set xlabel "Time (s)"
set xrange [0:6.1e-9]
set yrange [0:11000]

set term pngcairo
set output "Field_profile_no_spatial.png"
set title ""
set arrow from 0,0 to 1e-10,10000 nohead ls 6 lw 2
set arrow from 1e-10,10000 to 5.9e-9,10000 ls 6 lw 2 nohead
set arrow from 5.9e-9,10000 to 6e-9,0 nohead ls 6 lw 2

f(x) = 10
p f(x) t ""
#-------------------------------------------------------------------------------------------#
