set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (degrees)"
set xrange[0:180]
set yrange[0:]
set style fill solid

#===================LLG====================#
#================50fs - 300K===============#
set output "LLG_Boltzmann_test_300K_50fs.eps"
f0 = "Output/LLG_300.000000K_50.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=300K, dt=50fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#================50fs - 1000K==============#
set output "LLG_Boltzmann_test_1000K_50fs.eps"
f0 = "Output/LLG_1000.000000K_50.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=1000K, dt=50fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#===============100fs - 300K===============#
set output "LLG_Boltzmann_test_300K_100fs.eps"
f0 = "Output/LLG_300.000000K_100.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=300K, dt=100fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===============100fs - 1000K==============#
set output "LLG_Boltzmann_test_1000K_100fs.eps"
f0 = "Output/LLG_1000.000000K_100.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=1000K, dt=100fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#===============500fs - 300K===============#
set output "LLG_Boltzmann_test_300K_500fs.eps"
f0 = "Output/LLG_300.000000K_500.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=300K, dt=500fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===============500fs - 1000K==============#
set output "LLG_Boltzmann_test_1000K_500fs.eps"
f0 = "Output/LLG_1000.000000K_500.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=1000K, dt=500fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#==============1000fs - 300K===============#
set output "LLG_Boltzmann_test_300K_1000fs.eps"
f0 = "Output/LLG_300.000000K_1000.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=300K, dt=1000fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#==============1000fs - 1000K==============#
set output "LLG_Boltzmann_test_1000K_1000fs.eps"
f0 = "Output/LLG_1000.000000K_1000.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLG data T=1000K, dt=1000fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
  
  
#===================LLB====================#
#================0.5fs - 300K==============#
set output "LLB_Boltzmann_test_300K_0.5fs.eps"
f0 = "Output/LLB_300.000000K_0.500000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=300K, dt=0.5fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#================0.5fs - 1000K=============#
set output "LLB_Boltzmann_test_1000K_0.5fs.eps"
f0 = "Output/LLB_1000.000000K_0.500000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=1000K, dt=0.5fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#===============1.0fs - 300K===============#
set output "LLB_Boltzmann_test_300K_1fs.eps"
f0 = "Output/LLB_300.000000K_1.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=300K, dt=1.0fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===============1.0fs - 1000K==============#
set output "LLB_Boltzmann_test_1000K_1fs.eps"
f0 = "Output/LLB_1000.000000K_1.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=1000K, dt=1.0fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#===============5.0fs - 300K===============#
set output "LLB_Boltzmann_test_300K_5fs.eps"
f0 = "Output/LLB_300.000000K_5.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=300K, dt=5.0fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#===============5.0fs - 1000K==============#
set output "LLB_Boltzmann_test_1000K_5fs.eps"
f0 = "Output/LLB_1000.000000K_5.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=1000K, dt=5.0fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
#==============10fs - 300K===============#
set output "LLB_Boltzmann_test_300K_10fs.eps"
f0 = "Output/LLB_300.000000K_10.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=300K, dt=10fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
  
#==============10fs - 1000K==============#
set output "LLB_Boltzmann_test_1000K_10fs.eps"
f0 = "Output/LLB_1000.000000K_10.000000fs.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "LLB data T=1000K, dt=10fs",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical' 
 
