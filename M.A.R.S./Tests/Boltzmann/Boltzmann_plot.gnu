set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set ylabel "Probability density"
set xlabel "{/Symbol Q}_m (degrees)"
set xrange[0:30]
set yrange[0:]
set style fill solid

#===================LLG====================#
#===================690K===================#
set output "LLG_Boltzmann_test_690K.eps"
f0 = "Output/LLG_690.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=690K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================680K===================#
set output "LLG_Boltzmann_test_680K.eps"
f0 = "Output/LLG_680.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=680K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================650K===================#
set output "LLG_Boltzmann_test_650K.eps"
f0 = "Output/LLG_650.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=650K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'
#===================600K====================#
set output "LLG_Boltzmann_test_600K.eps"
f0 = "Output/LLG_600.000000K.dat"

p f0 u ($1+$3):($2/$4) smooth freq with boxes title "Simulation data T=600K",\
  '' u ($1):($5/$6) w l lw 3 t 'Analytical'

#===================LLB====================#
unset key
set ticslevel 0
set view 44,203
set dgrid3d 50,50

set xlabel "m_z"
set ylabel "m_x"
set zlabel "P(m_x,m_z)" rotate by 90 offset 4,0
unset ztics 

set xrange [-1.1:1.1]
set yrange [-1.1:1.1]
#===================750K===================#
f0 = "Output/LLB_750.000000K.dat"
set output "LLB_Boltzmann_test_750K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l
#===================700K===================#
f0 = "Output/LLB_700.000000K.dat"
set output "LLB_Boltzmann_test_700K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l
#===================690K===================#
f0 = "Output/LLB_690.000000K.dat"
set output "LLB_Boltzmann_test_690K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l
#===================680K===================#
f0 = "Output/LLB_680.000000K.dat"
set output "LLB_Boltzmann_test_680K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l
#===================650K===================#
f0 = "Output/LLB_650.000000K.dat"
set output "LLB_Boltzmann_test_650K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l
#===================600K===================#
f0 = "Output/LLB_600.000000K.dat"
set output "LLB_Boltzmann_test_600K.eps" 
sp f0 u 2:1:( ($3/$5*$4/$5)/2) w l








