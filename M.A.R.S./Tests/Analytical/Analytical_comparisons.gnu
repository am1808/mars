set term pngcairo size 1200,1080

set key outside
set autoscale y
set format y "%.2e"
set ylabel "Magnetisation"
set xlabel "Time (ns)"
set xrange [0:2e-09]
set mxtics 2
set mytics 2
set grid xtics ytics mxtics mytics
datafileLLG = "Output/LLG.dat"
datafileLLB = "Output/LLB.dat"
set size square

set ytics format "%1.1f"
set xtics ("0.0" 0, "0.5" 5e-10, "1.0" 1e-9, "1.5" 1.5e-9, "2.0" 2.0e-9)
show key


set output "LLG_analytical_test.png"
p datafileLLG u 10:4 w l lt rgb "#00FFFF" lw 2 t 'x',\
  '' u 10:5 w l lt rgb "#0000FF" lw 2 t 'y',\
  '' u 10:6 w l lt rgb "#7FFF00" lw 2 t 'z',\
  '' u 10:1 w l dashtype 5 lt rgb "#FF1493" lw 2 t 'Ax',\
  '' u 10:2 w l dashtype 5 lt rgb "#00FF7F" lw 2 t 'Ay',\
  '' u 10:3 w l dashtype 5 lt rgb "#FF4500" lw 2 t 'Az'

set output "LLB_analytical_test.png"
p datafileLLB u 10:4 w l lt rgb "#00FFFF" lw 2 t 'x',\
  '' u 10:5 w l lt rgb "#0000FF" lw 2 t 'y',\
  '' u 10:6 w l lt rgb "#7FFF00" lw 2 t 'z',\
  '' u 10:1 w l dashtype 5 lt rgb "#FF1493" lw 2 t 'Ax',\
  '' u 10:2 w l dashtype 5 lt rgb "#00FF7F" lw 2 t 'Ay',\
  '' u 10:3 w l dashtype 5 lt rgb "#FF4500" lw 2 t 'Az'


set key inside top left
set autoscale y
set format y "%.2e"
set ylabel "Error"

set output "LLG_analytical_test_error.png"
p datafileLLG u 10:7 w l t 'Error_x',\
  '' u 10:8 w l t 'Error_y',\
  '' u 10:9 w l t 'Error_z'

set output "LLB_analytical_test_error.png"
p datafileLLB u 10:7 w l t 'Error_x',\
  '' u 10:8 w l t 'Error_y',\
  '' u 10:9 w l t 'Error_z'
