set term postscript eps color enhanced font "Times-Roman,25"
set key font "Times-Roman,23"
set key center tm

set xlabel "Time [ps]"
set xrange[0:25]

set key inside top right

f0 = "Output/LLB1.000000K.dat"
f1 = "Output/LLB300.000000K.dat"
f2 = "Output/LLB500.000000K.dat"
f3 = "Output/LLB600.000000K.dat"

A0 = "Output/Atomistic_data/Hard/output_1K_noEq"
A1 = "Output/Atomistic_data/Hard/output_300K_noEq"
A2 = "Output/Atomistic_data/Hard/output_500K_noEq"
A3 = "Output/Atomistic_data/Hard/output_600K_noEq"

### M_x
set output "Transverse_comparison_LLB_mx.eps"
set ylabel "m_x"
set yrange [-1.1:1.1]
set key inside bottom right
p A0 every 10 u ($1*1.0e12):($2) w p ls 2 ps 1 t '1K',\
  A1 every 10 u ($1*1.0e12):($2) w p ls 4 ps 1 t '300K',\
  A2 every 10 u ($1*1.0e12):($2) w p ls 8 ps 1 t '500K',\
  A3 every 10 u ($1*1.0e12):($2) w p ls 6 ps 1 t '600K',\
  f0 every 10 u ($1*1.0e12):($2) w l ls 2 lw 6 t '',\
  f1 every 10 u ($1*1.0e12):($2) w l ls 4 lw 6 t '',\
  f2 every 10 u ($1*1.0e12):($2) w l ls 8 lw 6 t '',\
  f3 every 10 u ($1*1.0e12):($2) w l ls 6 lw 6 t ''
  
### M_y
set output "Transverse_comparison_LLB_my.eps"
set ylabel "m_y"
set yrange [-1.1:1.1]
set key inside bottom right
p A0 every 10 u ($1*1.0e12):($3) w p ls 2 ps 1 t '1K',\
  A1 every 10 u ($1*1.0e12):($3) w p ls 4 ps 1 t '300K',\
  A2 every 10 u ($1*1.0e12):($3) w p ls 8 ps 1 t '500K',\
  A3 every 10 u ($1*1.0e12):($3) w p ls 6 ps 1 t '600K',\
  f0 every 10 u ($1*1.0e12):($3) w l ls 2 lw 6 t '',\
  f1 every 10 u ($1*1.0e12):($3) w l ls 4 lw 6 t '',\
  f2 every 10 u ($1*1.0e12):($3) w l ls 8 lw 6 t '',\
  f3 every 10 u ($1*1.0e12):($3) w l ls 6 lw 6 t ''
  
### M_z
set output "Transverse_comparison_LLB_mz.eps"
set ylabel "m_z"
set yrange [0:1.05]
set key inside bottom right
p A0 every 10 u ($1*1.0e12):($4) w p ls 2 ps 1 t '1K',\
  A1 every 10 u ($1*1.0e12):($4) w p ls 4 ps 1 t '300K',\
  A2 every 10 u ($1*1.0e12):($4) w p ls 8 ps 1 t '500K',\
  A3 every 10 u ($1*1.0e12):($4) w p ls 6 ps 1 t '600K',\
  f0 every 10 u ($1*1.0e12):($4) w l ls 2 lw 6 t '',\
  f1 every 10 u ($1*1.0e12):($4) w l ls 4 lw 6 t '',\
  f2 every 10 u ($1*1.0e12):($4) w l ls 8 lw 6 t '',\
  f3 every 10 u ($1*1.0e12):($4) w l ls 6 lw 6 t ''
  
### M_l  
set output "Transverse_comparison_LLB_ml.eps"
set ylabel "m_l"
set yrange [0:1.05]
set key inside bottom right
p A0 every 10 u ($1*1.0e12):($5) w p ls 2 ps 1 t '1K',\
  A1 every 10 u ($1*1.0e12):($5) w p ls 4 ps 1 t '300K',\
  A2 every 10 u ($1*1.0e12):($5) w p ls 8 ps 1 t '500K',\
  A3 every 10 u ($1*1.0e12):($5) w p ls 6 ps 1 t '600K',\
  f0 every 10 u ($1*1.0e12):($5) w l ls 2 lw 6 t '',\
  f1 every 10 u ($1*1.0e12):($5) w l ls 4 lw 6 t '',\
  f2 every 10 u ($1*1.0e12):($5) w l ls 8 lw 6 t '',\
  f3 every 10 u ($1*1.0e12):($5) w l ls 6 lw 6 t ''
