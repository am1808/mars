/* LLB_Thermal_Properties.hpp
 *  Created on: 13 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file LLB_Thermal_Properties.hpp
 * \brief Header file for LLB susceptibility function. */

#ifndef LLB_THERMAL_PROPERTIES_HPP_
#define LLB_THERMAL_PROPERTIES_HPP_

#include <vector>

#include "../../hdr/Structures.hpp"

extern int LLB_Thermal_Properties(const int, const int,const std::vector<unsigned int>, const Grain_t, LLB_t*);

#endif /* LLB_THERMAL_PROPERTIES_HPP_ */
