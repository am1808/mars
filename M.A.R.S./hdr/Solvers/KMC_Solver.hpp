/*
 * KMC_Solver.hpp
 *
 *  Created on: 19 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file KMC_Solver.hpp
 * \brief Header file for kMC solver function. */

#ifndef KMC_SOLVER_HPP_
#define KMC_SOLVER_HPP_

#include <string>
#include "../../hdr/Structures.hpp"

extern int KMC_solver(const int,const int,const Interaction_t,const double,const bool,const bool,const double,Grain_t*,const std::string="");

#endif /* KMC_SOLVER_HPP_ */
