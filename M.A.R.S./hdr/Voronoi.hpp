/* Voronoi.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Voronoi.hpp
 * \brief Header file for Voronoi construction function. */

#ifndef VORONOI_HPP_
#define VORONOI_HPP_

#include "../hdr/Structures.hpp"

extern int Voronoi(const Structure_t Sys, const double Interaction_radius,Voronoi_t*Data);

#endif /* VORONOI_2_HPP_ */
