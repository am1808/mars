/* ConfigFile_import.hpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file ConfigFile_import.hpp
 * \brief Header file for the ConfigFile class. */

#ifndef CONFIGFILE_IMPORT_HPP_
#define CONFIGFILE_IMPORT_HPP_

#include <string>
#include <map>

#include "../../hdr/Config_File/Convert.hpp"

/** \brief Class containing all parameter data from the configuration file.
 *
 * This class contains a hash table which stores all configuration parameters.
 * The configuration file is read and the hash table is generated on construction.
 */

class ConfigFile
{
private:
	std::map<std::string, std::string> contents;        //!< Mapping of data and configuration file key.
	std::string FileName;                               //!< Configuration file name.
	const std::string BoldRedFont = "\033[1;4;31m";     //!< ANSI colour code for errors.
	const std::string ResetFont = "\033[0m";            //!< ANSI colour code for resting terminal output.

	void removeComment(std::string&) const;

	void lowerCase(std::string &) const;

	void KeepCase(std::string &, size_t const) const;

	bool onlyWhitespace(const std::string&) const;

	void FormatVector(std::string &, size_t const) const;

	bool validLine(const std::string&) const;

	void extractKey(std::string&, size_t const&, const std::string&) const;

	void extractValue(std::string&, size_t const&, const std::string&) const;

	void extractContents(const std::string&);

	void parseLine(const std::string&, size_t const);

	bool keyExists(const std::string&) const;

	void ExtractKeys();

public:
	ConfigFile(const std::string&);

	template <typename ValueType>
	ValueType getValueOfKey(const std::string&key) const;

};



#endif /* CONFIGFILE_IMPORT_HPP_ */
