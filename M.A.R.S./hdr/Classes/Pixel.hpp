/*
 * Pixel.hpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Pixel.hpp
 * @brief Header file for Pixel class */

#ifndef PIXEL_HPP_
#define PIXEL_HPP_

#include <vector>
#include "../Structures.hpp"
/** @brief Class for a pixel that is used within the pixelmap.
 *
 * This class provides the basic functionality of a pixel within the pixelmap.
 * Pixel specific data access from the pixelmap should be handled by this class alone. */
class Pixel {
    private:
    // Pixel information
    unsigned int ID;                       //!< Pixel ID
    std::pair<double,double> centre;       //!< Pixel position coordinate
    // Pixel data
    int Grain;                             //!< Grain the pixel resides within
    double Temperature;                    //!< Pixel temperature
    Vec3 Magnetisation,                    //!< Pixel magnetisation
         Field,                            //!< Pixel Field
         Easy_Axis;                        //!< Pixel easy axis

public:
    Pixel (unsigned int,double,double);
    void set_M(Vec3);
    void set_M(double M);
    void set_EA(Vec3 EA);
    void set_EA(double EA);
    void set_Temp(double);
    void set_H(Vec3);
    void set_Grain(unsigned int);
    void set_Coords(std::pair<double,double>);
    unsigned int  get_ID() const ;
    int  get_Grain() const ;
    Vec3 get_M() const ;
    Vec3 get_EA() const ;
    double get_Mx() const ;
    double get_My() const ;
    double get_Mz() const ;
    double get_Temp() const ;
    Vec3 get_H() const ;
    std::pair<double,double> get_Coords() const ;
};

#endif /* PIXEL_HPP_ */
