/*
 * RecLayer.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecLayer.hpp
 * @brief Header file for RecLayer class */

#ifndef RECLAYER_HPP_
#define RECLAYER_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
/** @brief Class for the recording layer.
 *
 * Class providing the information specific to the recording layer. */
class RecLayer{

protected:

    unsigned int Bit_number_X;          //!< Number of bits per track.
    unsigned int Bit_number_Y;          //!< Number of tracks in the system.
    double Bit_size_X;                  //!< Size of bits in x-dimension.
    double Bit_size_Y;                  //!< Size of bits in y-dimension.
    double Bit_spacing_X;               //!< Spacing between bits along track.
    double Bit_spacing_Y;               //!< Spacing between tracks.
    std::vector<int> Writable_data;     //!< Array containing binary data for bits to be written.
    std::vector<std::vector<unsigned int>> Grain_in_bit;    //!< List of grains within each bit.

public:

    RecLayer(const ConfigFile cfg, double H_zdir);
    ~RecLayer();
    void setGrainsinBit(const unsigned int Num_Grains,const std::vector<double> PosX,
            const std::vector<double> PosY,const std::vector<std::pair<double,double>> Bit_positions);
    std::vector<unsigned int> getGrainsinBit(const unsigned int idx) const ;
    void setBitsX(unsigned int val);
    void setBitsY(unsigned int val);
    double getBitSizeX() const;
    double getBitSizeY() const;
    double getBitSpacingX() const;
    double getBitSpacingY() const;
    unsigned int getBitsX() const;
    unsigned int getBitsY() const;
    unsigned int getBitsA() const;
    int getWritableData(unsigned int idx) const;

};

#endif /* RECLAYER_HPP_ */
