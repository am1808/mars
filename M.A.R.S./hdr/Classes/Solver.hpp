/*
 * Solver.hpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Solver.hpp
 * @brief Header file for Solver_t class */

#ifndef SOLVER_H_
#define SOLVER_H_

#include <string>
#include "../Config_File/ConfigFile_import.hpp"
#include "../Structures.hpp"

/// Available solvers
enum struct Solvers {
    kMC,    //!< Kinetic Monte Carlo solver
    LLB,    //!< Landau-Lifshitz-Bloch solver
    LLG     //!< Landau-Lifshitz-Gilbert solver
};
/// Available solver behaviours
enum struct Behaviours {
    Standard,           //!< Default solver behaviour
    Thermoremanence     //!< Prevent m_eq=0 when temperature exceeds the Curie point
};

/**
 * @brief Class containing the numerical solvers.
 *
 * This class contains all solvers and their associated parameters and settings.
 * Multiple solvers can be configured for use enabling more simple creation of
 * simulations that require switching between solvers. */
class Solver_t {
public:
    Solver_t(unsigned Num_grains, unsigned int Num_Layers,ConfigFile cfg, const std::vector<ConfigFile> Materials_config);
    Solver_t(ConfigFile cfg);
    ~Solver_t();

    int Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers,
                         std::vector<unsigned int>*Included_grains_in_layer,const Grain_t*Grain);

    void Force_specific_solver_construction(unsigned Num_grains, unsigned int Num_Layers,ConfigFile cfg, const std::vector<ConfigFile> Materials_config, Solvers desired);

    int Integrate(const Voronoi_t*VORO, unsigned int Num_layers, const Interaction_t*Int_sys,double Cx, double Cy, Grain_t*Grains);

    void set_dt(double timeStep, Solvers desired_solver);
    void set_dt(double timeStep);
    double get_dt(Solvers desired) const;
    double get_dt() const;
    void set_f0(double F0);
    double get_f0() const;
    void set_Solver(Solvers new_solver);
    Solvers get_Solver() const;
    void set_allow_Tc(bool option, Solvers desired_solver);
    void set_ml_output(bool option);
    std::vector<double> get_Alpha() const;
    double get_Gamma() const;
    double get_mEQ(unsigned int idx) const;
    double get_ChiPara(unsigned int idx) const;
    double get_ChiPerp(unsigned int idx) const;
    bool get_Exclusion() const;
    std::string get_ChiParaFit(unsigned int idx) const;
    std::string get_ChiPerpFit(unsigned int idx) const;
    std::string get_SusType(unsigned int idx) const;
    double get_ChiScaling(unsigned int idx) const;
    void set_Alpha(double val);
    void set_ChiParaFit(double a0,double a1,double a2,double a3,double a4,
                        double a5, double a6, double a7, double a8,
                        double a9, double a1_2, double b0, double b1,
                        double b2, double b3, double b4);
    void set_ChiParaFitIdv(std::vector<double> Fit);
    void set_ChiPerpFit(double a0,double a1,double a2,double a3,double a4,
                        double a5, double a6, double a7, double a8,
                        double a9, double a1_2, double b0, double b1,
                        double b2, double b3, double b4);
    void set_ChiPerpFitIdv(std::vector<double> Fit);
    void set_SusType(std::string type);
    void set_ChiScaling(double val);
    void EmptyChiIdv();
    void empty_Alpha();
    void empty_ChiParaFit();
    void empty_ChiPerpFit();
    void empty_SusType();
    void empty_ChiScaling();
    void set_Chi_size(unsigned size);


    void setMeasTime(double Time, Solvers desired_solver);
    double getMeasTime() const;
    double getMeasTime(Solvers Desired) const;
    unsigned int getOutputSteps() const;
    std::vector<double> get_ChiPerpFitIdv(unsigned int) const;
    std::vector<double> get_ChiParaFitIdv(unsigned int) const;

    void disableExclusion();
    void enableExclusion();

private:

    void Import(ConfigFile cfg);
    void Fittings_import(const unsigned int Num_Layers, std::vector<ConfigFile> Materials_config);
    void Fitting_per_grain(const unsigned Num_grains, std::vector<ConfigFile> Materials_config);

    int KMC(const unsigned int Num_Layers,const Interaction_t*Interac,
                   const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain);

    int LLG(const unsigned int Num_Layers,const Interaction_t*Interac,
            const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain);

    int LLB(const unsigned int Num_Layers,const Interaction_t*Interac,
             const Voronoi_t*VORO,const double Centre_x, const double Centre_y,Grain_t*Grain);
#ifndef DOXYGEN_SHOULD_SKIP_THIS
    std::vector<Vec3> LLB_STEP(unsigned int Num_Grains, unsigned int Num_Layers,unsigned int Integratable_grains_in_layer,
                               std::vector<unsigned int>*Included_grains_in_layer,std::vector<unsigned int>*Included_grains_in_system,
                               std::vector<Vec3>*RNG_PARA_NUM,std::vector<Vec3>*RNG_PERP_NUM,const Interaction_t*Interac,Grain_t*Grain);
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

    int KMC_core(const Vec3 Easy_axis,const Vec3 h_eff,
                 const double KV_over_kBT,const double MsVHk_over_kBT,Vec3*Spin);



    Solvers type;               //!< Current selected solver
    Behaviours behaviour_kMC;   //!< Behaviour of kMC solver
    Behaviours behaviour_LLG;   //!< Behaviour of LLG solver

    // Solver flags
    bool T_variation;            //!< Flag to determine kMC and LLG behaviour when T>=T_c
    bool M_length_variation;    //!< Flag to set m length variation in kMC
    bool Exclusion;             //!< Flag to set exclusion zone use

    // kMC only parameters
    double f0;                    //!< Attempt frequency (Hz)

    // kMC,LLG,LLB parameters
    double dt_kMC;                  //!< Time step for kMC solver (s)
    double dt_LLG;                  //!< Time step for LLG solver (s)
    double dt_LLB;                  //!< Time step for LLB solver (s)
    double Exclusion_range_negX;    //!< Exclusion zone range in negative x-axis
    double Exclusion_range_posX;    //!< Exclusion zone range in positive x-axis
    double Exclusion_range_negY;    //!< Exclusion zone range in negative y-axis
    double Exclusion_range_posY;    //!< Exclusion zone range in positive y-axis
    double Measurement_time_kMC;    //!< Time at which data should be output when using kMC solver
    double Measurement_time_LLG;    //!< Time at which data should be output when using LLG solver
    double Measurement_time_LLB;    //!< Time at which data should be output when using LLB solver


    // LLG/LLB parameters
    std::vector<double> Alpha;      //!< Damping constant
    const double Gamma;             //!< Gyro-magnetic ratio (Oe<SUP>-1</SUP>s<SUP>-1</SUP>)

    // LLB only parameters
    std::vector<double> Chi_para;      //!< Parallel susceptibility
    std::vector<double> Chi_perp;      //!< Perpendicular susceptibility
    std::vector<double> Alpha_PARA;    //!< Parallel damping constant
    std::vector<double> Alpha_PERP;    //!< Perpendicular damping constant
    std::vector<double> m_EQ;          //!< Equilibrium magnetisation

    // These are vectors as they are set PER layer
    std::vector<std::string> Susceptibility_Type;    //!< Method used for susceptibility modelling
    //! Parallel susceptibility fitting parameters
    //@{
    std::vector<double> a0_PARA,a1_PARA,a2_PARA,
                        a3_PARA,a4_PARA,
                        a5_PARA,a6_PARA,a7_PARA,
                        a8_PARA,a9_PARA,
                        a1_2_PARA,b0_PARA,
                        b1_PARA,b2_PARA,b3_PARA,
                        b4_PARA;
    //@}
    //! Perpendicular susceptibility fitting parameters
    //@{
    std::vector<double> a0_PERP,a1_PERP,a2_PERP,
                        a3_PERP,a4_PERP,a5_PERP,
                        a6_PERP,a7_PERP,a8_PERP,
                        a9_PERP,
                        a1_2_PERP,b0_PERP,
                        b1_PERP,b2_PERP,b3_PERP,
                        b4_PERP;
    //@}
    std::vector<double> Chi_scaling_factor;    //!< Scailing factor for susceptibility (applied when susceptibility is in units of Am<SUP>-1</SUP>&frasl;T)
    /*#######################################################################################*/
    // These vectors will be of size Grains
    std::vector<double> Ga0_PARA,Ga1_PARA,Ga2_PARA,
                        Ga3_PARA,Ga4_PARA,
                        Ga5_PARA,Ga6_PARA,Ga7_PARA,
                        Ga8_PARA,Ga9_PARA,
                        Ga1_2_PARA,Gb0_PARA,
                        Gb1_PARA,Gb2_PARA,Gb3_PARA,
                        Gb4_PARA;
    std::vector<double> Ga0_PERP,Ga1_PERP,Ga2_PERP,
                        Ga3_PERP,Ga4_PERP,Ga5_PERP,
                        Ga6_PERP,Ga7_PERP,Ga8_PERP,
                        Ga9_PERP,
                        Ga1_2_PERP,Gb0_PERP,
                        Gb1_PERP,Gb2_PERP,Gb3_PERP,
                        Gb4_PERP;
    std::vector<double> GChi_scaling_factor;    //!< Scailing factor for susceptibility (applied when susceptibility is in units of Am<SUP>-1</SUP>&frasl;T)
    /*#######################################################################################*/
    std::string unit_susceptibility;           //!< Units used for the susceptibilities

    // Terminal formating
    const std::string BoldCyanFont;      //!< ANSI colour code for parameters
    const std::string BoldYellowFont;    //!< ANSI colour code for parameter values
    const std::string BoldRedFont;       //!< ANSI colour code for errors
    const std::string ResetFont;         //!< ANSI colour code to reset to default output


};

#endif /* SOLVER_H_ */
