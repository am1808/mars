/*
 * HAMR_laser.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file HAMRLaser.hpp
 * @brief Header file for HAMR_Laser class */

#ifndef HAMRLASER_HPP_
#define HAMRLASER_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
/** @brief Class for a HAMR like laser profile.
 *
 * Base class for a laser like those found in the recording head of HAMR systems. */
class HAMR_Laser {

protected:

    double Temp_Max;        //!< Maximum temprature of the laser.
    double Gradient;        //!< Gradient of the profile.
    double Cooling_time;    //!< A sixth of the time taken to perform a laser pulse.
    double Temperature;     //!< Current temperature of the grain.

public:

    HAMR_Laser(const ConfigFile cfg);
    ~HAMR_Laser();

    void ApplyT(const double Environ_Temp, const double Time, double*Temperature, Grain_t*Grains);
    double getInstTemp() const;
    double getTemp() const;
    double getGrad() const;
    double getCoolingTime() const;
    double getProfileTime() const;

};

#endif /* HAMRLASER_HPP_ */
