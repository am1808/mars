/*
 * Vec3.hpp
 *
 *  Created on: 5 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Vec3.hpp
 * @brief Header file for Vec3 class */

#ifndef VEC3_HPP_
#define VEC3_HPP_

#include <iostream>

/** \brief Class for a 3-vector.
 *
 * This class contains basic 3-Vector operations.
 */
class Vec3{

public:

    Vec3();
    Vec3(double x,double y,double z);
    Vec3(const Vec3& vector);
    ~Vec3();

    double x,       //!< X-component
           y,       //!< Y-component
           z;       //!< Z-component

    // Operators
    friend std::ostream& operator<<(std::ostream& os, const Vec3& vec);

    // Mathematical operations
    friend Vec3 operator+(Vec3 const &lhs, Vec3 const &rhs);
    friend Vec3 operator-(Vec3 const &lhs, Vec3 const &rhs);
    friend Vec3 operator-(Vec3 const &rhs);
    friend double operator*(Vec3 const &lhs, Vec3 const &rhs); // Dot product
    friend Vec3 operator*(Vec3 const &lhs,double const &rhs);
    friend Vec3 operator*(double const &lhs,Vec3 const &rhs);
    friend Vec3 operator%(Vec3 const &lhs, Vec3 const &rhs); // Cross product
    friend Vec3 operator/(Vec3 const &lhs,double const &rhs);

    void operator+=(Vec3 const &rhs);
    void operator-=(Vec3 const &rhs);
    void operator*=(double const &rhs);
    void operator/=(double const &rhs);
    void operator=(double const &rhs);
};



#endif /* VEC3_HPP_ */
