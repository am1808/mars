/*
 * RecHead.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecHead.hpp
 * @brief Header file for RecHead class */

#include <cmath>
#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
#include "Writer.hpp"
#include "HAMRLaser.hpp"
#include "PixelMap.hpp"

#ifndef RECHEAD_HPP_
#define RECHEAD_HPP_
/** @brief Sub-class for a write head with a near field transducer (NFT).
 *
 * Sub-class of Writer and HAMR_laser. This class provides the functionality of a write head
 * for HAMR systems. It adds spatial information to the HAMR_laser.
 */
class RecHead : public Writer, public HAMR_Laser {

protected:

    bool Pulsing;         //!< Sets if laser should turn pulse during writing
    double FWHM_X;        //!< Full width at half maximum of the laser profile in the x-dimension
    double FWHM_Y;        //!< Full width at half maximum of the laser profile in the y-dimension
    double NFT_disp_X;    //!< Distance between field profile and laser profile in x-direction

public:


    RecHead(const ConfigFile cfg);
    ~RecHead();

    void ApplyTspatialCont(const unsigned int Num_Layers, const Voronoi_t VORO, const double Environ_Temp,
                           const double Time, double*Temperature, Grain_t*Grains);

    void ApplyTspatialContPM(const unsigned int Num_Layers, const Voronoi_t VORO, const double Environ_Temp,
                             const double Time, double&Temperature, Grain_t&Grains, PixelMap&PixMap);

    void ApplyTspatial(const Voronoi_t VORO, const unsigned int Num_Layers,
                       const double Environ_Temp, const double Time,
                       double*Spatial_Temp, Grain_t*Grain);
    void ApplyTspatialPM(const Voronoi_t VORO, const unsigned int Num_Layers, const double Environ_Temp,
                       const double Time, double&Spatial_Temp, Grain_t&Grains, PixelMap&PixMap);


    bool getPulsing();

};
#endif /* RECHEAD_HPP_ */
