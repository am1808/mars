/*
 * BasicLaser.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file BasicLaser.hpp
 * @brief Header file for BasicLaser class */

#ifndef BASICLASER_HPP_
#define BASICLASER_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
/// Possible states of the basic laser
enum struct BlConditions{Min,        //!< Laser is at minimum power.
                         Heating,    //!< Laser is ramping up.
                         Max,        //!< Laser at maximum power.
                         Cooling     //!< Laser is ramping down.
};
/** @brief Class for a Hdc like laser.
 *
 * This class provides the functionality of a constant temperature laser.
 * The laser can be turned off and on and will ramp up/down in a linear fashion
 * within the time specified by the Rate variable. This class does not provide
 * any spatial profiles.
 */
class BasicLaser {

protected:

    BlConditions state;    //!< Current state of the laser
    bool Active;           //!< Tracks if the laser is on or off
    bool Instantaneous;    //!< Special flag to indicate the rate of temperature change is zero.
    double TempMax;        //!< Maximum temperature produced by the laser
    double TempMin;        //!< System temperature when the laser is off
    double TempCur;        //!< Current temperature produced by the the laser
    double Rate;           //!< Rate of temperature change when heating/cooling

    void updateState();

public:
    BasicLaser(const ConfigFile cfg, const double Environ_temp);
    ~BasicLaser();
    void turnOn();
    void turnOff();
    void updateLaser(double dt);
    void setTempMax(double val);
    double getTemp() const;
    double getTempMax() const;
    double getRate() const;
    double getCoolingTime() const;
    bool isOn() const;
    bool isMax() const;
    bool isMin() const;

};

#endif /* BASICLASER_HPP_ */
