/*
 * PixelMap.hpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file PixelMap.hpp
 * @brief Header file for PixelMap class */

#ifndef M_A_R_S__SRC_PIXELMAP_HPP_
#define M_A_R_S__SRC_PIXELMAP_HPP_

#include <unordered_set>
#include "Pixel.hpp"
#include "../Config_File/ConfigFile_import.hpp"
/// List of available output data.
enum struct DataType {M,    //!< Magnetisation
                      EA,   //!< Easy axis
                      H,    //!< Field
                      T,    //!< Temperature
                      ALL   //!< All data
};
/** @brief Class for the pixelmap.
 *
 * This class contains the functionality to create a pixelmap to discretise the granular
 * system into pixels using the pixel class. */
class PixelMap {

private:
    std::vector<std::vector<Pixel>> Cells;    //!< 2D list of pixels within the pixel map
    double cellsize;                          //!< Size of each pixel
public:
    PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cellsize);
    PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cellsize, const ConfigFile cfg, const bool isImport=false);
    virtual ~PixelMap();

    void Discretise(const Voronoi_t &VORO, const Grain_t &Grains);
    void MapVal(const Grain_t&Grains, const DataType type);
    void Print(const std::string filename);
    void PrintwUpdate(const std::string filename, const Grain_t&Grain, const DataType type);
    void StoreinGrain(Grain_t&Grains);
    void ClearH();
    void ClearT();


    void InRect(const double xMin, const double yMin, const double xMax, const double yMax,
                std::vector<unsigned int>&PixelList,std::unordered_set<unsigned int>&ImpactedGrains) const;
    Pixel Access(const unsigned int X, const unsigned int Y) const;
    Pixel Access(const unsigned int ID) const;
    unsigned int getRows() const;
    unsigned int getCols() const;
    double getCellsize() const;
};

#endif /* M_A_R_S__SRC_PIXELMAP_HPP_ */
