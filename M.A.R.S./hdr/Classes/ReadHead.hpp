/*
 * ReadHead.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadHead.hpp
 * @brief Header file for ReadHead class */

#ifndef READHEAD_HPP_
#define READHEAD_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
#include "RecLayer.hpp"
/** @brief Class for a read head.
 *
 * This class provides basic functionality of a read head. */
class ReadHead{

protected:

    double SizeX;       //!< Width of read head
    double SizeY;       //!< Length of read head
    double PosX;        //!< Position of read head in x-direction.
    double PosY;        //!< Position of read head in y-direction.

public:

    ReadHead(const RecLayer& RecordingLayer);
    ReadHead(const ConfigFile cfg);
    ~ReadHead();

    double getSizeX() const;
    double getSizeY() const;
    double getPosX() const;
    double getPosY() const;
    void setPosX(double x);
    void setPosY(double y);

};

#endif /* READHEAD_HPP_ */
