/*
 * Hac.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Hac.hpp
 * @brief Header file for Hac class */

#ifndef HAC_HPP_
#define HAC_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"

/** @brief Base class for an alternating magnetic field.
 *
 * This class provides the functionality of an alternating magnetic field. */
class Hac {

protected:

    // Instantaneous values
    Vec3 field;       //!< Field vector
    double mag;       //!< Field magnitude

    // Descriptive values
    Vec3 dirn;         //!< Direction vector
    double amp;        //!< Amplitude (Oe)
    double freq;       //!< Frequency (Hz)
    double phaseShift; //!< Phase shift in radians

public:

    Hac(const ConfigFile cfg);
    ~Hac();

    void updateField(double time);
    void updateMag(double time);
    void setDirn(Vec3 Direction);
    void setFreq(double frequency);
    void setAmp(double Amplitude);
    Vec3 getField() const;
    double getMag() const;
    Vec3 getDirn() const;
    double getFreq() const;
    double getPhase() const;
    double getAmp() const;


};

#endif /* HAC_HPP_ */
