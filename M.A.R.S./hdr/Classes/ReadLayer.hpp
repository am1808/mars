/*
 * ReadLayer.hpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadLayer.hpp
 * @brief Header file for ReadLayer class */

#ifndef READLAYER_HPP_
#define READLAYER_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
#include "RecLayer.hpp"
/** @brief Class for recording layer information for read back simulations.
 *
 * Class providing a reduced set of information specific to the recording layer.
 * This class is designed to reduce the required input data for read only simulations. */
class ReadLayer{

protected:

    double Track_spacing;   //!< Spacing between tracks.
    double Track_size;      //!< Size of track (y-dimension).
    double Track_number;    //!< Number of tracks.

public:

    ReadLayer(const RecLayer& RecordingLayer);
    ReadLayer(const ConfigFile cfg);
    ~ReadLayer();
    double getBitSpacingY() const;
    double getBitsY() const;
    double getBitSizeY() const;
};

#endif /* READLAYER_HPP_ */
