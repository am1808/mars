/*
 * Generate_interactions.hpp
 *
 *  Created on: 9 Feb 2019
 *      Author: Ewan Rannala
 */

/** \file Generate_interactions.hpp
 * \brief Header file for interactions generation function. */

#ifndef INTERACTIONS_AND_NEIGHBOURS_HPP_
#define INTERACTIONS_AND_NEIGHBOURS_HPP_

#include "../../hdr/Structures.hpp"

extern int Generate_interactions(const int Num_layers,const std::vector<double> Grain_Vol,
		const double Magneto_CUT_OFF,const std::string Magneto_type, const Material_t MAT,const Voronoi_t VORO,Interaction_t*Int_system);

#endif /* INTERACTIONS_AND_NEIGHBOURS_HPP_ */
