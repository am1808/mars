/* Structures.hpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Structures.hpp
 * \brief Header file for data structs. */

#ifndef STRUCTURES_HPP_
#define STRUCTURES_HPP_

#include <vector>
#include <string>
#include <iostream>

#include "Globals.hpp"
#include "Classes/Vec3.hpp"

/** \brief Structure for grain information.
 *
 * <P> This structure contains the information of every grain in a system </P> */
struct Grain_t {

    std::vector<std::string> mEQ_Type;                //!< Model used for equilibrium magnetisation
    std::vector<std::string> Callen_power_range; //!< Scheme used for Callen-Callen
    std::vector<Vec3> m;                         //!< Magnetisation 3-vector
    std::vector<Vec3> Easy_axis;                 //!< Easy axis 3-vector
    std::vector<double> Temp;                     //!< Temperature [K]
    std::vector<double> diameter;                 //!< Diameter [nm]
    std::vector<double> Vol;                     //!< Volume [nm<SUP>3</SUP>]
    std::vector<double> K;                         //!< Anisotropy [erg&frasl;cc]
    std::vector<double> Tc;                         //!< Curie Temperature    [K]
    std::vector<double> Ms;                         //!< Saturation magnetisation [emu&frasl;cc]
    std::vector<std::string> Ani_method;                 //!< Method used to model the grain's anisotropy
    std::vector<double> Callen_power;             //!< Callen-Callen exponent
    std::vector<double> Callen_factor_lowT;         //!< Callen-Callen pre-factor for low range temperatures
    std::vector<double> Callen_factor_midT;         //!< Callen-Callen pre-factor for mid range temperatures
    std::vector<double> Callen_factor_highT;     //!< Callen-Callen pre-factor for high range temperatures
    std::vector<double> Callen_power_lowT;         //!< Callen-Callen exponent for low range temperatures
    std::vector<double> Callen_power_midT;         //!< Callen-Callen exponent for mid range temperatures
    std::vector<double> Callen_power_highT;         //!< Callen-Callen exponent for high range temperatures
    std::vector<double> Callen_range_lowT;         //!< Callen-Callen low temperature range definition
    std::vector<double> Callen_range_midT;         //!< Callen-Callen mid temperature range definition
    std::vector<double> Crit_exp;                 //!< Critical exponent for M(T) behaviour if Heisenberg bulk-like behaviour is assumed
    //@{
    //! Critical exponents for M(T) behaviour if polynomial expression is used
    std::vector<double> a0_mEQ,a1_mEQ,a2_mEQ,
                            a3_mEQ,a4_mEQ,
                            a5_mEQ,a6_mEQ,a7_mEQ,
                            a8_mEQ,a9_mEQ,a1_2_mEQ,
                            b1_mEQ,b2_mEQ;
    //@}
    std::vector<Vec3> H_appl;                     //!< Applied field 3-vector [Oe]
    std::vector<std::vector<unsigned int>> Pixel; //!< List of pixels within the grain
};

/** \brief Structure for material/layer information
 *
 * <P> This Structure contains the information for a material/layer in the system </P> */
struct Material_t {

    std::vector<std::string> Mag_Type;                //!< Type of initial magnetisation (Random or assigned)
    std::vector<Vec3> Initial_mag;                    //!< Initial magnetisation for all grains within the layer
    std::vector<Vec3> Initial_anis;                    //!< Initial easy axis for all grains within the layer
    std::vector<std::string> Type;                    //!< Material type (Currently unused)
    std::vector<std::string> Tc_Dist_Type;            //!< Distribution type for Curie temperature
    std::vector<std::string> K_Dist_Type;            //!< Distribution type for anisotropy
    std::vector<std::string> J_Dist_Type;            //!< Distribution type for exchange interactions
    std::vector<std::string> mEQ_Type;                //!< Model used for equilibrium magnetisation
    std::vector<std::string> Susceptibility_Type;    //!< Model used for susceptibility
    std::vector<std::string> Callen_power_range;    //!< Scheme used for Callen-Callen
    std::vector<std::string> Ani_method;            //!< Method used to model material's anisotropy
    std::vector<double> Ms;                            //!< Saturation magnetisation [emu&frasl;cc]
    std::vector<double> Tc_Ddist_d0;                   //!< Scale parameter for scaling law of Curie temperature with system dimension
    std::vector<double> Tc_Ddist_nu;                   //!< Critical exponent of scaling law of Curie temperature with system dimension
    std::vector<double> Tc_inf;                        //!< Maximum value of Curie temperature [K]
    std::vector<double> Avg_Tc;                        //!< Average Curie temperature [K]
    std::vector<double> StdDev_Tc;                    //!< Standard deviation of Curie temperature
    std::vector<double> Avg_K;                        //!< Average anisotropy [erg&frasl;cc]
    std::vector<double> StdDev_K;                    //!< Standard deviation of anisotropy
    std::vector<double> StdDev_J;                    //!< standard deviation of exchange strength
    std::vector<double> z;                            //!< Vertical position of layer's centre [nm]
    std::vector<double> dz;                            //!< Thickness of layer [nm]
    std::vector<double> Anis_angle;                    //!< Anisotropy angle of dispersion [deg]
    std::vector<double> H_sat;                        //!< Exchange field saturation [Oe]
    std::vector<double> Easy_axis_polar;            //!< Easy axis polar angle [deg]
    std::vector<double> Easy_axis_azimuth;            //!< Easy axis azimuthal angle [deg]
    std::vector<double> Callen_power;                //!< Callen-callen exponent
    std::vector<double> Callen_factor_lowT;            //!< Callen-Callen pre-factor for low range temperatures
    std::vector<double> Callen_factor_midT;            //!< Callen-Callen pre-factor for mid range temperatures
    std::vector<double> Callen_factor_highT;        //!< Callen-Callen pre-factor for high range temperatures
    std::vector<double> Callen_power_lowT;            //!< Callen-Callen exponent for low range temperatures
    std::vector<double> Callen_power_midT;            //!< Callen-Callen exponent for mid range temperatures
    std::vector<double> Callen_power_highT;            //!< Callen-Callen exponent for high range temperatures
    std::vector<double> Callen_range_lowT;            //!< Callen-Callen low temperature range definition
    std::vector<double> Callen_range_midT;            //!< Callen-Callen mid temperature range definition
    std::vector<double> Crit_exp;                    //!< Critical exponent for M(T) behaviour if Heisenberg bulk-like behaviour is assumed
    //@{
    //! Critical exponents for M(T) behaviour if polynomial expression is used
    std::vector<double> a0_mEQ,a1_mEQ,a2_mEQ,
                            a3_mEQ,a4_mEQ,
                            a5_mEQ,a6_mEQ,a7_mEQ,
                            a8_mEQ,a9_mEQ,a1_2_mEQ,
                            b0_mEQ,b1_mEQ,b2_mEQ,
                            b3_mEQ,b4_mEQ;
    //@}
    // Out of plane exchange
    std::vector<double> Hexch_str_out_plane_UP;        //!< Exchange field strength exerted on layers above [Oe]
    std::vector<double> Hexch_str_out_plane_DOWN;    //!< Exchange field strength exerted on layers below [Oe]

};

/** \brief Structure for system structure information
 *
 * <P> This structure contains information required to generate the system's structure </P> */
struct Structure_t {

    double Dim_x;                           //!< System x-dimension [nm]
    double Dim_y;                           //!< system y-dimension [nm]
    double Grain_width;                     //!< Average grain width [nm]
    double Max_width_limit;                 //!< Upper limit of grain width
    double Packing_fraction;                //!< Fraction of system area filled by grains
    double StdDev_grain_pos;                //!< Standard deviation in grain position
    double Magneto_Interaction_Radius;      //!< Interaction radius for magnetostatics [nm]
    unsigned int Num_layers;                //!< Number of layers within the system
    std::string Magnetostatics_gen_type;    //!< Method for magnetostatics generation [import OR dipole]

};

/** \brief Structure for voronoi construction data
 *
 * <P> This structure contains all information used in the Voronoi construction </P> */
struct Voronoi_t {

    unsigned int Num_Grains;                                            //!< Number of grains per layer
    double x_max;                                                        //!< Maximum allowed grain centre in x-axis [nm]
    double y_max;                                                        //!< Maximum allowed grain centre in y-axis [nm]
    double Int_Rad;                                                        //!< Interaction radius for exchange [nm]
    double Centre_X;                                                    //!< System centre in x-direction [nm]
    double Centre_Y;                                                    //!< System centre on y-direction [nm]
    double Average_area=0.0;                                            //!< Average grain area [nm<SUP>2</SUP>]
    double Average_contact_length=0.0;                                    //!< Average grain contact length [nm]
    double Input_grain_width=0.0;                                 	   //!< Input average grain width [nm]
    double Real_grain_width=0.0;                                 	   //!< Obtained average grain width [nm]
    double Vx_MIN=0.0;                                                    //!< Minimum x-position held by a vertex [nm]
    double Vx_MAX=0.0;                                                    //!< Maximum x-position held by a vertex [nm]
    double Vy_MIN=0.0;                                                    //!< Minimum y-position held by a vertex [nm]
    double Vy_MAX=0.0;                                                    //!< Maximum y-position held by a vertex [nm]
    std::vector<double> Pos_X_final;                                    //!< Grain positions in x-axis [nm]
    std::vector<double> Pos_Y_final;                                    //!< Grain positions in y-axis [nm]
    std::vector<double> Geo_grain_centre_X;                                //!< Grain geometrical centre in x-axis [nm]
    std::vector<double> Geo_grain_centre_Y;                                //!< Grain geometrical centre in y-axis [nm]
    std::vector<double> Grain_diameter;                                    //!< Grain diameters [nm]
    std::vector<double> Grain_Area;                                        //!< Grain areas [nm<SUP>2</SUP>]
    std::vector<std::vector<double>> Contact_lengths;                    //!< Grain contact lengths [nm]
    std::vector<std::vector<double>> Vertex_X_final;                    //!< Grain vertices in x-axis [nm]
    std::vector<std::vector<double>> Vertex_Y_final;                    //!< Grain vertices in y-axis [nm]
    std::vector<std::vector<unsigned int>> Neighbour_final;                //!< Grain nearest neighbour list
    std::vector<std::vector<unsigned int>> Magnetostatic_neighbours;    //!< Grain magnetostatic neighbour list

};

/** \brief Structure for interaction data
 *
 * <P> This structure contains all data required for interactions </P> */
struct Interaction_t {

    std::vector<std::vector<unsigned int>> Magneto_neigh_list;    //!< Magnetostatic neighbour lists
    std::vector<std::vector<unsigned int>> Exchange_neigh_list;    //!< Exchange interaction neighbour list
    std::vector<std::vector<double>> Wxx;                        //!< W-matrix xx element
    std::vector<std::vector<double>> Wxy;                        //!< W-matrix xy element
    std::vector<std::vector<double>> Wxz;                        //!< W-matrix xz element
    std::vector<std::vector<double>> Wyy;                        //!< W-matrix yy element
    std::vector<std::vector<double>> Wyz;                        //!< W-matrix yz element
    std::vector<std::vector<double>> Wzz;                        //!< W-matrix zz element
    std::vector<std::vector<double>> H_exch_str;                //!< Exchange field strength [Oe]

};

/** \brief Structure for environment parameters
 *
 * <P> This structure contains information about the environment the simulation occurs in. </P> */
struct Expt_environment_t {

    double Temperature;         //!< System temperature in the absence of a laser pulse
};

/** \brief Structure for data files used for entire system import
 *
 * <P> This structure contains the names of all files required to generate a system from imported data </P> */
struct Data_input_t {

    std::string Voro_file;         //!< Data file containing all information required by Voronoi_t
    std::string St_Mat_file;       //!< Data file containing Number of grains, layers and layer thickness
    std::string Pos_file;          //!< Data file containing grain positions
    std::string Vert_file;         //!< Data file containing grain vertices
    std::string Geo_file;          //!< Data file containing grain geometrical centres
    std::string Neigh_file;        //!< Data file containing grain nearest neighbours
    std::string Mag_neigh_file;    //!< Data file containing grain magnetostatic neighbours
    std::string Area_file;         //!< Data file containing grain areas
    std::string CL_file;           //!< Data file containing grain contact lengths
    std::string GpV_file;          //!< Data file containing grain data vectors
    std::string GpC_file;          //!< Data file containing grain Callen-Callen data
    std::string Gp_file;           //!< Data file containing grain scalar data
    std::string GpEQ_file;         //!< Data file containing mEQ data
    std::string CHI_file;          //!< Data file containing Chi fit parameters
    std::string CHI_PP_file;       //!< Data file containing Chi perpendicular fit parameters for individual grains
    std::string CHI_PA_file;       //!< Data file containing Chi parallel fit parameters for individual grains
    std::string ALPHA_file;        //!< Data file containing damping parameter per layer
    std::string IpM_file;          //!< Data file containing magnetostatic neighbours list
    std::string IpE_file;          //!< Data file containing exchange neighbours list
    std::string IpW_file;          //!< Data file containing W-matrix elements
    std::string IpH_file;          //!< Data file containing exchange field strengths
    std::string PixelMap_file;     //!< Data file containing pixel map data

};

#endif /* STRUCTURES_HPP_ */
