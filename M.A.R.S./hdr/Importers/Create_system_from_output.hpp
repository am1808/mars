/*
 * Create_system_from_output.hpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Create_system_from_output.hpp
 * \brief Header file for system generation from input files function. */

#ifndef CREATE_SYSTEM_FROM_OUTPUT_HPP_
#define CREATE_SYSTEM_FROM_OUTPUT_HPP_

#include "../Structures.hpp"
#include "../Classes/Solver.hpp"

extern int Read_in_system(const Data_input_t IN_data, Structure_t*Structure, Voronoi_t*VORO,
                          Material_t*Materials, Grain_t*Grain, Interaction_t*Int,Solver_t*Solver);

#endif /* CREATE_SYSTEM_FROM_OUTPUT_HPP_ */
