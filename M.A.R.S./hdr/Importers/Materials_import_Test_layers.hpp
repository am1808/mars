/* Materials_import_test_layers.hpp
 *  Created on: 29 Jul 2018
 *      Author: Andrea Meo, Samuel Ewan Rannala
 */

/** \file Materials_import_Test_layers.hpp
 * \brief Header file for Material layers import for LLB tests function. */


#ifndef MATERIALS_IMPORT_TEST_LAYERS_HPP_
#define MATERIALS_IMPORT_TEST_LAYERS_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int Materials_import_Test_layers(const std::string,const int,std::vector<ConfigFile>*,Material_t*);

#endif /* MATERIALS_IMPORT_TEST_LAYERS_HPP_ */
