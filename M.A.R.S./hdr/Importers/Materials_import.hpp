/* Materials_import.hpp
 *  Created on: 29 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Materials_import.hpp
 * \brief Header file for Materials import function. */

#ifndef MATERIALS_IMPORT_HPP_
#define MATERIALS_IMPORT_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int Materials_import(const ConfigFile,const int,std::vector<ConfigFile>*,Material_t*);

#endif /* MATERIALS_IMPORT_HPP_ */
