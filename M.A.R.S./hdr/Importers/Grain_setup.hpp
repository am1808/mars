/* Grain_setup.hpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Grain_setup.hpp
 * \brief Header file for grain construction function. */

#ifndef GRAIN_SETUP_HPP_
#define GRAIN_SETUP_HPP_

#include <string>
#include "../../hdr/Structures.hpp"

extern int Grain_setup(const unsigned int Num_grains,const unsigned int Num_Layers,
					   const Material_t Material, const std::vector<double> Grain_area,
					   const std::vector<double> Grain_diameter, const double Temperature, Grain_t*Grain);


#endif /* GRAIN_SETUP_HPP_ */
