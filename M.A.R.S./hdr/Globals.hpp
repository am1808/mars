/* Globals.hpp
 *  Created on: 3 Aug 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Globals.hpp
 * \brief Header file for global variables. */

#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_

#include <random>

extern const double PI;  //!< Pi constant
extern const double KB;  //!< Boltzmann constant in CGS units
extern std::mt19937_64 Gen; //!< Mersenne twister pseudorandom number generator

#endif /* GLOBALS_HPP_ */
