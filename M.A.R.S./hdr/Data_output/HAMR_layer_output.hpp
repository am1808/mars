/*
 * HAMR_layer_output.hpp
 *
 *  Created on: 8 Oct 2018
 *      Author: ewan
 */

/** \file HAMR_layer_output.hpp
 * \brief Header file for HAMR output functions related to entire layer averages. */

#ifndef HAMR_LAYER_OUTPUT_HPP_
#define HAMR_LAYER_OUTPUT_HPP_

#include "../../hdr/Structures.hpp"

extern int HAMR_layer_averages(const int, const int, const Grain_t, const double, const double,
						const double, const std::vector<double>, const std::string);

#endif /* HAMR_LAYER_OUTPUT_HPP_ */
