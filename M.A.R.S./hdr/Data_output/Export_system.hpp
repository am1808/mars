/*
 * Export_system.hpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Export_system.hpp
 * \brief Header file for system export functions. */

#ifndef EXPORT_SYSTEM_HPP_
#define EXPORT_SYSTEM_HPP_

#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/Solver.hpp"


extern int Export_system(const int Num_layers, const std::vector<double> dz, const Voronoi_t VORO,
                         const Interaction_t Int, const Grain_t Grain, const Solver_t Solver,
                         const std::string Output_iter);

#endif /* EXPORT_SYSTEM_HPP_ */
