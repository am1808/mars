/*
 * Test_KMC_Rate.hpp
 *
 *  Created on: 27 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file Test_KMC_Rate.hpp
 * \brief Header file for kMC coercivity test. */

#ifndef TEST_KMC_HYST_HPP_
#define TEST_KMC_HYST_HPP_

extern int KMC_Rate_test();

#endif /* TEST_KMC_HYST_HPP_ */
