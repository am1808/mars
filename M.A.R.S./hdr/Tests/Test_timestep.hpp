/* Test_timestep.hpp
 *  Created on: 24 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file Test_timestep.hpp
 * \brief Header file for timestep test. */

#ifndef TEST_LLB_TIMESTEP_HPP_
#define TEST_LLB_TIMESTEP_HPP_

extern int Timestep_test();

#endif /* TEST_LLB_TIMESTEP_HPP_ */
