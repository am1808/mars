/*
 * Read_back.hpp
 *
 *  Created on: 4 Nov 2019
 *      Author: Ewan Rannala
 */

/** \file Read_back.hpp
 * \brief Header file for data read back simulation. */

#ifndef READ_BACK_HPP_
#define READ_BACK_HPP_

#include "../Structures.hpp"
#include "../Config_File/ConfigFile_import.hpp"
#include "../Classes/ReadLayer.hpp"
#include "../Classes/ReadHead.hpp"

extern int data_read_back(const Grain_t&Grain, const Voronoi_t&VORO, const ReadLayer ReadingLayer, const std::string FILENAME, ReadHead ReadingHead);

extern int data_read_back_from_import(ConfigFile cfg);

#endif /* READ_BACK_HPP_ */
