/*
 * HAMR.hpp
 *
 *  Created on: 21 Aug 2018
 *      Author: ewan
 */

/** \file HAMR.hpp
 * \brief Header file for simulation of thermal and field profiles over an entire system. */

#ifndef HAMR_HPP_
#define HAMR_HPP_

#include "../../hdr/Config_File/ConfigFile_import.hpp"

extern int HAMR(ConfigFile);

#endif /* HAMR_HPP_ */
