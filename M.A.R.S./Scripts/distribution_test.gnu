#!/usr/bin/gnuplot

f0 = "../Output/LN_test.dat"
f1 = "../Output/LN_G_test.dat"

#bin_width = 1;
set boxwidth bin_width
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + bin_width/2.0 )


plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes t 'n',f1 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes t 'ln'
#p f0 u (rounded($1)):(1./(PARTICLES*bin_width)) w p

pause -1
