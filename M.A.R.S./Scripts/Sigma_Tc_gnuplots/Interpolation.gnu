set term postscript eps color enhanced font "Times-Roman,16"
set output "Interpolation.eps"

system('echo -n "\tInterpolating data..."')

####### File
#OG_data_file = "Final_magnetisation.dat"

####### Data inputs 
#Ms=1100
#T_min=500
#T_max=800
#T_step=0.5
#T_min_RANGE=600
#T_max_RANGE=750

####### Gnuplot inputs
#interp_min=5
#interp_max=33
#interp_steps=2
#COL_T=4
#COL_Mz=3

####### Derivative and fit
N=(T_max-T_min)/T_step
dx=T_step*0.5
x0=NaN
y0=NaN
a1=1
b1=1
c1=1
d(y)=($0==0)?(y1=y,1/0):(y2=y1,y1=y,y1-y2)

output_blocklines=1
do for [i=interp_min:interp_max:interp_steps]{
	interp_points=i
	output_blocklines=interp_points #+1 need this +1 if writing interpolated data to input file 

	# Set the output files for the interpolated data (FIT file) and the log files for fittings (LOG file)
	# Data is copied to this file for interpolation
	Interp_input(interp_points)=sprintf("Interpolation_%.0f_point_data_IN.dat",interp_points)
	# Interpolated values are written to this file
	Interp_output(interp_points)=sprintf("Interpolation_%.0f_point_data.dat",interp_points)
	fit_logfile(interp_points)=sprintf("Interpolation_%.0f_point_logfile.dat",interp_points)
	set print Interp_output(interp_points) append		# All prints now go to this file
	set fit logfile fit_logfile(interp_points)		# All fit logs now go to this file
	set fit quiet


	# Now the data needs to be extracted from the original file and placed into the FIT file
	do for [j=0:(N-interp_points)]{
		
		k_end = (interp_points-1)
		set print Interp_input(interp_points) append
		# These are the lines we want to extract from the original file
		do for [k=0:k_end]{
			input_datarow=j+k

			# stats reads in the specified row and column STATS_min is used to extract that value into gnuplot as a variable
			stats OG_data_file every ::input_datarow::input_datarow u COL_T nooutput
			T=STATS_min
			stats OG_data_file every ::input_datarow::input_datarow u COL_Mz nooutput
			Mz=STATS_min

			print T,Mz
		}
		
		f(x)=a1*(x**2)+b1*x+c1
	
		# Select datarow from FIT file
		output_datarow_start=output_blocklines*j
		output_datarow_end  = output_blocklines*j+k_end

		fit f(x) Interp_input(interp_points) every ::output_datarow_start::output_datarow_end u 1:2 via a1,b1,c1

		stats Interp_input(interp_points) every ::output_datarow_start::output_datarow_end u 1 nooutput
		T_mid=STATS_mean	# Select the mid point of the block
		Mz_interp=f(T_mid)

		set print Interp_output(interp_points) append
		print T_mid,Mz_interp
	}
	set xrange[T_min_RANGE:T_max_RANGE]
	set xlabel "Tp (K)"
	set ylabel "M/|M|"

# FOR DATA IN INPUTFILE ONLY Interp_points u 1:2 w l t 'Smoothed '.interp_points.' Points', OG_data_file u COL_T:COL_Mz w p t 'OG-data'

	p OG_data_file u COL_T:COL_Mz w p pt 6 ps 0.5 lc 2 t 'OG-data',\
	  Interp_output(interp_points) u 1:2 w l lw 2 lc 1 t 'Smoothed '.interp_points.' Points'
	
#	p OG_data_file u (dx=$4-x0,x0=$4,$4-dx/2):(dy=$3-y0,y0=$3,dy/dx) w l t 'OG-data',\
#	  Interp_output(interp_points) u (dx=$1-x0,x0=$1,$1-dx/2):(dy=$2-y0,y0=$2,dy/dx) w lp pt 6 ps 0.5 lw 2 t 'derivative Smoothed '.interp_points.' Points'

#	set ylabel "dM_s" 
#	p OG_data_file u ($4-dx):(d($3*Ms)) w l t 'OG-data',\
#	  Interp_output(interp_points) u ($1-dx):(d($2*Ms)) w lp pt 6 ps 0.5 lw 2 t 'derivative Smoothed '.interp_points.' Points'


	unset xrange
}

system('echo "done."')
