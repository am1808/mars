set term postscript eps color enhanced font "Times-Roman,16"
set output "Derivatives.eps"

system('echo -n "\tDifferentiating interpolated data..."')

####### File
#OG_data_file = "Final_magnetisation.dat"

####### Data inputs
#Ms=1100
#T_min=500
#T_max=800
#T_step=0.5
#T_min_RANGE=600
#T_max_RANGE=750

####### Gnuplot inputs
#interp_min=5
#interp_max=33
#interp_steps=2

####### Derivative
Num_points=(T_max-T_min)/T_step
dx=T_step*0.5
d(y)=($0==0)?(y1=y,1/0):(y2=y1,y1=y,y1-y2)
x0=NaN
y0=NaN


# loop over all interpolations
do for [i=interp_min:interp_max:interp_steps]{
	interp_points=i
	Total_input_rows=Num_points-interp_points-1

	input_file(interp_points)=sprintf("Interpolation_%.0f_point_data.dat",interp_points)
	derivative_output(interp_points)=sprintf("Derivative_data_%.0f_point.dat",interp_points)
	IN_FILE = input_file(interp_points)
	set print derivative_output(interp_points) #append
	
	# Extract input data and output derivative
	do for [j=0:Total_input_rows]{
		input_row=j
		
		stats IN_FILE every ::j::j u 1 nooutput
		T_one=STATS_min
		stats IN_FILE every ::(j+1)::(j+1) u 1 nooutput
		T_two=STATS_min
		stats IN_FILE every ::j::j u 2 nooutput
		Mz_one=STATS_min
		stats IN_FILE every ::(j+1)::(j+1) u 2 nooutput
		Mz_two=STATS_min
		
		T_mid = T_one+dx
#		d = (Mz_two-Mz_one)/(T_two-T_one)
		d = (Mz_two-Mz_one)
		d_Ms = d*Ms
		print T_mid,d,d_Ms
	}
	
	# Now that we have a file for the differentiated data we can plot it
	
	set xrange[T_min_RANGE:T_max_RANGE]
	set xlabel "Tp (K)"
	set ylabel "dM_s"
	
	p OG_data_file u ($4-dx):(d($3*Ms)) w l t 'OG-data',\
	  derivative_output(interp_points) u 1:3 w lp pt 6 ps 0.5 lw 2 t 'derivative Smoothed '.interp_points.' Points'

	unset xrange
	
}

system('echo "done."')
