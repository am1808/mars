# Set input for Gnuplot scripts

if [ -d "Gnuplot_outputs" ]; then
	echo "Output folder exists... Wiping contents."
	rm Gnuplot_outputs/Interpolation_*
	rm Gnuplot_outputs/Derivative_*
	rm Sigma_Tc_values.dat
else
	echo "Creating output folder."
	mkdir Gnuplot_outputs
fi


# File attributes
cp ../../Output/Final_magnetisation.dat ./Temp.dat
sed 1d Temp.dat > Final_magnetisation.dat
rm Temp.dat


OG_data_file_in='"Final_magnetisation.dat"'
COL_T_in=4
COL_Mz_in=3

# Data inputs
Ms_in=1100
T_min_in=400
T_max_in=1000
T_step_in=0.25
# Graph Temp range
T_min_RANGE_in=650
T_max_RANGE_in=750

# Desired Outputs
interp_min_in=5
interp_max_in=21
interp_steps_in=2

# Intial Sigma_Tc fitting guesses
Height_guess=60
Centre_guess=680
Width_guess=3.0


# Run interpolation Gnuplot script
gnuplot -e interp_min=$interp_min_in -e interp_max=$interp_max_in -e interp_steps=$interp_steps_in -e Ms=$Ms_in -e T_min=$T_min_in -e T_max=$T_max_in -e T_step=$T_step_in -e T_min_RANGE=$T_min_RANGE_in -e T_max_RANGE=$T_max_RANGE_in -e COL_T=$COL_T_in -e COL_Mz=$COL_Mz_in -e OG_data_file=$OG_data_file_in Interpolation.gnu
# Run differentiation Gnuplot script
gnuplot -e interp_min=$interp_min_in -e interp_max=$interp_max_in -e interp_steps=$interp_steps_in -e Ms=$Ms_in -e T_min=$T_min_in -e T_max=$T_max_in -e T_step=$T_step_in -e T_min_RANGE=$T_min_RANGE_in -e T_max_RANGE=$T_max_RANGE_in -e COL_T=$COL_T_in -e COL_Mz=$COL_Mz_in -e OG_data_file=$OG_data_file_in Differentiate.gnu
# Detemine Gaussian fir from differential data
gnuplot -e interp_min=$interp_min_in -e interp_max=$interp_max_in -e interp_steps=$interp_steps_in -e Ms=$Ms_in -e T_min=$T_min_in -e T_max=$T_max_in -e T_step=$T_step_in -e Height=$Height_guess -e Centre=$Centre_guess -e Width=$Width_guess -e OG_data_file=$OG_data_file_in Sigma_Tc_fit.gnu
# Clean up directory
echo -n "Cleaning working directory..."
mv Interpolation_* Gnuplot_outputs
mv Derivative_* Gnuplot_outputs
mv Sigma_Tc_fit_* Gnuplot_outputs
echo "done."
