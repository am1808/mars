#!/usr/bin/gnuplot
set term postscript enhanced color eps 

system('mkdir Grain_dist')

##############
#AREA input: Use sigma and mu as indicated in the termial output of MARS
#Dia  input: Use muD and sigmaDlg from input cfg file
##############

set fit quiet logfile '/dev/null'

################################################AREA##################################################
f0 = "../Output/area_file.dat"

set ylabel "Probability density"
set xlabel "Grain Area (nm^2)"
#========= Create table to store histogram data =========#
bin_width = 1;
set boxwidth bin_width
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + bin_width/2.0 )

set table "Grain_dist/Hist_area.tmp"
plot f0 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes
unset table

#============== Set distribution functions ==============#
galton(x)=(1.0/(x*sigmalg*sqrt(2.*pi)))*exp(-((log(x)-mulg)**2)/(2.*sigmalg**2))
print mulg,sigmalg
#========================================================#
set output "Grain_dist/AREA_Expected_distributions.eps"


plot "Grain_dist/Hist_area.tmp" w l not '',\
     galton(x) w l lw 3 lc -1 t sprintf('Log-norm {/Symbol m}=%1.2f, {/Symbol s}=%1.2f',mulg,sigmalg)
     
#========================================================#
set output "Grain_dist/AREA_Fitted_distributions.eps"

fit galton(x) "Grain_dist/Hist_area.tmp" u 1:2 via sigmalg,mulg 

#print k,theta

plot "Grain_dist/Hist_area.tmp" w l not '',\
      galton(x) w l lw 3 lc -1 t sprintf('Log-norm {/Symbol m}=%1.2f, {/Symbol s}=%1.2f',mulg,sigmalg)
     
##############################################DIAMETER################################################
f1 = "../Output/diameter_file.dat"

set xlabel "Grain Diameter (nm)"
#========= Create table to store histogram data =========#
bin_width = 0.1;
set boxwidth bin_width
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + bin_width/2.0 )

set table "Grain_dist/Hist_dia.tmp"
plot f1 using (rounded($1)):(1./(PARTICLES*bin_width)) smooth frequency with boxes
unset table
#============== Set distribution functions ==============#
sigmaD = sigmaDlg*muD
muDlg  = log(muD/sqrt(1.0+((sigmaD*sigmaD)/(muD*muD))))

galton(x)=(1.0/(x*sigmaDlg*sqrt(2.*pi)))*exp(-((log(x)-muDlg)**2)/(2.*sigmaDlg**2))

print muD,sigmaD
print muDlg,sigmaDlg
print muDlg,sqrt(log(1.0+(sigmaD*sigmaD)/(muD*muD)))
#========================================================#
set output "Grain_dist/DIA_Expected_distributions.eps"


plot "Grain_dist/Hist_dia.tmp" w l not '',\
      galton(x) w l lw 3 lc -1 t sprintf('Log-norm {/Symbol m}=%1.2f, {/Symbol s}=%1.2f',muDlg,sigmaDlg)
#========================================================#
set output "Grain_dist/DIA_Fitted_distributions.eps"

fit galton(x) "Grain_dist/Hist_dia.tmp" u 1:2 via sigmaDlg,muDlg 

plot "Grain_dist/Hist_dia.tmp" w l not '',\
      galton(x) w l lw 3 lc -1 t sprintf('Log-norm {/Symbol m}=%1.2f, {/Symbol s}=%1.2f',muDlg,sigmaDlg)
     
