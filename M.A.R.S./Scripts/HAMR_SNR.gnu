set term postscript enhanced eps color font "Times-Roman,26" size 10,5
set key font "Times-Roman,16"
set key center tm

load "~/Nextcloud/Gnuplot_colour_profiles/Blue.colour"


do for [ii=Start:Files]{

	set output sprintf('Read_back_Img_%1d.eps', ii)
	set multiplot layout 2,1
	set xrange [-10:]
	
	set cbrange [-1:1]
	set palette defined (-1 "#00539B", 0.0 "#A5ACAF", 1 "#FFB612")
	set title "M_z"
	p "../Time_".Layer."_".ii.".dat" u 1:2:5 w filledcurve palette t ''
	
	set title "Signal"
	p "../Read_back_Img_".ii.".dat" i 0 u 2:5 w l ls 8 t ''
	
	unset multiplot
}
