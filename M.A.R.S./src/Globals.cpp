/*
 * Globals.cpp
 *
 *  Created on: 5 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Globals.cpp
 * @brief Source file for global variables definitions. */

#include "../hdr/Globals.hpp"

const double PI=3.14159265358979;
const double KB=1.38064900000000e-16;
std::mt19937_64 Gen;
