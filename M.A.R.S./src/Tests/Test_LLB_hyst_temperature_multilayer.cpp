/* Test_LLB_hyst_temperature_multilayer.cpp
 *  Created on: 10 Dec 2019
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Test_LLB_hyst_temperature_multilayer.cpp
 * @brief Test simulation for temperature variable hysteresis with a multi-layered system. */

// System header files
#include <fstream>
#include <iostream>
#include <string>
#include "math.h"

#include "../../hdr/Classes/Solver.hpp"
// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"

int LLB_hyst_temperature_multilayer_test(){
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Material;
	Interaction_t Int_Mat;
	Grain_t Grain;
	Vec3 H_appl_unit;
	int H_step=100,integration_step=10000,equilibration_step=5000;
	double Phi_H=0.0, time=0.0, H_delta;
	std::vector<Vec3> Magnetisation;
	// Create structures to store layer magnetisation components and set them to zero
	std::vector<double> Magnetisation_layers_x;
	std::vector<double> Magnetisation_layers_y;
	std::vector<double> Magnetisation_layers_z;
	std::vector<double> Magnetisation_layers_l;
	std::vector<double> Easy_axis_layers_x;
	std::vector<double> Easy_axis_layers_y;
	std::vector<double> Easy_axis_layers_z;
	std::cout << "Performing LLB hysteresis temperature multilayer test... \n" << std::flush;

	const ConfigFile CONFIG_FILE("Tests/LLB_hysteresis_temperature_multilayer/test_parameters.cfg");
	std::vector<ConfigFile> Materials_Config;
	const std::string FILE_LOC="Tests/LLB_hysteresis_temperature_multilayer/";


	// Generate system parameters
	double Temperature=700.0;
	// Import structure parameters
	Structure_import(CONFIG_FILE, &Structure);
	// Import material parameters
	Materials_import_Test_layers(FILE_LOC, Structure.Num_layers, &Materials_Config, &Material);
	// Import time step and damping
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers, CONFIG_FILE, Materials_Config);
//	Magnetisation.resize(Voronoi_data.Num_Grains*Structure.Num_layers);
	Magnetisation_layers_x.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_y.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_z.resize(Structure.Num_layers,0.0);
	Magnetisation_layers_l.resize(Structure.Num_layers,0.0);
	Easy_axis_layers_x.resize(Structure.Num_layers,0.0);
	Easy_axis_layers_y.resize(Structure.Num_layers,0.0);
	Easy_axis_layers_z.resize(Structure.Num_layers,0.0);


	// Loop over grains to find largest anisotropy
	double H_K_max = 2.0*Grain.K[0]/Grain.Ms[0];
	double H_K_min = 2.0*Grain.K[0]/Grain.Ms[0];
	for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
		int Offset = Voronoi_data.Num_Grains*layer;
		for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
			const double H_K = 2.0*Grain.K[i+Offset]/Grain.Ms[i+Offset];
			if(H_K > H_K_max){
				H_K_max = H_K;
			}
			else if(H_K < H_K_min){
				H_K_min = H_K;
			}
		}
	}
	std::cout << "\n\t***\tMax Hk = " << H_K_max << std::endl;
	std::cout << "\n\t***\tMin Hk = " << H_K_min << std::endl;
	// Calculate max field applied based on maximum H_K
	double H_k_MAG = H_K_max*(1+0.5);
	double H_appl_MAG = 1.0*H_k_MAG;
	H_delta = (4.0*H_appl_MAG)/H_step;

	// TEMPERATURE LOOP
	for(int Temps=1;Temps<7;++Temps){
		if(Temps==1){Temperature=0;}
		else if(Temps==2){Temperature=100;}
		else if(Temps==3){Temperature=300;}
		else if(Temps==4){Temperature=500;}
		else if(Temps==5){Temperature=600;} // Lower temperatures require a lot of time and a high number of grains
		else if(Temps==6){Temperature=700;}

		std::string LLB_OUTPUT_name = "Tests/LLB_hysteresis_temperature_multilayer/Output/LLB"+std::to_string(Temperature)+"K.dat";
		std::ofstream LLB_OUTPUT(LLB_OUTPUT_name.c_str());
		//----------------------------- Header of output file ---------------------------//
		LLB_OUTPUT << "#";
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << "mx" << layer+1 << " my" << layer+1 << " mz" << layer+1 << " ";
		}
		LLB_OUTPUT << "mx my mz ";
		LLB_OUTPUT << "Hx Hy Hz H_MAG Hk ";
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << "ex" << layer+1 << " ey" << layer+1 << " ez" << layer+1 << " ";
		}
		for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
			LLB_OUTPUT << "m.H" << layer+1 << " ";
		}
		LLB_OUTPUT << "m.H time\n";
		//-------------------------------------------------------------------------------//

		for(double Theta_H = 90.0; Theta_H>-1.0; Theta_H-=15.0){
			H_appl_MAG = 1.0*H_k_MAG;
			std::cout << "\nSimulating at " << Theta_H << " Degrees and " << Temperature << " K" << std::endl;

			if(Theta_H == 0.0){Theta_H=0.1;}
			else if(Theta_H == 90.0){Theta_H=89.9;}

			int step_count=0, rep_count=0;
			H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
			H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
			H_appl_unit.z = cos(Theta_H*PI/180.0);
			for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
				int Offset = Voronoi_data.Num_Grains*layer;
				for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
					Grain.H_appl[grain+Offset].x=H_appl_unit.x*H_appl_MAG;
					Grain.H_appl[grain+Offset].y=H_appl_unit.y*H_appl_MAG;
					Grain.H_appl[grain+Offset].z=H_appl_unit.z*H_appl_MAG;
				}
			}
			// EQUILIBRATE SYSTEM AT MAX FIELD AND SIMULATION TEMPERATURE
			time=0.0;
			while(rep_count<equilibration_step){
				Solver.Integrate(&Voronoi_data, Structure.Num_layers, &Int_Mat, 0.0, 0.0, &Grain);
				time += Solver.get_dt();
				std::cout << "\t\t\t\tEquilibration repetition: " << rep_count << "\r" << std::flush;
				rep_count++;
			}
			std::cout << "\nEquilibration terminated. Starting hysteresis loop.\n" << std::endl;
			// BEGIN HYSTERESIS LOOP and reset time
			time=0.0;
			while(step_count<=H_step){
				rep_count = 0;
				double Magnetisation_total_x = 0.0;
				double Magnetisation_total_y = 0.0;
				double Magnetisation_total_z = 0.0;
				double Magnetisation_total_l = 0.0;
				while(rep_count<integration_step){
	                Solver.Integrate(&Voronoi_data, Structure.Num_layers, &Int_Mat, 0.0, 0.0, &Grain);
					time += Solver.get_dt();
					std::cout << "\t\t\t\tRepetition: " << rep_count << "\r" << std::flush;
					rep_count++;
				}
				// CALCULATE MAGNETISATION OF LAYERS
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					int Offset = Voronoi_data.Num_Grains*layer;
					// Dummy variables for calculating layer magnetisation
					double Magnetisation_layers_dummy_x = 0.0;
					double Magnetisation_layers_dummy_y = 0.0;
					double Magnetisation_layers_dummy_z = 0.0;
					double Magnetisation_layers_dummy_l = 0.0;
					double Easy_axis_layers_dummy_x     = 0.0;
					double Easy_axis_layers_dummy_y     = 0.0;
					double Easy_axis_layers_dummy_z     = 0.0;
					for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
						Magnetisation_total_x += Grain.m[i+Offset].x;
						Magnetisation_total_y += Grain.m[i+Offset].y;
						Magnetisation_total_z += Grain.m[i+Offset].z;
						Magnetisation_total_l += sqrt(Grain.m[i+Offset].x*Grain.m[i+Offset].x+Grain.m[i+Offset].y*Grain.m[i+Offset].y+Grain.m[i+Offset].z*Grain.m[i+Offset].z);
						Magnetisation_layers_dummy_x += Grain.m[i+Offset].x;
						Magnetisation_layers_dummy_y += Grain.m[i+Offset].y;
						Magnetisation_layers_dummy_z += Grain.m[i+Offset].z;
						Magnetisation_layers_dummy_l += sqrt(Grain.m[i+Offset].x*Grain.m[i+Offset].x+Grain.m[i+Offset].y*Grain.m[i+Offset].y+Grain.m[i+Offset].z*Grain.m[i+Offset].z);
						Easy_axis_layers_dummy_x     += Grain.Easy_axis[i+Offset].x;
						Easy_axis_layers_dummy_y     += Grain.Easy_axis[i+Offset].y;
						Easy_axis_layers_dummy_z     += Grain.Easy_axis[i+Offset].z;
					}
					Magnetisation_layers_x[layer] = Magnetisation_layers_dummy_x;
					Magnetisation_layers_y[layer] = Magnetisation_layers_dummy_y;
					Magnetisation_layers_z[layer] = Magnetisation_layers_dummy_z;
					Magnetisation_layers_l[layer] = Magnetisation_layers_dummy_l;
					Easy_axis_layers_x[layer]     = Easy_axis_layers_dummy_x;
					Easy_axis_layers_y[layer]     = Easy_axis_layers_dummy_y;
					Easy_axis_layers_z[layer]     = Easy_axis_layers_dummy_z;
				}
				// Normalise layers magnetisation
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					Magnetisation_layers_x[layer] /= double(Voronoi_data.Num_Grains);
					Magnetisation_layers_y[layer] /= double(Voronoi_data.Num_Grains);
					Magnetisation_layers_z[layer] /= double(Voronoi_data.Num_Grains);
					Magnetisation_layers_l[layer] /= double(Voronoi_data.Num_Grains);
					Easy_axis_layers_x[layer]     /= double(Voronoi_data.Num_Grains);
					Easy_axis_layers_y[layer]     /= double(Voronoi_data.Num_Grains);
					Easy_axis_layers_z[layer]     /= double(Voronoi_data.Num_Grains);
				}
				Magnetisation_total_x /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
				Magnetisation_total_y /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
				Magnetisation_total_z /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
				Magnetisation_total_l /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
				// OUTPUT TO FILE
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					LLB_OUTPUT << Magnetisation_layers_x[layer] << " " << Magnetisation_layers_y[layer] << " " << Magnetisation_layers_z[layer] << " ";
				}
				LLB_OUTPUT << Magnetisation_total_x << " " << Magnetisation_total_y << " " << Magnetisation_total_z << " ";
				LLB_OUTPUT << H_appl_unit << " " << H_appl_MAG << " " << /*H_k_MAG*/H_K_max << " ";
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					LLB_OUTPUT << Easy_axis_layers_x[layer] << " " << Easy_axis_layers_y[layer] << " " << Easy_axis_layers_z[layer] << " ";
				}
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					LLB_OUTPUT << Magnetisation_layers_x[layer]*H_appl_unit.x + Magnetisation_layers_y[layer]*H_appl_unit.y + Magnetisation_layers_z[layer]*H_appl_unit.z << " ";
				}
				LLB_OUTPUT << Magnetisation_total_x*H_appl_unit.x+Magnetisation_total_y*H_appl_unit.y+Magnetisation_total_z*H_appl_unit.z << " "
						   << time << "\n";
				LLB_OUTPUT << std::flush;
				// UPDATE FIELD VALUE
				if(step_count < H_step/2.0){H_appl_MAG -= H_delta;}
				else{H_appl_MAG += H_delta;}
				for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
					int Offset = Voronoi_data.Num_Grains*layer;
					for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
						Grain.H_appl[grain+Offset].x=H_appl_unit.x*H_appl_MAG;
						Grain.H_appl[grain+Offset].y=H_appl_unit.y*H_appl_MAG;
						Grain.H_appl[grain+Offset].z=H_appl_unit.z*H_appl_MAG;
					}
				}
				std::cout << "\tStep: " << step_count << " of " << H_step << "\r" << std::flush;
				++step_count;
			}
			if(Theta_H == 0.1){Theta_H=0.0;}
			else if(Theta_H == 89.9){Theta_H=90.0;}

			LLB_OUTPUT << "\n\n";
			Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,&Grain);
		}
		LLB_OUTPUT.close();
	}

	return 0;
}
