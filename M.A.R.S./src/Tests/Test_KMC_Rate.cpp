/*
 * Test_KMC_Rate.cpp
 *
 *  Created on: 27 Nov 2018
 *      Author: Ewan Rannala
 */

/** \file Test_KMC_Rate.cpp
 * \brief Test simulation for kMC solver. Determines coercivity as a function of sweep rate. */

#include <iostream>
#include <fstream>
#include "math.h"

#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Globals.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
/** Returns sign of argument. If argument is zero, then zero is returned. */

template <typename T>
int sign(T Val){
    if(Val < 0){return -1;}
    else if(Val > 0){return 1;}
    else{return 0;}
}
/** Returns boolean if sign of the arguments is different. */
template <typename Q>
bool Sign_change(Q Val1, Q Val2){
    if(sign(Val1)!=0 || sign(Val2)!=0){
        if(sign(Val1)*sign(Val2)==-1){return true;}
        else{return false;}
    }
    else{return false;}
}
#endif
/** This function tests the kMC solver. The coercivity is dependent on the sweep rate of the applied
 * field. This test obtains the coercivity as a function of sweep rate for comparison with the
 * theoretical relationship derived by Chantrell in 1988 under the assumption of constant attempt
 * frequency and an easy axis parallel to the applied field.
 * <BR>
 * (See R W Chantrell and K O’grady. Time dependence and rate dependence of the coercivity of particulate
 * recording media Related content Magnetic characterization of recording media.
 * J. Phys. D: Appl. Phys, 21:1469, 1988)
 */
int KMC_Rate_test(){
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Material;
    Interaction_t Int_Mat;
    Grain_t Grain;
    std::vector<ConfigFile> Materials_Config;
    int Inter_steps = 100;
    double Phi_H=0.0, Theta_H=1e-3, time=0.0, H_delta = 50, Avg_K=0.0, Avg_Ms=0.0, Avg_Vol=0.0, mDOTh_A=0.0, mDOTh_B=0.0;
    Vec3 H_appl_unit, Avg_m, Avg_e;
    std::ofstream KMC_Output("Tests/KMC_Rate/Output/KMC_Hyst.dat");
    std::ofstream KMC_Hc_Output("Tests/KMC_Rate/Output/KMC_Hc.dat");

    KMC_Output << "mx my mz Hx Hy Hz Ex Ey Ez H_MAG Hk m.H Temp Rate time\n";
    KMC_Hc_Output << "Rate Temp Hc_left Hc_right error\n";

    std::cout << "Performing Hyst(R,T) test for KMC..." << std::endl;
    const ConfigFile TEST_CFG_FILE("Tests/KMC_Rate/test_parameters.cfg");
    const std::string MAT_FILE_LOC="Tests/KMC_Rate";
    Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers, &Materials_Config, &Material);
    Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,&Grain);
    Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);
    Solver_t Solver(TEST_CFG_FILE);
    // This test is only designed for the kMC
    Solver.Force_specific_solver_construction(Voronoi_data.Num_Grains,Structure.Num_layers, TEST_CFG_FILE, Materials_Config, Solvers::kMC);
    Solver.set_Solver(Solvers::kMC);
    // Save initial mag for backup
    std::vector<Vec3> MAG_BACK(Voronoi_data.Num_Grains);
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
        MAG_BACK[grain].x = Grain.m[grain].x;
        MAG_BACK[grain].y = Grain.m[grain].y;
        MAG_BACK[grain].z = Grain.m[grain].z;
    }

    H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
    H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
    H_appl_unit.z = cos(Theta_H*PI/180.0);

    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
        Avg_K  += Grain.K[grain];
        Avg_Ms += Grain.Ms[grain];
        Avg_Vol += Grain.Vol[grain];  // Volume is in nm^3
    }
    Avg_K /= Voronoi_data.Num_Grains;
    Avg_Ms /= Voronoi_data.Num_Grains;
    Avg_Vol /= Voronoi_data.Num_Grains;

    //double H_k_MAG = ((2.0*Grain.K[0])/Grain.Ms[0]);
    double H_k_MAG = (2.0*Avg_K)/Avg_Ms;
    double H_appl_MAG = 1.5*H_k_MAG;
    std::cout << "\nVol: " << Avg_Vol << " nm^3\nK: " << Avg_K << " erg/cc\nMs: " << Avg_Ms << " emu/cc\nHk: " << H_k_MAG << " Oe\nHappl: " << H_appl_MAG << " Oe" << std::endl;


    for(double Temp=100;Temp<800;Temp+=100){
        H_appl_MAG = 1.5*H_k_MAG;
        std::cout << "Temperature = " << Temp
                  << "\tKV/kT = " << (Avg_K*Avg_Vol*1e-21)/(Temp*KB) << std::endl;
            for(double Rate=1;Rate<1e+15;Rate*=10){
                std::cout << "Rate = " << Rate << std::endl;
                Solver.set_dt(H_delta/(Rate*Inter_steps));
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                Grain.Temp[grain]=Temp;
                Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
                Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
                Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
            }

            // INITIAL OUTPUT TO FILE
            Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
                Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
                Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
                Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
                Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
                Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
            }
            KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
                       << H_appl_MAG << " " << H_k_MAG << " "
                       << Avg_m*H_appl_unit << " " << Temp << " "
                       << Rate << " " << time << "\n" << std::flush;

            while(H_appl_MAG>=(-1.5*H_k_MAG)){    // First half of loop
                H_appl_MAG-=(H_delta/Inter_steps);
                std::cout << H_appl_MAG << "\r";
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
                    Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
                    Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
                }
                Solver.Integrate(&Voronoi_data, Structure.Num_layers, &Int_Mat, 0.0, 0.0, &Grain);
                time += Solver.get_dt();

                // Average over the entire system then write out
                Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
                    Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
                    Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
                    Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
                    Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
                    Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
                }
                mDOTh_A = Avg_m*H_appl_unit;
                // OUTPUT TO FILE
                KMC_Output << Avg_m << " " << H_appl_unit << " "<< Avg_e << " "
                           << H_appl_MAG << " " << H_k_MAG << " " << mDOTh_A << " "
                           << Temp << " " << Rate << " " << time << "\n" << std::flush;
                if(Sign_change(mDOTh_A, mDOTh_B)){
                    KMC_Hc_Output << Rate << " " << Temp << " " << H_appl_MAG << " ";
                }
                mDOTh_B = mDOTh_A;
            }
            while(H_appl_MAG<=(1.5*H_k_MAG)){    // Second half of loop
                std::cout << H_appl_MAG << "\r";
                H_appl_MAG+=(H_delta/Inter_steps);
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Grain.H_appl[grain].x = H_appl_unit.x*H_appl_MAG;
                    Grain.H_appl[grain].y = H_appl_unit.y*H_appl_MAG;
                    Grain.H_appl[grain].z = H_appl_unit.z*H_appl_MAG;
                }

                Solver.Integrate(&Voronoi_data, Structure.Num_layers, &Int_Mat, 0.0, 0.0, &Grain);
                time += Solver.get_dt();
                // Average over the entire system then write out
                Avg_m.x=Avg_m.y=Avg_m.z=Avg_e.x=Avg_e.y=Avg_e.z=0.0;
                for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                    Avg_m.x += Grain.m[grain].x/Voronoi_data.Num_Grains;
                    Avg_m.y += Grain.m[grain].y/Voronoi_data.Num_Grains;
                    Avg_m.z += Grain.m[grain].z/Voronoi_data.Num_Grains;
                    Avg_e.x += Grain.Easy_axis[grain].x/Voronoi_data.Num_Grains;
                    Avg_e.y += Grain.Easy_axis[grain].y/Voronoi_data.Num_Grains;
                    Avg_e.z += Grain.Easy_axis[grain].z/Voronoi_data.Num_Grains;
                }
                mDOTh_A = Avg_m*H_appl_unit;
                // OUTPUT TO FILE
                KMC_Output << Avg_m << " " << H_appl_unit << " " << Avg_e << " "
                                   << H_appl_MAG << " " << H_k_MAG << " "
                                   << Avg_m*H_appl_unit << " " << Temp << " "
                                   << Rate << " " << time << "\n" << std::flush;
                if(Sign_change(mDOTh_A, mDOTh_B)){
                    KMC_Hc_Output << H_appl_MAG << " " << (H_delta/Inter_steps) << std::endl;
                }
                mDOTh_B = mDOTh_A;
            }
            time = 0.0;
            KMC_Output << "\n\n";
            Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temp,&Grain);
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                Grain.m[grain].x = MAG_BACK[grain].x;
                Grain.m[grain].y = MAG_BACK[grain].y;
                Grain.m[grain].z = MAG_BACK[grain].z;
        }    }
        KMC_Hc_Output << "\n\n";
    }
    KMC_Output.close();
    KMC_Hc_Output.close();
    return 0;
}
