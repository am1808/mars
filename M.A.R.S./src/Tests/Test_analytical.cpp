/* LLB_analytical.cpp
 *  Created on: 22 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Test_analytical.cpp
 * \brief Test simulation for LLB and LLG solvers. Compares LLB/LLG output with an analytical solution. */

// System header files
#include <iostream>
#include <fstream>
#include "math.h"

#include "../../hdr/Classes/Solver.hpp"
//M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Materials_import_Test_LLB.hpp"

/** This function performs an analytical test for the LLB/LLG solver. <BR>
 * <P>
 *  Test to verify the correct implementation of the Heun scheme for the LLB/LLG solver.
 *  The test simulates the damped precessional motion of grains whose
 *  magnetisation is initialised along a direction transverse to the
 *  easy-axis and subjected to an external field along the easy-axis.
 *  The test is performed at 0K and the components of the magnetisation
 *  are tested against the LLB analytical solution:
 *  \f[
 *     \begin{aligned}
 *       M_x(t) &= \text{sech}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg) \cos(\gamma \alpha_{\perp} Ht) \\
 *       M_y(t) &= -\text{sech}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg) \sin(\gamma \alpha_{\perp} Ht) \\
 *       M_z(t) &= \text{tanh}\bigg(\frac{\gamma \alpha_{\perp} Ht}{m^2}\bigg).
 *     \end{aligned}
 *  \f]
 *  Or the LLG analytical solution:
 *  \f[
 *     \begin{aligned}
 *       M_x(t) &= \frac{1.0}{\text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg)} \cos(\frac{\gamma Ht}{1+\alpha^2}t) \\
 *       M_y(t) &= \frac{1.0}{\text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg)} \sin(\frac{\gamma Ht}{1+\alpha^2}t) \\
 *       M_z(t) &= \text{cosh}\bigg(\frac{\alpha \gamma H}{1+\alpha^2}t \bigg).
 *     \end{aligned}
 *  \f]
 *  This test requires the Callen-Callen description of the anisotropy, not the version proposed by Garanin.
 *
 */
int analytic_test(){

	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::fstream Output;

	// Read configuration file and setup parameters
	const ConfigFile TEST_CFG_FILE("Tests/Analytical/test_parameters.cfg");
	Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_LLB(TEST_CFG_FILE, Structure.Num_layers, &Materials_Config, &Material);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,&Grain);
	Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,TEST_CFG_FILE,Materials_Config);
	int steps = static_cast<int>(2e-9/Solver.get_dt());
	std::cout << steps << std::endl;
	std::cout << "Performing analytical test using the ";

	// Generate output files
	switch(Solver.get_Solver())
	{
	case Solvers::kMC:
		std::cout << "kMC solver.\n Skipping test." << std::endl;
		return 1;
	case Solvers::LLB:
		std::cout << "LLB solver." << std::endl;
		Output.open ("Tests/Analytical/Output/LLB.dat", std::fstream::out);
		break;
	case Solvers::LLG:
		std::cout << "LLG solver." << std::endl;
		Output.open ("Tests/Analytical/Output/LLG.dat", std::fstream::out);
		break;
	}
	Output << "Ax Ay Az x y z Ex Ey Ez t" << std::endl;

	// Set easy axis to zero. Set applied field.
	double Field_Mag=400.0*PI;
	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
		Grain.Easy_axis[grain]=0.0;
		Grain.H_appl[grain].x=0.0;
		Grain.H_appl[grain].y=0.0;
		Grain.H_appl[grain].z=Field_Mag;
	}
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);

	std::cout << "Performing integration steps..." << std::endl;
	double Ax, Ay, Az;
	for(int step=0; step<steps; ++step){
		std::cout << "Step: " << step << " of " << steps << "\r";
		Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_Mat,0.0,0.0,&Grain);
		switch(Solver.get_Solver())
		{
		case Solvers::LLB:
			Ax = cos(Solver.get_Gamma()*Field_Mag*step*Solver.get_dt()) / cosh(Solver.get_Gamma()*Solver.get_Alpha()[0]*Field_Mag*step*Solver.get_dt());
			Ay = sin(Solver.get_Gamma()*Field_Mag*step*Solver.get_dt()) / cosh(Solver.get_Gamma()*Solver.get_Alpha()[0]*Field_Mag*step*Solver.get_dt());
			Az = tanh( (Solver.get_Gamma()*Solver.get_Alpha()[0]*Field_Mag*step*Solver.get_dt()));
			break;
		case Solvers::LLG:
			Ax = 1.0/(cosh((Solver.get_Alpha()[0]*Field_Mag*Solver.get_Gamma()*step*Solver.get_dt())/(1+Solver.get_Alpha()[0]*Solver.get_Alpha()[0])))*cos((Solver.get_Gamma()*Field_Mag*step*Solver.get_dt())/(1+Solver.get_Alpha()[0]*Solver.get_Alpha()[0]));
			Ay = 1.0/(cosh((Solver.get_Alpha()[0]*Field_Mag*Solver.get_Gamma()*step*Solver.get_dt())/(1+Solver.get_Alpha()[0]*Solver.get_Alpha()[0])))*sin((Solver.get_Gamma()*Field_Mag*step*Solver.get_dt())/(1+Solver.get_Alpha()[0]*Solver.get_Alpha()[0]));
			Az = tanh((Solver.get_Alpha()[0]*Solver.get_Gamma()*Field_Mag*step*Solver.get_dt())/(1+Solver.get_Alpha()[0]*Solver.get_Alpha()[0]));
			break;
		default:
			break;
		}
		Output << Ax << " " << Ay << " " << Az << " " << Grain.m[0] << " "
			   << fabs(Ax-Grain.m[0].x) << " " << fabs(Ay-Grain.m[0].y) << " " << fabs(Az-Grain.m[0].z) << " "
			   << step*Solver.get_dt() << "\n";
	}
	Output.flush();Output.close();
	return 0;
}
