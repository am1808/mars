/* Test_LLB_transverse_relaxation.cpp
 *
 *  Created on: 11 Jun 2019
 *      Author: Ewan Rannala, Andrea Meo
 */

/** \file Test_LLB_transverse_relaxation.cpp
 * \brief Test simulation for LLB. Determines transverse relaxation for comparison with atomistic results. */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"

#include "../../hdr/Classes/Solver.hpp"
// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Materials_import_Test_LLB.hpp"


int LLB_trans(){

	Structure_t Structure;
	Material_t Material;
	Voronoi_t Voronoi_data;
	Interaction_t Int_Mat;
	Grain_t Grain;
	const ConfigFile CONFIG_FILE("Tests/LLB_Transverse/test_parameters.cfg");
	std::vector<ConfigFile> Materials_Config;

	std::cout << "Performing LLB transverse relaxation... \n" << std::flush;

// Generate system parameters
	// Import structure parameters
	Structure_import(CONFIG_FILE, &Structure);
	// overwrite number of layers to be 1
	Structure.Num_layers = 1;
	// Import material parameters
	Materials_import_Test_LLB(CONFIG_FILE, Structure.Num_layers, &Materials_Config, &Material);
	// Import time step and damping
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,CONFIG_FILE,Materials_Config);
	// This test is only designed for the LLB

	for(int Temps=1;Temps<=4;++Temps){
		double Temperature, time=0.0, Mx_AVG=0.0, My_AVG=0.0, Mz_AVG=0.0,Length_AVG=0.0;

		if(Temps==1){Temperature=1.0;}
		else if(Temps==2){Temperature=300.0;}
		else if(Temps==3){Temperature=500.0;}
		else if(Temps==4){Temperature=600.0;}

		std::cout << "Running for " << Temperature << " K..." << std::endl;

		std::ofstream LLB_OUTPUT("Tests/LLB_Transverse/Output/LLB"+std::to_string(Temperature)+"K.dat");

		Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Temperature,&Grain);
		Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);

		// No applied field
		for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
			Grain.H_appl[grain].x=0.0;
			Grain.H_appl[grain].y=0.0;
			Grain.H_appl[grain].z=0.0;
		}
		// Reset time
		time=0.0;
		while(time<=25.0e-12){
			// Perform relaxation
			int num_grains=Voronoi_data.Num_Grains;
			Mx_AVG=My_AVG=Mz_AVG=Length_AVG=0.0;
			for(int grain=0;grain<num_grains;++grain){
				double Length = sqrt(Grain.m[grain].x*Grain.m[grain].x+Grain.m[grain].y*Grain.m[grain].y+Grain.m[grain].z*Grain.m[grain].z);
				Length_AVG += Length;
				Mx_AVG += Grain.m[grain].x/Length;
				My_AVG += Grain.m[grain].y/Length;
				Mz_AVG += Grain.m[grain].z/Length;
			}
			Mx_AVG /= num_grains;
			My_AVG /= num_grains;
			Mz_AVG /= num_grains;
			Length_AVG /= num_grains;

			LLB_OUTPUT << time << " " << Mx_AVG << " " << My_AVG << " " << Mz_AVG << " " << Length_AVG << std::endl;

			Solver.Integrate(&Voronoi_data, Structure.Num_layers, &Int_Mat, 0.0, 0.0, &Grain);
			time += Solver.get_dt();
		}
	}
	return 0;
}


