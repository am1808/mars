/* Test_hyst.cpp
 *  Created on: 10 Dec 2019
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Test_hyst.cpp
 * @brief Test simulation for zero Kelvin hysteresis. */

// System header files
#include <fstream>
#include <iostream>
#include "math.h"

#include "../../hdr/Classes/Solver.hpp"
// M.A.R.S. specific header files
#include "../../hdr/Structures.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Importers/Materials_import_Test_layers.hpp"

int Test_hyst(){
    Structure_t Structure;
    Material_t Material;
    Voronoi_t Voronoi_data;
    Interaction_t Int_Mat;
    Grain_t Grain;
    std::vector<ConfigFile> Materials_Config;
    std::fstream OUTPUT;
    double m_thresh=1.0e-15;

    const ConfigFile TEST_CFG_FILE("Tests/Hysteresis/test_parameters.cfg");
    const std::string MAT_FILE_LOC="Tests/Hysteresis";
    Structure_import(TEST_CFG_FILE, &Structure);
    Materials_import_Test_layers(MAT_FILE_LOC,Structure.Num_layers, &Materials_Config, &Material);
    Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,&Grain);
    Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Material,Voronoi_data,&Int_Mat);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,TEST_CFG_FILE,Materials_Config);
    std::cout << "Performing hysteresis test using the ";

    // Set up output file
    switch(Solver.get_Solver())
    {
    case Solvers::kMC:
        std::cout << "kMC solver." << std::endl;
        OUTPUT.open ("Tests/Hysteresis/Output/kMC.dat", std::fstream::out);
        break;
    case Solvers::LLG:
        std::cout << "LLG solver." << std::endl;
        OUTPUT.open ("Tests/Hysteresis/Output/LLG.dat", std::fstream::out);
        break;
    case Solvers::LLB:
        std::cout << "LLB solver." << std::endl;
        OUTPUT.open ("Tests/Hysteresis/Output/LLB.dat", std::fstream::out);
        break;
    }
    // Write column headings
    OUTPUT << "#";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "mx" << layer+1 << " my" << layer+1 << " mz" << layer+1 << " ";
    }
    OUTPUT << "mx my mz Hx Hy Hz H_MAG ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "Hk" << layer+1 << " ";
    }
    OUTPUT << "Hk ";
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "ex" << layer+1 << " ey" << layer+1 << " ez" << layer+1 << " ";
    }
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        OUTPUT << "m.H" << layer+1 << " ";
    }
    OUTPUT << "m.H time\n";

    Vec3 H_appl_unit;
    int H_step=100;
    double Phi_H=0.0, time=0.0, H_delta;
    std::vector<Vec3> Magnetisation(Voronoi_data.Num_Grains*Structure.Num_layers);
    // Create structures to store layer magnetisation components and set them to zero
    std::vector<Vec3> Magnetisation_layers(Structure.Num_layers);
    std::vector<double> Magnetisation_layers_l(Structure.Num_layers,0.0);
    std::vector<Vec3> Easy_axis_layers(Structure.Num_layers);
    Vec3 Magnetisation_total_old;
    double Magnetisation_total_old_l = 0.0;

    // Loop over grains to find largest anisotropy
    double H_K_max = 2.0*Grain.K[0]/Grain.Ms[0];
    double H_K_min = 2.0*Grain.K[0]/Grain.Ms[0];
    for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
        int Offset = Voronoi_data.Num_Grains*layer;
        for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
            const double H_K = 2.0*Grain.K[i+Offset]/Grain.Ms[i+Offset];
            if(H_K > H_K_max){
                H_K_max = H_K;
            }
            else if(H_K < H_K_min){
                H_K_min = H_K;
            }
        }
    }
    std::cout << "\n\t***\tMax Hk = " << H_K_max << std::endl;
    std::cout << "\n\t***\tMin Hk = " << H_K_min << std::endl;
    // Calculate max field applied based on maximum H_K
    double H_k_MAG = H_K_max*(1+0.5);
    double H_appl_MAG = 1.0*H_k_MAG;
    H_delta = (4.0*H_appl_MAG)/H_step;

    for(double Theta_H = 90.0; Theta_H>-1.0; Theta_H-=15.0){
        H_appl_MAG = 1.0*H_k_MAG;
        std::cout << "Simulating at " << Theta_H << " Degrees" << std::endl;

        if(Theta_H == 0.0){Theta_H=0.1;}
        else if(Theta_H == 90.0){Theta_H=89.9;}

        int step_count=0, rep_count=0;
        H_appl_unit.x = sin(Theta_H*PI/180.0)*cos(Phi_H*PI/180.0);
        H_appl_unit.y = sin(Theta_H*PI/180.0)*sin(Phi_H*PI/180.0);
        H_appl_unit.z = cos(Theta_H*PI/180.0);
        for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
            int Offset = Voronoi_data.Num_Grains*layer;
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                Grain.H_appl[grain+Offset]=H_appl_unit*H_appl_MAG;
            }
        }
        Magnetisation_total_old   = 0.0;
        Magnetisation_total_old_l = 0.0;
        for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
            int Offset = Voronoi_data.Num_Grains*layer;
            for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                Magnetisation[i+Offset] = Grain.m[i+Offset];
                Magnetisation_total_old   += Grain.m[i+Offset];
                Magnetisation_total_old_l += sqrt(Grain.m[i+Offset]*Grain.m[i+Offset]);
            }
        }
        Magnetisation_total_old   /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);
        Magnetisation_total_old_l /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);
        std::cout << "\n\tStep: " << "0" << " of " << H_step << "\r" << std::flush;
        while(step_count<=H_step){
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_Mat,0.0,0.0,&Grain);
            time += Solver.get_dt();

            Vec3 Magnetisation_total;
            double Magnetisation_total_l = 0.0;
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                int Offset = Voronoi_data.Num_Grains*layer;
                // Dummy variables for calculating layer magnetisation
                Vec3 Magnetisation_layers_dummy;
                double Magnetisation_layers_dummy_l = 0.0;
                Vec3 Easy_axis_layers_dummy;
                for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                    Magnetisation_total   += Grain.m[i+Offset];
                    Magnetisation_total_l += sqrt(Grain.m[i+Offset]*Grain.m[i+Offset]);
                    Magnetisation_layers_dummy   += Grain.m[i+Offset];
                    Magnetisation_layers_dummy_l += sqrt(Grain.m[i+Offset]*Grain.m[i+Offset]);
                    Easy_axis_layers_dummy       += Grain.Easy_axis[i+Offset];
                }
                Magnetisation_layers[layer]   = Magnetisation_layers_dummy;
                Magnetisation_layers_l[layer] = Magnetisation_layers_dummy_l;
                Easy_axis_layers[layer]       = Easy_axis_layers_dummy;
            }
            // Normalise layers magnetisation
            for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                Magnetisation_layers[layer]   /= static_cast<double>(Voronoi_data.Num_Grains);
                Magnetisation_layers_l[layer] /= static_cast<double>(Voronoi_data.Num_Grains);
                Easy_axis_layers[layer]       /= static_cast<double>(Voronoi_data.Num_Grains);
            }
            Magnetisation_total   /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);
            Magnetisation_total_l /= static_cast<double>(Voronoi_data.Num_Grains*Structure.Num_layers);

            if((fabs(Magnetisation_total.x-Magnetisation_total_old.x) < m_thresh &&
               fabs(Magnetisation_total.y-Magnetisation_total_old.y) < m_thresh &&
               fabs(Magnetisation_total.z-Magnetisation_total_old.z) < m_thresh)){
                // OUTPUT TO FILE
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    OUTPUT << Magnetisation_layers[layer] << " ";
                }
                OUTPUT << Magnetisation_total << " "
                       << H_appl_unit << " " << H_appl_MAG << " ";
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    OUTPUT << 2.0*Grain.K[layer*Voronoi_data.Num_Grains]/Grain.Ms[layer*Voronoi_data.Num_Grains] << " ";
                }
                OUTPUT << H_K_max << " ";
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    OUTPUT << Easy_axis_layers[layer] << " ";
                }
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    OUTPUT << Magnetisation_layers[layer]*H_appl_unit << " ";
                }
                OUTPUT << Magnetisation_total*H_appl_unit << " "
                           << time << "\n";
                OUTPUT << std::flush;
                if(step_count < H_step/2.0){H_appl_MAG -= H_delta;}
                else{H_appl_MAG += H_delta;}
                ++step_count;
                rep_count=0;
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    int Offset = Voronoi_data.Num_Grains*layer;
                    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                        Grain.H_appl[grain+Offset].x=H_appl_unit.x*H_appl_MAG;
                        Grain.H_appl[grain+Offset].y=H_appl_unit.y*H_appl_MAG;
                        Grain.H_appl[grain+Offset].z=H_appl_unit.z*H_appl_MAG;
                    }
                }
                Magnetisation_total_old   = 0.0;
                Magnetisation_total_old_l = 0.0;
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    int Offset = Voronoi_data.Num_Grains*layer;
                    for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                        Magnetisation[i+Offset] = Grain.m[i+Offset];
                        Magnetisation_total_old   += Grain.m[i+Offset];
                        Magnetisation_total_old_l += sqrt(Grain.m[i+Offset]*Grain.m[i+Offset]);
                    }
                }
                Magnetisation_total_old   /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
                Magnetisation_total_old_l /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
                std::cout << "\tStep: " << step_count << " of " << H_step << "\r" << std::flush;
            }
            else{
                Magnetisation_total_old   = 0.0;
                Magnetisation_total_old_l = 0.0;
                for(unsigned int layer=0; layer<Structure.Num_layers; ++layer){
                    int Offset = Voronoi_data.Num_Grains*layer;
                    for(unsigned int i=0;i<Voronoi_data.Num_Grains;++i){
                        Magnetisation[i+Offset] = Grain.m[i+Offset];
                        Magnetisation_total_old   += Grain.m[i+Offset];
                        Magnetisation_total_old_l += sqrt(Grain.m[i+Offset]*Grain.m[i+Offset]);
                    }
                }
                Magnetisation_total_old   /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
                Magnetisation_total_old_l /= double(Voronoi_data.Num_Grains*Structure.Num_layers);
                std::cout << "\t\t\t\tRepetition: " << rep_count << "\t\r" << std::flush;
                ++rep_count;
                if(rep_count >= 1500000000){
                    std::cout << "Too many repetitions" << std::endl;
                    OUTPUT.close();
                    return 1;
                }
            }
        }
        if(Theta_H == 0.1){Theta_H=0.0;}
        else if(Theta_H == 89.9){Theta_H=90.0;}

        OUTPUT << "\n\n";
        Grain_setup(Voronoi_data.Num_Grains, Structure.Num_layers,Material,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,0.0,&Grain);
    }
    OUTPUT.close();

    return 0;
}
