/* ConfigFile_import.cpp
 *  Created on: 27 Jul 2018
 *      Author: Samuel Ewan Rannala
 */

/** \file ConfigFile_import.cpp
 * \brief Function implementations for the ConfigFile class. */

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
/** Removes the remaining line when ';' or '#' is encountered.
 *
 * @param[out] line Line of configuration file.
 */
void ConfigFile::removeComment(std::string &line) const{
	// If ; or # is present remove ; or # and all characters following.
	if(line.find('#') != line.npos){
		line.erase(line.find('#'));
	}
	if(line.find(';') != line.npos){
		line.erase(line.find(';'));
	}
}
/** Set all characters to lower case in line.
 *
 * @param[out] line Line of configuration file.
 */
void ConfigFile::lowerCase(std::string &line) const{
	transform(line.begin(),line.end(),line.begin(),::tolower);
}
/** Set characters to lower case except those surrounded by quotes.
 *
 * @param[out] line Line of configuration file.
 * @param[in] lineNum Line number
 */
void ConfigFile::KeepCase(std::string &line, size_t const lineNum) const{
	// Remove case for everything upto and including =
	std::string::iterator it=line.begin();
	it += line.find('"');
	transform(line.begin(),it,line.begin(),::tolower);
	// Remove Quotes
	line.erase(line.find('"'),1);
	if(line.find('"') == line.npos){
		throw std::runtime_error(BoldRedFont + "CFG error: Bad Case sensitive input, missing '\"' on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	line.erase(line.find('"'),1);
}
/** Return boolean indicating the line contains just whitespace.
 *
 * @param[in] line Line of configuration file.
 */
bool ConfigFile::onlyWhitespace(const std::string &line) const{
	// True if only whitespace is present.
	return (line.find_first_not_of(' ') == line.npos);
}
/** Remove braces used to specify vector and also replace comma separators with whitespace.
 *
 * @param[out] line Line of configuration file.
 * @param[in] lineNum Line number.
 */
void ConfigFile::FormatVector(std::string &line, size_t const lineNum) const{
	// Remove braces and replace commas with whitespace.
	line.erase(line.find('{'),1);
	if(line.find('}') == line.npos){
		throw std::runtime_error(BoldRedFont + "CFG error: Bad vector input, missing '}' on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	line.erase(line.find('}'),1);
	std::replace(line.begin(),line.end(), ',' , ' ');
}
/** Return boolean indicating if line is valid or not.
 *
 * @param[in] line Line of configuration file.
 */
bool ConfigFile::validLine(const std::string &line) const{
	std::string temp = line;
	temp.erase(0, temp.find_first_not_of('\t'));
	if(temp[0]=='='){return false;} // Invalid line if no key is present.
	for(size_t i=temp.find('=')+1;i<temp.length();++i){
		if(temp[i] != ' ' && temp[i] != '\t'){return true;} // Valid line of key and value are found.
	}
	return false;
}
/** Extract the keys from configuration file for use in map.
 *
 * @param[out] key Configuration file key
 * @param[in] separator_pos Position of key and value separator
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractKey(std::string &key, size_t const &separator_pos, const std::string &line) const{
	// Set key as substring before '=' and remove any white space.
	key=line.substr(0,separator_pos);
	if(key.find("/t")!=line.npos || key.find(" ")!=line.npos){
		key.erase(key.find_first_of("\t "));
	}
}
/** Extract the values from configuration file for use in map.
 *
 * @param[out] value Configuration file data
 * @param[in] separator_pos Position of key and value separator
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractValue(std::string &value, size_t const &separator_pos, const std::string &line) const{
	// Set value as substring after '=' and remove any whitespace.
	value = line.substr(separator_pos+1);
	value.erase(0,value.find_first_not_of("\t "));
	value.erase(value.find_last_not_of("\t ")+1);
}
/** Extract contents of a line in the configuration file.
 * Check and ensure only unique keys are present and insert a key-value pair for that line.
 *
 * @param[in] line Line of configuration file.
 */
void ConfigFile::extractContents(const std::string &line){
	std::string temp=line;
	temp.erase(0, temp.find_first_not_of("\t "));
	size_t separator_pos = temp.find("=");
	std::string key, value;
	extractKey(key, separator_pos, temp);
	extractValue(value, separator_pos, temp);
	if(!keyExists(key)){
		contents.insert(std::pair<std::string, std::string>(key, value));
	}
	else{throw std::runtime_error(BoldRedFont + "CFG error: Key name conflict, all keys must be unique. -> " + key + " in " + FileName + ResetFont + "\n");}
}
/** Parse line searching for expected formatting, then extract the contents of the line.
 *
 * @param line Line of configuration file.
 * @param lineNum Line number.
 */
void ConfigFile::parseLine(const std::string &line, size_t const lineNum){
	if (line.find("=") == line.npos){
		throw std::runtime_error(BoldRedFont + "CFG error: Couldn't find separator on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	if (!validLine(line)){
		throw std::runtime_error(BoldRedFont + "CFG error: Bad format on line " + std::to_string(lineNum) + " in " + FileName + ResetFont + "\n");
	}
	extractContents(line);
}
/** Extract contents of the configuration file and generate the key-value container. Once this is performed
 * the configuration file is no longer used for I/O. */
void ConfigFile::ExtractKeys(){
	std::ifstream File(FileName.c_str());
	if(!File){throw std::runtime_error(BoldRedFont + "CFG error: " + FileName + " not found"  + ResetFont + "\n");}
	std::string line;
	size_t lineNum=0;
	while(std::getline(File,line)){
		lineNum++;
		std::string temp=line;
		if(temp.empty()){continue;}
		removeComment(temp);
		if(onlyWhitespace(temp)){continue;}
		if(temp.find("{")!=temp.npos){FormatVector(temp,lineNum);}
		if(temp.find('"')!=temp.npos){KeepCase(temp,lineNum);}
		else{lowerCase(temp);}
		parseLine(temp,lineNum);
		}
	File.close();
}

/** This function is called during construction. The routine opens the configuration file
 * and proceeds to remove comments and blank lines, and then parses the line. All
 * parameters from the configuration file are stored in a hash table once the entire
 * file has been parsed. */
ConfigFile::ConfigFile(const std::string &FileName){
	this->FileName = FileName;
	ExtractKeys();
}

/** Checks for duplicate parameter entries.
 *
 */
bool ConfigFile::keyExists(const std::string &key) const{
	std::string temp=key;
	lowerCase(temp);
	return contents.find(temp) != contents.end();
}

/** Returns the value for the associated key from the hash table.
 * \tparam ValueType `std::string`, `double`, `int`, `std::vector<double>`, `std::vector<int>` or `bool`.
 * \param[in] key a pointer to the string indicating the requested key.
 * \return Value associated with the requested key. */
//@{
template <typename ValueType>
ValueType ConfigFile::getValueOfKey(const std::string &key) const{
	std::string temp=key;
	lowerCase(temp);
	if (!keyExists(temp)){
		throw std::runtime_error(BoldRedFont + "CFG error: Key not found -> " + temp + " in " + FileName + ResetFont + "\n");
	}
	return Convert::string_to_T<ValueType>(FileName, contents.find(temp)->second, temp);
}

template std::string ConfigFile::getValueOfKey<std::string>(const std::string&) const;
template std::vector<std::string> ConfigFile::getValueOfKey<std::vector<std::string>>(const std::string&) const;
template std::vector<double> ConfigFile::getValueOfKey<std::vector<double>>(const std::string&) const;
template std::vector<unsigned int> ConfigFile::getValueOfKey<std::vector<unsigned int>>(const std::string&) const;
template std::vector<int> ConfigFile::getValueOfKey<std::vector<int>>(const std::string&) const;
template double ConfigFile::getValueOfKey<double>(const std::string&) const;
template unsigned int ConfigFile::getValueOfKey<unsigned int>(const std::string&) const;
template int ConfigFile::getValueOfKey<int>(const std::string&) const;
template bool ConfigFile::getValueOfKey<bool>(const std::string&) const;
//@}
