/*
 * Hdc.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Hdc.cpp
 * @brief Definitions of Hdc class methods. */

#include <cmath>
#include "../../hdr/Classes/Hdc.hpp"
/** The constructor initialises the class using the data from the configuration file.
 * the field is always initialised at the minimum strength in the off state.
 *
 * @param[in] cfg Configuration file data.
 */
Hdc::Hdc(const ConfigFile cfg)
    : state(HdcConditions::min)
    , On(false)
    , CurTime(0.0)
    , OnTimer(1.0e230)
{
        std::vector<double> dirn_tmp = cfg.getValueOfKey<std::vector<double>>("FieldDC:Unit_vector");
        double tmpMag = sqrt(dirn_tmp[0]*dirn_tmp[0]+dirn_tmp[1]*dirn_tmp[1]+dirn_tmp[2]*dirn_tmp[2]);
        dirn.x = dirn_tmp[0]/tmpMag;
        dirn.y = dirn_tmp[1]/tmpMag;
        dirn.z = dirn_tmp[2]/tmpMag;

        min  = cfg.getValueOfKey<double>("FieldDC:Minimum_strength");
        max  = cfg.getValueOfKey<double>("FieldDC:Maximum_strength");
        if(min!=max){
            ramp_time = cfg.getValueOfKey<double>("FieldDC:Ramp_time");
            if(ramp_time==0){rate=0.0;}
            else{rate = (max-min)/ramp_time;}
        } else {
            ramp_time = 1.0;
            rate = 0.0;
        }

        updateField(0.0);
    }
/** default deconstructor. */
Hdc::~Hdc() {}
/** Updates the field's magnitude and 3-vector.
 *
 * @param[in] dt Timestep
 */
void Hdc::updateField(double dt){
     updateTime(dt);
     TimedSwitch();
     updateMag(dt);
     field=dirn*mag;
 }
/** Toggles the fields power state. */
void Hdc::Switch(){
    On=!On;
}
/** Turns the field on. */
 void Hdc::turnOn(){
     On=true;
 }
/** Turns the field off. */
void Hdc::turnOff(){
    On=false;
}
/** Set field to turn off for specified duration. */
void Hdc::turnOnfor(const double Timer){
    On=true;
    setTimer(Timer);
}
/** Set field to turn off for specified duration. */
void Hdc::turnOfffor(const double Timer){
    On=true;
    setTimer(Timer);
}
/** Set the internal timer. */
void Hdc::setTimer(const double Timer){
    OnTimer = Timer;
}
/** Performs switch based on internal timer. */
void Hdc::TimedSwitch(){
    if(CurTime>=OnTimer){
        Switch();
        CurTime=0.0;
    }
}
/** Updates the internal time of the field. */
void Hdc::updateTime(const double dt){
    CurTime += dt;
}
/** Updates the field state based on the current magnitude. */
void Hdc::updateState(){
    switch(state)
    {
    case HdcConditions::min:
        if(On){state = HdcConditions::increasing;}
        break;
    case HdcConditions::increasing:
        if(mag>=max){state = HdcConditions::max;}
        break;
    case HdcConditions::max:
        if(!On){state = HdcConditions::decreasing;}
        break;
    case HdcConditions::decreasing:
        if(mag<=min){state = HdcConditions::min;}
        break;
    }
}
/** Updates the fields magnitude according to the current state.
 *
 * @param[in] dt Timestep
 */
void Hdc::updateMag(double dt){
    switch(state)
    {
    case HdcConditions::min:
        mag=min;
        break;
    case HdcConditions::max:
        mag=max;
        break;
    case HdcConditions::increasing:
        if(rate==0){mag=max;}
        else{mag += dt*rate;}
        break;
    case HdcConditions::decreasing:
        if(rate==0){mag=min;}
        else{mag -= dt*rate;}
        break;
    }
    updateState();
}
/** Sets the field direction vector.
 *
 * @param[in] Direction Field direction vector.
 */
 void Hdc::setDirn(Vec3 Direction){
     if(Direction.x==0.0 && Direction.y==0.0 && Direction.z==0.0){
     } else {
         Direction = Direction/sqrt(Direction*Direction);
     }
     dirn = Direction;
 }
/** Sets the field's maximum strength.
 *
 * @param[in] Maximum field strength.
 */
void Hdc::setMax(double Maximum){
    max = Maximum;
}
/** Sets the field's minimum strength.
 *
 * @param[in] Minimum field strength.
 */
void Hdc::setMin(double Minimum){
    min = Minimum;
}
/** Returns the Field's 3-vector. */
 Vec3 Hdc::getField() const {return field;}
 /** Returns the field's magnitude. */
 double Hdc::getMag() const {return mag;}
 /** Returns the field's current state. */
 HdcConditions Hdc::getState() const {return state;}
/** Returns the field's direction vector. */
 Vec3 Hdc::getDirn() const {return dirn;}
 /** Returns the fields minimum strength. */
 double Hdc::getMin() const {return min;}
 /** Returns the fields maximum strength. */
 double Hdc::getMax() const {return max;}
 /** Returns the time required to switch between field strengths. */
 double Hdc::getRampTime() const {return ramp_time;}
 /** Returns the rate of change of the field (inverse of ramp_time). */
 double Hdc::getRate() const {return rate;}
