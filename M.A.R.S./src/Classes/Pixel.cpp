/*
 * Pixel.cpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Pixel.cpp
 * @brief Definitions of Pixel class methods. */

#include "../../hdr/Classes/Pixel.hpp"
/** The constructor initialises a pixel with position and an ID, all other values are set to
 * zero. The Grain ID is set to -1 which indicates the pixel is not within a grain.
 *
 * @param[in] id
 * @param[in] X
 * @param[in] Y
 */
Pixel::Pixel(unsigned int id, double X, double Y)
    : ID(id)
    , centre(std::make_pair(X,Y))
    , Grain(-1)
    , Temperature(0.0)
    , Magnetisation(0.0,0.0,0.0)
    , Field(0.0,0.0,0.0)
    , Easy_Axis(0.0,0.0,0.0)
{}

/** Sets the magnetisation */
void Pixel::set_M(Vec3 M){Magnetisation=M;}
/** Sets the magnetisation. */
void Pixel::set_M(double M){Magnetisation=M;}
/** Sets the easy axis. */
void Pixel::set_EA(Vec3 EA){Easy_Axis=EA;}
/** Sets the easy axis. */
void Pixel::set_EA(double EA){Easy_Axis=EA;}
/** Sets the temperature. */
void Pixel::set_Temp(double T){Temperature=T;}
/** Sets the field. */
void Pixel::set_H(Vec3 H){Field=H;}
/** Sets the grain ID. */
void Pixel::set_Grain(unsigned int G){Grain=G;}
/** Set the pixels centre coordinates. */
void Pixel::set_Coords(std::pair<double,double> C){centre=C;}
/** Returns the pixel ID. */
unsigned int Pixel::get_ID() const {return ID;}
/** Returns the grain the pixel resides within. */
int Pixel::get_Grain() const {return Grain;}
/** Returns the pixels magnetisation. */
Vec3 Pixel::get_M() const {return Magnetisation;}
/** Returns the pixels easy axis. */
Vec3 Pixel::get_EA() const {return Easy_Axis;}
/** Returns the pixels magnetisation x-component. */
double Pixel::get_Mx() const {return Magnetisation.x;}
/** Returns the pixels magnetisation y-component. */
double Pixel::get_My() const {return Magnetisation.y;}
/** Returns the pixels magnetisation z-component. */
double Pixel::get_Mz() const {return Magnetisation.z;}
/** Returns the pixels temperature. */
double Pixel::get_Temp() const {return Temperature;}
/** Returns the field applied to the pixel. */
Vec3 Pixel::get_H() const {return Field;}
/** Returns the pixels centre coordinates. */
std::pair<double,double> Pixel::get_Coords() const {return centre;}

