/*
 * Writer.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Writer.cpp
 * @brief Definitions of Writer class methods. */

#include <unordered_set>
#include "../../hdr/Classes/Writer.hpp"
#include "../../hdr/Classes/PixelMap.hpp"
/** The constructor initialises the sub and parent classes using the data from the configuration file.
 *
 * @param[in] cfg Configuration file data.
 */
Writer::Writer(const ConfigFile cfg)
    : Hdc(cfg)
    , Pos_X(0.0)
    , Pos_Y(0.0)
{
    H_width_X = cfg.getValueOfKey<double>("WriteHead:Field_Width_X");
    H_width_Y = cfg.getValueOfKey<double>("WriteHead:Field_Width_Y");
    speed = cfg.getValueOfKey<double>("WriteHead:Speed");
}
/** default deconstructor. */
Writer::~Writer(){}
/** Sets the position of the write head in the x-dimension.
 *
 * @param[in] x X-coordinate.
 */
void Writer::setX(double x){Pos_X=x;}
/** Sets the position of the write head in the y-dimension.
 *
 * @param[in] y Y-coordinate.
 */
void Writer::setY(double y){Pos_Y=y;}
/** Increments the x-position of the write head by dx.
 *
 * @param[in] dx Displacement in x-direction.
 */
void Writer::moveX(double dx){Pos_X += dx;}
/** Increments the y-position of the write head by dy.
 *
 * @param[in] dy Displacement in y-direction.
 */
void Writer::moveY(double dy){Pos_Y += dy;}
/** Apply field to grains within the the profile. The Distance between the centre of the profile and the
 * grains is checked to be within the limits of the field profile. Grains within the profile have their field
 * data updated equal to the applied field, all other grains have their field data set to zero.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] dt Timestep.
 * @param[out] Grains Grain.H_appl is updated for all the grains in the system.
 */
void Writer::ApplyHspatial(const Voronoi_t VORO, const unsigned int Num_Layers,
                           const double dt, Grain_t*Grains){

    updateField(dt);

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];
        if(fabs(Pos_X-Grain_X)<=(H_width_X*0.5) && fabs(Pos_Y-Grain_Y)<=(H_width_Y*0.5)){
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains->H_appl[grain_in_system] = getField();
            }
        } else {
            // Reset all the other grains not in range of writing head to zero applied field
            for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
                unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
                Grains->H_appl[grain_in_system] = Vec3(0.0,0.0,0.0);
            }
        }
    }
}
/** Apply field to pixels in the pixel map and then produce average field for each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] dt Timestep.
 * @param[out] Grains Grain.H_appl is updated for all the grains in the system.
 * @param PixMap PixelMap.Field is updated for all pixels.
 */
 void Writer::ApplyHspatialPM(const Voronoi_t VORO, const unsigned int Num_Layers, const double dt,
                              Grain_t&Grains,PixelMap&PixMap){

     updateField(dt);
     PixMap.ClearH();
     for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
         unsigned int offset = Layer*VORO.Num_Grains;
         for(unsigned int grain=0;grain<VORO.Num_Grains;++grain){
             unsigned int grain_in_system = grain+offset;
             Grains.H_appl[grain_in_system] = Vec3(0.0,0.0,0.0);
         }
     }
     std::vector<unsigned int> PixelList;
     std::unordered_set<unsigned int> ImpactedGrains;
     PixMap.InRect((Pos_X-H_width_X*0.5), (Pos_Y-H_width_Y*0.5), (Pos_X+H_width_X*0.5),
                   (Pos_Y+H_width_Y*0.5),PixelList,ImpactedGrains);
     // Apply field to all pixels
     for(size_t Pix=0;Pix<PixelList.size();++Pix){
         PixMap.Access(PixelList[Pix]).set_H(getField());
     }
     // Determine grain average field
     for(auto it=ImpactedGrains.begin();it!=ImpactedGrains.end();++it){
         unsigned int grain = *it;
         Vec3 H(0,0,0);
         for(unsigned int px=0;px<Grains.Pixel[grain].size();++px){
             H += PixMap.Access(Grains.Pixel[grain][px]).get_H();
         }
         H /= Grains.Pixel[grain].size();
         for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
             Grains.H_appl[grain+(VORO.Num_Grains*Layer)]=H;
         }
     }
}
/** Returns the x-position for the write head. */
double Writer::getX() const {return Pos_X;}
/** Returns the y-position for the write head. */
double Writer::getY() const {return Pos_Y;}
/** Returns the x-width of the field profile of the write head. */
double Writer::getHwidthX() const {return H_width_X;}
/** Returns the y-width of the field profile of the write head. */
double Writer::getHwidthY() const {return H_width_Y;}
/** Returns the speed of the write head. */
double Writer::getSpeed() const {return speed;}
