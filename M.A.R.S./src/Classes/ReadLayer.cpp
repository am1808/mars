/*
 * ReadLayer.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadLayer.cpp
 * @brief Definitions of ReadLayer class methods. */

#include "../../hdr/Classes/ReadLayer.hpp"
/** The constructor initialises the class using the data already present it the RecLayer class.*/
ReadLayer::ReadLayer(const RecLayer& RecordingLayer){
    Track_spacing = RecordingLayer.getBitSpacingY();
    Track_size    = RecordingLayer.getBitSizeY();
    Track_number  = RecordingLayer.getBitsY();
}
/** The constructor initialises the class using the configuration file data. */
ReadLayer::ReadLayer(const ConfigFile cfg)
    :Track_spacing(0.0)
    ,Track_size(0)
    ,Track_number(0)
{
    Track_spacing = cfg.getValueOfKey<double>("ReadingLayer:Track_spacing");
    Track_size    = cfg.getValueOfKey<double>("ReadingLayer:Bit_length");
    Track_number  = cfg.getValueOfKey<double>("ReadingLayer:Tracks");
}
/** Default deconstructor. */
ReadLayer::~ReadLayer(){}
/** Returns the track spacing. */
double ReadLayer::getBitSpacingY() const {return Track_spacing;}
/** Returns the number of tracks. */
double ReadLayer::getBitsY() const {return Track_number;}
/** Returns the size of the tracks (y-dimension). */
double ReadLayer::getBitSizeY() const {return Track_size;}
