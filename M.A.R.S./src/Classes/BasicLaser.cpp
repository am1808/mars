/*
 * BasicLaser.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file BasicLaser.cpp
 * @brief Definitions of BasicLaser class methods. */

#include "../../hdr/Classes/BasicLaser.hpp"
/** the constructor initialises the class using the data from the configuration file.
 *  The field is created in the 'off' state and the minimum temperature is set to the
 *  input environment temperature.
 *
 * @param[in] cfg Configuration file data.
 * @param[in] Environ_temp Temperature when laser is off.
 */
BasicLaser::BasicLaser(const ConfigFile cfg, const double Environ_temp)
    : state(BlConditions::Min)
    , Active(false)
    , TempMin(Environ_temp)
    , TempCur(Environ_temp)
{
    TempMax = cfg.getValueOfKey<double>("Laser:Temp");
    Instantaneous = !cfg.getValueOfKey<bool>("Laser:Include_heating_phase");
    if(!Instantaneous){
        Rate = cfg.getValueOfKey<double>("Laser:Rate");
    } else {
        Rate = 0.0;
    }
}
/** Default deconstructor */
BasicLaser::~BasicLaser(){}
/** Updates the laser state based on the current temperature. */
void BasicLaser::updateState(){
    switch(state)
    {
    case BlConditions::Min:
        if(Active){state = BlConditions::Heating;}
        break;
    case BlConditions::Heating:
        if(TempCur>=TempMax){state = BlConditions::Max;}
        break;
    case BlConditions::Max:
        if(!Active){state = BlConditions::Cooling;}
        break;
    case BlConditions::Cooling:
        if(TempCur<=TempMin){state = BlConditions::Min;}
        break;
    }
}
/** Turns the field on. */
void BasicLaser::turnOn(){Active=true;}
/** Turns the field off. */
void BasicLaser::turnOff(){Active=false;}
/** Updates the current temperature of the laser according to the state.
 *
 * @param[in] dt Timestep
 */
void BasicLaser::updateLaser(double dt){
    updateState();
    switch(state)
    {
    case BlConditions::Min:
        TempCur=TempMin;
        break;
    case BlConditions::Max:
        TempCur=TempMax;
        break;
    case BlConditions::Heating:
        if(Instantaneous){TempCur=TempMax;}
        else{
            TempCur += dt*Rate;
            if(TempCur>TempMax){TempCur=TempMax;}
        }
        break;
    case BlConditions::Cooling:
        if(Instantaneous){TempCur=TempMin;}
        else{
            TempCur -= dt*Rate;
            if(TempCur<TempMin){TempCur=TempMin;}
        }
        break;
    }
    updateState();
}
/** Sets the maximum temperature produced by the laser. */
void BasicLaser::setTempMax(double val){TempMax=val;}
/** Returns the current temperature of the laser. */
double BasicLaser::getTemp() const {return TempCur;}
/** Returns the maximum temperature produced by the laser. */
double BasicLaser::getTempMax() const {return TempMax;}
/** Returns the rate of temperature change. */
double BasicLaser::getRate() const {return Rate;}
/** Returns the time required to ramp up/down. */
double BasicLaser::getCoolingTime() const {
    if(Rate>0.0){
        return (TempMax-TempMin)/Rate;
    } else {
        return 0.0;
    }
}
/** Returns the boolean representing the power state of the laser. */
bool BasicLaser::isOn() const {return Active;}
/** Returns boolean indicating if the laser it at maximum power. */
bool BasicLaser::isMax() const {
    switch(state)
    {
    case BlConditions::Max:
        return true;
    default:
        break;
    }
    return false;
}
/** Returns boolean indicating if the laser is completely off. */
bool BasicLaser::isMin() const {
     switch(state)
     {
     case BlConditions::Min:
         return true;
     default:
         break;
     }
     return false;
 }
