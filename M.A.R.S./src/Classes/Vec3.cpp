/*
 * Vec3.cpp
 *
 *  Created on: 5 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Vec3.cpp
 * @brief Definition of Vec3 class methods. */

#include "../../hdr/Classes/Vec3.hpp"

Vec3::Vec3(){
    x=y=z=0.0;
}

Vec3::Vec3(double x,double y,double z){
    this->x = x;
    this->y = y;
    this->z = z;
}

Vec3::Vec3(const Vec3& vector){
    x=vector.x;
    y=vector.y;
    z=vector.z;
}

Vec3::~Vec3(){}
/** Overloaded operator for printing out a vector.*/
std::ostream& operator<<(std::ostream& os, const Vec3& vec){
    return os << vec.x << " " << vec.y << " " << vec.z;
}
/** Returns the sum of the two vectors. */
Vec3 operator+(Vec3 const &lhs, Vec3 const &rhs){
    return Vec3(lhs.x + rhs.x,lhs.y + rhs.y,lhs.z + rhs.z);
}
/** Returns the difference between the two vectors. */
Vec3 operator-(Vec3 const &lhs, Vec3 const &rhs){
    return Vec3(lhs.x - rhs.x,lhs.y - rhs.y,lhs.z - rhs.z);
}
/** Returns the negative of the input vector. */
Vec3 operator-(Vec3 const &rhs){
    return Vec3(-rhs.x,-rhs.y,-rhs.z);
}
/** Returns the dot product of two vectors. */
double operator*(Vec3 const &lhs, Vec3 const &rhs){
    return lhs.x*rhs.x+lhs.y*rhs.y+lhs.z*rhs.z;
}
/** Returns the vector multipled by the scalar. */
Vec3 operator*(Vec3 const &lhs, double const &rhs){
    return Vec3(lhs.x * rhs,lhs.y * rhs,lhs.z * rhs);
}
/** Returns the vector multiplied by the scalar. */
Vec3 operator*(double const &lhs, Vec3 const &rhs){
    // Call operator+(Vec3, double)
    return rhs*lhs;
}
/** Returns the vector divided by the scalar. */
Vec3 operator/(Vec3 const &lhs, double const &rhs){
    return Vec3(lhs.x / rhs,lhs.y / rhs,lhs.z / rhs);
}
/** Returns the cross product of two vectors. */
Vec3 operator%(Vec3 const &lhs, Vec3 const &rhs){
    return Vec3((lhs.y*rhs.z - lhs.z*rhs.y),(lhs.z*rhs.x - lhs.x*rhs.z),(lhs.x*rhs.y - lhs.y*rhs.x));
}
/** Increments by the rhs and assigns the result. */
void Vec3::operator+=(Vec3 const &rhs){
    x+=rhs.x;
    y+=rhs.y;
    z+=rhs.z;
}
/** Decreases by the rhs and assigns the result. */
void Vec3::operator-=(Vec3 const &rhs){
    x-=rhs.x;
    y-=rhs.y;
    z-=rhs.z;
}
/** Multiplies by the rhs and assigns the result. */
void Vec3::operator*=(double const &rhs){
    x*=rhs;
    y*=rhs;
    z*=rhs;
}
/** Divides by the rhs and assigns the result. */
void Vec3::operator/=(double const &rhs){
    x/=rhs;
    y/=rhs;
    z/=rhs;
}
/** Assigns the double to each component of the vector. */
void Vec3::operator=(double const &rhs){
    x=y=z=rhs;
}
