/*
 * Solver.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file Solver.cpp
 * \brief Definition of solver class methods. */


#include "../../../hdr/Classes/Solver.hpp"

#include <iostream>
#include <cmath>

/** The constructor sets up the solver specified in the configuration file, this includes all
 * the material dependent parameters.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] cfg Configuration file data.
 * @param[in] Materials_config List of material configuration files.
 */
Solver_t::Solver_t(unsigned Num_grains, unsigned int Num_Layers, ConfigFile cfg, const std::vector<ConfigFile> Materials_config)
    : behaviour_kMC(Behaviours::Standard)
    , behaviour_LLG(Behaviours::Standard)
    , Exclusion(false)
    , dt_kMC(1.0e-6)
    , dt_LLG(1.0e-12)
    , dt_LLB(1.0e-15)
    , Measurement_time_kMC(dt_kMC)
    , Measurement_time_LLG(dt_LLG)
    , Measurement_time_LLB(dt_LLB)
    , Alpha(0)
    , Gamma(1.760859644e+7)
    , BoldCyanFont("\033[1;36m")
    , BoldYellowFont("\033[1;33m")
    , BoldRedFont("\033[1;4;31m")
    , ResetFont("\033[0m")
{
    // Get value of solver selection from file
    std::string Selection=cfg.getValueOfKey<std::string>("Solver:Solver_selection");
    if(Selection!="kmc" && Selection!="llb" && Selection!="llg"){
        std::cout << "CFG error: Solver selection invalid." << std::endl;
        exit(EXIT_FAILURE);
    }
    else{
        if(Selection=="kmc"){type=Solvers::kMC;}
        else if(Selection=="llg"){type=Solvers::LLG;}
        else if(Selection=="llb"){type=Solvers::LLB;}
    }

    Exclusion=cfg.getValueOfKey<bool>("Solver:inclusion_zone");
    if(Exclusion){
        // Input is in grain widths
        double tempNX = cfg.getValueOfKey<double>("Solver:inclusion_range_negX");
        double tempPX = cfg.getValueOfKey<double>("Solver:inclusion_range_posX");
        double tempNY = cfg.getValueOfKey<double>("Solver:inclusion_range_negY");
        double tempPY = cfg.getValueOfKey<double>("Solver:inclusion_range_posY");
        double width_temp=1.0;
        if(!cfg.getValueOfKey<bool>("Solver:inclusion_in_nm")){
            width_temp = cfg.getValueOfKey<double>("Struct:Avg_Grain_width");
        }
        // Set parameter up to be in real units
        Exclusion_range_negX=tempNX*width_temp;
        Exclusion_range_posX=tempPX*width_temp;
        Exclusion_range_negY=tempNY*width_temp;
        Exclusion_range_posY=tempPY*width_temp;
    }

    Import(cfg);
    switch(type)
    {
    case Solvers::LLG:
    case Solvers::LLB:
        Fittings_import(Num_Layers,Materials_config);
        Fitting_per_grain(Num_grains,Materials_config);
        break;
    default:
        break;
    }
}

/** The constructor sets up the solver specified in the configuration file, this does not read in
 * any material dependent parameters.
 * This is useful for simulations which use previously generated systems (i.e. no input system parameters).
 *
 * @param[in] cfg Configuration file data.
 */
Solver_t::Solver_t(ConfigFile cfg)
    : behaviour_kMC(Behaviours::Standard)
    , behaviour_LLG(Behaviours::Standard)
    , Exclusion(false)
    , dt_kMC(1.0e-6)
    , dt_LLG(1.0e-12)
    , dt_LLB(1.0e-15)
    , Measurement_time_kMC(dt_kMC)
    , Measurement_time_LLG(dt_LLG)
    , Measurement_time_LLB(dt_LLB)
    , Alpha(0)
    , Gamma(1.760859644e+7)
    , BoldCyanFont("\033[1;36m")
    , BoldYellowFont("\033[1;33m")
    , BoldRedFont("\033[1;4;31m")
    , ResetFont("\033[0m")
{
    // Get value of solver selection from file
    std::string Selection=cfg.getValueOfKey<std::string>("Solver:Solver_selection");
    if(Selection!="kmc" && Selection!="llb" && Selection!="llg"){
        std::cout << "CFG error: Solver selection invalid." << std::endl;
        exit(EXIT_FAILURE);
    }
    else{
        if(Selection=="kmc"){type=Solvers::kMC;}
        else if(Selection=="llg"){type=Solvers::LLG;}
        else if(Selection=="llb"){type=Solvers::LLB;}
    }
    Exclusion=cfg.getValueOfKey<bool>("Solver:inclusion_zone");
    if(Exclusion){
        // Input is in grain widths
        double tempNX = cfg.getValueOfKey<double>("Solver:inclusion_range_negX");
        double tempPX = cfg.getValueOfKey<double>("Solver:inclusion_range_posX");
        double tempNY = cfg.getValueOfKey<double>("Solver:inclusion_range_negY");
        double tempPY = cfg.getValueOfKey<double>("Solver:inclusion_range_posY");
        double width_temp=1.0;
        if(!cfg.getValueOfKey<bool>("Solver:inclusion_in_nm")){
            width_temp = cfg.getValueOfKey<double>("Struct:Avg_Grain_width");
        }
        // Set parameter up to be in real units
        Exclusion_range_negX=tempNX*width_temp;
        Exclusion_range_posX=tempPX*width_temp;
        Exclusion_range_negY=tempNY*width_temp;
        Exclusion_range_posY=tempPY*width_temp;
    }
    Import(cfg);
}
/** Default deconstructor */
Solver_t::~Solver_t(){}

/** Force the initialisation of the specified solver. This allows simulations to force the user to
 * provide specific solver parameters preventing unexpected behaviour in the case of a change of solver
 * during runtime
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] cfg Configuration file data.
 * @param[in] Materials_config List of material configuration files.
 * @param[in] desired Desired solver to initialise.
 */
void Solver_t::Force_specific_solver_construction(unsigned Num_grains, unsigned int Num_Layers, ConfigFile cfg, const std::vector<ConfigFile> Materials_config, Solvers desired)
{
    if(desired!=type){
        Import(cfg);
        switch(desired) // Ensure Material information required by dynamic solvers is present
        {
        case Solvers::LLG:
        case Solvers::LLB:
            Fittings_import(Num_Layers,Materials_config);
            Fitting_per_grain(Num_grains,Materials_config);
            break;
        default:
            break;
        }
    }
}
/** Handles the assignment of solver variables using the data provided from the configuration file.
 *
 * @param[in] cfg Configuration file data.
 */
void Solver_t::Import(ConfigFile cfg)
{
    switch(type)
    {
    case Solvers::kMC:
        dt_kMC = cfg.getValueOfKey<double>("KMC:dt");
        Measurement_time_kMC = cfg.getValueOfKey<double>("KMC:Measurement_time");
        T_variation = cfg.getValueOfKey<bool>("Solver:T_dependent_variables");
        if(T_variation){
            M_length_variation = cfg.getValueOfKey<bool>("Solver:M_length_variable");
            if(cfg.getValueOfKey<bool>("Solver:Handle_Tc")){
                behaviour_kMC=Behaviours::Thermoremanence;
            }
        }
        else{
            M_length_variation = false;
            behaviour_kMC=Behaviours::Standard;
        }
        f0 = cfg.getValueOfKey<double>("KMC:F0");
        break;
    case Solvers::LLG:
        dt_LLG = cfg.getValueOfKey<double>("LLG:dt");
        Measurement_time_LLG = cfg.getValueOfKey<double>("LLG:Measurement_time");
        T_variation = cfg.getValueOfKey<bool>("Solver:T_dependent_variables");
        if(T_variation){
            M_length_variation = cfg.getValueOfKey<bool>("Solver:M_length_variable");
            if(cfg.getValueOfKey<bool>("Solver:Handle_Tc")){
                behaviour_LLG=Behaviours::Thermoremanence;
            }
        }
        break;
    case Solvers::LLB:
        dt_LLB = cfg.getValueOfKey<double>("LLB:dt");
        Measurement_time_LLB = cfg.getValueOfKey<double>("LLB:Measurement_time");
        break;
    }
}

/** Assigns susceptibility values for each grain.
 *
 * @param[in] Num_grains Number of grains per layer
 * @param[in] sigma Dispersion for log-normal distribution
 */
void Solver_t::Fitting_per_grain(const unsigned Num_grains, std::vector<ConfigFile> Materials_config){
    
    Ga0_PARA.clear();Ga1_PARA.clear();Ga2_PARA.clear();
    Ga3_PARA.clear();Ga4_PARA.clear();Ga5_PARA.clear();
    Ga6_PARA.clear();Ga7_PARA.clear();Ga8_PARA.clear();
    Ga9_PARA.clear();Ga1_2_PARA.clear();
    Gb0_PARA.clear();Gb1_PARA.clear();Gb2_PARA.clear();
    Gb3_PARA.clear();Gb4_PARA.clear();
    
    Ga0_PERP.clear();Ga1_PERP.clear();Ga2_PERP.clear();
    Ga3_PERP.clear();Ga4_PERP.clear();Ga5_PERP.clear();
    Ga6_PERP.clear();Ga7_PERP.clear();Ga8_PERP.clear();
    Ga9_PERP.clear();Ga1_2_PERP.clear();
    Gb0_PERP.clear();Gb1_PERP.clear();Gb2_PERP.clear();
    Gb3_PERP.clear();Gb4_PERP.clear();
    
    
    for(unsigned layer=0;layer<Chi_scaling_factor.size();++layer){
        double SIG=Materials_config[layer].getValueOfKey<double>("Mat:StdDev_K");
        std::lognormal_distribution<double> dist(0.0,SIG);
        
        for(unsigned grain=0;grain<Num_grains;++grain){
            if(SIG!=0.0){
                double RNG=dist(Gen);
                
                Ga0_PERP.push_back(a0_PERP[layer]*RNG);
                Ga1_PERP.push_back(a1_PERP[layer]*RNG);
                Ga2_PERP.push_back(a2_PERP[layer]*RNG);
                Ga3_PERP.push_back(a3_PERP[layer]*RNG);
                Ga4_PERP.push_back(a4_PERP[layer]*RNG);
                Ga5_PERP.push_back(a5_PERP[layer]*RNG);
                Ga6_PERP.push_back(a6_PERP[layer]*RNG);
                Ga7_PERP.push_back(a7_PERP[layer]*RNG);
                Ga8_PERP.push_back(a8_PERP[layer]*RNG);
                Ga9_PERP.push_back(a9_PERP[layer]*RNG);
                Ga1_2_PERP.push_back(a1_2_PERP[layer]*RNG);
                Gb0_PERP.push_back(b0_PERP[layer]*RNG);
                Gb1_PERP.push_back(b1_PERP[layer]*RNG);
                Gb2_PERP.push_back(b2_PERP[layer]*RNG);
                Gb3_PERP.push_back(b3_PERP[layer]*RNG);
                Gb4_PERP.push_back(b4_PERP[layer]*RNG);                
            }
            else {
                Ga0_PERP.push_back(a0_PERP[layer]);
                Ga1_PERP.push_back(a1_PERP[layer]);
                Ga2_PERP.push_back(a2_PERP[layer]);
                Ga3_PERP.push_back(a3_PERP[layer]);
                Ga4_PERP.push_back(a4_PERP[layer]);
                Ga5_PERP.push_back(a5_PERP[layer]);
                Ga6_PERP.push_back(a6_PERP[layer]);
                Ga7_PERP.push_back(a7_PERP[layer]);
                Ga8_PERP.push_back(a8_PERP[layer]);
                Ga9_PERP.push_back(a9_PERP[layer]);
                Ga1_2_PERP.push_back(a1_2_PERP[layer]);
                Gb0_PERP.push_back(b0_PERP[layer]);
                Gb1_PERP.push_back(b1_PERP[layer]);
                Gb2_PERP.push_back(b2_PERP[layer]);
                Gb3_PERP.push_back(b3_PERP[layer]);
                Gb4_PERP.push_back(b4_PERP[layer]);                
            }
            Ga0_PARA.push_back(a0_PARA[layer]);
            Ga1_PARA.push_back(a1_PARA[layer]);
            Ga2_PARA.push_back(a2_PARA[layer]);
            Ga3_PARA.push_back(a3_PARA[layer]);
            Ga4_PARA.push_back(a4_PARA[layer]);
            Ga5_PARA.push_back(a5_PARA[layer]);
            Ga6_PARA.push_back(a6_PARA[layer]);
            Ga7_PARA.push_back(a7_PARA[layer]);
            Ga8_PARA.push_back(a8_PARA[layer]);
            Ga9_PARA.push_back(a9_PARA[layer]);
            Ga1_2_PARA.push_back(a1_2_PARA[layer]);
            Gb0_PARA.push_back(b0_PARA[layer]);
            Gb1_PARA.push_back(b1_PARA[layer]);
            Gb2_PARA.push_back(b2_PARA[layer]);
            Gb3_PARA.push_back(b3_PARA[layer]);
            Gb4_PARA.push_back(b4_PARA[layer]);
        }
    }
}


/** Handles the assignment of the material dependent solver variables using the data provided from the
 * configuration files.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Materials_config List of material configuration files.
 */
void Solver_t::Fittings_import(const unsigned int Num_Layers, std::vector<ConfigFile> Materials_config){

//	std::cout << "\t\tLLB fitting import function has been called" << std::endl;
    // Clear all vectors before pushing back new ones for each layer
    Susceptibility_Type.clear();
    Chi_scaling_factor.clear();
    a0_PARA.clear();
    a1_PARA.clear();
    a2_PARA.clear();
    a3_PARA.clear();
    a4_PARA.clear();
    a5_PARA.clear();
    a6_PARA.clear();
    a7_PARA.clear();
    a8_PARA.clear();
    a9_PARA.clear();
    a1_2_PARA.clear();
    b0_PARA.clear();
    b1_PARA.clear();
    b2_PARA.clear();
    b3_PARA.clear();
    b4_PARA.clear();
    a0_PERP.clear();
    a1_PERP.clear();
    a2_PERP.clear();
    a3_PERP.clear();
    a4_PERP.clear();
    a5_PERP.clear();
    a6_PERP.clear();
    a7_PERP.clear();
    a8_PERP.clear();
    a9_PERP.clear();
    a1_2_PERP.clear();
    b0_PERP.clear();
    b1_PERP.clear();
    b2_PERP.clear();
    b3_PERP.clear();
    b4_PERP.clear();

    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        Alpha.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Alpha"));
//        std::cout << "Alpha[" << Layer << "] = " << Alpha.back() << std::endl;

        // Susceptibility parameters
        Susceptibility_Type.push_back(
                Materials_config[Layer].getValueOfKey<std::string>("Mat:susceptibility_type"));
//        std::cout << "SusType[" << Layer << "] = " << Susceptibility_Type.back() << std::endl;
        if (Susceptibility_Type.back() != "default" && Susceptibility_Type.back()!= "kazantseva_susceptibility_fit"
            && Susceptibility_Type.back() != "vogler_susceptibility_fit"&& Susceptibility_Type.back() != "inv_susceptibility_fit") {
                std::cout << BoldRedFont << " Materials_config[Layer] error: Material " << std::to_string(Layer)
                          << " Unknown Susceptibility type. -> " << Susceptibility_Type.back() << ResetFont << std::endl;
            exit(EXIT_FAILURE);
        }
        if (Susceptibility_Type.back() == "default") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(9.54393845712027);
            a0_PARA.push_back(1.21e-3);
            a1_PARA.push_back(-2.2e-7);
            a2_PARA.push_back(0.000);
            a3_PARA.push_back(1.95e-13);
            a4_PARA.push_back(-1.3e-17);
            a5_PARA.push_back(0.000);
            a6_PARA.push_back(-4.00e-23);
            a7_PARA.push_back(0.000);
            a8_PARA.push_back(0.000);
            a9_PARA.push_back(-6.51e-32);
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(2.12e-3);
            b1_PARA.push_back(0.000);
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(2.11e-3);
            a1_PERP.push_back(1.10e-1);
            a2_PERP.push_back(-8.55e-1);
            a3_PERP.push_back(3.42);
            a4_PERP.push_back(-7.85);
            a5_PERP.push_back(1.03e+1);
            a6_PERP.push_back(-6.86e-1);
            a7_PERP.push_back(7.97e-1);
            a8_PERP.push_back(1.54);
            a9_PERP.push_back(-6.27e-1);
            a1_2_PERP.push_back(0.000);
            b0_PERP.push_back(4.85e-3);
            b1_PERP.push_back(0.000);
            b2_PERP.push_back(0.000);
            b3_PERP.push_back(0.000);
            b4_PERP.push_back(0.000);
        }
        // N. Kazantseva's functions parameters
        else if (Susceptibility_Type.back()== "kazantseva_susceptibility_fit") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PARA"));
            a2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PARA"));
            a3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PARA"));
            a4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PARA"));
            a5_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PARA"));
            a6_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PARA"));
            a7_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PARA"));
            a8_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PARA"));
            a9_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PARA"));
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(0.000);
            b1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PARA"));
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(0.000);
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(0.000);
            b2_PERP.push_back(0.000);
            b3_PERP.push_back(0.000);
            b4_PERP.push_back(0.000);
        }
        // Vogler's functions parameters
        else if (Susceptibility_Type.back()
                == "vogler_susceptibility_fit") {
            unit_susceptibility = "1/T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(0.000);
            a2_PARA.push_back(0.000);
            a3_PARA.push_back(0.000);
            a4_PARA.push_back(0.000);
            a5_PARA.push_back(0.000);
            a6_PARA.push_back(0.000);
            a7_PARA.push_back(0.000);
            a8_PARA.push_back(0.000);
            a9_PARA.push_back(0.000);
            a1_2_PARA.push_back(0.000);
            b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PARA"));
            b1_PARA.push_back(0.000);
            b2_PARA.push_back(0.000);
            b3_PARA.push_back(0.000);
            b4_PARA.push_back(0.000);
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PERP"));
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PERP"));
            b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PERP"));
            b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PERP"));
            b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PERP"));
        }
        // M. Ellis' functions parameters
        else if (Susceptibility_Type.back() == "inv_susceptibility_fit") {
            unit_susceptibility = "T";
            Chi_scaling_factor.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:Susceptibility_factor"));
            a0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PARA"));
            a1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PARA"));
            a2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PARA"));
            a3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PARA"));
            a4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PARA"));
            a5_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PARA"));
            a6_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PARA"));
            a7_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PARA"));
            a8_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PARA"));
            a9_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PARA"));
            a1_2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PARA"));
            b0_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PARA"));
            b1_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PARA"));
            b2_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PARA"));
            b3_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PARA"));
            b4_PARA.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PARA"));
            a0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a0_PERP"));
            a1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_PERP"));
            a2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a2_PERP"));
            a3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a3_PERP"));
            a4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a4_PERP"));
            a5_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a5_PERP"));
            a6_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a6_PERP"));
            a7_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a7_PERP"));
            a8_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a8_PERP"));
            a9_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a9_PERP"));
            a1_2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:a1_2_PERP"));
            b0_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b0_PERP"));
            b1_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b1_PERP"));
            b2_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b2_PERP"));
            b3_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b3_PERP"));
            b4_PERP.push_back(Materials_config[Layer].getValueOfKey<double>("Mat:b4_PERP"));
        }
    }
}

//########################ACCESSORS########################//
void Solver_t::set_Chi_size(unsigned size){
    Chi_para.resize(size);
    Chi_perp.resize(size);
}
/** Sets the timestep for the specified solver.
 *
 * @param[in] timestep Timestep to be used by the specified integrator (s).
 * @param[in] desired  Solver for which the timestep will be set.
 */
void Solver_t::set_dt(double timestep, Solvers desired){
    switch(desired)
    {
    case Solvers::kMC:
        dt_kMC = timestep;
        break;
    case Solvers::LLG:
        dt_LLG = timestep;
        break;
    case Solvers::LLB:
        dt_LLB = timestep;
        break;
    }
}
/** Sets the timestep for the currently selected solver.
 *
 * @param[in] timestep Timestep to be used by the specified integrator (s).
 */
void Solver_t::set_dt(double timestep){
    switch(type)
    {
    case Solvers::kMC:
        dt_kMC = timestep;
        break;
    case Solvers::LLG:
        dt_LLG = timestep;
        break;
    case Solvers::LLB:
        dt_LLB = timestep;
        break;
    }
}
/** Returns the timestep of the desired solver.
 *
 * @param[in] desired_solver
 */
double Solver_t::get_dt(Solvers desired_solver) const {
    switch(desired_solver)
    {
    case Solvers::kMC:
        return dt_kMC;
    case Solvers::LLG:
        return dt_LLG;
    case Solvers::LLB:
        return dt_LLB;
    }
    std::cout << "error: Invalid Solver selection in get_dt()" << std::endl;
    exit(EXIT_FAILURE);
    return 0;
}
/** Returns the timestep of the currently selected solver. */
double Solver_t::get_dt() const {
    switch(type)
    {
    case Solvers::kMC:
        return dt_kMC;
    case Solvers::LLG:
        return dt_LLG;
    case Solvers::LLB:
        return dt_LLB;
    }
    std::cout << "error: Invalid Solver selection in get_dt()" << std::endl;
    exit(EXIT_FAILURE);
    return 0;
}
/** Sets the attempt frequency used by the Kinetic Monte Carlo solver.
 *
 * @param[in] F0 Attempt frequency [Hz]
 */
void Solver_t::set_f0(double F0){
    f0 = F0;
}
/** Returns the attempt frequency used by the Kinetic Monte Carlo solver. */
double Solver_t::get_f0() const {
    return f0;
}
/** Set behaviour of the LLG and kMC solvers with regards to the Curie point.
 * When true the desired solver will no longer allow m_eq to equal zero. This enables
 * the LLG/kMC to work beyond the Curie point.
 * Use of these solvers with this behaviour turned off will result in zero devisions producing NaNs.
 *
 * @param[in] option Boolean used to set behaviour
 * @param[in] desired_solver Desired solver
 */
void Solver_t::set_allow_Tc(bool option, Solvers desired_solver){
    switch(desired_solver)
    {
    case Solvers::kMC:
        if(option){
            behaviour_kMC = Behaviours::Thermoremanence;
        } else {
            behaviour_kMC = Behaviours::Standard;
        }
        break;
    case Solvers::LLG:
        if(option){
            behaviour_LLG = Behaviours::Thermoremanence;
        } else {
            behaviour_LLG = Behaviours::Standard;
        }
        break;
    case Solvers::LLB:
        break;
    }
}
/** Set output of magnetisation for kMC solver. If set to true then the magnetisation
 * will be multiplied by m_eq on output. This helps in switching between kMC and LLB as
 * it reduces a possible discontinuity in the magnetisation when switching from unity
 * magnetisation to the variable length magnetisation produced by the LLB.
 *
 * @param option Boolean option to set unit-vector output of the magnetisation.
 */
void Solver_t::set_ml_output(bool option){
    M_length_variation=option;
}
/** Set the enabled solver to be used when integrating.
 *
 * @param new_solver Solver to enable.
 */
void Solver_t::set_Solver(Solvers new_solver){
    type = new_solver;
}
/** Returns the currently enabled solver. */
Solvers Solver_t::get_Solver() const {
    return type;
}
/** Returns the list of damping constants.
 * If none are present will return a vector of size one with a value of unity.*/
std::vector<double> Solver_t::get_Alpha() const {
    if(Alpha.size()>0){
        return Alpha;
    } else {
        return std::vector<double>(1,0.0);
    }
}
/** Returns Gyro-magnetic ratio. */
double Solver_t::get_Gamma() const {
    return Gamma;
}
/** Returns equilibrium magnetisation for the specified grain.
 * @param[in] idx Grain ID.
 */
double Solver_t::get_mEQ(unsigned int idx) const {
    return m_EQ[idx];
}
/** Returns parallel susceptibility for the specified grain.
 * @param[in] idx Grain ID.
 */
double Solver_t::get_ChiPara(unsigned int idx) const {
    return Chi_para[idx];
}
/** Returns perpendicular susceptibiliy for the specified grain.
 * @param[in] idx Grain ID.
 */
double Solver_t::get_ChiPerp(unsigned int idx) const {
    return Chi_perp[idx];
}
/** Returns boolean indicating whether the exclusion zone is enabled for the currently enabled solver. */
bool Solver_t::get_Exclusion() const {
    return Exclusion;
}
/** Returns a string containing all the Parallel susceptibility fits for the desired grain ID. */
std::string Solver_t::get_ChiParaFit(unsigned int idx) const {
    std::string s;
    s = std::to_string(a0_PARA[idx])   + " " + std::to_string(a1_PARA[idx]) + " "
      + std::to_string(a2_PARA[idx])   + " " + std::to_string(a3_PARA[idx]) + " "
      + std::to_string(a4_PARA[idx])   + " " + std::to_string(a5_PARA[idx]) + " "
      + std::to_string(a6_PARA[idx])   + " " + std::to_string(a7_PARA[idx]) + " "
      + std::to_string(a8_PARA[idx])   + " " + std::to_string(a9_PARA[idx]) + " "
      + std::to_string(a1_2_PARA[idx]) + " "
      + std::to_string(b0_PARA[idx]) + " " + std::to_string(b1_PARA[idx]) + " "
      + std::to_string(b2_PARA[idx]) + " " + std::to_string(b3_PARA[idx]) + " "
      + std::to_string(b4_PARA[idx]);
    return s;
}
/** Returns a string containing all the Perpendicular susceptibility fits for the desired grain ID. */
std::string Solver_t::get_ChiPerpFit(unsigned int idx) const {
    std::string s;
    s = std::to_string(a0_PERP[idx])   + " " + std::to_string(a1_PERP[idx]) + " "
      + std::to_string(a2_PERP[idx])   + " " + std::to_string(a3_PERP[idx]) + " "
      + std::to_string(a4_PERP[idx])   + " " + std::to_string(a5_PERP[idx]) + " "
      + std::to_string(a6_PERP[idx])   + " " + std::to_string(a7_PERP[idx]) + " "
      + std::to_string(a8_PERP[idx])   + " " + std::to_string(a9_PERP[idx]) + " "
      + std::to_string(a1_2_PERP[idx]) + " "
      + std::to_string(b0_PERP[idx]) + " " + std::to_string(b1_PERP[idx])   + " "
      + std::to_string(b2_PERP[idx]) + " " + std::to_string(b3_PERP[idx])   + " "
      + std::to_string(b4_PERP[idx]);
    return s;
}
/** Returns the susceptibiliy fit used for the selected layer. */
std::string Solver_t::get_SusType(unsigned int idx) const {
	return Susceptibility_Type[idx];
}
/** Returns the susceptibility fit scaling factor for the selected layer. */
double Solver_t::get_ChiScaling(unsigned int idx) const {
	return Chi_scaling_factor[idx];
}
/** Returns perpendicular susceptibility fit parameters. */
std::vector<double> Solver_t::get_ChiPerpFitIdv(unsigned idx) const {
    std::vector<double> OUT;
    OUT.push_back(Ga0_PERP[idx]);
    OUT.push_back(Ga1_PERP[idx]);
    OUT.push_back(Ga2_PERP[idx]);
    OUT.push_back(Ga3_PERP[idx]);
    OUT.push_back(Ga4_PERP[idx]);
    OUT.push_back(Ga5_PERP[idx]);
    OUT.push_back(Ga6_PERP[idx]); 
    OUT.push_back(Ga7_PERP[idx]);
    OUT.push_back(Ga8_PERP[idx]);
    OUT.push_back(Ga9_PERP[idx]);
    OUT.push_back(Ga1_2_PERP[idx]);
    OUT.push_back(Gb0_PERP[idx]);
    OUT.push_back(Gb1_PERP[idx]);
    OUT.push_back(Gb2_PERP[idx]);
    OUT.push_back(Gb3_PERP[idx]);
    OUT.push_back(Gb4_PERP[idx]);

    return OUT;
}

/** Returns parallel susceptibility fir parameters. */
std::vector<double> Solver_t::get_ChiParaFitIdv(unsigned idx) const {    
     std::vector<double> OUT;
     OUT.push_back(Ga0_PARA[idx]);
     OUT.push_back(Ga1_PARA[idx]);
     OUT.push_back(Ga2_PARA[idx]);
     OUT.push_back(Ga3_PARA[idx]);
     OUT.push_back(Ga4_PARA[idx]);
     OUT.push_back(Ga5_PARA[idx]);
     OUT.push_back(Ga6_PARA[idx]);
     OUT.push_back(Ga7_PARA[idx]);
     OUT.push_back(Ga8_PARA[idx]);
     OUT.push_back(Ga9_PARA[idx]);
     OUT.push_back(Ga1_2_PARA[idx]);
     OUT.push_back(Gb0_PARA[idx]);
     OUT.push_back(Gb1_PARA[idx]);
     OUT.push_back(Gb2_PARA[idx]);
     OUT.push_back(Gb3_PARA[idx]);
     OUT.push_back(Gb4_PARA[idx]);

    return OUT;
}


/** Set damping parameter.
 *
 * @param[in] val
 */
void Solver_t::set_Alpha(double val){
	Alpha.push_back(val);
}
/** Set the fit parameters for Chi parallel.
 *
 * @param[in] a0
 * @param[in] a1
 * @param[in] a2
 * @param[in] a3
 * @param[in] a4
 * @param[in] a5
 * @param[in] a6
 * @param[in] a7
 * @param[in] a8
 * @param[in] a9
 * @param[in] a1_2
 * @param[in] b0
 * @param[in] b1
 * @param[in] b2
 * @param[in] b3
 * @param[in] b4
 */
void Solver_t::set_ChiParaFit(double a0,double a1,double a2,double a3,double a4,
                              double a5, double a6, double a7, double a8,
                              double a9, double a1_2, double b0, double b1,
                              double b2, double b3, double b4){
    a0_PARA.push_back(a0); a1_PARA.push_back(a1); a2_PARA.push_back(a2); a3_PARA.push_back(a3);
    a4_PARA.push_back(a4); a5_PARA.push_back(a5); a6_PARA.push_back(a6); a7_PARA.push_back(a7);
    a8_PARA.push_back(a8); a9_PARA.push_back(a9); a1_2_PARA.push_back(a1_2); b0_PARA.push_back(b0);
    b1_PARA.push_back(b1); b2_PARA.push_back(b2); b3_PARA.push_back(b3); b4_PARA.push_back(b4);
}

void Solver_t::set_ChiParaFitIdv(std::vector<double> Fit){
    Ga0_PARA.push_back(Fit[0]); Ga1_PARA.push_back(Fit[1]); Ga2_PARA.push_back(Fit[2]); Ga3_PARA.push_back(Fit[3]);
    Ga4_PARA.push_back(Fit[4]); Ga5_PARA.push_back(Fit[5]); Ga6_PARA.push_back(Fit[6]); Ga7_PARA.push_back(Fit[7]);
    Ga8_PARA.push_back(Fit[8]); Ga9_PARA.push_back(Fit[9]); Ga1_2_PARA.push_back(Fit[10]); Gb0_PARA.push_back(Fit[11]);
    Gb1_PARA.push_back(Fit[12]); Gb2_PARA.push_back(Fit[13]); Gb3_PARA.push_back(Fit[14]); Gb4_PARA.push_back(Fit[15]);
}
/** Set the fit parameters for Chi perpendicular.
 *
 * @param[in] a0
 * @param[in] a1
 * @param[in] a2
 * @param[in] a3
 * @param[in] a4
 * @param[in] a5
 * @param[in] a6
 * @param[in] a7
 * @param[in] a8
 * @param[in] a9
 * @param[in] a1_2
 * @param[in] b0
 * @param[in] b1
 * @param[in] b2
 * @param[in] b3
 * @param[in] b4
 */
void Solver_t::set_ChiPerpFit(double a0,double a1,double a2,double a3,double a4,
                              double a5, double a6, double a7, double a8,
                              double a9, double a1_2, double b0, double b1,
                              double b2, double b3, double b4){
    a0_PERP.push_back(a0); a1_PERP.push_back(a1); a2_PERP.push_back(a2); a3_PERP.push_back(a3);
    a4_PERP.push_back(a4); a5_PERP.push_back(a5); a6_PERP.push_back(a6); a7_PERP.push_back(a7);
    a8_PERP.push_back(a8); a9_PERP.push_back(a9); a1_2_PERP.push_back(a1_2); b0_PERP.push_back(b0);
    b1_PERP.push_back(b1); b2_PERP.push_back(b2); b3_PERP.push_back(b3); b4_PERP.push_back(b4);
}

void Solver_t::set_ChiPerpFitIdv(std::vector<double> Fit){
     Ga0_PERP.push_back(Fit[0]); Ga1_PERP.push_back(Fit[1]); Ga2_PERP.push_back(Fit[2]); Ga3_PERP.push_back(Fit[3]);
     Ga4_PERP.push_back(Fit[4]); Ga5_PERP.push_back(Fit[5]); Ga6_PERP.push_back(Fit[6]); Ga7_PERP.push_back(Fit[7]);
     Ga8_PERP.push_back(Fit[8]); Ga9_PERP.push_back(Fit[9]); Ga1_2_PERP.push_back(Fit[10]); Gb0_PERP.push_back(Fit[11]);
     Gb1_PERP.push_back(Fit[12]); Gb2_PERP.push_back(Fit[13]); Gb3_PERP.push_back(Fit[14]); Gb4_PERP.push_back(Fit[15]);
}
/** Sets the susceptibility fit type for next layer. 
 *
 * @param[in] idx
 */
void Solver_t::set_SusType(std::string type){
	 Susceptibility_Type.push_back(type);
}
/** Sets the susceptibility scailing fit factor for the next layer.
 *
 * @param[in] val
 */
void Solver_t::set_ChiScaling(double val){
	Chi_scaling_factor.push_back(val);
}
/** Empty damping vector Alpha for new import.
 */
void Solver_t::empty_Alpha(){
	Alpha.clear();
}
/** Empty whole Chi parallel fit parameters vector for new import.
 */
void Solver_t::empty_ChiParaFit(){
    a0_PARA.clear(); a1_PARA.clear(); a2_PARA.clear(); a3_PARA.clear();
    a4_PARA.clear(); a5_PARA.clear(); a6_PARA.clear(); a7_PARA.clear();
    a8_PARA.clear(); a9_PARA.clear(); a1_2_PARA.clear(); b0_PARA.clear();
    b1_PARA.clear(); b2_PARA.clear(); b3_PARA.clear(); b4_PARA.clear();
}
/** Empty whole Chi perpendicular fit parameters vector for new import.
 */
void Solver_t::empty_ChiPerpFit(){
    a0_PERP.clear(); a1_PERP.clear(); a2_PERP.clear(); a3_PERP.clear();
    a4_PERP.clear(); a5_PERP.clear(); a6_PERP.clear(); a7_PERP.clear();
    a8_PERP.clear(); a9_PERP.clear(); a1_2_PERP.clear(); b0_PERP.clear();
    b1_PERP.clear(); b2_PERP.clear(); b3_PERP.clear(); b4_PERP.clear();
}
/** Empty whole susceptibility fit type vector for new import.
 */
void Solver_t::empty_SusType(){
	 Susceptibility_Type.clear();
}
/** Empty whole susceptibility scaling fit factor vector for new import.
 */
void Solver_t::empty_ChiScaling(){
	Chi_scaling_factor.clear();
}
/** Empty all individual susceptibility fit parameters. */
void Solver_t::EmptyChiIdv(){
    Ga0_PARA.clear();
    Ga1_PARA.clear();
    Ga2_PARA.clear();
    Ga3_PARA.clear();
    Ga4_PARA.clear();
    Ga5_PARA.clear();
    Ga6_PARA.clear();
    Ga7_PARA.clear();
    Ga8_PARA.clear();
    Ga9_PARA.clear();
    Ga1_2_PARA.clear();
    Gb0_PARA.clear();
    Gb1_PARA.clear();
    Gb2_PARA.clear();
    Gb3_PARA.clear();
    Gb4_PARA.clear();
    Ga0_PERP.clear();
    Ga1_PERP.clear();
    Ga2_PERP.clear();
    Ga3_PERP.clear();
    Ga4_PERP.clear();
    Ga5_PERP.clear();
    Ga6_PERP.clear();
    Ga7_PERP.clear();
    Ga8_PERP.clear();
    Ga9_PERP.clear();
    Ga1_2_PERP.clear();
    Gb0_PERP.clear();
    Gb1_PERP.clear();
    Gb2_PERP.clear();
    Gb3_PERP.clear();
    Gb4_PERP.clear();
}
/** Set the measurement time for the desired solver.
 *
 * @param[in] Time New measurement time.
 * @param[in] desired_solver Desired solver.
 */
void Solver_t::setMeasTime(double Time, Solvers desired_solver){
    switch(desired_solver)
    {
    case Solvers::kMC:
        Measurement_time_kMC=Time;
        break;
    case Solvers::LLG:
        Measurement_time_LLG=Time;
        break;
    case Solvers::LLB:
        Measurement_time_LLB=Time;
        break;
    }
}
/** Returns the measurement time for the currently enabled solver. */
double Solver_t::getMeasTime() const {
    switch(type)
    {
    case Solvers::kMC:
        return Measurement_time_kMC;
    case Solvers::LLG:
        return Measurement_time_LLG;
    case Solvers::LLB:
        return Measurement_time_LLB;
    }
    std::cout << "error: Invalid Solver selection in getMeasTime()" << std::endl;
    exit(EXIT_FAILURE);
    return 0;
}
/** Returns the measurement time for the specified solver.
 *
 * @param[in] Desired solver.
 */
double Solver_t::getMeasTime(Solvers Desired) const {
    switch(Desired)
    {
    case Solvers::kMC:
        return Measurement_time_kMC;
    case Solvers::LLG:
        return Measurement_time_LLG;
    case Solvers::LLB:
        return Measurement_time_LLB;
    }
    std::cout << "error: Invalid Solver selection in getMeasTime()" << std::endl;
    exit(EXIT_FAILURE);
    return 0;
}
/** Returns the number of integration steps before required to reach the measurement time. */
unsigned int Solver_t::getOutputSteps() const {
    switch(type)
    {
    case Solvers::kMC:
        if(Measurement_time_kMC==0.0){return -1;}
        else if(Measurement_time_kMC<dt_kMC){return 1;}
        return round(Measurement_time_kMC/dt_kMC);
    case Solvers::LLG:
        if(Measurement_time_LLG==0.0){return -1;}
        else if(Measurement_time_LLG<dt_LLG){return 1;}
        return round(Measurement_time_LLG/dt_LLG);
    case Solvers::LLB:
        if(Measurement_time_LLB==0.0){return -1;}
        else if(Measurement_time_LLB<dt_LLB){return 1;}
        return round(Measurement_time_LLB/dt_LLB);
    }
    std::cout << "error: Invalid Solver selection in getOutputSteps()" << std::endl;
    exit(EXIT_FAILURE);
    return 0;
}
/** Disable the exclusion zone behaviour. */
void Solver_t::disableExclusion(){
    Exclusion=false;
}
/** Enable the exclusion zone behaviour. */
void Solver_t::enableExclusion(){
    Exclusion=true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
/** Performs an integration step using the currently enabled solver.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_layers Number of layers in the system.
 * @param[in] Int_sys Magnetic interactions data structure.
 * @param[in] Cx Centre of integration on a-axis (provides position for exclusion zone).
 * @param[in] Cy Centre of integration on y-axis (provides position for exclusion zone).
 * @param[out] Grains Grains data structure.
 */
int Solver_t::Integrate(const Voronoi_t*VORO, unsigned int Num_layers, const Interaction_t*Int_sys, double Cx, double Cy, Grain_t*Grains){

    // TODO fix error reporting (state needs to output 1 if fails and 0 if works).
    int state=1;
    switch(type)
    {
    case Solvers::kMC:
        state=KMC(Num_layers,Int_sys,VORO,Cx,Cy,Grains);
        break;
    case Solvers::LLG:
        state=LLG(Num_layers,Int_sys,VORO,Cx,Cy,Grains);
        break;
    case Solvers::LLB:
        state=LLB(Num_layers,Int_sys,VORO,Cx,Cy,Grains);
        break;
    }
    return state;
}
