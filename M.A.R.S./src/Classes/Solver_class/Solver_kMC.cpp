/*
 * Solver_kMC.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file Solver_kMC.cpp
 * \brief kMC solver function, used to set up and call kMC core. */

#include "math.h"
#include <iostream>
#include <string>

#include "../../../hdr/Classes/Solver.hpp"
/** Wrapper function for the kMC numerical solver. This function handles determination of the exclusion zone
 * and grain parameters (e.g. Exchange and magnetostatic fields, etc.).
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac   Magnetic interactions data structure.
 * @param[in] VORO      Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for exclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for exclusion zone).
 * @param[out] Grain Grains data structure.
 */
int Solver_t::KMC(const unsigned int Num_Layers,const Interaction_t*Interac,
                const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain){
    std::vector<unsigned int> Included_grains_in_layer;
    std::vector<unsigned int> Included_grains_in_system;
    if(Exclusion==false){
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            Included_grains_in_layer.push_back(g);
            Included_grains_in_system.push_back(g);
        }
        for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
            unsigned int offset=VORO->Num_Grains*L;
            for(unsigned int g=0;g<VORO->Num_Grains;++g){
                Included_grains_in_system.push_back(g+offset);
            }
        }
    }
    else{
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            double Distance_X=0.0, Distance_Y=0.0;
            bool In_X=false;
            // Check if within X boundaries
            if(VORO->Pos_X_final[g]>Centre_x){// Positive X boundary
                Distance_X = VORO->Pos_X_final[g] - Centre_x;
                if(Distance_X<=Exclusion_range_posX){In_X=true;}
            }
            else if(VORO->Pos_X_final[g]<Centre_x){// Negative X boundary
                Distance_X = Centre_x - VORO->Pos_X_final[g];
                if(Distance_X<=Exclusion_range_negX){In_X=true;}
            }
            else{In_X=true;}
            // Check Y boundaries if within X boundaries
            if(In_X==true){
                if(VORO->Pos_Y_final[g]>Centre_y){// Positive Y boundary
                    Distance_Y = VORO->Pos_Y_final[g] - Centre_y;
                    if(Distance_Y<=Exclusion_range_posY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else if(VORO->Pos_Y_final[g]<Centre_y){// Negative Y boundary
                    Distance_Y = Centre_y - VORO->Pos_Y_final[g];
                    if(Distance_Y<=Exclusion_range_negY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else{
                    Included_grains_in_layer.push_back(g);
                    Included_grains_in_system.push_back(g);
                }
            }
            for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
                unsigned int offset=VORO->Num_Grains*L;
                for(size_t g=0;g<Included_grains_in_layer.size();++g){
                    Included_grains_in_system.push_back(Included_grains_in_layer[g]+offset);
                }
            }
        }
    }
    size_t Integratable_grains_in_layer = Included_grains_in_layer.size();
    size_t Integratable_grains_in_system = Included_grains_in_system.size();

    m_EQ.resize(Integratable_grains_in_system);

    // Internal variables
    std::vector<Vec3> H_eff (Integratable_grains_in_system), H_magneto (Integratable_grains_in_system),
                      H_exchange (Integratable_grains_in_system);

    // Determine Ms(t) and K(t)
    std::vector<double> K_t(Integratable_grains_in_system,0.0);
    std::vector<double> Ms_t(Integratable_grains_in_system,0.0);
    std::vector<std::vector<double>> m_EQ_exch_neigh(Integratable_grains_in_system);
    std::vector<std::vector<double>> m_EQ_magneto_neigh(Integratable_grains_in_system);
    Chi_para.resize(Num_Layers*VORO->Num_Grains);
    Chi_perp.resize(Num_Layers*VORO->Num_Grains);

/* ####################### WORKING WITH INCLUSION ZONES #######################
 * Due to the inclusion zone, there are multiple arrays to loop through.
 * There are two inclusion zone related arrays "Included_grains_in_layer"
 * and "Included_grains_in_system" for grains per layer and per system.
 * The other two arrays are for the entire system, again per layer and per
 * system.
 * All loops APART from the anisotropy loop only require iteration through
 * "Included_grains_in_system" as there is no difference in behaviour between
 * layers.
 * The anisotropy loop requires iteration through "Included_grains_in_layer" as
 * the ani_method can change from layer to layer.
 * In all loops there must be a conversion from the inclusion zone arrays to
 * the general system arrays, in order to access grain values correctly.
 *
 * This can get confusing so to try and make things less difficult the
 * iterators are names as so:
 *
 * G_in_sys_in_incl - Only grains within inclusion zone but for ALL layers
 *
 * G_in_layer_in_incl - Only grains within inclusion zone but for a single
 *                      layers
 *
 * grain_in_sys - Grains in the entire system for ALL layers
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    if(!T_variation){       // Case where experimental values are used. - NO Ms(t) and K(t)
        for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
            unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
            K_t[G_in_sys_in_incl]= Grain->K[grain_in_sys];
            Ms_t[G_in_sys_in_incl]=Grain->Ms[grain_in_sys];
            for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                m_EQ_exch_neigh[G_in_sys_in_incl].push_back(1.0);
            }
            for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                m_EQ_magneto_neigh[G_in_sys_in_incl].push_back(1.0);
            }
        }
    }
    else{
        for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
            unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
            double Temp = Grain->Temp[grain_in_sys];
            double Tc = Grain->Tc[grain_in_sys];

            for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                m_EQ_exch_neigh[G_in_sys_in_incl].push_back(1.0);
            }
            for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                m_EQ_magneto_neigh[G_in_sys_in_incl].push_back(1.0);
            }

            if(Grain->mEQ_Type[grain_in_sys]=="bulk"){
                switch(behaviour_kMC)
                {
                case Behaviours::Thermoremanence:
                    if(Temp>=0.99999*Tc){
                        m_EQ[G_in_sys_in_incl] = pow(1.0-(0.99999),Grain->Crit_exp[grain_in_sys]);
                    } else {
                        m_EQ[G_in_sys_in_incl] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                case Behaviours::Standard:
                    if(Temp>=Tc){
                        m_EQ[G_in_sys_in_incl]=0.0;
                    } else {
                        m_EQ[G_in_sys_in_incl] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                }
            } else {
                if(Temp<Tc){
                    m_EQ[G_in_sys_in_incl] = Grain->a0_mEQ[grain_in_sys] + Grain->a1_2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,0.5) + Grain->a1_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,1) +
                                         Grain->a2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,2) + Grain->a3_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,3) + Grain->a4_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,4) +
                                         Grain->a5_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,5) + Grain->a6_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,6) + Grain->a7_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,7) +
                                         Grain->a8_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,8) +Grain->a9_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,9);
                }
                else{
                    m_EQ[G_in_sys_in_incl] = 1.0/(1.0/Grain->a0_mEQ[grain_in_sys] + Grain->b1_mEQ[grain_in_sys]*((Tc-Temp)/Tc) + Grain->b2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,2));
                }

            }

            Ms_t[G_in_sys_in_incl]=Grain->Ms[grain_in_sys]*m_EQ[grain_in_sys];
            //#################### FOR GRAIN NEIGHBOURS ####################//
            // Exchange neighbours
            for(size_t neigh=0;neigh<m_EQ_exch_neigh[G_in_sys_in_incl].size();++neigh){
                int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
                double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                if(Grain->mEQ_Type[NEIGHBOUR_ID]=="bulk"){
                    switch(behaviour_kMC)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_exch_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_exch_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_exch_neigh[G_in_sys_in_incl][neigh]=0.0;
                        } else {
                            m_EQ_exch_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                } else {
                    if(Temp_neigh<Tc_neigh){
                        m_EQ_exch_neigh[G_in_sys_in_incl][neigh] = Grain->a0_mEQ[NEIGHBOUR_ID] + Grain->a1_2_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,0.5) + Grain->a1_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,1) +
                                         Grain->a2_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,2) + Grain->a3_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,3) + Grain->a4_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,4) +
                                         Grain->a5_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,5) + Grain->a6_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,6) + Grain->a7_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,7) +
                                         Grain->a8_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,8) +Grain->a9_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,9);
                    }
                    else{
                        m_EQ_exch_neigh[G_in_sys_in_incl][neigh] = 1.0/(1.0/Grain->a0_mEQ[NEIGHBOUR_ID] + Grain->b1_mEQ[NEIGHBOUR_ID]*((Temp_neigh-Tc_neigh)/Tc_neigh) + Grain->b2_mEQ[NEIGHBOUR_ID]*pow((Temp_neigh-Tc_neigh)/Tc_neigh,2));
                    }
                }
            }

            // Magnetostatic neighbours
            for(size_t neigh=0;neigh<m_EQ_magneto_neigh[G_in_sys_in_incl].size();++neigh){
                int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
                double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                if(Grain->mEQ_Type[NEIGHBOUR_ID]=="bulk"){
                    switch(behaviour_kMC)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_magneto_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_magneto_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]=0.0;
                        } else {
                            m_EQ_magneto_neigh[G_in_sys_in_incl][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                } else {
                    if(Temp_neigh<Tc_neigh){
                        m_EQ_magneto_neigh[G_in_sys_in_incl][neigh] = Grain->a0_mEQ[NEIGHBOUR_ID] + Grain->a1_2_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,0.5) + Grain->a1_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,1) +
                                         Grain->a2_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,2) + Grain->a3_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,3) + Grain->a4_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,4) +
                                         Grain->a5_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,5) + Grain->a6_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,6) + Grain->a7_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,7) +
                                         Grain->a8_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,8) +Grain->a9_mEQ[NEIGHBOUR_ID]*pow((Tc_neigh-Temp_neigh)/Tc_neigh,9);
                    }
                    else{
                        m_EQ_magneto_neigh[G_in_sys_in_incl][neigh] = 1.0/(1.0/Grain->a0_mEQ[NEIGHBOUR_ID] + Grain->b1_mEQ[NEIGHBOUR_ID]*((Temp_neigh-Tc_neigh)/Tc_neigh) + Grain->b2_mEQ[NEIGHBOUR_ID]*pow((Temp_neigh-Tc_neigh)/Tc_neigh,2));
                    }
                }
            }
        }
        // Anisotropy - This loop requires separation per layer as the ani_method can differ
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset = Integratable_grains_in_layer*Layer;
            if(Grain->Ani_method[Offset]=="callen"){
                for(unsigned int G_in_layer_in_incl=0;G_in_layer_in_incl<Integratable_grains_in_layer;++G_in_layer_in_incl){
                    unsigned int G_in_sys_in_incl = G_in_layer_in_incl+Offset;
                    unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
                    double Temp = Grain->Temp[grain_in_sys];

                    if (Temp <= Grain->Callen_range_lowT[grain_in_sys]){
                        K_t[G_in_sys_in_incl]=Grain->K[grain_in_sys]*Grain->Callen_factor_lowT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_lowT[grain_in_sys]);
                    }
                    else if (Temp > Grain->Callen_range_midT[grain_in_sys]){
                        K_t[G_in_sys_in_incl]=Grain->K[grain_in_sys]*Grain->Callen_factor_highT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_highT[grain_in_sys]);
                    } else {
                        K_t[G_in_sys_in_incl]=Grain->K[grain_in_sys]*Grain->Callen_factor_midT[grain_in_sys] * pow(m_EQ[grain_in_sys],Grain->Callen_power_midT[grain_in_sys]);
                    }
                }
            }
            else{ // Chi method
                Susceptibilities(VORO->Num_Grains, Num_Layers, &Included_grains_in_layer, Grain);
                for(unsigned int G_in_layer_in_incl=0;G_in_layer_in_incl<Integratable_grains_in_layer;++G_in_layer_in_incl){
                    unsigned int G_in_sys_in_incl = G_in_layer_in_incl+Offset;
                    unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
                    K_t[G_in_sys_in_incl] = (Grain->Ms[grain_in_sys]*pow(m_EQ[G_in_sys_in_incl],2.0))/(2.0*Chi_perp[grain_in_sys]);
                }
            }
        }
    }

//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
        H_magneto[G_in_sys_in_incl]=0.0;
        for(size_t neigh=0;neigh<Interac->Wxx[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
            H_magneto[G_in_sys_in_incl].x += Ms_t[G_in_sys_in_incl]*(Interac->Wxx[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]);
            H_magneto[G_in_sys_in_incl].y += Ms_t[G_in_sys_in_incl]*(Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wyy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]);
            H_magneto[G_in_sys_in_incl].z += Ms_t[G_in_sys_in_incl]*(Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]+
                                                                   Interac->Wzz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[G_in_sys_in_incl][neigh]);
        }
        H_exchange[G_in_sys_in_incl]=0.0;
        for(size_t neigh=0;neigh<Interac->H_exch_str[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[G_in_sys_in_incl]+=Interac->H_exch_str[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID]*m_EQ_exch_neigh[G_in_sys_in_incl][neigh];
        }
    }
    for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain
        H_eff[G_in_sys_in_incl] = Grain->H_appl[grain_in_sys] + H_magneto[G_in_sys_in_incl] + H_exchange[G_in_sys_in_incl];
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(unsigned int G_in_sys_in_incl=0;G_in_sys_in_incl<Integratable_grains_in_system;++G_in_sys_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[G_in_sys_in_incl]; // Set the grain

        double Hk = (2.0*K_t[G_in_sys_in_incl])/Ms_t[G_in_sys_in_incl];
        double KV_over_kBT = (K_t[G_in_sys_in_incl]*Grain->Vol[grain_in_sys]*1.0e-21)/(KB*Grain->Temp[grain_in_sys]);
        double MsVHk_over_kBT = (Ms_t[G_in_sys_in_incl]*Grain->Vol[grain_in_sys]*1.0e-21*Hk)/(KB*Grain->Temp[grain_in_sys]);

        // Perform KMC for a single grain
        KMC_core(Grain->Easy_axis[grain_in_sys],(H_eff[G_in_sys_in_incl]/Hk),KV_over_kBT,MsVHk_over_kBT,&Grain->m[grain_in_sys]);

        // Output magnetisation values with correct lengths if desired -- allows for easier transfer between KMC and LLB solvers
        if(M_length_variation){
            Grain->m[grain_in_sys] *= m_EQ[G_in_sys_in_incl];
        }
    }
    return 0;
}
