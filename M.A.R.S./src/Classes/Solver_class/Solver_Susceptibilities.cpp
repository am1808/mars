/*
 * Solver_Susceptibilities.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Solver_Susceptibilities.cpp
 * \brief This function determines the susceptibility for use in the LLB solver. */

#include <iostream>
#include "math.h"
#include <string>
#include <vector>

#include "../../../hdr/Classes/Solver.hpp"

/** Multiple fitting methods are available within this function. The desired fit is selected in
 *  the material configuration file with the <B>Mat:susceptibility_type</B> parameter. <BR>
 *  The available options are: "default", "kazantseva_susceptibility_fit",
 *  "vogler_scusceptibility_fit" or "inv_scusceptibility_fit".<BR>
 *    <B>If none of these options are found the function will always use the Kazantseva fitting method.</B><BR>
 *  <P>
 *  The kazantseva fitting method originates from "Dynamic response of the magnetisation to picosecond
 *  heat pulses" Natalia Kazantseva 2008 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \tilde{\chi}_{\parallel} =\left \{
 *      \begin{aligned}
 *          a_0 + \sum_{i=1}^{9} &a_i(T_c-T)^i, && \text{if } T < T_c \\
 *             &b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *         \tilde{\chi}_{\bot} =\left \{
 *      \begin{aligned}
 *          a_0 + \sum_{i=1}^{9} &a_i\bigg(1-\frac{T}{1.068T_c}\bigg)^i, && \text{if } T < 1.065T_c \\
 *          &b_0\frac{1}{4\pi}\frac{T}{T-T_c}, && \text{if } T > 1.065T_c
 *         \end{aligned} \right.
 *  \f]
 * <B>If</B><EM> "Default"</EM> is used the following parameters are applied:
 * <center>
 * | Parameter | \f$\chi_{\parallel}\f$ | \f$\chi_{\bot}\f$        |
 * | :-------: | :--------------------: | :----------------------: |
 * |\f$a_0\f$  |\f$2.11\times10^4\f$    |\f$1.21\times10^4\f$      |
 * |\f$a_1\f$  |\f$1.10\times10^6\f$    |\f$-2.20\f$               |
 * |\f$a_2\f$  |\f$-8.55\times10^6\f$   |\f$0.0\f$                 |
 * |\f$a_3\f$  |\f$3.42\times10^6\f$    |\f$1.95\times10^{-6}\f$   |
 * |\f$a_4\f$  |\f$-7.85\times10^7\f$   |\f$-1.3\times10^{-10}\f$  |
 * |\f$a_5\f$  |\f$1.03\times10^8\f$    |\f$0.0\f$                 |
 * |\f$a_6\f$  |\f$-6.86\times10^6\f$   |\f$-4.00\times10^{-16}\f$ |
 * |\f$a_7\f$  |\f$7.97\times10^6\f$    |\f$0.0\f$                 |
 * |\f$a_8\f$  |\f$1.54\times10^7\f$    |\f$0.0\f$                 |
 * |\f$a_9\f$  |\f$-6.27\times10^6\f$   |\f$-6.51\times10^{-25}\f$ |
 * |\f$b_0\f$  |\f$4.85\times10^4\f$    |\f$2.12\times10^4\f$      |
 * </center>
 * <EM>"kazantseva_susceptibility_fit", "vogler_scusceptibility_fit" <EM>and</EM>
 * "inv_scusceptibility_fit"</EM> options use parameters defined in the material configuration files.
 * </P>
 *  The Vogler fitting method originates from Vogler et al, J. Appl. Phys. 119, 223903 (2016) https://doi.org/10.1063/1.4953390 <BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \chi_{\parallel} =\left \{
 *      \begin{aligned}
 *          &\frac{a_0}{T_c-T}, && \text{if } T < T_c \\
 *             &\frac{b_0}{T-T_c}, && \text{if } T \geq T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *         \chi_{\bot} =\left \{
 *      \begin{aligned}
 *          &a_0, && \text{if } T < T_c \\
 *          &b_0, && \text{if } T \geq T_c
 *         \end{aligned} \right.
 *  \f]
 * <P>
 *    The Inverse susceptibility fit originates from "Simulations of magnetic reversal properties in
 *    granular recording media" Matthew Ellis 2015<BR>
 *  The Susceptibilities are given by:
 *  \f[
 *      \frac{1}{\chi_{\parallel}} =\left \{
 *      \begin{aligned}
 *          a_0 + a_{\frac{1}{2}}\bigg(\frac{T_c-T}{T_c}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *          &b_0 + \sum_{i=1}^{4} b_i\bigg(\frac{T-T_c}{T_c}\bigg)^i, && \text{if } T \geq T_c
 *           \end{aligned} \right.
 *    \f]
 *    \f[
 *      \frac{1}{\chi_{\bot}} =\left \{
 *      \begin{aligned}
 *          a_0 + a_{\frac{1}{2}}\bigg(\frac{|T_c-T|}{Tc}\bigg)^{\frac{1}{2}} &+ \sum_{i=1}^{9} a_i\bigg(\frac{T_c-T}{T_c}\bigg)^i, && \text{if } T < T_c \\
 *          &b_0 + \sum_{i=1}^{4} b_i\bigg(\frac{T-T_c}{T_c}\bigg)^i, && \text{if } T \geq T_c
 *         \end{aligned} \right.
 *  \f]
 *    </P>
 *    <P>
 *    \param[in] Num_Grains Number of grains per layer.
 *    \param[in] Num_Layers Number of layers in the system.
 *    \param[in] Grain Struct containing grain specific data.
 */
int Solver_t::Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers,
                               std::vector<unsigned int>*Included_grains_in_layer, const Grain_t*Grain){

    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        int Offset = Num_Grains*Layer; // Needs to be the number of grains per layer
        std::string Susceptibility_type_DUMMY =Susceptibility_Type[Layer];
        // Temporary variables to make code more clean
        double t_Chi_scaling_factor = Chi_scaling_factor[Layer];

        for(unsigned int grain_in_layer=0;grain_in_layer<Included_grains_in_layer->size();++grain_in_layer){
            int grain_in_sys = Included_grains_in_layer->at(grain_in_layer)+Offset;
            double Temperature = Grain->Temp[grain_in_sys], Tc = Grain->Tc[grain_in_sys];
            
            double t_a0_PARA =Ga0_PARA[grain_in_sys], t_a1_PARA =Ga1_PARA[grain_in_sys],
                   t_a2_PARA =Ga2_PARA[grain_in_sys], t_a3_PARA =Ga3_PARA[grain_in_sys],
                   t_a4_PARA =Ga4_PARA[grain_in_sys], t_a5_PARA =Ga5_PARA[grain_in_sys],
                   t_a6_PARA =Ga6_PARA[grain_in_sys], t_a7_PARA =Ga7_PARA[grain_in_sys],
                   t_a8_PARA =Ga8_PARA[grain_in_sys], t_a9_PARA =Ga9_PARA[grain_in_sys],
                   t_a1_2_PARA =Ga1_2_PARA[grain_in_sys];
            double t_b0_PARA =Gb0_PARA[grain_in_sys], t_b1_PARA =Gb1_PARA[grain_in_sys],
                   t_b2_PARA =Gb2_PARA[grain_in_sys], t_b3_PARA =Gb3_PARA[grain_in_sys],
                   t_b4_PARA =Gb4_PARA[grain_in_sys];
            double t_a0_PERP =Ga0_PERP[grain_in_sys], t_a1_PERP =Ga1_PERP[grain_in_sys],
                   t_a2_PERP =Ga2_PERP[grain_in_sys], t_a3_PERP =Ga3_PERP[grain_in_sys],
                   t_a4_PERP =Ga4_PERP[grain_in_sys], t_a5_PERP =Ga5_PERP[grain_in_sys],
                   t_a6_PERP =Ga6_PERP[grain_in_sys], t_a7_PERP =Ga7_PERP[grain_in_sys],
                   t_a8_PERP =Ga8_PERP[grain_in_sys], t_a9_PERP =Ga9_PERP[grain_in_sys],
                   t_a1_2_PERP =Ga1_2_PERP[grain_in_sys];
            double t_b0_PERP =Gb0_PERP[grain_in_sys],
                   t_b1_PERP =Gb1_PERP[grain_in_sys],  t_b2_PERP =Gb2_PERP[grain_in_sys],
                   t_b3_PERP =Gb3_PERP[grain_in_sys],  t_b4_PERP =Gb4_PERP[grain_in_sys];
            

//####################################### DETERMINE THE FITTING METHOD #######################################//
//==================== If susceptibility is default or Kazantseva_susceptibility_fit ========================//
            if(Susceptibility_Type[Layer]=="default" || Susceptibility_Type[Layer]=="kazantseva_susceptibility_fit"){
                if(Temperature<Tc){
                    Chi_para[grain_in_sys] = t_a0_PARA * (Tc/ (4*PI*(Tc-Temperature)) )
                                           + t_a1_PARA * (Tc-Temperature)
                                           + t_a2_PARA * pow((Tc-Temperature),2.0)
                                           + t_a3_PARA * pow((Tc-Temperature),3.0)
                                           + t_a4_PARA * pow((Tc-Temperature),4.0)
                                           + t_a5_PARA * pow((Tc-Temperature),5.0)
                                           + t_a6_PARA * pow((Tc-Temperature),6.0)
                                           + t_a7_PARA * pow((Tc-Temperature),7.0)
                                           + t_a8_PARA * pow((Tc-Temperature),8.0)
                                           + t_a9_PARA * pow((Tc-Temperature),9.0);
                }
                else{Chi_para[grain_in_sys] = t_b0_PARA * (Tc/ (4.0*PI*(Temperature-Tc)) );}
                // Chi_para must not be a negative value.
                if(Chi_para[grain_in_sys]<0){Chi_para[grain_in_sys] -= 1.1*Chi_para[grain_in_sys];}
                Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;

                if(Temperature<1.068*Tc){
                    Chi_perp[grain_in_sys] = t_a0_PERP
                                           + t_a1_PERP*(1.0 - (Temperature/(1.068*Tc)))
                                           + t_a2_PERP*pow((1.0 - (Temperature/(1.068*Tc))),2.0)
                                           + t_a3_PERP*pow((1.0 - (Temperature/(1.068*Tc))),3.0)
                                           + t_a4_PERP*pow((1.0 - (Temperature/(1.068*Tc))),4.0)
                                           + t_a5_PERP*pow((1.0 - (Temperature/(1.068*Tc))),5.0)
                                           + t_a6_PERP*pow((1.0 - (Temperature/(1.068*Tc))),6.0)
                                           + t_a7_PERP*pow((1.0 - (Temperature/(1.068*Tc))),7.0)
                                           + t_a8_PERP*pow((1.0 - (Temperature/(1.068*Tc))),8.0)
                                           + t_a9_PERP*pow((1.0 - (Temperature/(1.068*Tc))),9.0);
                }
                else{Chi_perp[grain_in_sys]= t_b0_PERP * (Tc/ (4.0*PI*(Temperature-Tc)) );}
                Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
            }
//==================== If susceptibility_type == vogler_scusceptibility_fit ==================================//
            else if(Susceptibility_Type[Layer]=="vogler_scusceptibility_fit"){
                // Determine first the inverse of susceptibility
                if(Temperature<Tc){
                    Chi_para[grain_in_sys] = t_a0_PARA/(Tc-Temperature);
                    Chi_perp[grain_in_sys] = t_a0_PERP
                                           + t_a1_PERP*m_EQ[grain_in_sys]
                                           + t_a2_PERP*pow(m_EQ[grain_in_sys],2.0)
                                           + t_a3_PERP*pow(m_EQ[grain_in_sys],3.0)
                                           + t_a4_PERP*pow(m_EQ[grain_in_sys],4.0);
                }
                // if T>=Tc
                else if(Temperature>=Tc){
                    Chi_para[grain_in_sys] = t_b0_PARA/(Temperature-Tc);
                    Chi_perp[grain_in_sys] = t_b0_PERP/(Temperature-Tc);
                }
                // Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor if any
                Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
            }
//==================== If susceptibility_type == inv_scusceptibility_fit =====================================//
            else{
                // Determine first the inverse of the susceptibility
                if(Temperature<Tc){
                    Chi_para[grain_in_sys] = t_a0_PARA
                                           + t_a1_PARA  * (Tc-Temperature)/Tc
                                           + t_a2_PARA  * pow((Tc-Temperature)/Tc,2)
                                           + t_a3_PARA  * pow((Tc-Temperature)/Tc,3)
                                           + t_a4_PARA  * pow((Tc-Temperature)/Tc,4)
                                           + t_a5_PARA  * pow((Tc-Temperature)/Tc,5)
                                           + t_a6_PARA  * pow((Tc-Temperature)/Tc,6)
                                           + t_a7_PARA  * pow((Tc-Temperature)/Tc,7)
                                           + t_a8_PARA  * pow((Tc-Temperature)/Tc,8)
                                           + t_a9_PARA  * pow((Tc-Temperature)/Tc,9)
                                           + t_a1_2_PARA * pow((Tc-Temperature)/Tc,0.5);

                    Chi_perp[grain_in_sys] = t_a0_PERP
                                           + t_a1_2_PERP * pow((Tc-Temperature)/Tc,0.5)
                                           + t_a1_PERP   * (Tc-Temperature)/Tc
                                           + t_a2_PERP   * pow((Tc-Temperature)/Tc,2)
                                           + t_a3_PERP   * pow((Tc-Temperature)/Tc,3)
                                           + t_a4_PERP   * pow((Tc-Temperature)/Tc,4)
                                           + t_a5_PERP   * pow((Tc-Temperature)/Tc,5)
                                           + t_a6_PERP   * pow((Tc-Temperature)/Tc,6)
                                           + t_a7_PERP   * pow((Tc-Temperature)/Tc,7)
                                           + t_a8_PERP   * pow((Tc-Temperature)/Tc,8)
                                           + t_a9_PERP   * pow((Tc-Temperature)/Tc,9);
                }
                // if T>=Tc
                else{
                    Chi_para[grain_in_sys] = t_b0_PARA
                                           + t_b1_PARA * (Temperature-Tc)/Tc
                                           + t_b2_PARA * pow((Temperature-Tc)/Tc,2)
                                           + t_b3_PARA * pow((Temperature-Tc)/Tc,3)
                                           + t_b4_PARA * pow((Temperature-Tc)/Tc,4);

                    Chi_perp[grain_in_sys] = t_b0_PERP
                                           + t_b1_PERP * (Temperature-Tc)/Tc
                                           + t_b2_PERP * pow((Temperature-Tc)/Tc,2)
                                           + t_b3_PERP * pow((Temperature-Tc)/Tc,3)
                                           + t_b4_PERP * pow((Temperature-Tc)/Tc,4);
                }
                // Calculate susceptibility by 1/Chi_para and 1/Chi_perp
                Chi_para[grain_in_sys] = 1.0/Chi_para[grain_in_sys];
                Chi_perp[grain_in_sys] = 1.0/Chi_perp[grain_in_sys];
                // Convert susceptibility in CGS units passing from 1/T to 1/Oe by *1e-4 T/Oe and rescaling by the scaling factor
                Chi_para[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
                Chi_perp[grain_in_sys] *= 1.000e-4*t_Chi_scaling_factor;
            }
        }
    }
    return 0;
}
