/*
 * Solver_LLG.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** \file Solver_LLG.cpp
 * \brief Function for Landau-Lifshitz-Gilbert solver
 */

#include <iostream>
#include <vector>
#include "math.h"
#include <string>

#include "../../../hdr/Classes/Solver.hpp"

/** This function acts on all grains within the system.
 * <P>
 * This function performs a single time step using the LLG equation of motion. This model requires normalisation of the magnetisation thus
 * it breaks down when \f$T \xrightarrow{} T_c\f$ as the ferromagnetic ordering is lost.
 * As a result any simulations which require high temperatures must use the LLB solver instead. <BR>
 * The LLG solver is useful for temperature independent simulations as it utilises larger time steps than the LLB thus reducing computation
 * time. <BR>
 * The maximum possible time step is \f$3ps\f$, however \f$1ps\f$ is the most reliable. (See LLG time step test). <BR>
 * <B> Please note if you utilise the transvser susceptibiliy method for thermal dependence of the anisotropy field a smaller timestep may be required \f$\approx 0.1ps\f$. </B><BR>
 * <P> The equation of motion is:
 * \f[
 *         \frac{\partial{\vec{m}^i}}{\partial{t}} = -\frac{\gamma_e}{1+\alpha^2} \left(\vec{m}^i \times \vec{H_{eff}}^i \right)-\frac{\alpha\gamma_e}{M_s\left(1+\alpha^2\right)} \vec{m}^i \times \left(\vec{m}^i \times \vec{H_{eff}}^i \right)
 * \f]
 * \f$\gamma_e\f$ is the electron gyromagnetic ratio. \f$|\gamma_e|=1.760859644\times10^7 \frac{rad}{Oes}\f$. <BR>
 * \f$\alpha\f$ is the damping constant. <BR>
 * \f$M_{s}\f$ is the saturation magnetisation in \f$\frac{emu}{cc}\f$. <BR>
 * \f$H_{eff}\f$ is the effective field in Oe, which accounts for thermal effects, interactions and applied fields. <BR>
 * \f$\vec{m}\f$ is the magnetisation unit vector. <BR>
 * </P>
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac Magnetic interactions data structure.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for exclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for exclusion zone).
 * @param[out] Grain Grains data structure.
 *
 */
int Solver_t::LLG(const unsigned int Num_Layers,const Interaction_t*Interac,
                const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain){

    // TODO implement the Ml variation (use a normalisation here to fix m<1).
    // TODO add a notification that LLG will not use Chi if LLG and Chi are selected
//################################################## INCLUSION ZONE #########################################################################################//
    std::normal_distribution<double> Gauss(0.0,1.0);
    std::vector<unsigned int> Included_grains_in_layer;
    std::vector<unsigned int> Included_grains_in_system;
    if(Exclusion==false){
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            Included_grains_in_layer.push_back(g);
            Included_grains_in_system.push_back(g);
        }
        for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
            unsigned int offset=VORO->Num_Grains*L;
            for(unsigned int g=0;g<VORO->Num_Grains;++g){
                Included_grains_in_system.push_back(g+offset);
            }
        }
    }
    else{
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            double Distance_X=0.0, Distance_Y=0.0;
            bool In_X=false;

            // Check if within X boundaries
            if(VORO->Pos_X_final[g]>Centre_x){// Positive X boundary
                Distance_X = VORO->Pos_X_final[g] - Centre_x;
                if(Distance_X<=Exclusion_range_posX){In_X=true;}
            }
            else if(VORO->Pos_X_final[g]<Centre_x){// Negative X boundary
                Distance_X = Centre_x - VORO->Pos_X_final[g];
                if(Distance_X<=Exclusion_range_negX){In_X=true;}
            }
            else{In_X=true;}

            // Check Y boundaries if within X boundaries
            if(In_X==true){
                if(VORO->Pos_Y_final[g]>Centre_y){// Positive Y boundary
                    Distance_Y = VORO->Pos_Y_final[g] - Centre_y;
                    if(Distance_Y<=Exclusion_range_posY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else if(VORO->Pos_Y_final[g]<Centre_y){// Negative Y boundary
                    Distance_Y = Centre_y - VORO->Pos_Y_final[g];
                    if(Distance_Y<=Exclusion_range_negY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else{
                    Included_grains_in_layer.push_back(g);
                    Included_grains_in_system.push_back(g);
                }
            }
            for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
                unsigned int offset=VORO->Num_Grains*L;
                for(size_t g=0;g<Included_grains_in_layer.size();++g){
                    Included_grains_in_system.push_back(Included_grains_in_layer[g]+offset);
                }
            }
        }
    }
    size_t Integratable_grains_in_layer = Included_grains_in_layer.size();
    size_t Integratable_grains_in_system = Included_grains_in_system.size();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    m_EQ.resize(Integratable_grains_in_system);

    // Internal variables
    std::vector<double> H_ani (Integratable_grains_in_system,0.0);
    std::vector<Vec3> M_initial (Integratable_grains_in_system), dmdt1 (Integratable_grains_in_system), dmdt2 (Integratable_grains_in_system),
                      Therm_field (Integratable_grains_in_system),H_eff (Integratable_grains_in_system), H_magneto (Integratable_grains_in_system),
                      H_exchange (Integratable_grains_in_system);

    //-- READ IN MAGNETISATION DATA
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        M_initial[grain_in_system] = Grain->m[grain_in_sys];
    }

    // Determine Ms(t) and K(t)
    std::vector<double> K_t(Integratable_grains_in_system,0.0);
    std::vector<double> Ms_t(Integratable_grains_in_system,0.0);
    std::vector<std::vector<double>> m_EQ_exch_neigh(Integratable_grains_in_system);
    std::vector<std::vector<double>> m_EQ_magneto_neigh(Integratable_grains_in_system);

    if(!T_variation){        // Case where experimental values are used. - NO Ms(t) and K(t)
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset = Integratable_grains_in_layer*Layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
                unsigned int grain_in_system = grain_in_layer+Offset;
                unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
                K_t[grain_in_system]= Grain->K[grain_in_sys];
                Ms_t[grain_in_system]=Grain->Ms[grain_in_sys];
                for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_exch_neigh[grain_in_system].push_back(1.0);
                }
                for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_magneto_neigh[grain_in_system].push_back(1.0);
                }
            }
        }
    }
    else{
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int Offset = Integratable_grains_in_layer*Layer;
            for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
                unsigned int grain_in_system = grain_in_layer+Offset;
                unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
                double Temp = Grain->Temp[grain_in_sys];
                double Tc = Grain->Tc[grain_in_sys];

                for(size_t neigh=0;neigh<Interac->Exchange_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_exch_neigh[grain_in_system].push_back(1.0);
                }
                for(size_t neigh=0;neigh<Interac->Magneto_neigh_list[grain_in_sys].size();++neigh){
                    m_EQ_magneto_neigh[grain_in_system].push_back(1.0);
                }

                switch(behaviour_LLG)
                {
                case Behaviours::Thermoremanence:
                    if(Temp>=0.99999*Tc){
                        m_EQ[grain_in_system] = pow(1.0-(0.99999),Grain->Crit_exp[grain_in_sys]);
                    } else {
                        m_EQ[grain_in_system] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                case Behaviours::Standard:
                    if(Temp>=Tc){
                        m_EQ[grain_in_system]=0.0;
                    } else {
                        m_EQ[grain_in_system] = pow(1.0-(Temp/Tc),Grain->Crit_exp[grain_in_sys]);
                    }
                    break;
                }

                // Callen-Callen
                if (Temp <= Grain->Callen_range_lowT[grain_in_sys]){
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_lowT[grain_in_sys] * pow(m_EQ[grain_in_system],Grain->Callen_power_lowT[grain_in_sys]);
                }
                else if (Temp > Grain->Callen_range_midT[grain_in_sys]){
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_highT[grain_in_sys] * pow(m_EQ[grain_in_system],Grain->Callen_power_highT[grain_in_sys]);
                } else {
                    K_t[grain_in_system]=Grain->K[grain_in_sys]*Grain->Callen_factor_midT[grain_in_sys] * pow(m_EQ[grain_in_system],Grain->Callen_power_midT[grain_in_sys]);
                }

                Ms_t[grain_in_system]=Grain->Ms[grain_in_sys]*m_EQ[grain_in_system];

                //#################### FOR GRAIN NEIGHBOURS ####################//
                // Exchange neighbours
                for(size_t neigh=0;neigh<m_EQ_exch_neigh[grain_in_sys].size();++neigh){
                    int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
                    double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                    double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                    double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                    switch(behaviour_LLG)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_exch_neigh[grain_in_system][neigh]=0.0;
                        } else {
                            m_EQ_exch_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                }
                // Magnetostatic neighbours
                for(size_t neigh=0;neigh<m_EQ_magneto_neigh[grain_in_sys].size();++neigh){
                    int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
                    double Temp_neigh     = Grain->Temp[NEIGHBOUR_ID];
                    double Tc_neigh       = Grain->Tc[NEIGHBOUR_ID];
                    double Crit_exp_neigh = Grain->Crit_exp[NEIGHBOUR_ID];

                    switch(behaviour_LLG)
                    {
                    case Behaviours::Thermoremanence:
                        if(Temp_neigh>=0.99999*Tc_neigh){
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(0.99999),Crit_exp_neigh);
                        } else {
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    case Behaviours::Standard:
                        if(Temp_neigh>=Tc_neigh){
                            m_EQ_magneto_neigh[grain_in_system][neigh]=0.0;
                        } else {
                            m_EQ_magneto_neigh[grain_in_system][neigh] = pow(1.0-(Temp_neigh/Tc_neigh),Crit_exp_neigh);
                        }
                        break;
                    }
                }
            }
        }
    }
//################################################## DETERMINE STOCHASTIC TERMS #########################################################################################//
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain

            double RNG_MAG = sqrt( ((2.0*Alpha[Layer]*KB*Grain->Temp[grain_in_sys])/(Gamma*Ms_t[grain_in_system]*(Grain->Vol[grain_in_sys]*1e-21)))/dt_LLG );

            Therm_field[grain_in_system].x = Gauss(Gen)*RNG_MAG;
            Therm_field[grain_in_system].y = Gauss(Gen)*RNG_MAG;
            Therm_field[grain_in_system].z = Gauss(Gen)*RNG_MAG;

            H_ani[grain_in_system] = (2.0*K_t[grain_in_system])/Ms_t[grain_in_system];
    }    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        double MdotE = (Grain->m[grain_in_sys]*Grain->Easy_axis[grain_in_sys]);
        H_eff[grain_in_system] = Grain->H_appl[grain_in_sys]+H_ani[grain_in_system]*MdotE*Grain->Easy_axis[grain_in_sys];
    }
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        H_magneto[grain_in_system] = 0.0;
        for(size_t neigh=0;neigh<Interac->Wxx[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
            H_magneto[grain_in_system].x += Ms_t[grain_in_system]*(Interac->Wxx[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);

            H_magneto[grain_in_system].y += Ms_t[grain_in_system]*(Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);

            H_magneto[grain_in_system].z += Ms_t[grain_in_system]*(Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wzz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);
        }
        H_exchange[grain_in_system] = 0.0;
        for(size_t neigh=0;neigh<Interac->H_exch_str[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[grain_in_system]+=Interac->H_exch_str[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID]*m_EQ_exch_neigh[grain_in_system][neigh];
        }
    }
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        H_eff[grain_in_system] += H_magneto[grain_in_system] + H_exchange[grain_in_system] + Therm_field[grain_in_system];
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### FIRST HEUN STEP ###########################################################################//
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
            Vec3 MxH   = Grain->m[grain_in_sys]%H_eff[grain_in_system];
            Vec3 MxMxH = Grain->m[grain_in_sys]%MxH;
            dmdt1[grain_in_system] = (-Gamma/(1.0+Alpha[Layer]*Alpha[Layer]))*(MxH+Alpha[Layer]*MxMxH);

            Grain->m[grain_in_sys] += dmdt1[grain_in_system]*dt_LLG;
            // Normalise
            double MagM = sqrt(Grain->m[grain_in_sys]*Grain->m[grain_in_sys]);
            Grain->m[grain_in_sys] /= MagM;
    }    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        double MdotE = (Grain->m[grain_in_sys]*Grain->Easy_axis[grain_in_sys]);
        H_eff[grain_in_system] = Grain->H_appl[grain_in_sys]+H_ani[grain_in_system]*MdotE*Grain->Easy_axis[grain_in_sys];
    }
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
        H_magneto[grain_in_system] = 0.0;
        for(size_t neigh=0;neigh<Interac->Wxx[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
            H_magneto[grain_in_system].x += Ms_t[grain_in_system]*(Interac->Wxx[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);

            H_magneto[grain_in_system].y += Ms_t[grain_in_system]*(Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);

            H_magneto[grain_in_system].z += Ms_t[grain_in_system]*(Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ_magneto_neigh[grain_in_system][neigh]+
                                                                   Interac->Wzz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ_magneto_neigh[grain_in_system][neigh]);
        }
        H_exchange[grain_in_system] = 0.0;
        for(size_t neigh=0;neigh<Interac->H_exch_str[grain_in_sys].size();neigh++){
            int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
            H_exchange[grain_in_system]+=Interac->H_exch_str[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID]*m_EQ_exch_neigh[grain_in_system][neigh];
        }
    }
    for(unsigned int grain_in_system=0;grain_in_system<Integratable_grains_in_system;++grain_in_system){
        H_eff[grain_in_system] += H_magneto[grain_in_system] + H_exchange[grain_in_system] + Therm_field[grain_in_system];
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########################################################################### SECOND HEUN STEP ##########################################################################//
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Integratable_grains_in_layer*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Integratable_grains_in_layer;++grain_in_layer){
            unsigned int grain_in_system = grain_in_layer+Offset;
            unsigned int grain_in_sys = Included_grains_in_system[grain_in_system]; // Set the grain
            Vec3 MxH = Grain->m[grain_in_sys]%H_eff[grain_in_system];
            Vec3 MxMxH = Grain->m[grain_in_sys]%MxH;
            dmdt2[grain_in_system] = (-Gamma/(1.0+Alpha[Layer]*Alpha[Layer]))*(MxH+Alpha[Layer]*MxMxH);

            Grain->m[grain_in_sys] = M_initial[grain_in_system] + (dmdt1[grain_in_system]+dmdt2[grain_in_system])*dt_LLG*0.5;
            // Normalise
            double MagM = sqrt(Grain->m[grain_in_sys]*Grain->m[grain_in_sys]);
            Grain->m[grain_in_sys] /= MagM;
    }    }
    return 0;
}
