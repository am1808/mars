/*
 * Solver_LLB.cpp
 *
 *  Created on: 11 May 2020
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file Solver_LLB.cpp
 * \brief Function for Landau-Lifshitz-Bloch solver
 */

#include <iostream>
#include <math.h>
#include <random>
#include <iomanip>

#include "../../../hdr/Classes/Solver.hpp"

#ifndef DOXYGEN_SHOULD_SKIP_THIS

std::vector<Vec3> Solver_t::LLB_STEP(unsigned int Num_Grains, unsigned int Num_Layers,unsigned int Integratable_grains_in_layer,
        std::vector<unsigned int>*Included_grains_in_layer,std::vector<unsigned int>*Included_grains_in_system,
        std::vector<Vec3>*RNG_PARA_NUM,std::vector<Vec3>*RNG_PERP_NUM,const Interaction_t*Interac,Grain_t*Grain){


    unsigned int Offset=0;
    size_t Integratable_grains_in_system = Included_grains_in_system->size();
    std::vector<Vec3> dmdt (Integratable_grains_in_system);
// grain_in_sys refers to global grain ID
//		m_EQ, Alpha_PARA, Alpha_PERP
// grain_in_incl refers to index of grains within inclusion zone
//		
//################################################################# DETERMINE TEMPERATURE DEPENDENT PARAMETERS ##########################################################//
    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        Offset = Num_Grains*Layer;
        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            unsigned int grain_in_sys = grain_in_layer+Offset;
            double Temperature = Grain->Temp[grain_in_sys];
            double Tc = Grain->Tc[grain_in_sys];

            if(Temperature<Tc){
                if(Grain->mEQ_Type[grain_in_sys]=="bulk"){
                    m_EQ[grain_in_sys] = pow(1.0-(Temperature/Tc),Grain->Crit_exp[grain_in_sys]); // PAPER
                }
                else if(Grain->mEQ_Type[grain_in_sys]=="polynomial"){
                    m_EQ[grain_in_sys] = Grain->a0_mEQ[grain_in_sys] + Grain->a1_2_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,0.5) + Grain->a1_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,1) +
                                                      Grain->a2_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,2) + Grain->a3_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,3) + Grain->a4_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,4) +
                                                      Grain->a5_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,5) + Grain->a6_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,6) + Grain->a7_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,7) +
                                                      Grain->a8_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,8) +Grain->a9_mEQ[grain_in_sys]*pow((Tc-Temperature)/Tc,9);
                }
                Alpha_PARA[grain_in_sys] = Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
                Alpha_PERP[grain_in_sys] = Alpha[Layer]*(1.0-(Temperature/(3.0*Tc)));
            }
            else{
                if(Grain->mEQ_Type[grain_in_sys]=="bulk"){
                    m_EQ[grain_in_sys] = 0.01; //  To avoid numerical issues
                }
                else if(Grain->mEQ_Type[grain_in_sys]=="polynomial"){
                    m_EQ[grain_in_sys] = 1.0/(1.0/Grain->a0_mEQ[grain_in_sys] + Grain->b1_mEQ[grain_in_sys]*((Temperature-Tc)/Tc) + Grain->b2_mEQ[grain_in_sys]*pow((Temperature-Tc)/Tc,2));
                }
                Alpha_PARA[grain_in_sys] = Alpha[Layer]*(2.0*Temperature)/(3.0*Tc);
                Alpha_PERP[grain_in_sys] = Alpha_PARA[grain_in_sys];
    		}  
    	}
    }
    // This is called for all grains (easier to code)
    Susceptibilities(Num_Grains, Num_Layers, Included_grains_in_layer, Grain);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE STOCHASTIC TERMS ######################################################################//
    double m_MAG_SQ=0.0;
    std::vector<Vec3> RNG_PARA (Integratable_grains_in_system), RNG_PERP (Integratable_grains_in_system);
    std::vector<double> H_ani (Integratable_grains_in_system), Internal_Exchange_field (Integratable_grains_in_system);

    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system->at(grain_in_incl); // Set the grain

        double Temperature = Grain->Temp[grain_in_sys];
        double Tc = Grain->Tc[grain_in_sys];
        double m_EQ_dummy = m_EQ[grain_in_sys];
        double Chi_PARA_dummy = Chi_para[grain_in_sys];
        double Alpha_PARA_dummy = Alpha_PARA[grain_in_sys];
        double Alpha_PERP_dummy = Alpha_PERP[grain_in_sys];

        double RNG_PARA_MAG = sqrt((Gamma*2.0*KB*Temperature*Alpha_PARA_dummy) / (dt_LLB*Grain->Ms[grain_in_sys]*(Grain->Vol[grain_in_sys]*1e-21)));
        RNG_PARA[grain_in_incl] = RNG_PARA_NUM->at(grain_in_incl)*RNG_PARA_MAG;
        double RNG_PERP_MAG = sqrt( (2.0*KB*Temperature*(Alpha_PERP_dummy-Alpha_PARA_dummy)) / (Gamma*Grain->Ms[grain_in_sys]*(Grain->Vol[grain_in_sys]*1e-21)*dt_LLB*(Alpha_PERP_dummy*Alpha_PERP_dummy)) );
        RNG_PERP[grain_in_incl] = RNG_PERP_NUM->at(grain_in_incl)*RNG_PERP_MAG;

        m_MAG_SQ = Grain->m[grain_in_sys]*Grain->m[grain_in_sys];

        if (Temperature <= Grain->Callen_range_lowT[grain_in_sys]){
            H_ani[grain_in_incl] = ((2.0*Grain->K[grain_in_sys])/Grain->Ms[grain_in_sys]) * Grain->Callen_factor_lowT[grain_in_sys] * pow(m_EQ_dummy,Grain->Callen_power_lowT[grain_in_sys]-1.0);
        }
        else if (Temperature > Grain->Callen_range_lowT[grain_in_sys] && Temperature <= Grain->Callen_range_midT[grain_in_sys]){
            H_ani[grain_in_incl] = ((2.0*Grain->K[grain_in_sys])/Grain->Ms[grain_in_sys]) * Grain->Callen_factor_midT[grain_in_sys] * pow(m_EQ_dummy,Grain->Callen_power_midT[grain_in_sys]-1.0);
        }
        else if (Temperature > Grain->Callen_range_midT[grain_in_sys]){
            H_ani[grain_in_incl] = ((2.0*Grain->K[grain_in_sys])/Grain->Ms[grain_in_sys]) * Grain->Callen_factor_highT[grain_in_sys] * pow(m_EQ_dummy,Grain->Callen_power_highT[grain_in_sys]-1.0);
        }

        if(Temperature <= Tc){
        	Internal_Exchange_field[grain_in_incl] = (1.0/(2.0*Chi_PARA_dummy))*(1.0-(m_MAG_SQ/(m_EQ_dummy*m_EQ_dummy)));
        }
        else{
        	Internal_Exchange_field[grain_in_incl] = (-1.0/Chi_PARA_dummy)*(1.0+((3.0*Tc)/(5.0*(Temperature-Tc)))*m_MAG_SQ);
        }
    }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################################################### DETERMINE EFFECTIVE H-FIELD #####################################################################//
    double MdotE=0.0;
    std::vector<Vec3> H_eff (Integratable_grains_in_system), H_magneto (Integratable_grains_in_system), H_exchange (Integratable_grains_in_system);

    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system->at(grain_in_incl); // Set the grain
        if(Grain->Ani_method[grain_in_sys]=="callen"){
            MdotE = (Grain->m[grain_in_sys]*Grain->Easy_axis[grain_in_sys])/sqrt(Grain->m[grain_in_sys]*Grain->m[grain_in_sys]);
            H_eff[grain_in_incl] = Internal_Exchange_field[grain_in_incl]*Grain->m[grain_in_sys]+Grain->H_appl[grain_in_sys]+H_ani[grain_in_incl]*MdotE*Grain->Easy_axis[grain_in_sys];
        }
        else{
            const double Chi_perp_inv = 1.0 / Chi_perp[grain_in_sys];
            // TODO Implement LN distribution of Chi_perp_inv sigma is an input and mu is Chi_perp_inv
            H_eff[grain_in_incl].x = Internal_Exchange_field[grain_in_incl]*Grain->m[grain_in_sys].x+Grain->H_appl[grain_in_sys].x-Grain->m[grain_in_sys].x*Chi_perp_inv;
            H_eff[grain_in_incl].y = Internal_Exchange_field[grain_in_incl]*Grain->m[grain_in_sys].y+Grain->H_appl[grain_in_sys].y-Grain->m[grain_in_sys].y*Chi_perp_inv;
            H_eff[grain_in_incl].z = Internal_Exchange_field[grain_in_incl]*Grain->m[grain_in_sys].z+Grain->H_appl[grain_in_sys].z;
        }
    }
    for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system->at(grain_in_incl); // Set the grain
        H_magneto[grain_in_incl] = 0.0;
        for(size_t neigh=0;neigh<Interac->Wxx[grain_in_sys].size();neigh++){
            unsigned int NEIGHBOUR_ID = Interac->Magneto_neigh_list[grain_in_sys][neigh];
            H_magneto[grain_in_incl].x += Grain->Ms[grain_in_sys]*(Interac->Wxx[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ[NEIGHBOUR_ID]);

            H_magneto[grain_in_incl].y += Grain->Ms[grain_in_sys]*(Interac->Wxy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wyy[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ[NEIGHBOUR_ID]);

            H_magneto[grain_in_incl].z += Grain->Ms[grain_in_sys]*(Interac->Wxz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].x*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wyz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].y*m_EQ[NEIGHBOUR_ID]+
                                                                  Interac->Wzz[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID].z*m_EQ[NEIGHBOUR_ID]);
            }
            H_exchange[grain_in_incl] = 0.0;
            for(size_t neigh=0;neigh<Interac->H_exch_str[grain_in_sys].size();neigh++){
                unsigned int NEIGHBOUR_ID = Interac->Exchange_neigh_list[grain_in_sys][neigh];
                H_exchange[grain_in_incl]+=Interac->H_exch_str[grain_in_sys][neigh]*Grain->m[NEIGHBOUR_ID]*m_EQ[NEIGHBOUR_ID];
        }    }
        for(unsigned int grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
            H_eff[grain_in_incl]+=H_magneto[grain_in_incl]+H_exchange[grain_in_incl];
        }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    for(size_t grain_in_incl=0;grain_in_incl<Included_grains_in_system->size();++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system->at(grain_in_incl); // Set the grain

        dmdt[grain_in_incl] = Gamma*(-1.0*(Grain->m[grain_in_sys]%H_eff[grain_in_incl])
                           + (Alpha_PARA[grain_in_sys]/(Grain->m[grain_in_sys]*Grain->m[grain_in_sys]))*((Grain->m[grain_in_sys]*H_eff[grain_in_incl])*Grain->m[grain_in_sys])
                           - (Alpha_PERP[grain_in_sys]/(Grain->m[grain_in_sys]*Grain->m[grain_in_sys])*(Grain->m[grain_in_sys]%(Grain->m[grain_in_sys]%(H_eff[grain_in_incl]+RNG_PERP[grain_in_incl])))))
                           + RNG_PARA[grain_in_incl];
    }
    return dmdt;
}

#endif /* DOXYGEN_SHOULD_SKIP_THIS */


/** This function acts on all grains within the system.
 * <P>
 * The LLB requires multiple divisions by \f$(T-T_c)\f$ thus if \f$T=T_c\f$ a zero division occurs producing undefined behaviour.
 * To prevent crashes the temperature is compared to the Curie point and incremented by 0.00001K if they are equal.
 * This may produce undesired behaviour or negatively impact simulation results, as such a warning is output to inform the user this has
 * occurred. To prevent this behaviour it is best to specify the Curie point to a high degree of accuracy (i.e. 3+ decimal places).
 * </P>
 * <P>
 * The LLB solver is valid for all temperatures. The specific version implemented here is the
 * sLLB-II (See R.F.L. Evans et al Phys. Rev. B 85, 014433 https://doi.org/10.1103/PhysRevB.85.014433).
 * This version of the LLB provides the correct Boltzmann distribution for all temperatures, whereas
 * the sLLB-I fails to do so at \f$T=T_c\f$. While the LLB is valid for all temperature ranges, it requires
 * greater calculations per time step and also smaller time steps than the LLG. <BR>
 * The most stable time step is \f$1fs\f$ (See LLB time step test.)
 * </P>
 * <P> The sLLB-II is:
 * \f[
 *         \frac{\partial{\vec{m}^i}}{\partial{t}}=
 *             \gamma_e \left( {\vec{m}^i} \times \vec{Heff}^i \right)
 *           -
 *           \frac{\gamma_e\alpha_{\parallel}}{{m^i}^2} \left(\vec{m}^i \cdot \vec{Heff}^i \right) \vec{m}^i
 *           +
 *           \frac{\gamma_e\alpha_{\bot}}{{m^i}^2} \left[ \vec{m}^i \times \left( \vec{m}^i \times \left(\vec{Heff}^i
 *           +
 *           \vec{\zeta}_{\bot}\right) \right) \right] + \vec{\zeta}_{ad}
 * \f]
 * \f$\vec{m}\f$ is the magnetisation unit vector. <BR>
 * \f$\vec{e}\f$ is the easy axis unit vector. <BR>
 * \f$\gamma_e\f$ is the electron gyromagnetic ratio. \f$|\gamma_e|=1.760859644\times10^7 \frac{rad}{Oes}\f$. <BR>
 * \f$\alpha_{\parallel}\f$ and \f$\alpha_{\bot}\f$ are the parallel and perpendicular damping constants:
 * \f[
 *     \alpha_{\parallel} = \frac{2}{3}\frac{T}{T_c}\lambda\quad\text{ and }\quad \left \{
 *           \begin{aligned}
 *               \alpha_{\bot} = \lambda\left(1-\frac{T}{3T_c}\right), && \text{if } T \leq T_c \\
 *               \alpha_{\bot} = \alpha_{\parallel} = \frac{2}{3}\frac{T}{T_c}\lambda, && \text{otherwise. }
 *           \end{aligned} \right.
 * \f]
 * \f$H_{eff}\f$ is the effective field in Oe, which accounts for intragrain exchange, interactions and applied fields. <BR>
 * \f[
 *         H_{intragrain} = \left \{
 *           \begin{aligned}
 *               \frac{1}{2\chi_{\parallel}}(1-\frac{m^2}{m_e^2}) && \text{if } T \leq T_c \\
 *                -\bigg(\frac{1}{\chi_{\parallel}}+\frac{3T_c}{5(T-T_c)\chi_{\parallel}}m^2\bigg) && \text{otherwise.}
 *           \end{aligned} \right.
 * \f]
 * \f$\chi_{\parallel}\f$ is the parallel susceptibility. <BR>
 * For more information see
 * int Solver_t::Susceptibilities(const unsigned int Num_Grains, const unsigned int Num_Layers, const std::vector<unsigned int>*Included_grains_in_system, const Grain_t*Grain) <BR>
 * The thermal dependency of anisotropy and magnetisation can be accounted for via two methods. Both methods
 * are implemented by varying the anisotropy field directly. <BR>
 * <P> Method one: Callen-Callen scailing. <BR>
 * The anisotropy field is given by:
 * \f[
 *         H_{ani} = \frac{2K}{M_s}
 * \f]
 *
 * The temperature dependence of anisotropy is described via Callen-Callen scailing:
 * \f[
 *         K(T) = K_0m_{e}^\gamma
 * \f]
 * \f$m_e\f$ is the equilibrium magnetisation and \f$\gamma\f$ is the material dependent critical exponent.
 * Combining these equations results in:
 * \f[
 *         H_{ani}(T) = \frac{2K}{M_s}m_{e}^{\gamma-1}
 * \f]
 * Finally the vector form for the anisotropy magnetisation used here is:
 * \f[
 *         \vec{H_{ani}} = H_{ani} (\vec{m}\bullet \vec{e}) \vec{e}
 * \f]
 * </P>
 * <P> Method two: Transverse susceptibility. <BR>
 * The Anisotropy field is directed along the z-axis and is given by:.
 * \f[
 *          \vec{H_{ani}} = \frac{-(m_x+m_y)}{\chi_{\bot}}
 * \f]
 * </P>
 * <P><B>
 * While the user is given the option to use either method, it should be noted that Callen-Callen
 * scailing produces a fictitious longitudinal component of the anisotropy at elevated temperatures.
 * Thus the transverse susceptibility method is optimal, however for soft systems where obtaining
 * the transverse susceptibility is not possible Callen-Callen scailing provides a simple and useful alternative.
 * </B></P>
 * The equilibrium magnetisation \f$m_e\f$ is determined via:
 * \f[
 *         m_e(T) = \left \{
 *          \begin{aligned}
 *                   \bigg(1-\frac{T}{T_c}\bigg)^{\beta}, && \text{if } T \leq T_c \\
 *                   0, && \text{otherwise.}
 *          \end{aligned} \right.
 * \f]
 * \f$\zeta_{ad}\f$ and \f$\zeta_{\bot}\f$ are the diffusion coefficients:
 * \f[
 *         \begin{aligned}
 *             <\zeta_{ad}^{i}(t)\zeta_{ad}^{j}(t-t^\prime)> = \frac{2k_{B}T\alpha_{\parallel}}{\gamma MsV}\delta_{ij}\delta_{ad}\delta(t) \\
 *          <\zeta_{\bot}^{i}(t)\zeta_{\bot}^{j}(t-t^\prime)> = \frac{2k_{B}T(\alpha_{\bot}-\alpha_{\parallel})}{\gamma MsV\alpha_{\bot}^2}\delta_{ij}\delta_{ad}\delta(t)
 *      \end{aligned}
 * \f]
 * \f$M_s\f$ is the saturation magnetisation in emu/cc. <BR>
 * \f$V\f$ is the grain volume in cc. <BR>
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] Interac Magnetic interactions data structure.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Centre_x Centre of integration on a-axis (provides position for exclusion zone).
 * @param[in] Centre_y Centre of integration on y-axis (provides position for exclusion zone).
 * @param[out] Grain Grains data structure.
 *
 */

int Solver_t::LLB(const unsigned int Num_Layers,const Interaction_t*Interac,
                  const Voronoi_t*VORO,const double Centre_x,const double Centre_y,Grain_t*Grain){

    std::vector<unsigned int> Included_grains_in_layer;
    std::vector<unsigned int> Included_grains_in_system;
    std::normal_distribution<double> Gauss(0.0,1.0);

    if(Exclusion==false){
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            Included_grains_in_layer.push_back(g);
            Included_grains_in_system.push_back(g);
        }
        for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
            unsigned int offset=VORO->Num_Grains*L;
            for(unsigned int g=0;g<VORO->Num_Grains;++g){
                Included_grains_in_system.push_back(g+offset);
            }
        }
    }
    else{
        for(unsigned int g=0;g<VORO->Num_Grains;++g){
            double Distance_X=0.0, Distance_Y=0.0;
            bool In_X=false;

            // Check if within X boundaries
            if(VORO->Pos_X_final[g]>Centre_x){// Positive X boundary
                Distance_X = VORO->Pos_X_final[g] - Centre_x;
                if(Distance_X<=Exclusion_range_posX){In_X=true;}
            }
            else if(VORO->Pos_X_final[g]<Centre_x){// Negative X boundary
                Distance_X = Centre_x - VORO->Pos_X_final[g];
                if(Distance_X<=Exclusion_range_negX){In_X=true;}
            }
            else{In_X=true;}

            // Check Y boundaries if within X boundaries
            if(In_X==true){
                if(VORO->Pos_Y_final[g]>Centre_y){// Positive Y boundary
                    Distance_Y = VORO->Pos_Y_final[g] - Centre_y;
                    if(Distance_Y<=Exclusion_range_posY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else if(VORO->Pos_Y_final[g]<Centre_y){// Negative Y boundary
                    Distance_Y = Centre_y - VORO->Pos_Y_final[g];
                    if(Distance_Y<=Exclusion_range_negY){
                        Included_grains_in_layer.push_back(g);
                        Included_grains_in_system.push_back(g);
                    }
                }
                else{
                    Included_grains_in_layer.push_back(g);
                    Included_grains_in_system.push_back(g);
                }
            }
            for(unsigned int L=1;L<Num_Layers;++L){ // Only perform if Num_Layers>1
                unsigned int offset=VORO->Num_Grains*L;
                for(size_t g=0;g<Included_grains_in_layer.size();++g){
                    Included_grains_in_system.push_back(Included_grains_in_layer[g]+offset);
                }
            }
        }
    }
    size_t Integratable_grains_in_layer = Included_grains_in_layer.size();
    size_t Integratable_grains_in_system = Included_grains_in_system.size();

    // Local parameters
    std::vector<Vec3> M_initial (Integratable_grains_in_system), dmdt1 (Integratable_grains_in_system), dmdt2 (Integratable_grains_in_system),
                      RNG_PARA (Integratable_grains_in_system), RNG_PERP (Integratable_grains_in_system),
                      RNG_PARA_NUM (Integratable_grains_in_system), RNG_PERP_NUM (Integratable_grains_in_system),
                      H_eff (Integratable_grains_in_system), H_magneto (Integratable_grains_in_system), H_exchange (Integratable_grains_in_system);
    std::vector<double>  Internal_Exchange_field (Integratable_grains_in_system,0.0), H_ani (Integratable_grains_in_system,0.0);
    unsigned int Tot_grains=VORO->Num_Grains*Num_Layers;
    // Resize LLB vectors
    Chi_para.resize(Tot_grains);
    Chi_perp.resize(Tot_grains);
    Alpha_PARA.resize(Tot_grains);
    Alpha_PERP.resize(Tot_grains);
    m_EQ.resize(Tot_grains);

    //-- READ IN MAGNETISATION DATA
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        M_initial[grain_in_incl] = Grain->m[grain_in_sys];
        // Save random numbers
        RNG_PARA_NUM[grain_in_incl].x = Gauss(Gen);
        RNG_PARA_NUM[grain_in_incl].y = Gauss(Gen);
        RNG_PARA_NUM[grain_in_incl].z = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].x = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].y = Gauss(Gen);
        RNG_PERP_NUM[grain_in_incl].z = Gauss(Gen);
    }
//################################################################# PREVENT TEMPERATURE EQUAL TO TC #########################################################//
    // The LLB fails when T=Tc, this condition ensures T=/=Tc. However this may cause unexpected behaviour so a warning needs to be displayed
    // each time this condition is met. The best way to overcome this limitation of the LLB is to define Tc to a high precision.
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        // Ensure T != Tc as solution will be infinity.
        if(Grain->Temp[grain_in_sys]==Grain->Tc[grain_in_sys]){
            std::cout << "\nWARNING: Temp=Tc for grain '" << grain_in_sys << "' increasing grain temperature by 0.00001K." << std::endl;
            Grain->Temp[grain_in_sys]+=0.00001;
    }    }
//###########################################################################################################################################################//

    // First step
    dmdt1 = LLB_STEP(VORO->Num_Grains,Num_Layers,Integratable_grains_in_layer,&Included_grains_in_layer,&Included_grains_in_system,
                     &RNG_PARA_NUM,&RNG_PERP_NUM,Interac,Grain);
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grain->m[grain_in_sys] += dmdt1[grain_in_incl]*dt_LLB;
        if(Grain->m[grain_in_sys].x > 1.1 || Grain->m[grain_in_sys].y > 1.1 || Grain->m[grain_in_sys].z>1.1){
            std::cout << "\nLLB error: magnetisation exceeding unity. " << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    // Second step
    dmdt2 = LLB_STEP(VORO->Num_Grains,Num_Layers,Integratable_grains_in_layer,&Included_grains_in_layer,&Included_grains_in_system,
                     &RNG_PARA_NUM,&RNG_PERP_NUM,Interac,Grain);
    for(size_t grain_in_incl=0;grain_in_incl<Integratable_grains_in_system;++grain_in_incl){
        unsigned int grain_in_sys = Included_grains_in_system[grain_in_incl]; // Set the grain
        Grain->m[grain_in_sys] = M_initial[grain_in_incl] + (dmdt1[grain_in_incl]+dmdt2[grain_in_incl])*dt_LLB*0.5;
        if(std::isnan(Grain->m[grain_in_sys].x) || std::isnan(Grain->m[grain_in_sys].y) || std::isnan(Grain->m[grain_in_sys].z)){
            std::cout << "\nLLB error: NaN magnetisation. " << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}



