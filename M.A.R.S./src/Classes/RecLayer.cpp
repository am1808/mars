/*
 * RecLayer.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecLayer.cpp
 * @brief Definitions of RecLayer class methods. */

#include <fstream>
#include "../../hdr/Classes/RecLayer.hpp"
/** The constructor initialises the class using the data from the configuration file and
 * the applied field direction. The applied field direction is used to convert the binary
 * input into field direction values.
 *
 * @param[in] cfg Configuration file data.
 * @param[in] H_zdir Direction of applied field in z-direction.
 */
RecLayer::RecLayer(const ConfigFile cfg, double H_zdir)
    : Grain_in_bit(0)
{
    Bit_number_X = cfg.getValueOfKey<unsigned int>("RecordingLayer:Bit_per_track");
    Bit_number_Y = cfg.getValueOfKey<unsigned int>("RecordingLayer:Tracks");
    Bit_size_X   = cfg.getValueOfKey<double>("RecordingLayer:Bit_width");
    Bit_size_Y   = cfg.getValueOfKey<double>("RecordingLayer:Bit_length");
    Bit_spacing_X = cfg.getValueOfKey<double>("RecordingLayer:Bit_spacing");
    Bit_spacing_Y = cfg.getValueOfKey<double>("RecordingLayer:Track_spacing");

    if(cfg.getValueOfKey<std::string>("WritableData:DataType")=="square-wave"){
        Writable_data.resize((Bit_number_X*Bit_number_Y));
        for(unsigned int i=0;i<(Bit_number_X*Bit_number_Y);++i){
            Writable_data[i]=fabs(H_zdir)>1.0e-4 ? H_zdir/fabs(H_zdir): 0;
            H_zdir *= -1;
        }
    }
    else if(cfg.getValueOfKey<std::string>("WritableData:DataType")=="binary"){
        // Open and read in digits from file
        std::string File_location = "Input/" + cfg.getValueOfKey<std::string>("WritableData:Data_Location");
        std::ifstream BINARY_DATA(File_location.c_str());
        if(!BINARY_DATA){throw std::runtime_error("CFG error: " + File_location + " not found"  + "\n");}
        int value;
        Writable_data.resize((Bit_number_X*Bit_number_Y));
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
        for(unsigned int i=0;i<(Bit_number_X*Bit_number_Y);++i){
            value = BINARY_DATA.get();
            if(isdigit(value)){value = value - '0';}
            if(value==0){Writable_data[i]=abs(H_zdir)>1.0e-4 ? -1*H_zdir/fabs(H_zdir) : 0;}
            else{Writable_data[i]=fabs(H_zdir)>1.0e-4 ? H_zdir/fabs(H_zdir): 0;}
        }
    } else {
        std::cout << " CFG error: Unknown Data write type. -> " << cfg.getValueOfKey<std::string>("WritableData:DataType") << std::endl;
        exit (EXIT_FAILURE);
    }
}
/** Default deconstructor. */
RecLayer::~RecLayer(){}
/** Determine  and create a list of the grains within each bit.
 *
 * @param[in] Num_Grains Number of grains within a layer.
 * @param[in] PosX List of x-positions of all grains.
 * @param[in] PosY List of y-positions of all grains.
 * @param[in] Bit_positions List of positions of all bits.
 */
void RecLayer::setGrainsinBit(const unsigned int Num_Grains,const std::vector<double> PosX,
        const std::vector<double> PosY,const std::vector<std::pair<double,double>> Bit_positions){

    Grain_in_bit.resize(Bit_positions.size());

    for(unsigned int Bit=0;Bit<Bit_positions.size();++Bit){
        double bit_position_X=Bit_positions[Bit].first;
        double bit_position_Y=Bit_positions[Bit].second;

        for(unsigned int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
            if(PosX[grain_in_layer]>(bit_position_X-Bit_size_X*0.5) && PosX[grain_in_layer]<(bit_position_X+Bit_size_X*0.5)){
                if(PosY[grain_in_layer]>(bit_position_Y-Bit_size_Y*0.5) && PosY[grain_in_layer]<(bit_position_Y+Bit_size_Y*0.5)){
                    Grain_in_bit[Bit].push_back(grain_in_layer);
                }
            }
        }
    }
}
/** Returns a lost of the grains within the bit specified.
 *
 * @param[in] idx Bit index.
 */
std::vector<unsigned int> RecLayer::getGrainsinBit(const unsigned int idx) const {
    return Grain_in_bit[idx];
}
/** Sets the number of bits in a track. */
void RecLayer::setBitsX(unsigned int val){Bit_number_X = val;}
/** Sets the number of tracks within the system. */
void RecLayer::setBitsY(unsigned int val){Bit_number_Y = val;}
/** Returns the size of bits in the x-dimension. */
double RecLayer::getBitSizeX() const {return Bit_size_X;}
/** Returns the size of the bits in the y-dimension. */
double RecLayer::getBitSizeY() const {return Bit_size_Y;}
/** Returns the spacing between bits along the track. */
double RecLayer::getBitSpacingX() const {return Bit_spacing_X;}
/** Returns the spacing between tracks. */
double RecLayer::getBitSpacingY() const {return Bit_spacing_Y;}
/** Returns the number of bits within a track. */
unsigned int RecLayer::getBitsX() const {return Bit_number_X;}
/** Returns the number of tracks. */
unsigned int RecLayer::getBitsY() const {return Bit_number_Y;}
/** Returns the total number of bits in the system. */
unsigned int RecLayer::getBitsA() const {return Bit_number_X*Bit_number_Y;}
/** Returns the data value for the specified bit. */
int RecLayer::getWritableData(unsigned int idx) const {return Writable_data[idx];}
