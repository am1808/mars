/*
 * HAMR_laser.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file HAMRLaser.cpp
 * @brief Definitions of HAMR_Laser class methods. */

#include <cmath>
#include "../../hdr/Classes/HAMRLaser.hpp"

// TODO update temperature to be an internal variable

/** the constructor initialises the class using data from the configuration file. */
HAMR_Laser::HAMR_Laser(const ConfigFile cfg):
    Temperature(0.0)
{
    Temp_Max = cfg.getValueOfKey<double>("LaserHAMR:Temperature");
    Gradient = cfg.getValueOfKey<double>("LaserHAMR:Thermal_Gradient");
    Cooling_time = cfg.getValueOfKey<double>("LaserHAMR:Cooling_time");
}
/** Default deconstructor. */
HAMR_Laser::~HAMR_Laser(){}
/** Update laser temperature and apply pulse to all grains within the system.
 *
 * @param[in] Environ_Temp Background temperature of the simulation.
 * @param[in] Time Current time w.r.t. start to laser pulse.
 * @param[in] Temperature Current laser temperature.
 * @param[out] Grains The temperature value is updated for all grains.
 */
void HAMR_Laser::ApplyT(const double Environ_Temp, const double Time, double*Temperature, Grain_t*Grains){

    *Temperature = Environ_Temp + (getTemp()-Environ_Temp)*exp( -pow( ((Time-3*getCoolingTime())/getCoolingTime()),2.0 ) );
    this->Temperature=*Temperature;
    for(size_t grain_num=0;grain_num<Grains->Temp.size();grain_num++){Grains->Temp[grain_num] = *Temperature;}

}
/** Returns current temperature of the laser. */
double HAMR_Laser::getInstTemp() const {return Temperature;}
/** Returns maximum laser temperature. */
double HAMR_Laser::getTemp() const {return Temp_Max;}
/** Returns gradient of the laser pulse. */
double HAMR_Laser::getGrad() const {return Gradient;}
/** Returns cooling time of the laser (This is one sixth the total pulse time. */
double HAMR_Laser::getCoolingTime() const {return Cooling_time;}
/** Returns entire duration of the laser pulse. */
double HAMR_Laser::getProfileTime() const {return 6.0*Cooling_time;}
