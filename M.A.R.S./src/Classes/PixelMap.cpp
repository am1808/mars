/*
 * PixelMap.cpp
 *
 *  Created on: 27 May 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file PixelMap.cpp
 * @brief Definitions of PixelMap class methods. */

#include <fstream>
#include "../../hdr/Classes/PixelMap.hpp"
/** The constructor initialises the pixelmap creating a grid of pixel objects. Upon
 * initialisation from scratch the pixels contain no system data. Otherwise, if the
 * pixelmap is imported, pixels are filled in with their properties
 *
 * @param[in] Xmin Minimum x-position of the pixelmap.
 * @param[in] Ymin Minimum y-position of the pixelmap.
 * @param[in] Xmax Maximum x-position of the pixelmap.
 * @param[in] Ymax Maximum y-position of the pixelmap.
 * @param[in] cell_size Size of each pixel.
 * @param[in] cfg Configuration file.
 * @param[in] isImport Flag to switch between Pixel Map creation modes.
// isImport has a default case of false.
 */
PixelMap::PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cell_size, const ConfigFile cfg, const bool isImport)
    : Cells(0)
    , cellsize(cell_size)
{
    // If length of input file name is zero, perform default creation of PixelMap
    if(isImport==false){

        std::cout << "Performing creation of Pixel Map from scratch ..." << std::endl;

        // Create PixelMap
        unsigned int ID=0,IDy=0;

        double y=Ymin;
        while(y<Ymax){
            double x=Xmin;
            Cells.push_back(std::vector<Pixel>());
            while(x<Xmax){
                // Want to save centre of cells
                Cells.at(IDy).push_back(Pixel(ID,x+cellsize*0.5,y+cellsize*0.5));
                ++ID;
                x += cellsize;
            }
            y += cellsize;
            ++IDy;
        }
    }
    // Otherwise import PixelMap
    else if(isImport==true){

        std::cout << "Performing import of Pixel Map ..." << std::endl;

        // Create PixelMap
        unsigned int ID=0,IDy=0;
        unsigned int cell_x=0,cell_y=0;
        Data_input_t Data_in;

        // Get filename of file containing PixelMap
        std::cout << "Reading in Pixel Map input file ..." << std::endl;
        Data_in.PixelMap_file = cfg.getValueOfKey<std::string>("DATA:PixelMap");
        // Open file with Pixelmap data
        std::ifstream INFILE(("Input/System/"+Data_in.PixelMap_file).c_str(), std::ifstream::in);
        // Check that the required files have been opened
        if(!INFILE){throw std::runtime_error("ERROR: Missing " + Data_in.PixelMap_file + " system input files\n");}

        // Import dimensions of PixelMap grid
        std::cout << "Importing grid dimension (Nx x Ny): ";
        INFILE >> cell_x >> cell_y;
        std::cout << cell_x << " " << cell_y << std::endl;

        std::cout << "Creating grid ..." << std::endl;
        for(size_t y=0; y<cell_y; ++y){
            Cells.push_back(std::vector<Pixel>());
            for(size_t x=0; x<cell_x; ++x){
                Cells.at(IDy).push_back(Pixel(ID,0.0,0.0));
                ++ID;
            }
            ++IDy;
        }

        std::cout << "Importing pixel values ..." << std::endl;
        // Read in PixelMap data
        for(size_t y=0;y<Cells.size();++y){
            for(size_t x=0;x<Cells[y].size();++x){
                int Grain = 0;
                double Temp=0.0;
                Vec3 Mag(0.0,0.0,0.0), EasyAxis(0.0,0.0,0.0), Field(0.0,0.0,0.0);
                std::pair<double,double> Centre;

                INFILE  >> Centre.second >> Centre.first >> Grain
                        >> Mag.x >> Mag.y >> Mag.z
                        >> EasyAxis.x >> EasyAxis.y >> EasyAxis.z
                        >> Field.x >> Field.y >> Field.z
                        >> Temp;

                // Assign properties to pixels
                Cells[y][x].set_Coords(Centre);
                Cells[y][x].set_Grain(Grain);
                Cells[y][x].set_M(Mag);
                Cells[y][x].set_EA(EasyAxis);
                Cells[y][x].set_H(Field);
                Cells[y][x].set_Temp(Temp);
            }
        }

        // Close file with PixelMap data
        INFILE.close();
    }
}
/** The constructor initialises the pixelmap creating a grid of pixel objects. Upon
 * initialisation the pixels contain no system data.
 *
 * @param[in] Xmin Minimum x-position of the pixelmap.
 * @param[in] Ymin Minimum y-position of the pixelmap.
 * @param[in] Xmax Maximum x-position of the pixelmap.
 * @param[in] Ymax Maximum y-position of the pixelmap.
 * @param[in] cell_size Size of each pixel.
 */
PixelMap::PixelMap(const double Xmin, const double Ymin, const double Xmax, const double Ymax, const double cell_size)
    : Cells(0)
    , cellsize(cell_size)
{
    // Create PixelMap
    unsigned int ID=0,IDy=0;

    double y=Ymin;
    while(y<Ymax){
        double x=Xmin;
        Cells.push_back(std::vector<Pixel>());
        while(x<Xmax){
            // Want to save centre of cells
            Cells.at(IDy).push_back(Pixel(ID,x+cellsize*0.5,y+cellsize*0.5));
            ++ID;
            x += cellsize;
        }
        y += cellsize;
        ++IDy;
    }
}
/** default deconstructor. */
PixelMap::~PixelMap() {}

static inline double isLEFT(double P0x,double P0y,double P1x,double P1y,double P2x,double P2y){
    return ((P1x-P0x)*(P2y-P0y)-(P2x-P0x)*(P1y-P0y));
}

/** Determines if a point lies within a polygon.
 * \param[in] double Px X-coordinate of point.
 * \param[in] double Py Y-coordinate of point.
 * \param[in] std::vector<double> Vx X-coordinates of polygon vertex points with V[n+1].
 * \param[in] std::vector<double> Vy Y-coordinates of polygon vertex points with V[n+1].
 * \param[in] int n Number of vertices.
 * \return Winding number (wn=0 when P is outside the polygon)
 */
static int wn_PnPoly(double Px,double Py,std::vector<double>Vx,std::vector<double>Vy,int n){

    int wn=0;
    for(int edge=0;edge<n;++edge){
        if(Vy[edge]<=Py){if(Vy[edge+1]>Py){ if(isLEFT(Vx[edge],Vy[edge],Vx[edge+1],Vy[edge+1],Px,Py)>0){++wn;} }  }// Upwards crossing
        else{ if(Vy[edge+1]<=Py){if(isLEFT(Vx[edge],Vy[edge],Vx[edge+1],Vy[edge+1],Px,Py)<0){--wn;} } } // Downwards crossing
    }
    // Special case for final edge
    if(Vy[n]<=Py){if(Vy[0]>Py){if(isLEFT(Vx[n],Vy[n],Vx[0],Vy[0],Px,Py)>0){++wn;} } } // Upwards crossing
    else{ if(Vy[0]<=Py){if(isLEFT(Vx[n],Vy[n],Vx[0],Vy[0],Px,Py)<0){--wn;} } } // Downwards crossing
    return wn;
}
/** Discretises the system, assigning system data to all pixels within the pixelmap.
 *
 * @param[in] VORO Voronoi data structure.
 * @param Grains Grain data structure.
 */
void PixelMap::Discretise(const Voronoi_t&VORO, const Grain_t&Grains){

    bool in_grain=false;
    std::cout << "Discretising system" << std::endl;
    for(size_t cell_Y=0;cell_Y<Cells.size();++cell_Y){
        for(size_t cell_X=0;cell_X<Cells[cell_Y].size();++cell_X){
            in_grain=false;
            double Px = Cells[cell_Y][cell_X].get_Coords().first;
            double Py = Cells[cell_Y][cell_X].get_Coords().second;
            for(unsigned int grain=0;grain<VORO.Num_Grains;++grain){
                if(wn_PnPoly(Px,Py,VORO.Vertex_X_final[grain],VORO.Vertex_Y_final[grain],VORO.Vertex_X_final[grain].size()-1)!=0){
                    Cells[cell_Y][cell_X].set_Grain(grain);
                    in_grain=true;
                    break;
                }
            }
            if(!in_grain){Cells[cell_Y][cell_X].set_Grain(-1);} // Not in any grain
        }
    }

    MapVal(Grains, DataType::ALL);

}
/** The pixel data is updated according the current system state. Specific data can be
 * updated using the input enumerator. If a specific data type is requested all other
 * data is left untouched.
 *
 * @param[in] Grains Grain data structure.
 * @param[in] type Enum to set the data type to be assigned.
 */
void PixelMap::MapVal(const Grain_t&Grains, const DataType type){

    for(size_t y=0;y<Cells.size();++y){
        for(size_t x=0;x<Cells[y].size();++x){
            int Grain = Cells[y][x].get_Grain();
            switch(type)
            {
            case DataType::M:
                if(Grain>=0){
                    Cells[y][x].set_M(Grains.m[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_M(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::EA:
                if(Grain>=0){
                    Cells[y][x].set_EA(Grains.Easy_axis[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_EA(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::H:
                if(Grain>=0){
                    Cells[y][x].set_H(Grains.H_appl[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
                }
                break;
            case DataType::T:
                if(Grain>=0){
                    Cells[y][x].set_Temp(Grains.Temp[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_Temp(0.0);
                }
                break;
            case DataType::ALL:
                if(Grain>=0){
                    Cells[y][x].set_M(Grains.m[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_EA(Grains.Easy_axis[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_H(Grains.H_appl[Cells[y][x].get_Grain()]);
                    Cells[y][x].set_Temp(Grains.Temp[Cells[y][x].get_Grain()]);
                } else {
                    Cells[y][x].set_M(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_EA(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
                    Cells[y][x].set_Temp(0.0);
                }
            }
        }
    }
}
/** Prints pixelmap data to the specified file.
 *
 * @param filename Output file name.
 */
void PixelMap::Print(const std::string filename){

    std::ofstream FILE(("Output/"+filename).c_str());

    FILE << Cells[0].size() << " " << Cells.size() << "\n";
    for(size_t y=0;y<Cells.size();++y){
        for(size_t x=0;x<Cells[y].size();++x){

            FILE << Cells[y][x].get_Coords().second << " " << Cells[y][x].get_Coords().first << " "
                 << Cells[y][x].get_Grain() << " " << Cells[y][x].get_M() << " "
                 << Cells[y][x].get_EA() << " " << Cells[y][x].get_H() << " "
                 << Cells[y][x].get_Temp() << "\n";
        }
        if(y<Cells.size()-1){FILE << "\n";} // don't print blank line after last row of cells
    }
    FILE << std::flush;
    FILE.close();
}
/** Convenience function to print pixelmap data after updating specified pixel data to
 * match the currents system state.
 *
 * @param filename Output file name.
 * @param Grains Grain data structure.
 * @param type Enum to set the data type to be assigned.
 */
void PixelMap::PrintwUpdate(const std::string filename, const Grain_t&Grains, const DataType type){

    // Want to print out the most up-to-date values for the grid
    MapVal(Grains, type);
    Print(filename);
}
/** Provides grains with list of their constituent pixels. This data enables simpler
 * assignment of pixel data by applying the grain data to all pixels within the list
 * for each grain.
 *
 * @param[out] Grains Grain data structure.
 */
void PixelMap::StoreinGrain(Grain_t&Grains){

    for(unsigned int y=0;y<Cells.size();++y){
        for(unsigned int x=0;x<Cells[0].size();++x){
            if(Cells[y][x].get_Grain()>=0){
                Grains.Pixel[Cells[y][x].get_Grain()].push_back(Cells[y][x].get_ID());
            }
        }
    }
}
/** Determines the pixels within the specified rectangle. Also provides determines which
 * grains contain pixels within the rectangle. This function is designed to help reduce
 * computational cost of applying a profile to all pixels by first narrowing down the
 * pixels that may be affected by the profile via the input bounding rectangle.
 *
 * @param[in] xMin Minimum x-position of rectangle.
 * @param[in] yMin Minimum y-position of rectangle.
 * @param[in] xMax Maximum x-position of rectangle.
 * @param[in] yMax Maximum y-position of rectangle.
 * @param[out] PixelList List of pixels within the rectangle.
 * @param[out] ImpactedGrains List of grains containing pixels within the rectangle.
 */
void PixelMap::InRect(const double xMin, const double yMin, const double xMax, const double yMax,
                      std::vector<unsigned int>&PixelList, std::unordered_set<unsigned int>&ImpactedGrains) const {

    PixelList.clear();
    ImpactedGrains.clear();

    unsigned int x_MIN=0, x_MAX=getCols()-1, y_MIN=0, y_MAX=getRows()-1;
    // Y (row) MIN
    for(unsigned int y=0;y<getRows()-1;++y){
        if(Access(0, y).get_Coords().second < yMin){y_MIN = (y+1);}
        else{break;}
    }
    // Y (row) MAX
    for(unsigned int y=getRows()-1;y>0;--y){
        if(Access(0, y).get_Coords().second > yMax){y_MAX = (y-1);}
        else{break;}
    }
    // x (col) MIN
    for(unsigned int x=0;x<getCols()-1;++x){
        if(Access(x, 0).get_Coords().first < xMin){x_MIN=(x+1);}
        else {break;}
    }
    // X (col) MAX
    for(unsigned int x=getCols()-1;x>0;--x){
        if(Access(x, 0).get_Coords().first > xMax){x_MAX = (x-1);}
        else{break;}
    }

    for(unsigned int y=y_MIN;y<=y_MAX;++y){
        for(unsigned int x=x_MIN;x<=x_MAX;++x){
            PixelList.push_back(Access(x, y).get_ID());
            ImpactedGrains.insert(Access(x, y).get_Grain());
        }
    }
}
/** Convenience function to set all pixel field data to zero. */
void PixelMap::ClearH(){
    for(unsigned int y=0;y<getRows();++y){
        for(unsigned int x=0;x<getCols();++x){
            Cells[y][x].set_H(Vec3(0.0,0.0,0.0));
        }
    }
}
/** Convenience function to set all pixel temperatures to zero. */
void PixelMap::ClearT(){
    for(unsigned int y=0;y<getRows();++y){
        for(unsigned int x=0;x<getCols();++x){
            Cells[y][x].set_Temp(0.0);
        }
    }
}
/** Access a specific pixel using array position within the pixelmap. */
Pixel PixelMap::Access(const unsigned int X, const unsigned int Y) const {
    return Cells[Y][X];
}
/** Access a specific pixel using its ID within the pixelmap. */
Pixel PixelMap::Access(const unsigned int ID) const {
    return Cells[(ID/Cells[0].size())][(ID%Cells[0].size())];
}
/** Returns the number of pixels in the x-dimension. */
unsigned int PixelMap::getRows() const {
    return Cells.size();
}
/** Returns the number of pixels in the y-dimension. */
unsigned int PixelMap::getCols() const {
    return Cells[0].size();
}
/** Returns the pixel size. */
double PixelMap::getCellsize() const{
    return cellsize;
}
