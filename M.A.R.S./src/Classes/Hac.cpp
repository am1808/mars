/*
 * Hac.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file Hac.cpp
 * @brief Definitions for Hac class methods.
 */

#include <cmath>
#include "../../hdr/Classes/Hac.hpp"
/** The constructor initialises using the data from the configuration file.
 *
 * @param[in] cfg Configuration file data.
 */
Hac::Hac(const ConfigFile cfg) {
    std::vector<double> dirn_tmp = cfg.getValueOfKey<std::vector<double>>("FieldAC:Unit_vector");
    double tmpMag = sqrt(dirn_tmp[0]*dirn_tmp[0]+dirn_tmp[1]*dirn_tmp[1]+dirn_tmp[2]*dirn_tmp[2]);
    dirn.x = dirn_tmp[0]/tmpMag;
    dirn.y = dirn_tmp[1]/tmpMag;
    dirn.z = dirn_tmp[2]/tmpMag;
    amp  = cfg.getValueOfKey<double>("FieldAC:Amplitude");
    freq = cfg.getValueOfKey<double>("FieldAC:Frequency");
    phaseShift = (PI/180.0)*cfg.getValueOfKey<double>("FieldAC:Phase");

    updateField(0.0);
}
/** Default deconstructor. */
Hac::~Hac() {}
/** Updates the field's magnitude and 3-vector.
 *
 * @param[in] time Current time
 */
void Hac::updateField(double time){
    updateMag(time);
    field=dirn*mag;
}
/** Update the field's magnitude, does not update the 3-vector.
 *
 * @param[in] time Current time
 */
void Hac::updateMag(double time){
    mag = amp*sin(2.0*PI*freq*time+phaseShift);
}
/** Sets the unit vector of the field.
 * The input direction is automatically normalised into a unit vector.
 *
 * @param[in] Direction Field direction vector.
 */
void Hac::setDirn(Vec3 Direction){
    Direction = Direction/sqrt(Direction*Direction);
    dirn = Direction;
}
/** Sets the frequency of the field.
 *
 * @param[in] Frequency
 */
void Hac::setFreq(double frequency){
    freq = frequency;
}
/** Sets the Amplitude of the field.
 *
 * @param[in] Amplitude
 */
void Hac::setAmp(double Amplitude){
    amp = Amplitude;
}
/** Returns field's 3-vector. */
Vec3 Hac::getField() const {return field;}
/** Returns the field's magnitude. */
double Hac::getMag() const {return mag;}
/** Returns the field's direction vector. */
Vec3 Hac::getDirn() const {return dirn;}
/** Returns the field's frequency. */
double Hac::getFreq() const {return freq;}
/** Returns the field's phase shift. */
double Hac::getPhase() const {return phaseShift;}
/** Returns the amplitude of the field. */
double Hac::getAmp() const {return amp;}
