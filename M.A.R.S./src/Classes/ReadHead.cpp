/*
 * ReadHead.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file ReadHead.cpp
 * @brief Definitions of ReadHead class methods. */

#include "../../hdr/Classes/ReadHead.hpp"
/** The constructor initialises the class using the data already present it the RecLayer class.*/
ReadHead::ReadHead(const RecLayer& RecordingLayer)
    :SizeX(1.0)
    ,PosX(0.0)
    ,PosY(0.0)
{
    SizeY = RecordingLayer.getBitSizeY();
}
/** The constructor initialises the class using the data from the configuration file.
 * The read head needs to know the sizes of the bits and tracks in order to average the
 * magnetisation over the correct size to create the data signal.
 */
ReadHead::ReadHead(const ConfigFile cfg)
    : SizeX(0.0)
    , SizeY(0.0)
    , PosX(0.0)
    , PosY(0.0)
{
    SizeX = cfg.getValueOfKey<double>("ReadHead:SizeX");
    SizeY = cfg.getValueOfKey<double>("ReadHead:SizeY");
}
/** Default deconstructor. */
ReadHead::~ReadHead(){}
/** Returns the size of the read head in the x-dimension. */
double ReadHead::getSizeX() const {return SizeX;}
/** Returns the size of the read head in the y-dimension. */
double ReadHead::getSizeY() const {return SizeY;}
/** Returns the positions of the read head in the x-dimension. */
double ReadHead::getPosX() const {return PosX;}
/** Returns the positions of the read head in the y-dimension. */
double ReadHead::getPosY() const {return PosY;}
/** Sets the position of the read head in the x-direction. */
void ReadHead::setPosX(double x){PosX = x;}
/** sets the position of the read head in the y-position. */
void ReadHead::setPosY(double y){PosY = y;}
