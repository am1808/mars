/*
 * RecHead.cpp
 *
 *  Created on: 1 Jun 2020
 *      Author: Samuel Ewan Rannala
 */

/** @file RecHead.cpp
 * @brief Definitions of RecHead class methods. */

#include "../../hdr/Classes/RecHead.hpp"
/** The constructor initialises the sub and parent classes using the data within the configuration file.
 *
 * @param cfg Configuration file data.
 */
RecHead::RecHead(const ConfigFile cfg)
    : Writer(cfg)
    , HAMR_Laser(cfg)
{
    FWHM_X   = cfg.getValueOfKey<double>("Recordinghead:FWHM_X");
    FWHM_Y   = cfg.getValueOfKey<double>("Recordinghead:FWHM_Y");
    NFT_disp_X = cfg.getValueOfKey<double>("Recordinghead:NFT_spacing_X");
    Pulsing    = cfg.getValueOfKey<bool>("Recordinghead:Pulsing");
}
/** Default deconstructor. */
RecHead::~RecHead(){}
/** Apply the temperature to grains within the system. The laser is assumed to be always
 * applied in this function. The distance between the centre of the profile and
 * the grains is determined and used to calculate the temperature applied to each grain.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time Current time w.r.t. the head.
 * @param[out] Temperature Current laser temperature.
 * @param[out] Grains Temperature data is updated for all grains.
 */
void RecHead::ApplyTspatialCont(const unsigned int Num_Layers, const Voronoi_t VORO, const double Environ_Temp,
                                const double Time, double*Temperature, Grain_t*Grains){

    double Temporal_temp, Spatial_temp;
    const double sigma_X = FWHM_X / sqrt(8.0 * log(2.0));
    const double sigma_Y = FWHM_Y / sqrt(8.0 * log(2.0));

    Cooling_time=0.0;

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

        //########## Vogler definition of the continuous laser profile.
        Temporal_temp = exp(-pow(Grain_X - (getSpeed()*Time+NFT_disp_X),2.0)/(2.0*pow(sigma_X,2.0)));
        Spatial_temp  = exp(-pow(Grain_Y - getY(),2.0)/(2.0*pow(sigma_Y,2.0)));
        *Temperature = Environ_Temp + (getTemp()-Environ_Temp)*Temporal_temp*Spatial_temp;
        // Assign temperature to each layer.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
            Grains->Temp[grain_in_system] = *Temperature;
        }
    }
}
/** Apply the mean temperature to the grains within the system, by first assigning the temperatures to
 * each pixel within the input pixelmap. The laser is assumed to be always applied in this
 * function. The temperature values of these pixels within each grain is then
 * averaged over to produce a mean temperature for each grain.
 *
 * @param[in] Num_Layers Number of layers in the system.
 * @param[in] VORO Voronoi data structure.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time Current time w.r.t. the laser.
 * @param[out] Temperature Current laser temperature.
 * @param[out] Grains Temperature data is updated for all grains.
 * @param[out] PixMap Temperature data is updated for all pixels.
 */
void RecHead::ApplyTspatialContPM(const unsigned int Num_Layers, const Voronoi_t VORO, const double Environ_Temp,
                                  const double Time, double&Temperature, Grain_t&Grains, PixelMap&PixMap){

    double Temporal_temp, Spatial_temp;
    const double sigma_X = FWHM_X / sqrt(8.0 * log(2.0));
    const double sigma_Y = FWHM_Y / sqrt(8.0 * log(2.0));

    Cooling_time=0.0;
    // Set Pixel values
    for(unsigned int y=0;y<PixMap.getRows();++y){
        for(unsigned int x=0;x<PixMap.getCols();++x){
            double Px = PixMap.Access(x, y).get_Coords().first;
            double Py = PixMap.Access(x, y).get_Coords().second;

            //########## Vogler definition of the continuous laser profile.
            Temporal_temp = exp(-pow(Px - (getSpeed()*Time+NFT_disp_X),2.0)/(2.0*pow(sigma_X,2.0)));
            Spatial_temp  = exp(-pow(Py - getY(),2.0)/(2.0*pow(sigma_Y,2.0)));
            Temperature = Environ_Temp + (getTemp()-Environ_Temp)*Temporal_temp*Spatial_temp;
            PixMap.Access(x,y).set_Temp(Temperature);
        }
    }
    // Set Grain values
    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
        double T=0.0;
        for(unsigned int Pix=0;Pix<Grains.Pixel[grain_in_layer].size();++Pix){
            T+=PixMap.Access(Grains.Pixel[grain_in_layer][Pix]).get_Temp();
        }
        T /= Grains.Pixel[grain_in_layer].size();
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Grains.Temp[grain_in_layer+(VORO.Num_Grains*Layer)]=T;
        }
    }
}
/** Apply temperature to the grains within the system. The laser is assumed to be pulsed
 * in this function. The distance between the centre of the profile and
 * the grains is determined and used to calculate the temperature applied to each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers within the system.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time current time w.r.t. laser.
 * @param[out] Spatial_Temp Current temperature produced by the laser.
 * @param[out] Grain Temperature data is updated for all grains.
 */
void RecHead::ApplyTspatial(const Voronoi_t VORO, const unsigned int Num_Layers,
                            const double Environ_Temp, const double Time,
                            double*Spatial_Temp, Grain_t*Grain){

    double Temporal_Temp = (getTemp()-Environ_Temp)*exp(-pow(((Time-3*getCoolingTime())/getCoolingTime()),2.0));

    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;grain_in_layer++){
        double Grain_X=VORO.Pos_X_final[grain_in_layer], Grain_Y=VORO.Pos_Y_final[grain_in_layer];

        //########## Generic definition of the laser profile.
        *Spatial_Temp = Environ_Temp + Temporal_Temp * exp(-(pow(Grain_X-getX(),2.0)/(getGrad()*pow(FWHM_X,2.0)) + pow(Grain_Y-getY(),2.0)/(getGrad()*pow(FWHM_Y,2.0))));
        //########## Vogler definition of the laser profile.
        //      *Spatial_Temp = Expt_laser.Laser_Temp_MIN + Temporal_Temp * exp(-( pow(Grain_X-cen_X,2.0)/(2.0*pow(sigma_X,2.0)) + pow(Grain_Y-cen_Y,2.0)/(2.0*pow(sigma_Y,2.0)) ) );
        // Assign temperature to each layer.
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            unsigned int grain_in_system = Layer*VORO.Num_Grains + grain_in_layer;
            Grain->Temp[grain_in_system] = *Spatial_Temp;
        }
    }
}
/** Apply the mean temperature to the grains within the system, by first assigning the temperatures to
 * each pixel within the input pixelmap. The laser is assumed to be pulsed in this
 * function. The temperature values of these pixels within each grain is then averaged over
 * to produce a mean temperature for each grain.
 *
 * @param[in] VORO Voronoi data structure.
 * @param[in] Num_Layers Number of layers within the system.
 * @param[in] Environ_Temp Environmental background temperature.
 * @param[in] Time current time w.r.t. laser.
 * @param[out] Spatial_Temp Current temperature produced by the laser.
 * @param[out] Grains Temperature data is updated for all grains.
 * @param[out] PixMap Temperature data is updated for all pixels.
 */
void RecHead::ApplyTspatialPM(const Voronoi_t VORO, const unsigned int Num_Layers, const double Environ_Temp,
                            const double Time, double&Spatial_Temp, Grain_t&Grains, PixelMap&PixMap){

    double Temporal_Temp = (getTemp()-Environ_Temp)*exp(-pow(((Time-3*getCoolingTime())/getCoolingTime()),2.0));

    // Set Pixel values
    for(unsigned int y=0;y<PixMap.getRows();++y){
        for(unsigned int x=0;x<PixMap.getCols();++x){
            double Px = PixMap.Access(x, y).get_Coords().first;
            double Py = PixMap.Access(x, y).get_Coords().second;

            //########## Generic definition of the laser profile.
            Spatial_Temp = Environ_Temp + Temporal_Temp * exp(-(pow(Px-getX(),2.0)/(getGrad()*pow(FWHM_X,2.0)) + pow(Py-getY(),2.0)/(getGrad()*pow(FWHM_Y,2.0))));
            PixMap.Access(x,y).set_Temp(Spatial_Temp);
        }
    }
    // Set Grain values
    for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
        double T=0.0;
        for(unsigned int Pix=0;Pix<Grains.Pixel[grain_in_layer].size();++Pix){
            T+=PixMap.Access(Grains.Pixel[grain_in_layer][Pix]).get_Temp();
        }
        T /= Grains.Pixel[grain_in_layer].size();
        for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
            Grains.Temp[grain_in_layer+(VORO.Num_Grains*Layer)]=T;
        }
    }
}
/** Returns boolean indicating if the laser is set to pulse. */
bool RecHead::getPulsing(){return Pulsing;}
