/*
 * Export_system.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** @file Export_system.cpp
 * @brief Functions for exporting the entire simulated system to data files
 * later use in MARS. */

#include "../../hdr/Data_output/Export_system.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

/** Exports all system data for future use. Multiple exports can be performed per simulation with the output_iter used to
 * differentiate different exports. Some files contain data which is constant throughout all simulations, these files are
 * overwritten on each call of this function.
 *
 * @param Num_layers Number of layers to export.
 * @param dz  Thickness of each layer.
 * @param VORO Voronoi data.
 * @param Int Interaction data.
 * @param Grain Grain data.
 * @param Solver Solver class.
 * @param Output_iter Export iteration number.
 */
int Export_system(const int Num_layers, const std::vector<double> dz, const Voronoi_t VORO,
                  const Interaction_t Int, const Grain_t Grain, Solver_t Solver,
                  const std::string Output_iter){

    std::cout << "Exporting system" << std::endl;

    const int total_grains = VORO.Num_Grains*Num_layers;

    std::ofstream OUTPUT_VORO_FILE     ("Output/voro.dat");
    std::ofstream OUTPUT_ST_MAT_FILE   ("Output/st_mat.dat");
    std::ofstream OUTPUT_GPV_FILE      (("Output/gpv_"+ Output_iter +".dat").c_str());
    std::ofstream OUTPUT_GPC_FILE      ("Output/gpc.dat");
    std::ofstream OUTPUT_GPEQ_FILE     ("Output/gpEQ.dat");
    std::ofstream OUTPUT_GP_FILE       (("Output/gp_"+ Output_iter + ".dat").c_str());
    std::ofstream OUTPUT_CHI_FILE      ("Output/Chi.dat");
    std::ofstream OUTPUT_CHI_PERP_IDV_FILE  ("Output/Chi_perp_Idv.dat");
    std::ofstream OUTPUT_CHI_PARA_IDV_FILE  ("Output/Chi_para_Idv.dat");
    std::ofstream OUTPUT_ALPHA_FILE    ("Output/alpha.dat");
    std::ofstream OUTPUT_INT_MN_FILE   ("Output/int_mn.dat");
    std::ofstream OUTPUT_INT_EN_FILE   ("Output/int_en.dat");
    std::ofstream OUTPUT_INT_W_FILE    ("Output/int_w.dat");
    std::ofstream OUTPUT_INT_HEXC_FILE ("Output/int_hexc.dat");

    std::cout << "Output files created" << std::endl;
    std::cout << "total_grains = " << total_grains << std::endl;

//#####################################################################################//

    OUTPUT_VORO_FILE << VORO.Centre_X << " " << VORO.Centre_Y << " "
                     << VORO.Vx_MAX   << " " << VORO.Vy_MAX << " "
                     << VORO.Vx_MIN   << " " << VORO.Vy_MIN << " "
                     << VORO.Input_grain_width << " " << VORO.Real_grain_width << std::endl;

    OUTPUT_ST_MAT_FILE << VORO.Num_Grains << " " << Num_layers << "\n";
    for(int LAYER=0;LAYER<Num_layers;++LAYER){
        OUTPUT_ST_MAT_FILE << dz[LAYER] << " ";
    }
    OUTPUT_ST_MAT_FILE << std::endl;
//    std::cout << "Voronoi written" << std::endl;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################################INTERACTIONS#####################################//

    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_MN_FILE << grain_in_system << " " << Int.Magneto_neigh_list[grain_in_system].size() << std::endl;
        for(size_t Mneigh=0;Mneigh<Int.Magneto_neigh_list[grain_in_system].size();++Mneigh){
            OUTPUT_INT_MN_FILE << Int.Magneto_neigh_list[grain_in_system][Mneigh] << " ";
        }
        OUTPUT_INT_MN_FILE << std::endl;
    }

    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_EN_FILE << grain_in_system << " " << Int.Exchange_neigh_list[grain_in_system].size() << std::endl;
        for(size_t Neigh_num=0;Neigh_num<Int.Exchange_neigh_list[grain_in_system].size();++Neigh_num){
            OUTPUT_INT_EN_FILE << Int.Exchange_neigh_list[grain_in_system][Neigh_num] << " ";
        }
        OUTPUT_INT_EN_FILE << std::endl;
    }

    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_W_FILE << grain_in_system << " " << Int.Wxx[grain_in_system].size() << std::endl;
        for(size_t W=0;W<Int.Wxx[grain_in_system].size();++W){
            OUTPUT_INT_W_FILE << Int.Wxx[grain_in_system][W] << " " << Int.Wxy[grain_in_system][W] << " "
                              << Int.Wxz[grain_in_system][W] << " " << Int.Wyy[grain_in_system][W] << " "
                              << Int.Wyz[grain_in_system][W] << " " << Int.Wzz[grain_in_system][W] << std::endl;
        }
        OUTPUT_INT_W_FILE << std::endl;
    }

    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_INT_HEXC_FILE << grain_in_system << " " << Int.H_exch_str[grain_in_system].size() << std::endl;
        for(size_t H=0;H<Int.H_exch_str[grain_in_system].size();++H){
            OUTPUT_INT_HEXC_FILE << Int.H_exch_str[grain_in_system][H] << " ";
        }
        OUTPUT_INT_HEXC_FILE << std::endl;
    }
//    std::cout << "Interactions written" << std::endl;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##################################GRAIN PARAMETERS###################################//

    std::vector<double> K_grain;
    std::vector<unsigned> Grains;
    for(unsigned g=0;g<VORO.Num_Grains;++g){Grains.push_back(g);}
    for(int L=1;L<Num_layers;++L){
        unsigned offset=VORO.Num_Grains*L;
        for(unsigned g=0;g<VORO.Num_Grains;++g){Grains.push_back(g+offset);}
    }

    Solver.set_Chi_size(VORO.Num_Grains*Num_layers);
    Solver.Susceptibilities(VORO.Num_Grains, Num_layers, &Grains, &Grain);
    for(int LAYER=0;LAYER<Num_layers;++LAYER){
        int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_CHI_FILE << Grain.Ani_method[Offset] << std::endl;
        OUTPUT_CHI_FILE << Solver.get_SusType(LAYER) << std::endl;
        OUTPUT_CHI_FILE << Solver.get_ChiScaling(LAYER) << std::endl;
        OUTPUT_CHI_FILE << Solver.get_ChiParaFit(LAYER) << std::endl;
        OUTPUT_CHI_FILE << Solver.get_ChiPerpFit(LAYER) << std::endl;

        for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
            unsigned grain_in_sys=grain_in_layer+Offset;
            double Temp=Grain.Temp[grain_in_sys];
            double Tc  = Grain.Tc[grain_in_sys];
            double m_EQ=0.0;
            if(Temp>=Tc){
                if(Grain.mEQ_Type[grain_in_sys]=="bulk"){m_EQ=0.0;}
                else{
                    m_EQ = 1.0/(1.0/Grain.a0_mEQ[grain_in_sys] + Grain.b1_mEQ[grain_in_sys]*((Tc-Temp)/Tc) + Grain.b2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,2));
                }
            }
            else {
                if(Grain.mEQ_Type[grain_in_sys]=="bulk"){m_EQ = pow(1.0-(Temp/Tc),Grain.Crit_exp[grain_in_sys]);}
                else{
                    m_EQ = Grain.a0_mEQ[grain_in_sys] + Grain.a1_2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,0.5) + Grain.a1_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,1) +
                           Grain.a2_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,2) + Grain.a3_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,3) + Grain.a4_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,4) +
                           Grain.a5_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,5) + Grain.a6_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,6) + Grain.a7_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,7) +
                           Grain.a8_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,8) + Grain.a9_mEQ[grain_in_sys]*pow((Tc-Temp)/Tc,9);
                }
            }
           K_grain.push_back((Grain.Ms[grain_in_sys]*m_EQ*m_EQ)/(2.0*Solver.get_ChiPerp(grain_in_sys)));
        }
    }

    // Grain Magnetisation and Easy axis
    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_GPV_FILE << Grain.m[grain_in_system]         << " "
                        << Grain.Easy_axis[grain_in_system] << " "
                        << Grain.H_appl[grain_in_system]    << std::endl;
    }
    // Grain Temp, Vol, K, Tc, Ms
    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        OUTPUT_GP_FILE << Grain.Temp[grain_in_system] << " " << Grain.diameter[grain_in_system] << " "
                       << Grain.Vol[grain_in_system]*1.0e-21 << " ";
        if(Grain.Ani_method[grain_in_system]=="chi"){
            OUTPUT_GP_FILE << K_grain[grain_in_system] << " ";
        }
        else{
            OUTPUT_GP_FILE << Grain.K[grain_in_system] << " ";
        }
        OUTPUT_GP_FILE << Grain.Tc[grain_in_system]  << " " << Grain.Ms[grain_in_system]   << std::endl;
    }
    // Layer Damping
    for(int LAYER=0;LAYER<Num_layers;++LAYER){
        OUTPUT_ALPHA_FILE << Solver.get_Alpha()[LAYER] << std::endl;
    }
//    std::cout << "Damping written" << std::endl;
    // Here I need to make the choice of saving the correct m_eq data
    // If BULK - want crit_exp not polynomial
    // Else - want polynomial not crit_exp
    for(int LAYER=0;LAYER<Num_layers;++LAYER){
        int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_GPEQ_FILE << Grain.mEQ_Type[Offset] << std::endl;
        if(Grain.mEQ_Type[Offset]=="bulk"){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPEQ_FILE << Grain.Crit_exp[grain_in_system] << std::endl;
            }
        }
        else if(Grain.mEQ_Type[Offset]=="polynomial"){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPEQ_FILE << Grain.a0_mEQ[grain_in_system] << " " << Grain.a1_mEQ[grain_in_system] << " "
                                 << Grain.a2_mEQ[grain_in_system] << " " << Grain.a3_mEQ[grain_in_system] << " "
                                 << Grain.a4_mEQ[grain_in_system] << " " << Grain.a5_mEQ[grain_in_system] << " "
                                 << Grain.a6_mEQ[grain_in_system] << " " << Grain.a7_mEQ[grain_in_system] << " "
                                 << Grain.a8_mEQ[grain_in_system] << " " << Grain.a9_mEQ[grain_in_system] << " "
                                 << Grain.a1_2_mEQ[grain_in_system] << " "
                                 << Grain.b1_mEQ[grain_in_system] << " " << Grain.b2_mEQ[grain_in_system] << std::endl;
            }
        }
    }
//    std::cout << "m_EQ written" << std::endl;
    for(int LAYER=0;LAYER<Num_layers;++LAYER){
        int Offset = VORO.Num_Grains*LAYER;
        OUTPUT_GPC_FILE << Grain.Callen_power_range[Offset] << std::endl;
        if(Grain.Callen_power_range[Offset]=="single"){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPC_FILE << Grain.Callen_power[grain_in_system] << std::endl;
            }
        }
        else if(Grain.Callen_power_range[Offset]=="multiple"){
            for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
                int grain_in_system = Offset+grain_in_layer;
                OUTPUT_GPC_FILE << Grain.Callen_factor_lowT[grain_in_system]  << " " << Grain.Callen_power_lowT[grain_in_system]  << " "
                                << Grain.Callen_range_lowT[grain_in_system]   << " " << Grain.Callen_factor_midT[grain_in_system] << " "
                                << Grain.Callen_power_midT[grain_in_system]   << " " << Grain.Callen_range_midT[grain_in_system]  << " "
                                << Grain.Callen_factor_highT[grain_in_system] << " " << Grain.Callen_power_highT[grain_in_system] << std::endl;
            }
        }
    }
    for(int grain_in_system=0;grain_in_system<total_grains;++grain_in_system){
        std::vector<double> PERP = Solver.get_ChiPerpFitIdv(grain_in_system);
        std::vector<double> PARA = Solver.get_ChiParaFitIdv(grain_in_system);

        for(unsigned i=0;i<PERP.size();++i){
            OUTPUT_CHI_PERP_IDV_FILE << PERP[i] << " ";
        }
        OUTPUT_CHI_PERP_IDV_FILE << std::endl;

        for(unsigned i=0;i<PARA.size();++i){
            OUTPUT_CHI_PARA_IDV_FILE << PARA[i] << " ";
        }
        OUTPUT_CHI_PARA_IDV_FILE << std::endl;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    return 0;
}
