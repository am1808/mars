/*
 * HAMR_switching_output.cpp
 *
 *  Created on: 3 Dec 2018
 *      Author: Ewan Rannala
 */

/** @file HAMR_switching_output.cpp
 * @brief Functions for output of the initial and final states of the system. */

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "../../hdr/Structures.hpp"

// This function makes the buffer of the initial data.
int HAMR_Switching_BUFFER(const int Num_layers, const Voronoi_t VORO, const Grain_t Grain, const std::vector<double> dz, std::vector<std::string>*BUFFER){

	BUFFER->resize(Num_layers*VORO.Num_Grains);

	for(int Layer=0;Layer<Num_layers;++Layer){
		int offset = Layer*VORO.Num_Grains;
		for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
			int grain_in_system = grain_in_layer+offset;
			BUFFER->at(grain_in_system) = std::to_string(grain_in_layer) + " " + std::to_string(Layer) + " "
											 + std::to_string(dz[Layer]) + " " + std::to_string(VORO.Grain_Area[grain_in_layer]) + " "+ std::to_string(Grain.K[grain_in_system]) + " "
					                         + std::to_string(Grain.m[grain_in_system].x) + " " + std::to_string(Grain.m[grain_in_system].y) + " " + std::to_string(Grain.m[grain_in_system].z);
	}	}
	return 0;
}

// This function adds the final data to the input buffer to produce the desired output file.
int HAMR_Switching_OUTPUT(const int Num_layers, const int Num_Grains,const Grain_t Grain, const std::vector<std::string> BUFFER, const std::string OUTPUT){

	std::ofstream OUTPUT_FILE;
	OUTPUT_FILE.open(OUTPUT.c_str());
	OUTPUT_FILE << "Grain_index Layer thickness grain_area Anisotropy mx_init my_init mz_init mx_final my_final mz_final" << std::endl;

	for(int Layer=0;Layer<Num_layers;++Layer){
		int offset = Layer*Num_Grains;
		for(int grain_in_layer=0;grain_in_layer<Num_Grains;++grain_in_layer){
			int grain_in_system = grain_in_layer+offset;
			OUTPUT_FILE << BUFFER[grain_in_system] << " " << std::to_string(Grain.m[grain_in_system].x)
												   << " " << std::to_string(Grain.m[grain_in_system].y)
												   << " " << std::to_string(Grain.m[grain_in_system].z) << std::endl;
	}	}
	OUTPUT_FILE.close();
	return 0;
}
