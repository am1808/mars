/*
 * HAMR_grain_output.cpp
 *
 *  Created on: 5 Oct 2018
 *      Author: ewan
 */

/** @file HAMR_grain_output.cpp
 * @brief Functions for output of grain individual data. */

#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "../../hdr/Structures.hpp"

/* Writes data for each grains vertices, magnetisation, temperature and applied field. */
// OUTPUT_PRE has a default case of no prefix for the output file. This is used in the case when a value isn't provided when calling the function.
int Idv_Grain_output(const int Num_Layers, const Voronoi_t VORO, const Grain_t Grain, const double Time,std::string OUTPUT,const std::string OUTPUT_PRE=""){

	std::string FILENAME_idv_START="Output/" + OUTPUT_PRE + "Time_";
	std::ofstream OUTPUT_FILE;

	for(int layer=0;layer<Num_Layers;++layer){
		OUTPUT_FILE.open((FILENAME_idv_START+std::to_string(layer)+"_"+OUTPUT).c_str());
		if(!OUTPUT_FILE){throw std::runtime_error("Cannot create "+FILENAME_idv_START+std::to_string(layer)+"_"+OUTPUT);}
		int offset = layer*VORO.Num_Grains;

		for(unsigned int grain_in_layer=0;grain_in_layer<VORO.Num_Grains;++grain_in_layer){
			int grain_in_system = grain_in_layer+offset;

			for(size_t j=0;j<VORO.Vertex_X_final[grain_in_layer].size();++j){
				OUTPUT_FILE << std::setprecision(10) << VORO.Vertex_X_final[grain_in_layer][j] << " "
						    << VORO.Vertex_Y_final[grain_in_layer][j] << " " << Grain.Temp[grain_in_system] << " "
						    << Grain.H_appl[grain_in_system].z << " " << Grain.m[grain_in_system].z << " "
						    << Time << " " << Grain.Tc[grain_in_system] << " " << std::endl;
			}
			OUTPUT_FILE << std::setprecision(10) << VORO.Vertex_X_final[grain_in_layer][0] << " "
					    << VORO.Vertex_Y_final[grain_in_layer][0] << " " << Grain.Temp[grain_in_system] << " "
					    << Grain.H_appl[grain_in_system].z << " " << Grain.m[grain_in_system].z << " "
						<< Time << " " << Grain.Tc[grain_in_system] << "\n\n" << std::endl;
		}
		OUTPUT_FILE.close();
	}
	//std::cout << Time << std::endl;
	return 0;
}
