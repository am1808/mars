/* Voronoi.cpp
 *  Created on: 3 May 2018
 *      Author: Samuel Ewan Rannala
 */

/** @file Voronoi.cpp
 * @brief Source file for Voronoi construction. */
// System header files
#include "math.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>
#include <random>

// M.A.R.S. specific header files
#include "../hdr/Structures.hpp"

// Load all header files required for voro++.
#include "voro++.hh"

bool Drag_and_Drop(double HozMin,double HozMax,double VerMin,double VerMax,
        std::vector<double>*Hoz,std::vector<double>*Ver,std::vector<double>*r){

    double dTh = (2.0*PI/1000.0);
    double HozPos=Hoz->back(), VerPos=Ver->back();
    double Boundary_check = (HozMax-HozMin)*0.5;
    double dVert= ((VerMax-VerMin)/1000.0);
    std::uniform_real_distribution<double> UNI(0.0,HozMax);

    HozPos=UNI(Gen);
    // Force particle within a box and set starting height
    if(HozPos-r->back()<HozMin){HozPos=HozMin+r->back();}
    else if(HozPos+r->back()>HozMax){HozPos=HozMax-r->back();}
    VerPos=VerMax+r->back();
    bool placed=false, falling=true, rolling=false;
    unsigned int contact;


//    std::cout << "\n" << HozPos << " " << VerPos << " " << r->back() << std::endl;
    while(!placed){
        double HozCont=0.0,VerCont=0.0,rCont=0.0;
        while(falling){
            // Drop
            VerPos -= dVert;
            // Check -- NB: for improved efficiency split box into grid for coarse check before fine
            for(unsigned int neigh=0; neigh<(Ver->size()-1);++neigh){
                double HozNeigh=Hoz->at(neigh), VerNeigh=Ver->at(neigh), RNeigh=r->at(neigh);
                double Hdist    = (HozPos-HozNeigh);
                double radSumSq = (r->back()+RNeigh)*(r->back()+RNeigh);

                //########### Periodic Boundary Checks ###########//
                if(Hdist>Boundary_check){HozNeigh+=HozMax;}// Move neigh to the right
                else if(Hdist<-Boundary_check){HozNeigh-=HozMax;}// Move neigh to the left
                //################################################//

               double distSq   = (HozPos-HozNeigh)*(HozPos-HozNeigh)
                               + (VerPos-VerNeigh)*(VerPos-VerNeigh);

               if(distSq==radSumSq || distSq<(radSumSq-r->back()*0.1)){ // Touching or intersecting
                   HozCont=HozNeigh;VerCont=VerNeigh;rCont=RNeigh;
                   falling=false;
                   rolling=true;
                   contact=neigh;
                   goto Rolling_label; //I know "Don't use goto", I'll follow that when I can break out of a nested loop.
               }
            }
            // Hit bottom
            if((VerPos-r->back())<=VerMin){ // Good improvement here would be maybe round up the comparison so it counts as touching when ~1e-12
                placed=true;
                falling=false;
            }
         }
Rolling_label:
         while(rolling){
//             std::cout << "\t Rolling" << std::endl;
             // Particle exactly on top
             if(HozPos==HozCont){
                 placed=true;
                 falling=true;
                 break;
             }
             // Particle grazing past
             if(VerPos<=VerCont){
                 rolling=false;
                 falling=true;
                 break;
             }
             // Roll the particle
             // Determine incident angle
             double angle = atan2((VerPos-VerCont),(HozPos-HozCont));
//             std::cout << "Angle: " << angle << std::endl;
             if(angle>1.570796){ //Roll left
                 while(angle<=PI && !placed){
//                     std::cout << "L " << angle << " " << PI << std::endl;
                     HozPos = HozCont + (r->back()+rCont)*cos(angle);
                     VerPos = VerCont + (r->back()+rCont)*sin(angle);
                     // Check contact
                     for(unsigned int neigh=0; neigh<(Ver->size()-1);++neigh){
                         if(neigh==contact){continue;}
                         double HozNeigh=Hoz->at(neigh), VerNeigh=Ver->at(neigh), RNeigh=r->at(neigh);

                         double Hdist     = (HozPos-HozNeigh);
                         double radSumSq = (r->back()+RNeigh)*(r->back()+RNeigh);

                         //########### Periodic Boundary Checks ###########//
                         if(Hdist>Boundary_check){HozNeigh+=HozMax;}// Move neigh to the right
                         else if(Hdist<-Boundary_check){HozNeigh-=HozMax;}// Move neigh to the left
                         //################################################//

                         double distSq   = (HozPos-HozNeigh)*(HozPos-HozNeigh)
                                         + (VerPos-VerNeigh)*(VerPos-VerNeigh);

                         if(distSq<radSumSq){ // We have hit another disc
                             // Is current disc's CoM in between the two contacting discs?
                             if(fabs(HozNeigh-HozCont) >= (fabs(HozNeigh-HozPos)+fabs(HozCont-HozPos))){
                                 placed=true;
                                 rolling=false;
                                 break;
                             }
                             else{ // Disc should now Roll around Neigh NOT Cont
                                 HozCont=HozNeigh;VerCont=VerNeigh;rCont=RNeigh;
                                 contact=neigh;
                                 goto Rolling_label;
                             }
                         }
                     }
                     if(!placed){
                         // Check for floor
                         if((VerPos-r->back())<=VerMin){
                             placed=true;
                             rolling=false;
                             break;
                         }
                         angle += dTh;
                     }
                 }
                 if(!placed){
                     rolling=false;
                     falling=true;
                 }
             }
             else{ // Roll right
                 while(angle>=0.0 && !placed){
//                     std::cout << "R " << angle << " " << 0.0 << std::endl;
                     HozPos = HozCont + (r->back()+rCont)*cos(angle);
                     VerPos = VerCont + (r->back()+rCont)*sin(angle);
                     for(unsigned int neigh=0; neigh<(Ver->size()-1);++neigh){
                         if(neigh==contact){continue;}
                         double HozNeigh=Hoz->at(neigh), VerNeigh=Ver->at(neigh), RNeigh=r->at(neigh);

                         double Hdist    = (HozPos-HozNeigh);
                         double radSumSq = (r->back()+RNeigh)*(r->back()+RNeigh);

                         //########### Periodic Boundary Checks ###########//
                         if(Hdist>Boundary_check){HozNeigh+=HozMax;}// Move neigh to the right
                         else if(Hdist<-Boundary_check){HozNeigh-=HozMax;}// Move neigh to the left
                         //################################################//

                         double distSq   = (HozPos-HozNeigh)*(HozPos-HozNeigh)
                                         + (VerPos-VerNeigh)*(VerPos-VerNeigh);

                         if(distSq<radSumSq){
                             // Is current disc's CoM in between the two contacting discs?
                             if(fabs(HozNeigh-HozCont) >= (fabs(HozNeigh-HozPos)+fabs(HozCont-HozPos))){
                                 placed=true;
                                 rolling=false;
                                 break;
                             }
                             else{ // Disc should now Roll around Neigh NOT Cont
                                 HozCont=HozNeigh;VerCont=VerNeigh;rCont=RNeigh;
                                 contact=neigh;
                                 goto Rolling_label;
                             }
                         }
                     }
                     if(!placed){ // No collision with other particles so check for floor collision
                         // Check for floor
                         if((VerPos-r->back())<=VerMin){
                             placed=true;
                             rolling=false;
                             break;
                         }
                         angle -= dTh;
                     }
                 }
                 if(!placed){
                     rolling=false;
                     falling=true;
                 }
             }
         }
         // Check that we aren't full
         if(VerPos+r->back()>VerMax){
             placed=false;
             break;
         }
    }

    Hoz->back()=HozPos, Ver->back()=VerPos;
//   std::cout << "\n" << HozPos << " " << VerPos << std::endl;


    return placed;
}


int Voronoi(const Structure_t Sys,const double Interaction_radius,Voronoi_t*Data){

    std::cout << "Performing Voronoi Construction...\n" << std::flush;

    // Temporary values
    double Local_Grain_width = Sys.Grain_width;
    double Local_Grain_height=(2.0/sqrt(3.0))*Local_Grain_width;
    double x,y,z=0.0, Area_temp, Grain_x, Grain_y, min_angle, Angle_temp, Angle_Vertex1,length_X, length_Y, length_HYP,
          Distance, Radius=Interaction_radius;
    int cell_id, min_pos, ID_temp, count=0;
    std::normal_distribution<double> Gauss(0.0,1.0);
    bool TermOut = true;  // Flag to set system information terminal output
    bool DaD_OUT = false; // Flag to set drag and drop terminal output

    // Temporary Holders
    std::vector<double> vertices, angle_hold, x_hold, y_hold, vx_hold, vy_hold;
    std::vector<std::vector<double>> Vertex_X_temp, Vertex_Y_temp;
    std::vector<std::vector<int>> Neighbour_temp;
    std::vector<int> Grain_ID, Grain_neighbours;

    std::vector<double> X,Y,r;
    Data->x_max=Sys.Dim_x;
    Data->y_max=Sys.Dim_y;
    Data->Input_grain_width=Sys.Grain_width;


    double mulg = log(Sys.Grain_width/sqrt(1.0+((pow(Sys.Grain_width*Sys.StdDev_grain_pos,2))/(pow(Sys.Grain_width,2)))));
    std::lognormal_distribution<double> LN(mulg,Sys.StdDev_grain_pos);


    if(Sys.StdDev_grain_pos>0.0){
        // Using Drop and Roll function
        bool space=true;
        int attempt=0, attempt_limit=100;
        std::uniform_int_distribution<int> Dirn(0,3);

        std::cout << "\tFilling space..." << std::flush;
        unsigned int Disc=0;
        int Di=0;
        while(space){
            if(attempt==attempt_limit){space=false;}

            X.push_back(0.0);
            Y.push_back(0.0);
            double radius=0.0;
            do{
                radius=LN(Gen)*0.5;
            }
            while(radius>Sys.Max_width_limit*(Sys.Grain_width*0.5) || radius<0.0*(Sys.Grain_width*0.5) ); // Max and Min limits
            r.push_back(radius);
            ++Di;//=Dirn(Gen);
            Di =0; //%= 3;
            bool Placed=false;
            std::vector<double> Hoz(X.size(),0.0), Ver(X.size(),0.0);

            // Set direction of drop
            switch (Di){
            case 0:     // From TOP
                Hoz=X;
                Ver=Y;
                Placed = Drag_and_Drop(0.0,Data->x_max,0.0,Data->y_max,&Hoz,&Ver,&r);
                break;
            case 1:     // From BOT
                Hoz=X;
                for(unsigned int p=0;p<Y.size();++p){
                    Ver[p] = -Y[p]+Data->y_max;
                }
                Placed = Drag_and_Drop(0.0,Data->x_max,0.0,Data->y_max,&Hoz,&Ver,&r);
                break;
            case 2:     // From RHS
                Hoz=Y;
                Ver=X;
                Placed = Drag_and_Drop(0.0,Data->y_max,0.0,Data->x_max,&Hoz,&Ver,&r);
                break;
            case 3:     // From LHS
                Hoz=Y;
                for(unsigned int p=0;p<Y.size();++p){
                    Ver[p]=-X[p]+Data->x_max;
                }
                Placed = Drag_and_Drop(0.0,Data->y_max,0.0,Data->x_max,&Hoz,&Ver,&r);
                break;
            }

            if(Placed){
                if(DaD_OUT){std::cout << "\t Placed! " << Disc << " ";}
                attempt=0;

                // Convert placed positions from relative values used in function
                switch (Di){
                case 0:     // From TOP
                    X.back()=Hoz.back();
                    Y.back()=Ver.back();
                    break;
                case 1:     // From BOT
                    X.back()=Hoz.back();
                    Y.back()=-1.0*(Ver.back()-Data->y_max);
                    break;
                case 2:     // From RHS
                    Y.back()=Hoz.back();
                    X.back()=Ver.back();
                    break;
                case 3:     // From LHS
                    Y.back()=Hoz.back();
                    X.back()=-1.0*(Ver.back()-Data->x_max);
                    break;
                }
                if(DaD_OUT){std::cout << "\tCoords (" << X.back() << "," << Y.back() << ")" << std::endl;}
            }
            else{
                if(DaD_OUT){std::cout << "\tFailed!" << std::endl;}
                --Disc;
                ++attempt;
                X.pop_back();
                Y.pop_back();
                r.pop_back();
            }
            ++Disc;
        }

        std::cout << "System filled." << std::endl;
        // This needs to use an average area value not just area for the input mean radius.
        //    std::cout << "\tExpected average area: " << area <<  " nm^2" << std::endl;

        std::ofstream DD("Output/Drag_and_drop.dat");
        std::ofstream DDA("Output/Drag_and_drop_AREA.dat");
        std::ofstream DDR("Output/Drag_and_drop_RAD.dat");
        for(unsigned int p=0;p<r.size();++p){
            DD << p << " " << X[p] << " " << Y[p] << " " << r[p] << std::endl;
            DDA << PI*r[p]*r[p] << std::endl;
            DDR << 2.0*r[p] << std::endl;
        }
        DD.close();
        DDA.close();
        DDR.close();

        // Determine packing fraction
        double Cvol = (Data->x_max-0.0)*(Data->y_max-0.0);
        double Vvol = 0.0;
        for(size_t p=0;p<r.size();++p){
            Vvol += PI*r[p]*r[p];
        }

        if(TermOut){
            std::cout << "\n\tContainer volume : " << Cvol
                    << "\n\tDisc volume      : " << Vvol
                    << "\n\tVolume diff      : " << Cvol-Vvol
                    << "\n\tPf               : " << (Vvol/Cvol)*100.0  << "%\n" << std::endl;
        }
    }
    else{
        int Bx=int(Sys.Dim_x/(Local_Grain_width));
        int By=int(Sys.Dim_y/(1.75*Local_Grain_height));
        Data->x_max = Bx*Local_Grain_width;
        Data->y_max = 1.5*By*Local_Grain_height;
        for(int j=0;j<By;++j){
            for(int i=0;i<Bx;++i){

                x = 0.5*Local_Grain_width  + Local_Grain_width*double(i);
                y = 0.5*Local_Grain_height + Local_Grain_height*(double(j)*1.5);
                X.push_back(x);
                Y.push_back(y);
                r.push_back(Local_Grain_width);
                x = 0.5*Local_Grain_width + Local_Grain_width*0.5 + Local_Grain_width*double(i);
                y = 0.5*Local_Grain_height+Local_Grain_height*0.75 + Local_Grain_height*(double(j)*1.5);
                X.push_back(x);
                Y.push_back(y);
                r.push_back(Local_Grain_width);
            }
        }
    }
//##################################NEW METHOD#########################################//
    double Block_size=0.0;
    if(Data->x_max<Data->y_max){
        Block_size = Data->x_max*0.1;
    }
    else{
        Block_size = Data->y_max*0.1;
    }

    const int Blocks_x = int(Sys.Dim_x/Block_size);
    const int Blocks_y = int(Sys.Dim_y/Block_size);
    const int Blocks_z = 1;

    if(TermOut){
        std::cout << "\tx_max: " << Data->x_max << " y_max: " << Data->y_max << std::endl;
    }
    int Iter=0;
    bool Lloyd=true;

while(Lloyd){

    // Create the container with the desired geometry.
    voro::container_poly       Container(0.0,Data->x_max,0.0,Data->y_max,-1.0,1.0,Blocks_x,Blocks_y,Blocks_z,true,true,false,8);
    voro::c_loop_all           Container_loop(Container);
    voro::voronoicell_neighbor Cell_with_neighbour_info;

//##############################GENERATE GRAIN POSITIONS###############################//
    for(unsigned int Disc=0;Disc<r.size();++Disc){
        ++count;
        Container.put(count,X[Disc],Y[Disc],z,r[Disc]);
    }
    count = 0;
//##################################OLD METHOD#########################################//

    Grain_ID.clear();
    x_hold.clear();y_hold.clear();
    vx_hold.clear();vy_hold.clear();
    Vertex_X_temp.clear();Vertex_Y_temp.clear();
    angle_hold.clear();
    Grain_neighbours.clear();
    Data->Vertex_X_final.clear();
    Data->Vertex_Y_final.clear();
    Data->Grain_Area.clear();
    Data->Grain_diameter.clear();
    Data->Contact_lengths.clear();
    Data->Neighbour_final.clear();
    Data->Geo_grain_centre_X.clear();
    Data->Geo_grain_centre_Y.clear();
    Neighbour_temp.clear();

//#########################OBTAIN CELL ID; X,Y COORD; VERTICES#########################//
    if(Container_loop.start()){
        do if(Container.compute_cell(Cell_with_neighbour_info,Container_loop)){
            Container_loop.pos(x,y,z);
//            std::cout << x << " " << y << " " << z << std::endl;
            cell_id = Container_loop.pid();
//            std::cout << "Cell: " << cell_id << std::endl;
            Grain_ID.push_back(cell_id);
//            std::cout << Grain_ID.size() << std::endl;
            x_hold.push_back(x);y_hold.push_back(y);
            Cell_with_neighbour_info.vertices(x,y,z,vertices);
            for(size_t i=0; i<vertices.size(); i+=3){
                if(vertices[i+2] == 1){
                    vx_hold.push_back(vertices[i]);
                    vy_hold.push_back(vertices[i+1]);
            }    }
            Vertex_X_temp.push_back(vx_hold);
            Vertex_Y_temp.push_back(vy_hold);
            vx_hold.clear();
            vy_hold.clear();
        }
        while (Container_loop.inc());
    }

    Data->Pos_X_final.resize(x_hold.size());
    Data->Pos_Y_final.resize(y_hold.size());
    Data->Vertex_X_final.resize(x_hold.size());
    Data->Vertex_Y_final.resize(y_hold.size());
    Neighbour_temp.resize(Data->Pos_X_final.size());

    Data->Num_Grains = Data->Pos_X_final.size();
    if(TermOut){
        std::cout << "\tNumber of Grains per layer: " << Data->Num_Grains << std::endl;
    }

//################################OBTAIN NEIGHBOUR INFO################################//
    if(Container_loop.start()){
        do if(Container.compute_cell(Cell_with_neighbour_info,Container_loop)){
            Cell_with_neighbour_info.neighbors(Grain_neighbours);
            for(size_t i=0; i<Grain_neighbours.size();++i){
                if(Grain_neighbours[i]>0){
                    Neighbour_temp[count].push_back(Grain_neighbours[i]-1);
            }    }
            ++count;
        }
        while (Container_loop.inc());
    }
    count=0;
//########################REORDER LISTS SO INDEX MATCHES GRAIN ID######################//
    Data->Neighbour_final.resize(Data->Pos_X_final.size());
    for(size_t i=0;i<Data->Pos_X_final.size();++i){
        ID_temp = Grain_ID[i]-1;            // Adjusts ID system to start from 0 instead of 1.
        Data->Pos_X_final[ID_temp] = x_hold[i];        // Place grain info into position=NewID.
        Data->Pos_Y_final[ID_temp] = y_hold[i];
        Data->Vertex_X_final[ID_temp].resize(Vertex_X_temp[i].size());
        Data->Vertex_Y_final[ID_temp].resize(Vertex_Y_temp[i].size());
        Data->Neighbour_final[ID_temp].resize(Neighbour_temp[i].size());
        for(size_t j=0;j<Vertex_X_temp[i].size();++j){
            Data->Vertex_X_final[ID_temp][j]=Vertex_X_temp[i][j];
            Data->Vertex_Y_final[ID_temp][j]=Vertex_Y_temp[i][j];
        }
        for(size_t j=0;j<Vertex_X_temp[i].size();++j){
            Data->Neighbour_final[ID_temp][j] = Neighbour_temp[i][j];
    }    }
    x_hold.clear();
    y_hold.clear();
//################################REORDER VERTEX LIST #################################//
    for(unsigned int i=0;i<Data->Num_Grains;++i){
      Grain_x=Data->Pos_X_final[i];
      Grain_y=Data->Pos_Y_final[i];
      vx_hold.resize(Data->Vertex_X_final[i].size());
      vy_hold.resize(Data->Vertex_Y_final[i].size());
      for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
          x_hold.push_back(Data->Vertex_X_final[i][j]);
          y_hold.push_back(Data->Vertex_Y_final[i][j]);
      }
      for(size_t j=0;j<x_hold.size();++j){
          Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          angle_hold.push_back(Angle_temp);
      }
      min_pos=0;
      for(size_t j=0;j<angle_hold.size();++j){
          min_angle=7.0;
          for(size_t k=0;k<angle_hold.size();++k){
              if(angle_hold[k]<min_angle){
                  min_angle=angle_hold[k];
                  min_pos=k;
          }    }
          vx_hold[j]=x_hold[min_pos];
          vy_hold[j]=y_hold[min_pos];
          angle_hold[min_pos]=7.0;
      }
      for(size_t j=0;j<vx_hold.size();++j){
          Data->Vertex_X_final[i][j]=vx_hold[j];
          Data->Vertex_Y_final[i][j]=vy_hold[j];
      }
        x_hold.clear();
        y_hold.clear();
        vx_hold.clear();
        vy_hold.clear();
        angle_hold.clear();
    }
//#############################DETERMINE SYSTEM CENTRE#################################//
    double Vx, Vy;
    for(unsigned int grain=0;grain<Data->Num_Grains;++grain){
      for(size_t vertex=0;vertex<Data->Vertex_X_final[grain].size();++vertex){
          Vx = Data->Vertex_X_final[grain][vertex];
          Vy = Data->Vertex_Y_final[grain][vertex];
          if(Vx>Data->Vx_MAX){Data->Vx_MAX=Vx;}
          else if(Vx<Data->Vx_MIN){Data->Vx_MIN=Vx;}
          if(Vy>Data->Vy_MAX){Data->Vy_MAX=Vy;}
          else if(Vy<Data->Vy_MIN){Data->Vy_MIN=Vy;}
    }    }
    Data->Centre_X = (Data->Vx_MAX-Data->Vx_MIN)/2.0+Data->Vx_MIN;
    Data->Centre_Y = (Data->Vy_MAX-Data->Vy_MIN)/2.0+Data->Vy_MIN;

    if(TermOut){
        std::cout << "\n\tSystem dimensions:\n\t\tX: " << Data->Vx_MIN << " to " << Data->Vx_MAX
                  << "\n\t\tY: " << Data->Vy_MIN << " to " << Data->Vy_MAX
                  << "\n\t\tCentre: " << Data->Centre_X << " , " << Data->Centre_Y << "\n" << std::endl;
    }
//################################APPLY GRAIN SPACING##################################//
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        double local_Px=Data->Pos_X_final[grain_num], local_Py=Data->Pos_Y_final[grain_num];
        for(size_t vert=0;vert<Data->Vertex_X_final[grain_num].size();++vert){
            double local_Vx, local_Vy;
            local_Vx = Data->Vertex_X_final[grain_num][vert];
            local_Vy = Data->Vertex_Y_final[grain_num][vert];
            Data->Vertex_X_final[grain_num][vert] = local_Px+(local_Vx-local_Px)*sqrt(Sys.Packing_fraction);
            Data->Vertex_Y_final[grain_num][vert] = local_Py+(local_Vy-local_Py)*sqrt(Sys.Packing_fraction);
    }    }
//#################################DETERMINE GRAIN AREA################################//
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        Area_temp=0.0;
        for(size_t i=0;i<Data->Vertex_X_final[grain_num].size();++i){
            int k=(i+1)%Data->Vertex_X_final[grain_num].size();
                Area_temp += (Data->Vertex_X_final[grain_num][i]*Data->Vertex_Y_final[grain_num][k]);
                Area_temp -= (Data->Vertex_X_final[grain_num][k]*Data->Vertex_Y_final[grain_num][i]);
            }
        Data->Grain_Area.push_back(Area_temp*0.5);
        Data->Grain_diameter.push_back(2*sqrt((Area_temp*0.5)/PI)); // circle assumption
    }
//###################REORDER NEIGHBOUR LIST TO MATCH VERTEX ORDERING###################//
    /* Here the Neighbours list is reordered, this is done by determining the angle (anti-clockwise)
     * from the grain and its neighbour. In order to match the vertices ordering, all determined
     * angles are shifted by the angle between the grain and the first vertex.
     */
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        vx_hold.resize(Data->Neighbour_final[grain_num].size());
        Grain_x=Data->Pos_X_final[grain_num];
        Grain_y=Data->Pos_Y_final[grain_num];
        Angle_Vertex1 = atan2((Data->Vertex_Y_final[grain_num][0]-Grain_y),(Data->Vertex_X_final[grain_num][0]-Grain_x));
      if(Angle_Vertex1<0){Angle_Vertex1+=6.283185307179586;}
        for(size_t j=0;j<Data->Neighbour_final[grain_num].size();++j){
            // Store Neighbour Coords
            x_hold.push_back(Data->Pos_X_final[Data->Neighbour_final[grain_num][j]]);
            y_hold.push_back(Data->Pos_Y_final[Data->Neighbour_final[grain_num][j]]);
        }
        for(size_t j=0;j<x_hold.size();++j){
            if((Grain_x-x_hold[j]) > (Data->Centre_X)){x_hold[j] += Data->x_max;}        // LEFT
            else if((Grain_x-x_hold[j]) < (-Data->Centre_X)){x_hold[j] -= Data->x_max;}    //RIGHT
            if((Grain_y-y_hold[j]) < (-Data->Centre_Y)){y_hold[j] -= Data->y_max;}        //TOP
            else if((Grain_y-y_hold[j]) > (Data->Centre_Y)){y_hold[j] += Data->y_max;}    //BOT
        }

          for(size_t j=0;j<x_hold.size();++j){
          Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          Angle_temp -= Angle_Vertex1;
          if(Angle_temp<0){Angle_temp+=6.283185307179586;}
          angle_hold.push_back(Angle_temp);
        }
        min_pos=0;
        for(size_t j=0;j<angle_hold.size();++j){
             min_angle=7.0;
             for(size_t k=0;k<angle_hold.size();++k){
                 if(angle_hold[k]<min_angle){
                     min_angle=angle_hold[k];
                     min_pos=k;
             }    }
             vx_hold[j]=Data->Neighbour_final[grain_num][min_pos];
             angle_hold[min_pos]=7.0;
        }
        for(size_t j=0;j<x_hold.size();++j){
          Data->Neighbour_final[grain_num][j]=vx_hold[j];
        }
        x_hold.clear();
        y_hold.clear();
        vx_hold.clear();
        vy_hold.clear();
        angle_hold.clear();
    }
//##############################DETERMINE CONTACT LENGTHS##############################//
    Data->Contact_lengths.resize(Data->Num_Grains);
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        for(size_t i=0;i<Data->Vertex_X_final[grain_num].size();++i){
            int k=(i+1)%Data->Vertex_X_final[grain_num].size();
            length_X = fabs(Data->Vertex_X_final[grain_num][i]-Data->Vertex_X_final[grain_num][k]);
            length_Y = fabs(Data->Vertex_Y_final[grain_num][i]-Data->Vertex_Y_final[grain_num][k]);
            length_HYP = sqrt(length_X*length_X+length_Y*length_Y);
            Data->Contact_lengths[grain_num].push_back(length_HYP);
    }    }
//########################DETERMINE AVG CONTACT LENGTH AND AREA########################//
    int counter=0;
    double AVG_Grain_diameter=0.0, VARIANCE=0.0;
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        AVG_Grain_diameter += Data->Grain_diameter[grain_num];
    }
    AVG_Grain_diameter /= Data->Num_Grains;
    for(unsigned int grain=0;grain<Data->Num_Grains;++grain){
        VARIANCE   += pow(Data->Grain_diameter[grain]-AVG_Grain_diameter,2.0);
    }
    VARIANCE /= Data->Num_Grains;
    double STDDEV = sqrt(VARIANCE);
    double Esq = AVG_Grain_diameter*AVG_Grain_diameter;// (Data->Average_area*Data->Average_area);
    double mu=log(Esq/sqrt(VARIANCE+Esq));
    double sig=sqrt(log((VARIANCE/Esq)+1.0));
    Data->Real_grain_width=sqrt(1+sig*sig)*exp(mu);
    if(TermOut){
        std::cout << "\n\tInput Grain parameter distribution: \n\t" <<
                     "\tmu: "      << mulg <<
                     "\tsigma:"    << Sys.StdDev_grain_pos <<
                     "\tMean[D]: " << sqrt(1+Sys.StdDev_grain_pos*Sys.StdDev_grain_pos)*exp(mulg) <<
                     "nm" << std::endl;
        /* Convert parameters from AREA distribution to DIAMETER distribution. */
        std::cout << "\tObtained grain size distribution: \n\t" <<
                     "\tmu: "      << mu <<
                     " sigma: "    << sig <<
                     "\tMean[D]: " << Data->Real_grain_width << "nm\n" << std::endl;
    }
    Data->Average_area = PI*pow(((sqrt(1+sig*sig)*exp(mu))*0.5),2.0);
    if(TermOut){std::cout << "\tGrain Area | Average = " << Data->Average_area << "nm^2" <<
                " | Standard deviation = " << PI*pow(STDDEV*0.5,2.0) << "nm^2" << std::endl;}

    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        for(size_t neigh_num=0;neigh_num<Data->Contact_lengths[grain_num].size();++neigh_num){
            Data->Average_contact_length += Data->Contact_lengths[grain_num][neigh_num];
            ++counter;
    }    }
    Data->Average_contact_length /= counter;
    if(TermOut){std::cout << "\tAverage grain contact length: " << Data->Average_contact_length << "nm" << std::endl;}
//##########################DETERMINE GEOMETRIC GRAIN CENTRES##########################//
    Data->Geo_grain_centre_X.resize(Data->Pos_X_final.size());
    Data->Geo_grain_centre_Y.resize(Data->Pos_Y_final.size());

    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        double CentroidX=0.0,CentroidY=0.0;
        for(size_t i=0;i<Data->Vertex_X_final[grain_num].size();++i){
            int k=(i+1)%Data->Vertex_X_final[grain_num].size();
            CentroidX += (Data->Vertex_X_final[grain_num][i]+Data->Vertex_X_final[grain_num][k])
                       * (Data->Vertex_X_final[grain_num][i]*Data->Vertex_Y_final[grain_num][k]
                       -  Data->Vertex_X_final[grain_num][k]*Data->Vertex_Y_final[grain_num][i]);

            CentroidY += (Data->Vertex_Y_final[grain_num][i]+Data->Vertex_Y_final[grain_num][k])
                       * (Data->Vertex_X_final[grain_num][i]*Data->Vertex_Y_final[grain_num][k]
                       -  Data->Vertex_X_final[grain_num][k]*Data->Vertex_Y_final[grain_num][i]);
        }
        Data->Geo_grain_centre_X[grain_num] = CentroidX/(6.0*Data->Grain_Area[grain_num]);
        Data->Geo_grain_centre_Y[grain_num] = CentroidY/(6.0*Data->Grain_Area[grain_num]);
    }
//###################################LLoyd's algorithm##################################//
/*  This repeats the Voronoi construction replacing the lattice sites
 *  with the controids from the previous construction.                  */
    double DeltaX=0.0,DeltaY=0.0;
    for(size_t i=0;i<X.size();++i){
        DeltaX += fabs(X[i]-Data->Geo_grain_centre_X[i]);
        DeltaY += fabs(Y[i]-Data->Geo_grain_centre_Y[i]);
    }
    DeltaX/=X.size();
    DeltaY/=X.size();
    std::cout << "\tCentroid lattice site delta: " << DeltaX << "nm " << DeltaY << "nm" << std::endl;
    if(DeltaX>1e8 || DeltaY>1e8){ // Currently disabled. Reduce limits to enable.
        TermOut=false;
        X=Data->Geo_grain_centre_X;
        Y=Data->Geo_grain_centre_Y;
        ++Iter;
        std::cout << "\tRepeating... " << Iter << std::endl;
    }
    else{Lloyd=false;}
}
    std::cout << std::endl;

//####################DETERMINE NEIGHBOURS FOR MAGNETOSTATIC MATRIX####################//
    if(Sys.Magnetostatics_gen_type=="dipole"){
        // Ensure the interaction radius is no larger then half the system size
        if (Radius>Data->Centre_X || Radius>Data->Centre_Y){
            std::cout << "\n\tInteraction radius too large" << std::endl;
            std::cout << "\t\tAdjusting radius..." << std::endl;
            if(Data->Centre_X>Data->Centre_Y){Radius=Data->Centre_Y/2.0;}
            else{Radius=Data->Centre_X/2.0;}
            std::cout << "\t\t    New Radius = " << Radius << "nm" << std::endl;
        }
        // Save interaction radius
        Data->Int_Rad = Radius;
        Data->Magnetostatic_neighbours.resize(Data->Num_Grains);
        double Neigh_x, Neigh_y;

        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
            Grain_x=Data->Pos_X_final[grain_num];            // DETERMINE GRAIN POSITION
            Grain_y=Data->Pos_Y_final[grain_num];
            for(unsigned int neigh_num=0;neigh_num<Data->Num_Grains;++neigh_num){
                if(neigh_num!=grain_num){            // PREVENT SELF-COUNTING
                    Neigh_x=Data->Pos_X_final[neigh_num];
                    Neigh_y=Data->Pos_Y_final[neigh_num];
                    if((Grain_x-Neigh_x) > (Data->Centre_X/2.0)){Neigh_x += Data->x_max;}                // LHS
                    else if((Grain_x-Neigh_x) < (-Data->Centre_X)){Neigh_x -= Data->x_max;}            // RHS
                    if((Grain_y-Neigh_y) < (-Data->Centre_Y)){Neigh_y -= Data->y_max;}                // TOP
                    else if((Grain_y-Neigh_y) > (Data->Centre_Y)){Neigh_y += Data->y_max;}            // BOT

                    Distance = sqrt((Grain_x-Neigh_x)*(Grain_x-Neigh_x) + (Grain_y-Neigh_y)*(Grain_y-Neigh_y));
                    if(Distance < Radius){Data->Magnetostatic_neighbours[grain_num].push_back(neigh_num);}
        }    }    }

        //---------------------------REORDER LIST    ---    Not sure if required.
        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
            vx_hold.resize(Data->Magnetostatic_neighbours[grain_num].size());
            Grain_x=Data->Pos_X_final[grain_num];
            Grain_y=Data->Pos_Y_final[grain_num];
            Angle_Vertex1 = atan2((Data->Vertex_Y_final[grain_num][0]-Grain_y),(Data->Vertex_X_final[grain_num][0]-Grain_x));
            if(Angle_Vertex1<0){Angle_Vertex1+=6.283185307179586;}
            for(size_t j=0;j<Data->Magnetostatic_neighbours[grain_num].size();++j){
                // Store Neighbour Coords
                x_hold.push_back(Data->Pos_X_final[Data->Magnetostatic_neighbours[grain_num][j]]);
                y_hold.push_back(Data->Pos_Y_final[Data->Magnetostatic_neighbours[grain_num][j]]);
            }
            for(size_t j=0;j<x_hold.size();++j){
                if((Grain_x-x_hold[j]) > (Data->Centre_X)){x_hold[j] += Data->x_max;}        // LEFT
                else if((Grain_x-x_hold[j]) < (-Data->Centre_X)){x_hold[j] -= Data->x_max;}    // RIGHT
                if((Grain_y-y_hold[j]) < (-Data->Centre_Y)){y_hold[j] -= Data->y_max;}        // TOP
                else if((Grain_y-y_hold[j]) > (Data->Centre_Y)){y_hold[j] += Data->y_max;}    // BOT
            }

            for(size_t j=0;j<x_hold.size();++j){
                Angle_temp = atan2((y_hold[j]-Grain_y),(x_hold[j]-Grain_x));
                if(Angle_temp<0){Angle_temp+=6.283185307179586;}
                Angle_temp -= Angle_Vertex1;
                if(Angle_temp<0){Angle_temp+=6.283185307179586;}
                angle_hold.push_back(Angle_temp);
            }
            min_pos=0;
            for(size_t j=0;j<angle_hold.size();++j){
                min_angle=7.0;
                for(size_t k=0;k<angle_hold.size();++k){
                    if(angle_hold[k]<min_angle){
                        min_angle=angle_hold[k];
                        min_pos=k;
                }    }
                vx_hold[j]=Data->Magnetostatic_neighbours[grain_num][min_pos];
                angle_hold[min_pos]=7.0;
            }
            for(size_t j=0;j<x_hold.size();++j){
                Data->Magnetostatic_neighbours[grain_num][j]=vx_hold[j];
            }
            x_hold.clear();
            y_hold.clear();
            vx_hold.clear();
            vy_hold.clear();
            angle_hold.clear();
        }

        double Avg_Mneigh=0.0;
        for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){Avg_Mneigh += Data->Magnetostatic_neighbours[grain_num].size();}
        Avg_Mneigh /= Data->Num_Grains;
        std::cout << "\tAverage Magnetostatic neighbours: " << Avg_Mneigh << std::endl;
    }
    double Avg_Neigh=0.0;
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){Avg_Neigh += Data->Neighbour_final[grain_num].size();}
    Avg_Neigh /= Data->Num_Grains;
    std::cout << "\tAverage Exchange Neighbours: " << Avg_Neigh << std::endl;
//#################################WRITE DATA TO FILES#################################//
    // POSITIONS
    std::ofstream POS_FILE("Output/pos_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        POS_FILE << Data->Pos_X_final[i] << " " << Data->Pos_Y_final[i] << "\n";
    }
    POS_FILE.flush();
    POS_FILE.close();
    // GEOMETRIC CENTRES
    std::ofstream GEO_FILE("Output/geo_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        GEO_FILE << Data->Geo_grain_centre_X[i] << " " << Data->Geo_grain_centre_Y[i] << "\n";
    }
    GEO_FILE.flush();
    GEO_FILE.close();
    // VERTICES
    std::ofstream VERT_FILE("Output/vert_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        VERT_FILE << i << " " << Data->Vertex_X_final[i].size() << "\n";
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            VERT_FILE << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        VERT_FILE << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
        VERT_FILE << "\n";
    }
    VERT_FILE.flush();
    VERT_FILE.close();

    std::ofstream VERT_GNU_FILE("Output/gnuplot_vert_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            VERT_GNU_FILE << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        VERT_GNU_FILE << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
        VERT_GNU_FILE << "\n\n";
    }
    VERT_GNU_FILE.flush();
    VERT_GNU_FILE.close();

    // NEIGHBOURS
    std::ofstream NEIGH_FILE("Output/neigh_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        NEIGH_FILE << i << " " << Data->Neighbour_final[i].size() << "\n";
        for(size_t j=0;j<Data->Neighbour_final[i].size();++j){
            NEIGH_FILE << Data->Neighbour_final[i][j] << " ";
        }
        NEIGH_FILE << "\n";
    }
    NEIGH_FILE.flush();
    NEIGH_FILE.close();
    if(Sys.Magnetostatics_gen_type=="dipole"){
        // MAGNETOSTATIC NEIGHBOURS
        std::ofstream MAG_NEIGH_FILE("Output/mag_neigh_file.dat");
        for(unsigned int i=0;i<Data->Num_Grains;++i){
            MAG_NEIGH_FILE << i << " " << Data->Magnetostatic_neighbours[i].size() << "\n";
            for(size_t j=0;j<Data->Magnetostatic_neighbours[i].size();++j){
                MAG_NEIGH_FILE << Data->Magnetostatic_neighbours[i][j] << " ";
            }
            MAG_NEIGH_FILE << "\n";
        }
        MAG_NEIGH_FILE.flush();
        MAG_NEIGH_FILE.close();
    }
    // AREAS
    std::ofstream AREA_FILE("Output/area_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        AREA_FILE << Data->Grain_Area[i] << "\n";
    }
    AREA_FILE.flush();
    AREA_FILE.close();
    // DIAMETERS
    std::ofstream DIAMETER_FILE("Output/diameter_file.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        DIAMETER_FILE << Data->Grain_diameter[i] << "\n";
    }
    DIAMETER_FILE.flush();
    DIAMETER_FILE.close();
    // CONTACT LENGTHS
    std::ofstream CONTACT_FILE("Output/contact_file.dat");
    for(size_t i=0;i<Data->Pos_X_final.size();++i){
      CONTACT_FILE << i << " " << Data->Contact_lengths[i].size() << "\n";
      for(size_t j=0;j<Data->Contact_lengths[i].size();++j){
          CONTACT_FILE << Data->Contact_lengths[i][j] << " ";
      }
      CONTACT_FILE << "\n";
    }
    CONTACT_FILE.flush();
    CONTACT_FILE.close();
    // OUTPUT FOR IMPROVED DETERMINATION OF MAGNETOSTATICS VIA SEPARATE CODE
    std::ofstream MAGNETO_OUT_1("Output/pc.dat");
    MAGNETO_OUT_1 << Data->Num_Grains << " " << Data->x_max << " " << Data->y_max << " " << Data->Average_area << std::endl;
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        MAGNETO_OUT_1 << i << " " << Data->Pos_X_final[i] << " " << Data->Pos_Y_final[i] << " " << Data->Neighbour_final[i].size() << std::endl;
    }
    MAGNETO_OUT_1.flush();
    MAGNETO_OUT_1.close();
    std::ofstream MAGNETO_OUT_2("Output/neighcheck.dat");
    for(unsigned int grain_num=0;grain_num<Data->Num_Grains;++grain_num){
        MAGNETO_OUT_2 << grain_num << "\n" << Data->Neighbour_final[grain_num].size() << std::endl;
        for(size_t i=0;i<Data->Neighbour_final[grain_num].size();++i){
            int k=(i+1)%Data->Neighbour_final[grain_num].size();

            MAGNETO_OUT_2 << Data->Neighbour_final[grain_num][i] << " "
                          << Data->Vertex_X_final[grain_num][i] << " " << Data->Vertex_Y_final[grain_num][i] << " "
                          << Data->Vertex_X_final[grain_num][k] << " " << Data->Vertex_Y_final[grain_num][k] << " "
                          << std::endl;
    }    }
    MAGNETO_OUT_2.flush();
    MAGNETO_OUT_2.close();
    std::ofstream MAGNETO_OUT_3("Output/initgs.dat");
    for(unsigned int i=0;i<Data->Num_Grains;++i){
        MAGNETO_OUT_3 << "# " << i << " " << Data->Vertex_X_final[i].size() << std::endl;
        for(size_t j=0;j<Data->Vertex_X_final[i].size();++j){
            MAGNETO_OUT_3 << Data->Vertex_X_final[i][j] << " " << Data->Vertex_Y_final[i][j] << "\n";
        }
        MAGNETO_OUT_3 << Data->Vertex_X_final[i][0] << " " << Data->Vertex_Y_final[i][0] << "\n";
    }
    MAGNETO_OUT_3.flush();
    MAGNETO_OUT_3.close();
    std::cout << "Construction completed." << std::endl;
    return 0;
}
