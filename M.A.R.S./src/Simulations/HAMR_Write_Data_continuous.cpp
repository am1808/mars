/*
 * HAMR_Write_Data_continuous.cpp
 *
 *  Created on: 28 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */
/** \file HAMR_Write_Data_continuous.cpp
 * \brief  Simulation of HAMR writing process with a continuously moving write head.
 *
 * <P> Data is written to the system via heat assisted magnetic recording. The write head is set to move
 * continuously applying a magnetic field once the head is in the position of a bit. The laser pulse
 * follows the write head and can be set to be either applied constantly or only when the head is
 * writing a bit, similar to the magnetic field. The system is first equilibrated for
 * <B><EM> HAMR:Equilibration_time </EM></B>, after this, all bit positions are calculated. If a
 * track is too long for the system then the number of bits per track is reduced, the same is done
 * for the number of tracks. A list of grains in each bit is also determined, allowing for individual
 * data for each bit to be output. </P>
 * <P> Each time step consists of an LLB integration, a movement of the write head and, if the head is
 * positions within a bit, the application of a magnetic field. Once all bits in a track have been
 * written the write head is moved back to its initial x-position and set to the centre of the
 * track in the y-direction. The process is then repeated for all required tracks. </P>
 * <P> Once all data has been written the system is exported for use with read back simulations. if
 * exclusion has been used then the system is made to equilibrate (forcing any non-integrated grains
 * to respond the change in the system). The system is then exported again. If
 * <B><EM> HAMR:ReadBack </EM></B> is set to <B> True </B> then a read back simulation is performed
 * before the simulation exits. </P> */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>


#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/RecHead.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Importers/Read_in_import.hpp"
#include "../../hdr/Importers/Create_system_from_output.hpp"

#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Simulations/Read_back.hpp"

#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_layer_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"
#include "../../hdr/Data_output/Export_system.hpp"

#include "../../hdr/Classes/PixelMap.hpp"
#include "../../hdr/Classes/Solver.hpp"


/** Performs HAMR data write over an entire system with a continuously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 *         <UL>
 *            <LI> Type <LI> SEED
 *        </UL>
 * <LI> Struct:
 *         <UL>
 *             <LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 *             <LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 *         </UL>
 * <LI> LLB:
 *         <UL>
 *             <LI> dt <LI> Exclusion_zone
 *             <LI> <STRONG> If Exclusion_zone == True </STRONG>
 *             <UL>
 *                 <LI> Exclusion_range_negX <LI> Exclusion_range_posX <LI> Exclusion_range_negY <LI> Exclusion_range_posY
 *             </UL>
 *         </UL>
 * <LI> HAMR:
 *         <UL>
 *             <LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 *             <LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 *             <LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 *             <LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Bit_width
 *             <LI> Bit_spacing <LI> Bit_spacing_X <LI> Bit_spacing_Y <LI> Bit_number_in_X
 *             <LI> Bit_number_in_Y <LI> Data_Binary <LI> Write_head_speed
 *         </UL>
 * <LI> EXPT:
 *         <UL>
 *             <LI> ReadBack
 *             <LI> <STRONG> If ReadBack == True </STRONG>
 *                 <UL>
 *                     <LI> Read_Head_speed <LI> Read_Head_record_interval <LI> Read_Head_width_X <LI> Read_Head_width_Y
 *                 </UL>
 *         </UL>
 * </UL>
 * */
int HAMR_write_data_cont(ConfigFile cfg){

    double Equilibration_time = cfg.getValueOfKey<double>("HAMRWC:Equilibration_time");
    double Environ_Temp = cfg.getValueOfKey<double>("HAMRWC:Environment_Temp");
    bool ReadBack = cfg.getValueOfKey<bool>("HAMRWC:ReadBack_after_write");
    bool ReadInput = cfg.getValueOfKey<bool>("HAMRWC:Read_input_structure");
    bool import_PixMap = cfg.getValueOfKey<bool>("HAMRWC:Import_PixelMap");
    // Declare all required variables
    Data_input_t IN_DATA;
    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    Interaction_t Int_system;
    Grain_t Grain;
    std::vector<ConfigFile> Materials_Config;
    std::vector<double> Normaliser,Mx,My,Mz,Ml,Grain_output_list;

    double Time = 0.0, Profile_time=0.0, Downtrack_time=0.0;
    double temperature,
           Head_X_init, Head_X, Head_Y;
    int File_count=0, Bit_X=0, Steps_until_output=0;
    unsigned int Track_num=0, Bit=0;
    int Tot_grains, Tot_bits, output_steps;
    std::string FILENAME_idv;
    std::string Output_switch_file_loc = "Output/HAMR_Grain_switching.dat";
    std::vector<std::string> Output_switch_buffer;
    std::string OUTPUT = "Output/HAMR_multi_layer.dat";
    std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends

    std::ofstream HEAD_OUTPUT ("Output/Head_position.dat");
    std::ofstream BIT_OUTPUT ("Output/Bit_desired_position.dat");

//####################IMPORT ALL REQUIRED DATA####################//
//     std::cout << "**** Setting solver ****" << std::endl;
     Structure_import(cfg, &Structure);
     Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
     Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
     Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grain);
     Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
//     std::cout << "\n **** Redefine solver for creation of system from scratch ****\n" << std::endl;
     Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
     if(ReadInput==true){
         std::cout << "Reading input file list for system import ..." << std::endl;
         Read_in_import(cfg, &IN_DATA);
         std::cout << "Reading in old system parameters ..." << std::endl;
         std::cout << "\tSolver : " << Solver.get_dt() << std::endl;
         Read_in_system(IN_DATA,&Structure,&Voronoi_data,&Materials,&Grain,&Int_system,&Solver);
         std::cout << "\tGrain width = " << Structure.Grain_width << std::endl;
         std::cout << "\tNum layers = " << Structure.Num_layers << std::endl;
         std::cout << "\tUse previously created system" << std::endl;
         std::cout << "\tExporting system after importing from old data" << std::endl;
         Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grain,Solver,"10");
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    RecHead  RecordingHead(cfg);
    RecLayer RecordingLayer(cfg,RecordingHead.getDirn().z);
    const Vec3 Hap_unit = RecordingHead.getDirn();
    double cell_size = Structure.Grain_width;
    PixelMap PixMap(Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,(cell_size/10.0),cfg,import_PixMap);
    if(import_PixMap==true){
        std::cout << "Exporting pixel map to file\n";
        PixMap.PrintwUpdate("PixelMap_from_input", Grain, DataType::ALL);
    }
    else{
        std::cout << "Descretising system into pixels " << std::endl;
        PixMap.Discretise(Voronoi_data, Grain);
    }
    // Best to check for suitable input before the simulation has been performed
    ReadLayer ReadingLayer(RecordingLayer);
    ReadHead ReadingHead(RecordingLayer);
//    std::cout << "\n **** Export system after setting read and write head parameters ****\n" << std::endl;
    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grain,Solver,"20");

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    if(RecordingHead.getRampTime()<Solver.get_dt()){
        std::cout << "Ramp time less than timestep!" << std::endl;
        exit(EXIT_FAILURE);
    }
//###################DETERMINE IMPORTANT VALUES###################//
    for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
    Tot_grains = Voronoi_data.Num_Grains*Structure.Num_layers;
    Tot_bits = RecordingLayer.getBitsA();
    output_steps = Solver.getOutputSteps();
    temperature = Environ_Temp;

    Head_X_init = -RecordingHead.getHwidthX()*0.5;
    Head_Y        = RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getBitSpacingY();
    Head_X        = Head_X_init;
    RecordingHead.setX(Head_X);
    RecordingHead.setY(Head_Y);

    double TOTAL_profile_time = RecordingLayer.getBitSizeX()/RecordingHead.getSpeed();
    double TOTAL_track_time   = Voronoi_data.Vx_MAX/RecordingHead.getSpeed();
    double Adj_Downtrack_time= -(RecordingHead.getHwidthX()*0.5)/RecordingHead.getSpeed(); // Adjusted downtrack time is used to start the laser pulse before the system.
    double Cooling_time = TOTAL_profile_time/6.0;
    double Tot_time = RecordingLayer.getBitsY()*TOTAL_track_time;

    std::cout << "\nSpeed = " << RecordingHead.getSpeed() << " nm/s" << std::endl;
    std::cout << "Time per bit = " << TOTAL_profile_time << " s" << std::endl;
    if(RecordingHead.getPulsing()){
        std::cout << "Cool_time = " << Cooling_time << " s" << std::endl;
    }
    std::cout << "Expected total writing time = "<< Tot_time << " s" << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##########Equilibrate the system for Equilibration_time#########//
    std::cout << "\nEquilibrating the system for " << Equilibration_time << " s" << std::endl;

    if(Solver.get_Exclusion()){
        std::cout << "Temporarily disabling Inclusion zone feature for equilibration" << std::endl;
        Solver.disableExclusion();
        // Remove laser pulse and applied field
        for(int g=0;g<Tot_grains;++g){
            Grain.Temp[g]=Environ_Temp;
            Grain.H_appl[g].x=0.0;
            Grain.H_appl[g].y=0.0;
            Grain.H_appl[g].z=0.0;
        }
        while(Time<Equilibration_time){
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
            Time += Solver.get_dt();
        }
        std::cout << "Equilibration terminated: resetting Inclusion zone for the rest of simulation" << std::endl;
        Solver.enableExclusion();
    }
    else{
        // Remove laser pulse and applied field
        for(int g=0;g<Tot_grains;++g){
            Grain.Temp[g]=Environ_Temp;
            Grain.H_appl[g].x=0.0;
            Grain.H_appl[g].y=0.0;
            Grain.H_appl[g].z=0.0;
        }
        while(Time<Equilibration_time){
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
            Time += Solver.get_dt();
        }
    }
    // Reset time to zero
    std::cout << "Resetting time to t = 0.0 s\n" << std::endl;
    Time = 0.0;

    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_switch_buffer);
    // Print initial configuration
    FILENAME_idv = std::to_string(File_count) + ".dat";
    Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
    HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
    PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grain, DataType::ALL);
    ++File_count;

    std::cout << "Output frequency: " << output_steps << " steps" << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###############Generate a list of bit positions#################//
    std::cout << "Generating Bit positions" << std::endl;
    double bit_position_X=  RecordingLayer.getBitSizeX()*0.5 + RecordingLayer.getBitSpacingX();
    double bit_position_Y=  RecordingLayer.getBitSizeY()*0.5 + RecordingLayer.getBitSpacingY();
    std::vector<std::pair<double,double>> Bit_positions;
    std::pair<double,double> XY_coords;
    for(unsigned int Bit_y=0;Bit_y<RecordingLayer.getBitsY();++Bit_y){
        for(unsigned int Bit_x=0;Bit_x<RecordingLayer.getBitsX();++Bit_x){
            XY_coords = std::make_pair(bit_position_X,bit_position_Y);
            Bit_positions.push_back(XY_coords);
            bit_position_X += RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX();
        }
        bit_position_X = Bit_positions[0].first;
        bit_position_Y += RecordingLayer.getBitSizeY()+RecordingLayer.getBitSpacingY();
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//######Generate a list of the grains contained in each bit#######//
    std::cout << "Generating grains in bit" << std::endl;
    std::vector<std::vector<unsigned int>> Bit_grain_list (Tot_bits);
    for(int Bit_number= 0;Bit_number<Tot_bits;++Bit_number){
        bit_position_X=Bit_positions[Bit_number].first;
        bit_position_Y=Bit_positions[Bit_number].second;

        for(unsigned int grain_in_layer=0;grain_in_layer<Voronoi_data.Num_Grains;++grain_in_layer){
            double grain_pos_X=Voronoi_data.Pos_X_final[grain_in_layer];
            double grain_pos_Y=Voronoi_data.Pos_Y_final[grain_in_layer];

            if(grain_pos_X>(bit_position_X-RecordingLayer.getBitSizeX()*0.5) && grain_pos_X<(bit_position_X+RecordingLayer.getBitSizeX()*0.5)){
                if(grain_pos_Y>(bit_position_Y-RecordingLayer.getBitSizeY()*0.5) && grain_pos_Y<(bit_position_Y+RecordingLayer.getBitSizeY()*0.5)){
                    Bit_grain_list[Bit_number].push_back(grain_in_layer);
    }    }    }    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//########Check that total bits and tracks will fit in the system########//
    if(RecordingLayer.getBitsX()*(RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX()) > Voronoi_data.Vx_MAX){
        std::cout << "System too small for number of bits in a single track!" << std::endl;
        int Bit_num_X = int(Voronoi_data.Vx_MAX/(RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX()));
        std::cout << "Reducing bits per track from " << RecordingLayer.getBitsX() << " to " << Bit_num_X << std::endl;
        RecordingLayer.setBitsX(Bit_num_X);
    }
    if(RecordingLayer.getBitsY()*(RecordingLayer.getBitSizeY()+RecordingLayer.getBitsY()) > Voronoi_data.Vy_MAX){
        std::cout << "System too small for number of tracks!" << std::endl;
        int Bit_num_Y = int(Voronoi_data.Vy_MAX/(RecordingLayer.getBitSizeY()+RecordingLayer.getBitsY()));
        std::cout << "Reducing tracks from " << RecordingLayer.getBitsY() << " to " << Bit_num_Y << std::endl;
        RecordingLayer.setBitsY(Bit_num_Y);
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//####################PRINT OUT BIT POSITIONS############################//
    for(unsigned int BIT=0;BIT<(RecordingLayer.getBitsX()*RecordingLayer.getBitsY());++BIT){
        BIT_OUTPUT << (Bit_positions[BIT].first-RecordingLayer.getBitSizeX()/2.0) << " " << (Bit_positions[BIT].second-RecordingLayer.getBitSizeY()/2.0) << " " << Bit_positions[BIT].first << " " << Bit_positions[BIT].second << std::endl;
        BIT_OUTPUT << (Bit_positions[BIT].first-RecordingLayer.getBitSizeX()/2.0) << " " << (Bit_positions[BIT].second+RecordingLayer.getBitSizeY()/2.0) << " " << Bit_positions[BIT].first << " " << Bit_positions[BIT].second << std::endl;
        BIT_OUTPUT << (Bit_positions[BIT].first+RecordingLayer.getBitSizeX()/2.0) << " " << (Bit_positions[BIT].second+RecordingLayer.getBitSizeY()/2.0) << " " << Bit_positions[BIT].first << " " << Bit_positions[BIT].second << std::endl;
        BIT_OUTPUT << (Bit_positions[BIT].first+RecordingLayer.getBitSizeX()/2.0) << " " << (Bit_positions[BIT].second-RecordingLayer.getBitSizeY()/2.0) << " " << Bit_positions[BIT].first << " " << Bit_positions[BIT].second << std::endl;
        BIT_OUTPUT << (Bit_positions[BIT].first-RecordingLayer.getBitSizeX()/2.0) << " " << (Bit_positions[BIT].second-RecordingLayer.getBitSizeY()/2.0) << " " << Bit_positions[BIT].first << " " << Bit_positions[BIT].second << std::endl;
        BIT_OUTPUT << std::endl << std::endl;
    }
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//############################SIMULATE WRITING###########################//
    std::cout << "Step Time Profile_Time Downtrack_Time Head_X Head_Y Bit_x Bit_y Bit Field" << std::endl;
    RecordingHead.setDirn(Vec3(Hap_unit.x,Hap_unit.y,Hap_unit.z*RecordingLayer.getWritableData(Bit)));

    while(Track_num<RecordingLayer.getBitsY()){
        // Perform these every step
//        std::cout << " ... Integrating the system ..." << std::endl;
        Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,Head_X,Head_Y,&Grain);
//        std::cout << " ... System integrated ..." << std::endl;
        Steps_until_output++;
        Time += Solver.get_dt();
        Downtrack_time += Solver.get_dt();
        Adj_Downtrack_time += Solver.get_dt();
        Head_X += Solver.get_dt()*RecordingHead.getSpeed();
        RecordingHead.moveX(Solver.get_dt()*RecordingHead.getSpeed());

#ifdef RealTimePrint
        std::cout << Time << "\t\r" << std::flush;
#endif

        if(Steps_until_output==output_steps){
            // Output write head position
            HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // BL
            HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y+RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TL
            HEAD_OUTPUT << (Head_X+RecordingHead.getHwidthX()/2.0) << " " << (Head_Y+RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TR
            HEAD_OUTPUT << (Head_X+RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TR
            HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // BL
            HEAD_OUTPUT << std::endl << std::endl;

            FILENAME_idv = std::to_string(File_count) + ".dat";
            Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
            HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
            PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grain, DataType::ALL);


            std::cout << File_count << " " << Time << " " << Profile_time << " " << Downtrack_time << " " << Head_X << " " << Head_Y << " " << Bit_X << " " << Track_num << " " << Bit << " " << RecordingHead.getMag()*RecordingLayer.getWritableData(Bit) << std::endl;
            ++File_count;
            Steps_until_output=0;
        }
        // TODO Look into reconfiguring based on timings not positions (to enable turning the field off before the end of a bit).
        /* Could set up two internal Hdc timers (for on and off).
         * Then determine length of time field is on and also length it is off.
         * Set timers based on those values and switch between them as we go.
         */
        // Once head is beyond the end of a bit increment the bit number
        if(Head_X > (Bit_X+1)*(RecordingLayer.getBitSpacingX()+RecordingLayer.getBitSizeX())){
            ++Bit_X;
            ++Bit;
            Profile_time = 0.0;    // This is required to reset profile time when RecordingLayer.getBitSpacingX()=0.0;
            RecordingHead.turnOff();
            if(Bit<RecordingLayer.getBitsX()*RecordingLayer.getBitsY()){
                RecordingHead.setDirn(Vec3(Hap_unit.x,Hap_unit.y,Hap_unit.z*RecordingLayer.getWritableData(Bit)));
            }
            else{RecordingHead.setDirn(Vec3(0.0,0.0,0.0));}
        }
        if(!RecordingHead.getPulsing()){
            RecordingHead.ApplyTspatialCont(Structure.Num_layers,Voronoi_data,Environ_Temp,Adj_Downtrack_time,&temperature,&Grain);
        }
        // Write head is inside a bit
        if(Head_X >= ((Bit_X+1)*RecordingLayer.getBitSpacingX()+Bit_X*RecordingLayer.getBitSizeX()) &&
           Head_X <= (Bit_X+1)*(RecordingLayer.getBitSpacingX()+RecordingLayer.getBitSizeX())){
            if(RecordingHead.getPulsing()){
                RecordingHead.ApplyTspatialCont(Structure.Num_layers,Voronoi_data,Environ_Temp,Downtrack_time,&temperature,&Grain);
            }
            RecordingHead.turnOn();
            RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),&Grain);
            Profile_time += Solver.get_dt();
        } else { // Outside of a bit (satisfied only when bit_spacing_X != 0)
            RecordingHead.turnOff();
            Profile_time = 0.0;
        }
        // Once head is at the end of the system or all bits are written
        if(Head_X > (RecordingLayer.getBitsX()*RecordingLayer.getBitSizeX() + (RecordingLayer.getBitsX()+1)*RecordingLayer.getBitSpacingX() + RecordingHead.getHwidthX()*0.5) || Head_X > (Voronoi_data.Vx_MAX+RecordingHead.getHwidthX()*0.5)){
            std::cout << "Final (Head_X,Head_Y): ( "<< Head_X << " , " << Head_Y << " ) nm" << std::endl;
            ++Track_num;
            // Reset values
            Downtrack_time=0.0;
            Adj_Downtrack_time = -(RecordingHead.getHwidthX()*0.5)/RecordingHead.getSpeed();
            Head_X = 0.0 - RecordingHead.getHwidthX()*0.5;
            if(RecordingLayer.getBitsY()>1){ // If we are moving to a new track get the next bit's y-position
                Head_Y = Bit_positions[Bit].second;
            }
            RecordingHead.setX(Head_X);
            RecordingHead.setY(Head_Y);
            Bit_X = 0;
        }
    }
    std::cout << "\nBits written: " << Bit-1 << " of " << Tot_bits << std::endl;
    Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grain,Solver,"0");
    // Perform simulation of entire completed system until more outputs are achieved.
    // ONLY required IF LLB_exclusion is used:
    if(Solver.get_Exclusion()){
        std::cout << "Simulating entire system for an extra 1ns, due to use of inclusion zone." << std::endl;
        std::cout << "Step Time Profile_Time Downtrack_Time Head_X Head_Y Bit_x Bit_y Bit Field" << std::endl;
        Solver.disableExclusion();
        int Output_counter=0;
        // Remove laser pulse and applied field
        for(int g=0;g<Tot_grains;++g){
            Grain.Temp[g]=Environ_Temp;
            Grain.H_appl[g].x=0.0;
            Grain.H_appl[g].y=0.0;
            Grain.H_appl[g].z=0.0;
        }
        // Integrate the whole system for extra 1ns
        double Time_final = Time;
        while(Time-Time_final<=1.0e-9){
            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
            Steps_until_output++;
            Time += Solver.get_dt();

            // Only output every X steps
            if(Steps_until_output==output_steps){
                // Output write head position
                HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // BL
                HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y+RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TL
                HEAD_OUTPUT << (Head_X+RecordingHead.getHwidthX()/2.0) << " " << (Head_Y+RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TR
                HEAD_OUTPUT << (Head_X+RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // TR
                HEAD_OUTPUT << (Head_X-RecordingHead.getHwidthX()/2.0) << " " << (Head_Y-RecordingHead.getHwidthY()/2.0) << " " << Head_X << " " << Head_Y << std::endl;  // BL
                HEAD_OUTPUT << std::endl << std::endl;

                FILENAME_idv = std::to_string(File_count) + ".dat";
                Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
                HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
                PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grain, DataType::ALL);

                std::cout << File_count << " " << Time << " " << Profile_time << " " << Downtrack_time << " " << Head_X << " " << Head_Y << " " << Bit_X << " " << Track_num << " " << Bit << " " << RecordingHead.getField().z << std::endl;
                ++File_count;
                Steps_until_output=0;
                ++Output_counter;
            }
        }
        // Output final system configuration at the end of extra 1ns independently of output steps
        FILENAME_idv = std::to_string(File_count) + ".dat";
        Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
        HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),temperature,Time,Grain_output_list,OUTPUT);
        PixMap.PrintwUpdate("PixelMap_"+FILENAME_idv, Grain, DataType::ALL);
        // Export final system
        std::cout << "Entire system evaluated" << std::endl;
        HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_switch_buffer,Output_switch_file_loc);
        Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grain,Solver,"1");

    }
    if(ReadBack==true){
        data_read_back(Grain,Voronoi_data,ReadingLayer,"Read_back.dat",ReadingHead);
    }
    return 0;
}
