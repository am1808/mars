/*
 * Data_longevity.cpp
 *
 *  Created on: 14 Nov 2019
 *      Author: Samuel Ewan Rannala
 */

/** @file Data_longevity.cpp
 * @brief Simulation for the read back of bits form a system after extended periods of time. */

#include <string>
#include <iostream>
#include <algorithm>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/ReadLayer.hpp"
#include "../../hdr/Classes/ReadHead.hpp"
#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Importers/Read_in_import.hpp"
#include "../../hdr/Importers/Create_system_from_output.hpp"
#include "../../hdr/Simulations/Read_back.hpp"
#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int Data_longevity(const ConfigFile cfg){
    Data_input_t IN_DATA;
    Structure_t Structure;
    Voronoi_t VORO;
    Material_t Materials;
    Grain_t Grain;
    Interaction_t INT;

    unsigned int Total_snaps = cfg.getValueOfKey<unsigned int>("DataLongevity:Total_snapshots");
    std::vector<double> Time_limit = cfg.getValueOfKey<std::vector<double>>("DataLongevity:Times");
    double Environ_Temp = cfg.getValueOfKey<double>("DataLongevity:Environment_Temp");
    if(Total_snaps>Time_limit.size()){
        Total_snaps=Time_limit.size();
    }
    std::sort(Time_limit.begin(),Time_limit.end());
    double Time=0.0;
    unsigned int Snap=0;

    std::string FILENAME_idv = std::to_string(Snap)+".dat";
    std::string Read_back_FILENAME = "DL_Read_back_Img_"+std::to_string(Snap)+".dat";
    Solver_t Solver(cfg);
    Read_in_import(cfg, &IN_DATA);
    Read_in_system(IN_DATA,&Structure,&VORO,&Materials,&Grain,&INT,&Solver);
    ReadHead ReadingHead(cfg);
    ReadLayer ReadingLayer(cfg);
    double Input_dt = Solver.get_dt();
    for(unsigned int gT=0;gT<Grain.Temp.size();++gT){
        Grain.Temp[gT] = Environ_Temp;
    }
    // Initial t=0 system read back
    data_read_back(Grain, VORO, ReadingLayer, Read_back_FILENAME, ReadingHead);
    // Output system image
    Idv_Grain_output(Structure.Num_layers,VORO,Grain,Time,FILENAME_idv,"DL_");

    // Loop over each required snapshot
    for(unsigned int snapshot=1;snapshot<=Total_snaps;++snapshot){
        // Select time_limit from input
        double snap_time = Time_limit[snapshot-1];
        if(Solver.get_dt()<(snap_time-Time)*0.0001){
            std::cout << "Increasing time step." << std::endl;
            Solver.set_dt((snap_time-Time)*0.001);
        }
        else if(Solver.get_dt()>=(snap_time-Time)){
            std::cout << "Decreasing time step." << std::endl;
            Solver.set_dt((snap_time-Time)*0.01);
        }

        std::cout << "Evolving system over " << snap_time << " seconds." << std::endl;
        Read_back_FILENAME = "Read_back_Img_DL" + std::to_string(snapshot) + ".dat";
        FILENAME_idv = std::to_string(snapshot) + ".dat";
        // Perform kMC until reach limit time
        while(Time<snap_time){
            Solver.Integrate(&VORO,Structure.Num_layers,&INT,0.0,0.0,&Grain);
            Time += Solver.get_dt();
        }
        data_read_back(Grain, VORO, ReadingLayer, Read_back_FILENAME, ReadingHead);
        Idv_Grain_output(Structure.Num_layers,VORO,Grain,Time,FILENAME_idv,"DL_");
        Solver.set_dt(Input_dt);
    }
    return 0;
}
