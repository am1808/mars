/*
 * HAMR_localised_write.cpp
 *
 *  Created on: 2 Oct 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR_localised_write.cpp
 * \brief Simulation of HAMR writing process for a system with a single centralised bit.
 *
 * <P> A single heat assisted magnetic recording write process is performed on a bit located at the
 * centre of the granular system. The system is first equilibrated for
 * <B><EM> HAMR:Equilibration_time </EM></B>. The simulation is performed until at least the write
 * process has been completed. However if <B><EM> HAMR:Run_time </EM></B> &gt;
 * <B><EM> 6&times;HAMR:Cooling_time </EM></B> then the simulation will continue after the write process is
 * completed until <B><EM> HAMR:Run_time </EM></B> is reached. </P>
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/RecHead.hpp"
#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_layer_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"

/** Performs HAMR data write over an entire system with a continuously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 * 		<UL>
 *			<LI> Type <LI> SEED
 *		</UL>
 * <LI> Struct:
 * 		<UL>
 * 			<LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 * 			<LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 * 		</UL>
 * <LI> LLB:
 * 		<UL>
 * 			<LI> dt <LI> Exclusion_zone
 * 			<LI> <STRONG> If Exclusion_zone == True </STRONG>
 * 			<UL>
 * 				<LI> Exclusion_range_negX <LI> Exclusion_range_posX <LI> Exclusion_range_negY <LI> Exclusion_range_posY
 * 			</UL>
 * 		</UL>
 * <LI> HAMR:
 * 		<UL>
 * 			<LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 * 			<LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 * 			<LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 * 			<LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Run_time
 * 		</UL>
 * </UL>
 * */
int HAMR_localised_write(ConfigFile cfg){

    double Environ_Temp = cfg.getValueOfKey<double>("HAMRLW:Environment_Temp");
    double Equilibration_time = cfg.getValueOfKey<double>("HAMRLW:Equilibration_time");
    double Run_time = cfg.getValueOfKey<double>("HAMRLW:Run_time");
	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Materials;
	Interaction_t Int_system;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml,Grain_output_list;
	int output_steps,file_writes,File_count=0,Sim_time_predictor=0;
	double Temperature=Environ_Temp, Time=0.0,
		   Profile_tot_time;
	std::string FILENAME_idv;
	std::string Output_switch_file_loc = "Output/HAMR_Grain_switching.dat";
	std::vector<std::string> Output_switch_buffer;
	std::string OUTPUT = "Output/HAMR_multi_layer.dat";
	std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends to it

//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
	Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
	RecHead RecordingHead(cfg);
    RecordingHead.setX(Voronoi_data.Centre_X);
    RecordingHead.setY(Voronoi_data.Centre_Y);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	if(RecordingHead.getRampTime()<Solver.get_dt()){
		std::cout << "Ramp time less than timestep!" << std::endl;
		exit(EXIT_FAILURE);
	}
//###################DETERMINE IMPORTANT VALUES###################//

	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
	output_steps = Solver.getOutputSteps();
	Profile_tot_time = RecordingHead.getProfileTime();
	if(Run_time > Profile_tot_time){file_writes = round(Run_time/Solver.getMeasTime());}
	else{file_writes = round(Profile_tot_time/Solver.getMeasTime());}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Equilibrate the system for Equilibration_time
    std::cout << "\nEquilibrating the system for " << Equilibration_time << " s" << std::endl;
	while(Time<Equilibration_time){
		Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
		Time += Solver.get_dt();
    }
    // Reset time to zero
    std::cout << "Resetting time to t = 0.0 s\n" << std::endl;
    Time = 0.0;

	HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
	HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_switch_buffer);

	std::cout << "Output steps: " << output_steps << std::endl;
	auto start = std::chrono::system_clock::now();

    // Run the simulation
    RecordingHead.turnOnfor(RecordingHead.getProfileTime()-RecordingHead.getRampTime());
	while(Time<Run_time){

		while(Time<Profile_tot_time){
			for(int Steps=0;Steps<output_steps;Steps++){
			    RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),&Grain);
			    RecordingHead.ApplyTspatial(Voronoi_data,Structure.Num_layers,Environ_Temp,Time,&Temperature,&Grain);
				Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,Voronoi_data.Centre_X,Voronoi_data.Centre_Y,&Grain);
				Time += Solver.get_dt();
			}

			FILENAME_idv = std::to_string(File_count) + ".dat";
			Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
			HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
			++File_count;

			// Predict simulation run time
			++Sim_time_predictor;
			if(Sim_time_predictor==10){
				auto end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
				double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
				std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
			}
		}
		// Continue simulation but no longer apply T or H profiles.
		for(int Steps=0;Steps<output_steps;Steps++){
			Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,Voronoi_data.Centre_X,Voronoi_data.Centre_Y,&Grain);
			Time += Solver.get_dt();
		}
		FILENAME_idv = std::to_string(File_count) + ".dat";
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
		HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
		++File_count;
	}
	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_switch_buffer,Output_switch_file_loc);
	return 0;
}



