/*
 * HAMR.cpp
 *
 *  Created on: 21 Aug 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR.cpp
 * \brief This simulation applies the temperature profile and field profile on an entire system.
 *
 * <P> The system is first allowed to equilibrate for a specified time. After equilibration the initial
 * magnetisation is recorded then the thermal and field profiles are activated. The system is evaluated
 * until <B><EM> HAMR:Run_time </EM></B> is reached with data recorded at the interval defined by
 * <B><EM> HAMR:Measurement_time. </EM></B>
 * </P>
 */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/Hdc.hpp"
#include "../../hdr/Classes/HAMRLaser.hpp"
#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"

#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_layer_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"



/* This is a simulation for applying temperature and field profiles to
 * an entire system to investigate their effects. */

/** Applies thermal and field profiles onto an entire granular system.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 * 		<UL>
 *			<LI> Type <LI> SEED
 *		</UL>
 * <LI> Struct:
 * 		<UL>
 * 			<LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 * 			<LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 * 		</UL>
 * <LI> LLB:
 * 		<UL>
 * 			<LI> dt
 * 		</UL>
 * <LI> HAMR:
 * 		<UL>
 * 			<LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 * 			<LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 * 			<LI> Measurement_time <LI> Equilibration_time <LI> Run_time
 * 		</UL>
 * </UL>
 * */
int HAMR(ConfigFile cfg){

    double Environ_Temp = cfg.getValueOfKey<double>("HAMR:Environment_Temp");
    double Equilibration_time = cfg.getValueOfKey<double>("HAMR:Equilibration_time");
    double Run_time = cfg.getValueOfKey<double>("HAMR:Run_time");
	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Materials;
	Interaction_t Int_system;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml,Grain_output_list;
	int output_steps,file_writes,File_count=0,Sim_time_predictor=0;
	double Temperature=Environ_Temp, Time=0.0;
	std::string FILENAME_idv;

	std::vector<std::string> Output_buffer;
	std::string OUTPUT_switch_loc = "Output/HAMR_Grain_switching.dat";
	std::string OUTPUT = "Output/HAMR_multi_layer.dat";
	std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends


//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
	Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
	Hdc FieldDC(cfg);
	HAMR_Laser  LaserPulse(cfg);

	if(FieldDC.getRampTime()<Solver.get_dt()){
		std::cout << "Ramp time less than timestep!" << std::endl;
		exit(EXIT_FAILURE);
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

	// Generate output list for entire system
	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
	output_steps = Solver.getOutputSteps();

	file_writes = round(Run_time/Solver.getMeasTime());
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Equilibrate the system for Equilibration_time
    std::cout << "\nEquilibrating the system for " << Equilibration_time << " s" << std::endl;
	while(Time<Equilibration_time){
		Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
		Time += Solver.get_dt();
    }
    // Reset time to zero
    std::cout << "Resetting time to t = 0.0 s\n" << std::endl;
    Time = 0.0;

	std::cout << "Output steps: " << output_steps << std::endl;
	auto start = std::chrono::system_clock::now();
	HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,FieldDC.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
	HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_buffer);

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Run the simulation
    FieldDC.turnOnfor(LaserPulse.getProfileTime()-FieldDC.getRampTime());
	while(Time<Run_time){
		for(int Steps=0;Steps<output_steps;Steps++){
			// Determine Temperature and Field.
		    LaserPulse.ApplyT(Environ_Temp,Time,&Temperature,&Grain);
		    FieldDC.updateField(Solver.get_dt());
		    for(unsigned int grain=0;grain<Grain.H_appl.size();++grain){Grain.H_appl[grain] = FieldDC.getField();}
			Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grain);
			Time += Solver.get_dt();
		}

		FILENAME_idv = std::to_string(File_count) + ".dat";
		Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
		HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,FieldDC.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
		++File_count;

		// Predict simulation run time
		++Sim_time_predictor;
		if(Sim_time_predictor==10){
			auto end = std::chrono::system_clock::now();
			std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
			double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
			std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
		}
	}
	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_buffer,OUTPUT_switch_loc);
	return 0;
}
