/*
 * FMR.cpp
 *
 *  Created on: 20 May 2020
 *      Author: Ewan Rannala
 */

/** @file FMR.cpp
 * @brief Simulation for ferromagnetic resonance. */

#include <iostream>
#include <fstream>
#include <iomanip>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/Hac.hpp"
#include "../../hdr/Classes/Hdc.hpp"
#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/FftComplex.hpp"

#include <complex>
typedef std::complex<double> cd;

void Hann_window(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){
        double multiplier = 0.5*(1-cos(2.0*PI*i/(a.size()-1)));
        a[i] *= multiplier;
    }
}

void Hamming_window(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){
        double multiplier = 0.54-0.46*cos(2.0*PI*i/(a.size()-1));
        a[i] *= multiplier;
    }
}

void BlackmanHarris7(std::vector<double>&a){
    for(unsigned int i=0;i<a.size();++i){

        double multiplier = 0.27105140069342*cos(2*PI*0*i/(a.size()-1))
                          - 0.43329793923448*cos(2*PI*1*i/(a.size()-1))
                          + 0.21812299954311*cos(2*PI*2*i/(a.size()-1))
                          - 0.06592544638803*cos(2*PI*3*i/(a.size()-1))
                          + 0.01081174209837*cos(2*PI*4*i/(a.size()-1))
                          - 0.00077658482522*cos(2*PI*5*i/(a.size()-1))
                          + 0.00001388721735*cos(2*PI*6*i/(a.size()-1));
        a[i] *= multiplier;
    }
}

/** Performs frequency or field swept FMR simulation.
 *
 * \param[in] cfg Configuration file. <BR>
 */
int FMR(const ConfigFile cfg){

    std::vector<double> Frequencies, Fields;
    int Num_iter=1;
    double Res=0.0, MaxFreq=0.0;
    std::string Analyt_pre="";

    double Run_time = cfg.getValueOfKey<double>("FMR:Run_time");
    double Start_time = cfg.getValueOfKey<double>("FMR:Start_recording_time");
    double Environ_Temp = cfg.getValueOfKey<double>("FMR:Environment_Temperature");
    std::string Type = cfg.getValueOfKey<std::string>("FMR:Sweep_Type");
    bool Set_Res = cfg.getValueOfKey<bool>("FMR:Set_resolution");
    if(Set_Res){Res=cfg.getValueOfKey<double>("FMR:Resolution");}


    Voronoi_t Voronoi_data;
    Structure_t Structure;
    Material_t Materials;
    std::vector<ConfigFile> Materials_Config;
    Interaction_t Int_system;
    Grain_t Grains, Grains_backup;

    Structure_import(cfg,&Structure);
    Materials_import(cfg,Structure.Num_layers,&Materials_Config,&Materials);
    Voronoi(Structure,Structure.Magneto_Interaction_Radius,&Voronoi_data);
    Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grains);
    Generate_interactions(Structure.Num_layers,Grains.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);

    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    Hac AC(cfg);
    Hdc DC(cfg);

    std::ofstream FMROUT;
    Grains_backup = Grains;

    if(Type!="frequency" && Type!="field" && Type!="none"){
        std::cout << "Unknown sweep type selected."; exit(EXIT_FAILURE);
    }
    else if(Type=="frequency"){
        Analyt_pre = "H"+std::to_string(DC.getMin())+"Oe_";
        std::vector<double> FreqRange = cfg.getValueOfKey<std::vector<double>>("fmr:Freq_Range");
        if(FreqRange.size()!=3){
            std::cout << "Incorrect frequency range vector. Requires {min, max, step}."; exit(EXIT_FAILURE);
        }
        // Fill Frequencies vector
        double temp=FreqRange[0];
        while(temp <= (FreqRange[1]+FreqRange[2])){
            Frequencies.push_back(temp);
            temp += FreqRange[2];
        }
        Num_iter=Frequencies.size();
        MaxFreq=Frequencies.back();
    }
    else if(Type=="field"){
        Analyt_pre = "F"+std::to_string(AC.getFreq()*1e-9)+"GHz_";
        MaxFreq=AC.getFreq();
        std::vector<double> FieldRange = cfg.getValueOfKey<std::vector<double>>("fmr:Field_Range");
        if(FieldRange.size()!=3){
            std::cout << "Incorrect field range vector. Requires {min, max, step}."; exit(EXIT_FAILURE);
        }
        // Fill Fields vector
        double temp=FieldRange[0];
        while(temp <= (FieldRange[1]+FieldRange[2])){
            Fields.push_back(temp);
            temp += FieldRange[2];
        }
        Num_iter = Fields.size();
    }
    
//################################# Determine that aliasing won't occur ################################//
    double SampleFreqMIN = MaxFreq*2.0;
    if((1.0/Solver.get_dt())<=SampleFreqMIN){
        std::cout << "\n####################\n" 
                  << "Solver timestep will cause aliasing, adjusting timestep...\n" << std::flush;
        while((1.0/Solver.get_dt())<=SampleFreqMIN){
            Solver.set_dt(Solver.get_dt()*0.1);
            Solver.setMeasTime(Solver.get_dt(),Solver.get_Solver());
        }
        std::cout << "\tNew timestep " << Solver.get_dt()
                  << "\n####################\n" << std::endl;
    } 
    else if((1.0/Solver.getMeasTime())<=SampleFreqMIN){
        Solver.setMeasTime(Solver.get_dt(), Solver.get_Solver());
    }
    
    std::ofstream SAnalytic_OUT(Analyt_pre+"Semi_analytic_results.dat");

//################################# Set desired frequency resolution ###################################//
    unsigned int length = Run_time/Solver.getMeasTime();
    if(Set_Res){
        std::cout << "Adjusting Run time to set FFT resolution." << std::endl;
        std::cout << "\tSample size: " << length << std::endl;
        std::cout << "\tSim time: " << Solver.getMeasTime()*length << std::endl;
        std::cout << "\tBin width: " << (1.0/Solver.getMeasTime())/length << std::endl;
        length = (1.0/Solver.getMeasTime())/Res;
        std::cout << "\t#### New values ####" << std::endl;
        std::cout << "\tSample size: " << length << std::endl;
        std::cout << "\tSim time: " << Solver.getMeasTime()*length << std::endl;
        std::cout << "\tBin width: " << (1.0/Solver.getMeasTime())/length << std::endl;
    }

//################################# Loop over desired frequencies/fields ###############################//
    for(int iter=0;iter<Num_iter;++iter){
        std::vector<double> mx_t;
        std::string filename_PRE = "Output/FMR_";
        std::string filename;
        if(Type=="field"){
            filename = std::to_string(Fields[iter]) + "_" + std::to_string(AC.getFreq()) + "_.dat";
            DC.setMin(Fields[iter]);
            DC.updateField(0.0);
        }
        else if(Type=="frequency"){
            filename = std::to_string(DC.getMin()) + "_" + std::to_string(Frequencies[iter]) + ".dat";
            AC.setFreq(Frequencies[iter]);
        }
        else{
            filename = std::to_string(DC.getMin()) + " " + std::to_string(AC.getFreq()) + "_.dat";
        }

        std::cout << "\nIteration " << iter+1 << " of " << Num_iter << std::endl;

        FMROUT.open(filename_PRE + filename);
//        FMROUT << "mx my mz DCx DCy DCz ACx ACy ACz time" << std::endl;

        for(unsigned int grain=0;grain<Grains.H_appl.size();++grain){
            Grains.H_appl.at(grain) = AC.getField()+DC.getField();
        }

        //######################################## Perform FMR #########################################//
        double Time=0.0;
        unsigned int steps=0, it=0, Written=0, Max_written=1000000;
//        std::cout << "| DC | " << DC.getField() << " | AC | " << AC.getAmp()*AC.getDirn()
//                  << " | Hk | " << Grains.Easy_axis[0]*2.0*Grains.K[0]/Grains.Ms[0] << std::endl;
        while(it<length){ // Perform simulation until the correct number of samples have been obtained

            Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,0.0,0.0,&Grains);
            Time += Solver.get_dt();
            ++steps;

#ifdef RealTimePrint
            std::cout << Time << "\t\r" << std::flush;
#endif

            Vec3 m;
            for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){
                m += Grains.m[grain];
            }
            m /= Voronoi_data.Num_Grains;
            if(steps==Solver.getOutputSteps()){
                if(Time>Start_time){
                    mx_t.push_back(m.x);
                    if(Written<Max_written){
                      FMROUT << std::setprecision(7) << m << "\t" << DC.getField() << "\t"
                             << AC.getField() << "\t" << Time << std::endl;
                        ++Written;
                    }
                    ++it;
                }
                steps=0;
            }
            // Update field - only AC field
            AC.updateField(Time);
            for(unsigned int grain=0;grain<Grains.H_appl.size();++grain){
                Grains.H_appl.at(grain) = AC.getField()+DC.getField();
            }
        }
        FMROUT.close();

//########################## Output predicted resonance frequency and damping ##########################//
        double Expected_f0 = (Solver.get_Gamma()/(2.0*PI))*(DC.getMin()+((Solver.get_mEQ(0)+Solver.get_ChiPara(0)*DC.getMin())/Solver.get_ChiPerp(0)));
        double Expected_alpha = (Solver.get_Alpha()[0]/(Solver.get_mEQ(0)+Solver.get_ChiPara(0)*DC.getMin()))*(1.0-(Grains.Temp[0]/(3.0*Grains.Tc[0])));

#if 0
        std::cout << Solver.get_Gamma() << "\t" << DC.getMin() << "\t" << Solver.get_Alpha()[0] << "\t"
                  << Environ_Temp << "\t" << Grains.Tc[0] << "\t" << Solver.get_mEQ(0) << "\t"
                  << sqrt(Grains.m[0]*Grains.m[0]) << "\t" << Solver.get_ChiPara(0) << "\t" << Solver.get_ChiPerp(0) << std::endl << std::endl;
#endif
        std::cout << "Expected Res freq: " << Expected_f0 << std::endl;
        std::cout << "Effective damping: " << Expected_alpha << std::endl;

//####################################### Obtain and output FFT ########################################//
        std::vector<cd> input(mx_t.begin(), mx_t.end());
        Fft::transform(input,false);
        // Output FFT
        filename_PRE = "Output/FFT_";
        std::ofstream FFTOUT(filename_PRE + filename);
        // Second half of FFT is a mirror image of the first half, so it can be ignored.
        for(unsigned int i=0;i<input.size()*0.5;++i){
            if( (i/Solver.getMeasTime())/length > 1.0e12 ){break;}
            FFTOUT << (i/Solver.getMeasTime())/length << "\t"
	            << (2.0/input.size())*std::abs(input[i]) << std::endl;
        }
        FFTOUT.close();
//####################################### Obtain and output IFFT #######################################//
#if 0
        // Perform IFFT for comparison with original data - for testing.
        Fft::transform(input,true);
        filename_PRE = "Output/IFFT_";
        std::ofstream IFFT(filename_PRE + filename);
        for(unsigned int i=0;i<input.size();++i){
            if( (i/Solver.getMeasTime())/length > 1.0e12 ){break;}
            IFFT << i*1e-15+Start_time<< " " << input[i].real()/input.size() << std::endl;
        }
        IFFT.close();
#endif
        // Reset grains
        Grains=Grains_backup;
    }
    SAnalytic_OUT.close();
    return 0;
}
