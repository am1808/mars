/*
 * HAMR_Write_Data.cpp
 *
 *  Created on: 28 Sep 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** \file HAMR_Write_Data.cpp
 * \brief Simulation of HAMR writing process with an instantaneously moving write head.
 *
 * <P> Data is written to the system via heat assisted magnetic recording. The write head is positioned
 * at the centre of a bit and the field and laser pulses are applied. Once a bit has been written the
 * write head is moved instantaneously to the centre of the next bit. </P>
 * <P> The system is first allowed to equilibrate for <B><EM> HAMR:Equilibration_time </EM></B>, once
 * the equilibration is complete a list of all bit positions is created. A list of grains in each bit
 * is also determined, allowing for individual data for each bit to be output. </P>
 * <P> Each time step consists of an LLB integration and the application of a magnetic field and laser
 * pulse. Once all bits and tracks have been written the final system is exported. If
 * <B><EM> HAMR:Readback </EM></B> is set to <B> True </B> then a read back simulation is performed
 * before the simulations exits. </P> */

#include <iostream>
#include <fstream>
#include <math.h>
#include <iomanip>
#include <string>
#include <chrono>

#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Structures.hpp"
#include "../../hdr/Classes/RecHead.hpp"
#include "../../hdr/Classes/Solver.hpp"
#include "../../hdr/Classes/PixelMap.hpp"
#include "../../hdr/Importers/Structure_import.hpp"
#include "../../hdr/Importers/Grain_setup.hpp"
#include "../../hdr/Importers/Materials_import.hpp"
#include "../../hdr/Voronoi.hpp"
#include "../../hdr/Interactions/Generate_interactions.hpp"
#include "../../hdr/Simulations/Read_back.hpp"
#include "../../hdr/Data_output/HAMR_grain_output.hpp"
#include "../../hdr/Data_output/HAMR_layer_output.hpp"
#include "../../hdr/Data_output/HAMR_switching_output.hpp"
#include "../../hdr/Data_output/Export_system.hpp"

/** Performs HAMR writing process with an instantaneously moving write head.
 *
 * \param[in] cfg Configuration file. <BR>
 * Required configuration file parameters:
 * <UL>
 * <LI> Sim:
 * 		<UL>
 *			<LI> Type <LI> SEED
 *		</UL>
 * <LI> Struct:
 * 		<UL>
 * 			<LI> Dim-x <LI> Dim-Y <LI> Num_Layers <LI> Avg_Grain_width <LI> Std_Dev_Grain_Vol
 * 			<LI> Packing_Fraction <LI> Interaction_radius <LI> Magnetostatics
 * 		</UL>
 * <LI> LLB:
 * 		<UL>
 * 			<LI> dt <LI> Exclusion_zone
 * 			<LI> <STRONG> If Exclusion_zone == True </STRONG>
 * 			<UL>
 * 				<LI> Exclusion_range_negX <LI> Exclusion_range_posX <LI> Exclusion_range_negY <LI> Exclusion_range_posY
 * 			</UL>
 * 		</UL>
 * <LI> HAMR:
 * 		<UL>
 * 			<LI> Temp_min <LI> Temp_max <LI> Cooling_time <LI> Applied_field_unit
 * 			<LI> Applied_field_minimum <LI> Applied_field_Strength <LI> Field_ramp_time
 * 			<LI> Measurement_time <LI> equilibration_time <LI> Field_X_width <LI> Field_Y_width
 * 			<LI> laser_fwhm_x <LI> laser_fwhm_y <LI> Thermal_gradient <LI> Bit_width
 * 			<LI> Bit_spacing <LI> Bit_spacing_X <LI> Bit_spacing_Y <LI> Bit_number_in_X
 * 			<LI> Bit_number_in_Y <LI> Data_Binary
 * 		</UL>
 * <LI> EXPT:
 * 		<UL>
 * 			<LI> ReadBack
 * 			<LI> <STRONG> If ReadBack == True </STRONG>
 * 				<UL>
 *		 			<LI> Read_Head_speed <LI> Read_Head_record_interval <LI> Read_Head_width_X <LI> Read_Head_width_Y
 * 				</UL>
 * 		</UL>
 * </UL>
 * */
int HAMR_write_data(ConfigFile cfg){

    double Equilibration_time = cfg.getValueOfKey<double>("HAMRWD:Equilibration_time");
    double Environ_Temp = cfg.getValueOfKey<double>("HAMRWD:Environment_Temp");
    bool ReadBack = cfg.getValueOfKey<bool>("HAMRWD:ReadBack_after_write");

	// Declare all required variables
	Voronoi_t Voronoi_data;
	Structure_t Structure;
	Material_t Materials;
	Interaction_t Int_system;
	Grain_t Grain;
	std::vector<ConfigFile> Materials_Config;
	std::vector<double> Normaliser,Mx,My,Mz,Ml,Grain_output_list;
	int output_steps,file_writes,Bit=0,File_count=0,Sim_time_predictor=0,
			Tot_bits;
	double Temperature, Time=0.0, Time_dur_pulse=0.0,
		   Tot_time;
	std::string FILENAME_idv;
	std::string Output_switch_file_loc = "Output/HAMR_Grain_switching.dat";
	std::vector<std::string> Output_switch_buffer;
	std::string OUTPUT = "Output/HAMR_multi_layer.dat";
	std::remove(OUTPUT.c_str()); // Remove old OUTPUT file - This is required as the function which uses this files appends

//####################IMPORT ALL REQUIRED DATA####################//
	Structure_import(cfg, &Structure);
//	HAMR_writing_expt_import(cfg, "HAMR_write_data",&Expt_data,&Expt_laser,&Expt_field,&Expt_timings);
//	Read_Back_import(cfg, &Expt_data_read);
	Materials_import(cfg, Structure.Num_layers, &Materials_Config, &Materials);
	Voronoi(Structure, Structure.Magneto_Interaction_Radius,&Voronoi_data);
	Grain_setup(Voronoi_data.Num_Grains,Structure.Num_layers,Materials,Voronoi_data.Grain_Area,Voronoi_data.Grain_diameter,Environ_Temp,&Grain);
	Generate_interactions(Structure.Num_layers,Grain.Vol,Structure.Magneto_Interaction_Radius,Structure.Magnetostatics_gen_type,Materials,Voronoi_data,&Int_system);
    Solver_t Solver(Voronoi_data.Num_Grains,Structure.Num_layers,cfg,Materials_Config);
    RecHead RecordingHead(cfg);
    RecLayer RecordingLayer(cfg,RecordingHead.getDirn().z);
    PixelMap PixMap(Voronoi_data.Vx_MIN,Voronoi_data.Vy_MIN,Voronoi_data.Vx_MAX,Voronoi_data.Vy_MAX,(Structure.Grain_width/10.0));
    PixMap.Discretise(Voronoi_data, Grain);
    // Best to check for suitable input before the simulation has been performed
    ReadLayer ReadingLayer(RecordingLayer);
    ReadHead ReadingHead(RecordingLayer);
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
	if(RecordingHead.getRampTime()<Solver.get_dt()){
		std::cout << "Ramp time less than timestep!" << std::endl;
		exit(EXIT_FAILURE);
	}
//###################DETERMINE IMPORTANT VALUES###################//

// TODO Need a method to determine what grains are in which bits. Probably best to make a function for this.


	for(unsigned int grain=0;grain<Voronoi_data.Num_Grains;++grain){Grain_output_list.push_back(grain);}
    Tot_bits = RecordingLayer.getBitsA();
	output_steps = Solver.getOutputSteps();
	Temperature = Environ_Temp;
// Want these to be assigned from input.
	RecordingHead.setX(RecordingLayer.getBitSizeX()*0.5+RecordingLayer.getBitSpacingX());
	RecordingHead.setY(RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getBitSpacingY());

	Tot_time = RecordingHead.getProfileTime()*Tot_bits;
	file_writes = round(Tot_time/Solver.getMeasTime());
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//##########Equilibrate the system for Equilibration_time#########//

    std::cout << "\nEquilibrating the system for " << Equilibration_time << " s" << std::endl;
	while(Time<Equilibration_time){
	    Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,RecordingHead.getX(),RecordingHead.getY(),&Grain);
	    Time += Solver.get_dt();
    }
    // Reset time to zero
    std::cout << "Resetting time to t = 0.0 s\n" << std::endl;
    Time = 0.0;

    HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
    HAMR_Switching_BUFFER(Structure.Num_layers,Voronoi_data,Grain,Materials.dz,&Output_switch_buffer);

    std::cout << "Output steps: " << output_steps << std::endl;
	std::cout << "Simulation length: " << Tot_time << std::endl;
	auto start = std::chrono::system_clock::now();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###############Generate a list of bit positions#################//
	std::cout << "Generating Bit positions" << std::endl;
	double bit_position_X=RecordingLayer.getBitSizeX()*0.5+RecordingLayer.getBitSpacingX();
	double bit_position_Y=RecordingLayer.getBitSizeY()*0.5+RecordingLayer.getBitSpacingY();
	std::vector<std::pair<double,double>> Bit_positions;
	std::pair<double,double> XY_coords;
	for(unsigned int Bit_y=0;Bit_y<RecordingLayer.getBitsY();++Bit_y){
		for(unsigned int Bit_x=0;Bit_x<RecordingLayer.getBitsX();++Bit_x){
			XY_coords = std::make_pair(bit_position_X,bit_position_Y);
			Bit_positions.push_back(XY_coords);
			bit_position_X += RecordingLayer.getBitSizeX()+RecordingLayer.getBitSpacingX();
		}
		bit_position_X = Bit_positions[0].first;
		bit_position_Y += RecordingLayer.getBitSizeY()+RecordingLayer.getBitSpacingY();
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//######Generate a list of the grains contained in each bit#######//
	std::cout << "Generating grains in bit" << std::endl;
	RecordingLayer.setGrainsinBit(Voronoi_data.Num_Grains,Voronoi_data.Pos_X_final,Voronoi_data.Pos_Y_final,Bit_positions);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//#########################Perform writing########################//
	// Simulation will run until all bits are written.
	for(int Bit_number=0;Bit_number<Tot_bits;++Bit_number){
		RecordingHead.setX(Bit_positions[Bit_number].first);
		RecordingHead.setY(Bit_positions[Bit_number].second);
	    RecordingHead.setDirn(Vec3(0.0,0.0,RecordingLayer.getWritableData(Bit)));
		std::cout << "Head Position: " << RecordingHead.getX() << " , " << RecordingHead.getY() << std::endl;

		// For a single bit write
	    RecordingHead.turnOnfor(RecordingHead.getProfileTime()-RecordingHead.getRampTime());
		while(Time_dur_pulse<RecordingHead.getProfileTime()){
			for(int Steps_until_output=0;Steps_until_output<output_steps;Steps_until_output++){
			    RecordingHead.ApplyTspatial(Voronoi_data,Structure.Num_layers,Environ_Temp,Time_dur_pulse,&Temperature,&Grain);
			    RecordingHead.ApplyHspatial(Voronoi_data,Structure.Num_layers,Solver.get_dt(),&Grain);
			    Solver.Integrate(&Voronoi_data,Structure.Num_layers,&Int_system,RecordingHead.getX(),RecordingHead.getY(),&Grain);
				Time += Solver.get_dt();
				Time_dur_pulse += Solver.get_dt();
				std::cout << "Time: " << Time << "\r";
			}
			/*
			 * Could do with having layer specific file outputs?
			 */
			FILENAME_idv = std::to_string(File_count) + ".dat";
			Idv_Grain_output(Structure.Num_layers,Voronoi_data,Grain,Time,FILENAME_idv);
			HAMR_layer_averages(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,RecordingHead.getMag(),Temperature,Time,Grain_output_list,OUTPUT);
			++File_count;

			// Predict simulation run time
			++Sim_time_predictor;
			if(Sim_time_predictor==10){
				auto end = std::chrono::system_clock::now();
				std::chrono::duration<double> elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
				double Predicted_Sim_time = (elapsed.count()/10) * file_writes;
				std::cout << "\n\n*****\n Predicted sim time: " << Predicted_Sim_time << " s\n*****\n\n" << std::endl;
		}	}
		Time_dur_pulse = 0.0;
	}
	std::cout << "Bits written: " << Tot_bits << std::endl;
	HAMR_Switching_OUTPUT(Structure.Num_layers,Voronoi_data.Num_Grains,Grain,Output_switch_buffer,Output_switch_file_loc);
	Export_system(Structure.Num_layers,Materials.dz,Voronoi_data,Int_system,Grain,Solver,"1");
	std::cout << "Finished data write" << std::endl;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//###########################Read Back############################//
    if(ReadBack==true){
        data_read_back(Grain,Voronoi_data,ReadingLayer,"Read_back.dat",ReadingHead);
	}
	return 0;
}
