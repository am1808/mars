/* Grain_setup.cpp
 *  Created on: 16 May 2018
 *      Author: Samuel Ewan Rannala, Andrea Meo
 */

/** @file Grain_setup.cpp
 * @brief Function to define all grain data using mainly the material data structure. */

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <random>

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"
#include "../../hdr/Distributions_generator.hpp"
#include "../../hdr/Globals.hpp"

int Grain_setup(const unsigned int Num_grains,const unsigned int Num_Layers,
                const Material_t Material, const std::vector<double> Grain_area,
                const std::vector<double> Grain_diameter, const double Temperature, Grain_t*Grain){

    std::cout << "Assigning grain data..." << std::endl;
    unsigned int Total_grains = Num_grains * Num_Layers;
    double phi, theta, EAx, EAy, EAz, EAx2, EAy2, EAz2;
    double Pi_o_180 = 0.0174532925199432957692369;
    // Resize all non-primitive type vectors
    Grain->m.resize(Total_grains);
    Grain->Easy_axis.resize(Total_grains);
    Grain->H_appl.resize(Total_grains);
    Grain->Pixel.resize(Total_grains);
    // Ensure all other vectors are initially empty
    // This allows grain_setup to be used multiple times to replace the grain data.
    Grain->Temp.clear();
    Grain->diameter.clear();
    Grain->Vol.clear();
    Grain->K.clear();
    Grain->Ms.clear();
    Grain->Tc.clear();
    Grain->Callen_power_range.clear();
    Grain->Callen_power.clear();
    Grain->Callen_factor_lowT.clear();
    Grain->Callen_factor_midT.clear();
    Grain->Callen_factor_highT.clear();
    Grain->Callen_power_lowT.clear();
    Grain->Callen_power_midT.clear();
    Grain->Callen_power_highT.clear();
    Grain->Callen_range_lowT.clear();
    Grain->Callen_range_midT.clear();
    Grain->Crit_exp.clear();
    Grain->a0_mEQ.clear();
    Grain->a1_mEQ.clear();
    Grain->a2_mEQ.clear();
    Grain->a3_mEQ.clear();
    Grain->a4_mEQ.clear();
    Grain->a5_mEQ.clear();
    Grain->a6_mEQ.clear();
    Grain->a7_mEQ.clear();
    Grain->a8_mEQ.clear();
    Grain->a9_mEQ.clear();
    Grain->a1_2_mEQ.clear();
    Grain->b1_mEQ.clear();
    Grain->b2_mEQ.clear();

    /*
     * NEED TO SET UP ANISOTROPY DISPERSION
     *         NEED TO ENSURE LIMIT IS [0:180) -> IF <0 COUNT BACK FROM 180 | IF >180 COUNT FORWARD FROM 180.
     *         GAUSSIAN DISTRIBUTION FOR POLAR ANGLE.
     *         NORMAL DISTRIBUTION FOR AZIMUTHAL ANGLE.
     *
     *         1. GENERATE UNIFORM DISTRIBUTION IN AZIMUTHAL ANGLE
     *                 std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
     *                 phi = 2.0*PI*uniformPHI;
     *         2. GENERATE GAUSSIAN DISTRIBUTION IN POLAR ANGLE [0:180)
     *                 deg*pi/180 = rad
     *                 Input 3 --> Input/180 should give correct upper limit for distribution.
     *                 std::uniform_real_distribution<double> uniformTHETA(0.0, Input/180.0);
     *                 theta = acos(1.0-2.0*uniformTHETA);
     *         3. CONVERT POLAR COORDINATES TO CARTESIAN COORDINATES
     *                 x = sin(theta)*cos(phi);
     *                 y = sin(theta)*sin(phi);
     *                 z = cos(phi);
     */


    for(unsigned int Layer=0;Layer<Num_Layers;++Layer){
        unsigned int Offset = Layer*Num_grains;
        // Polar and azimuthal angle generator - different per layer.

        std::uniform_real_distribution<double> uniformPHI(0.0, 1.0);
        std::uniform_real_distribution<double> uniformTHETA(0.0, Material.Anis_angle[Layer]/180.0);
        std::normal_distribution<double> Norm(0.0,1.0);

        for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
            unsigned int Grain_in_system = Grain_in_Layer+Offset;
    //================== initial Magnetisation ==================//
            if(Material.Mag_Type[Layer]=="assigned"){
                Grain->m[Grain_in_system].x=Material.Initial_mag[Layer].x;
                Grain->m[Grain_in_system].y=Material.Initial_mag[Layer].y;
                Grain->m[Grain_in_system].z=Material.Initial_mag[Layer].z;
            }
            else if(Material.Mag_Type[Layer]=="random"){
                double X=Norm(Gen),Y=Norm(Gen),Z=Norm(Gen);
                double MAG = sqrt(X*X+Y*Y+Z*Z);
                // Normalise the magnetisation
                X /= MAG; Y /= MAG; Z /= MAG;
                Grain->m[Grain_in_system].x = X;
                Grain->m[Grain_in_system].y = Y;
                Grain->m[Grain_in_system].z = Z;
            }
    //=========================== Anisotropy direction ===========================//
            // Generate dispersion in anisotropy direction
            phi = 2.0*PI*uniformPHI(Gen);
            theta = acos(1.0-2.0*uniformTHETA(Gen));
            EAx=sin(theta)*cos(phi);
            EAy=sin(theta)*sin(phi);
            EAz=cos(theta);
            // Rotate easy axes to match desired initial direction
            EAx2 = EAx*cos(Material.Easy_axis_polar[Layer]*Pi_o_180) + EAz*sin(Material.Easy_axis_polar[Layer]*Pi_o_180);
            EAy2 = EAy;
            EAz2 = -EAx*sin(Material.Easy_axis_polar[Layer]*Pi_o_180) + EAz*cos(Material.Easy_axis_polar[Layer]*Pi_o_180);
            // Azimuthal rotation.
            Grain->Easy_axis[Grain_in_system].x = EAx2*cos(Material.Easy_axis_azimuth[Layer]*Pi_o_180) - EAy2*sin(Material.Easy_axis_azimuth[Layer]*Pi_o_180);
            Grain->Easy_axis[Grain_in_system].y = EAx2*sin(Material.Easy_axis_azimuth[Layer]*Pi_o_180) + EAy2*cos(Material.Easy_axis_azimuth[Layer]*Pi_o_180);
            Grain->Easy_axis[Grain_in_system].z = EAz2;
            Grain->Temp.push_back(Temperature);
            Grain->diameter.push_back(Grain_diameter[Grain_in_Layer]);
            Grain->Vol.push_back(Grain_area[Grain_in_Layer]*Material.dz[Layer]);
            Grain->Ms.push_back(Material.Ms[Layer]);
            //================== Anisotropy T dependence ==================//
            Grain->Ani_method.push_back(Material.Ani_method[Layer]);
            Grain->Callen_power_range.push_back(Material.Callen_power_range[Layer]);
            Grain->Callen_power.push_back(Material.Callen_power[Layer]);
            Grain->Callen_factor_lowT.push_back (Material.Callen_factor_lowT[Layer]);
            Grain->Callen_factor_midT.push_back (Material.Callen_factor_midT[Layer]);
            Grain->Callen_factor_highT.push_back(Material.Callen_factor_highT[Layer]);
            Grain->Callen_power_lowT.push_back  (Material.Callen_power_lowT[Layer]);
            Grain->Callen_power_midT.push_back  (Material.Callen_power_midT[Layer]);
            Grain->Callen_power_highT.push_back (Material.Callen_power_highT[Layer]);
            Grain->Callen_range_lowT.push_back  (Material.Callen_range_lowT[Layer]);
            Grain->Callen_range_midT.push_back  (Material.Callen_range_midT[Layer]);
    //================== Equilibrium magnetisation T dependence ==================//
            Grain->Crit_exp.push_back(Material.Crit_exp[Layer]);
            Grain->mEQ_Type.push_back(Material.mEQ_Type[Layer]);
            Grain->a0_mEQ.push_back  (Material.a0_mEQ[Layer]);
            Grain->a1_mEQ.push_back  (Material.a1_mEQ[Layer]);
            Grain->a2_mEQ.push_back  (Material.a2_mEQ[Layer]);
            Grain->a3_mEQ.push_back  (Material.a3_mEQ[Layer]);
            Grain->a4_mEQ.push_back  (Material.a4_mEQ[Layer]);
            Grain->a5_mEQ.push_back  (Material.a5_mEQ[Layer]);
            Grain->a6_mEQ.push_back  (Material.a6_mEQ[Layer]);
            Grain->a7_mEQ.push_back  (Material.a7_mEQ[Layer]);
            Grain->a8_mEQ.push_back  (Material.a8_mEQ[Layer]);
            Grain->a9_mEQ.push_back  (Material.a9_mEQ[Layer]);
            Grain->a1_2_mEQ.push_back(Material.a1_2_mEQ[Layer]);
            Grain->b1_mEQ.push_back  (Material.b1_mEQ[Layer]);
            Grain->b2_mEQ.push_back  (Material.b2_mEQ[Layer]);
        }
    //================== Curie Temperature and Anisotropy distribution generators ==================//
        if(Material.Tc_Dist_Type[Layer]=="d"){
            double nu=Material.Tc_Ddist_nu[Layer], d0=Material.Tc_Ddist_d0[Layer];
            double lambda = 1.0/nu;
            for(unsigned int Grain_in_Layer=0;Grain_in_Layer<Num_grains;++Grain_in_Layer){
                unsigned int Grain_in_system = Grain_in_Layer+Offset;
                // d0 and Grain->diameter are both in [nm]
                Grain->Tc.push_back(Material.Tc_inf[Layer]*(1.0-pow((d0/(Grain->diameter[Grain_in_system])),lambda)));
            }
        }
        else{
            Dist_gen(Num_grains,Material.Tc_Dist_Type[Layer],Material.Avg_Tc[Layer],Material.StdDev_Tc[Layer],&Grain->Tc);
        }
        Dist_gen(Num_grains,Material.K_Dist_Type[Layer],Material.Avg_K[Layer],Material.StdDev_K[Layer],&Grain->K);
    }
    return 0;
}


