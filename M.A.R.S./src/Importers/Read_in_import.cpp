/*
 * Read_in_import.cpp
 *
 *  Created on: 11 Nov 2019
 *      Author: Ewan Rannala
 */

/** @file Read_in_import.cpp
 * @brief Function to read in all required system data file names from the configuration file data.*/

#include "../../hdr/Structures.hpp"
#include "../../hdr/Config_File/ConfigFile_import.hpp"

int Read_in_import(const ConfigFile cfg, Data_input_t*Data_in){

	Data_in->Voro_file = cfg.getValueOfKey<std::string>("DATA:Voro");
	Data_in->St_Mat_file = cfg.getValueOfKey<std::string>("DATA:Str_Mat");
	Data_in->Pos_file = cfg.getValueOfKey<std::string>("DATA:Pos");
	Data_in->Vert_file = cfg.getValueOfKey<std::string>("DATA:Vert");
	Data_in->Geo_file = cfg.getValueOfKey<std::string>("DATA:Geo");
	Data_in->Neigh_file = cfg.getValueOfKey<std::string>("DATA:Neigh");
	Data_in->Mag_neigh_file = cfg.getValueOfKey<std::string>("DATA:Mag_Neigh");
	Data_in->Area_file = cfg.getValueOfKey<std::string>("DATA:Area");
	Data_in->CL_file = cfg.getValueOfKey<std::string>("DATA:CL");
	Data_in->GpV_file = cfg.getValueOfKey<std::string>("DATA:GpV");
	Data_in->GpC_file = cfg.getValueOfKey<std::string>("DATA:GpC");
	Data_in->Gp_file = cfg.getValueOfKey<std::string>("DATA:Gp");
	Data_in->GpEQ_file = cfg.getValueOfKey<std::string>("DATA:GpEQ");
	Data_in->CHI_file = cfg.getValueOfKey<std::string>("DATA:CHI");
        Data_in->CHI_PP_file = cfg.getValueOfKey<std::string>("DATA:Chi_Perp");
        Data_in->CHI_PA_file = cfg.getValueOfKey<std::string>("DATA:Chi_Para");
        Data_in->ALPHA_file = cfg.getValueOfKey<std::string>("DATA:ALPHA");
	Data_in->IpM_file = cfg.getValueOfKey<std::string>("DATA:IpM");
	Data_in->IpE_file = cfg.getValueOfKey<std::string>("DATA:IpE");
	Data_in->IpW_file = cfg.getValueOfKey<std::string>("DATA:IpW");
	Data_in->IpH_file = cfg.getValueOfKey<std::string>("DATA:IpH");

	return 0;
}
